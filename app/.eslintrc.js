// This is a workaround for https://github.com/eslint/eslint/issues/3458
require('@rushstack/eslint-config/patch/modern-module-resolution');

module.exports = {
  env: {
    browser: true,
    es6: true,
    jest: true,
  },
  extends: ['@rushstack/eslint-config', '@rushstack/eslint-config/react', 'plugin:react-hooks/recommended'],
  parserOptions: {
    project: './tsconfig.strict.json',
    tsconfigRootDir: __dirname,
  },
  rules: {
    '@typescript-eslint/no-floating-promises': 'off',
    '@typescript-eslint/typedef': 'off',
    '@typescript-eslint/naming-convention': [
      'error',
      {
        selector: 'interface',
        format: ['PascalCase'],
      },
    ],
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint(react/jsx-no-bind)': 'off',
  },
  settings: {
    react: {
      version: '16.13',
    },
  },
};
