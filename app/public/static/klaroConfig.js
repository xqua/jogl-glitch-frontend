var klaroConfig = {
  default: true,
  translations: {
    en: {
      privacyPolicyUrl: '/data',
      consentNotice: {
        description:
          'This site uses cookies and gives you control over what you want to activate. You can always change or withdraw your consent later.',
        learnMore: 'Personalize',
      },
      consentModal: {
        title: 'Services we would like to use',
        description: 'This website uses cookies to ensure you get the best experience on our website.',
      },
      privacyPolicy: {
        text: 'Click here to know more about our {privacyPolicy}.',
        name: 'cookie policy',
      },
      poweredBy: 'view config',
      decline: 'Decline',
      ok: 'Got it',
      purposes: {
        analytics: 'Analytics',
      },
      googleTagManager: {
        description: 'Collection of visitor interactions data within the app',
      },
      googleAnalytics: {
        description: 'Collection of visitor statistics',
      },
      hotjar: {
        description: 'Collection of the data on how visitors are using our app',
      },
      tawk: {
        description: 'Messaging tool',
      },
    },
    fr: {
      privacyPolicyUrl: '/data',
      consentNotice: {
        description:
          'Ce site utilise des cookies et vous donne le contrôle sur ceux que vous souhaitez activer. Vous pouvez toujours modifier ou retirer votre consentement plus tard.',
        learnMore: 'Personnaliser',
      },
      consentModal: {
        title: 'Nous souhaitons utiliser certains services',
        description: 'En poursuivant votre navigation sur ce site, vous acceptez l’utilisation de cookies.',
      },
      privacyPolicy: {
        text: "Cliquer ici afin d'en savoir davantage sur notre {privacyPolicy}.",
        name: "politique d'utilisation des cookies",
      },
      poweredBy: 'voir la configuration',
      decline: 'Refuser',
      ok: 'Accepter',
      purposes: {
        analytics: 'Analytics',
      },
      googleTagManager: {
        description: 'Collecte de données sur les interactions des utilisateurs',
      },
      googleAnalytics: {
        description: 'Collecte des statistiques de visites',
      },
      hotjar: {
        description: "Collecte de données sur les comportements des utilisateurs à travers l'application",
      },
      tawk: {
        description: 'Outil de messagerie',
      },
    },
    es: {
      privacyPolicyUrl: '/data',
      consentNotice: {
        description:
          'This site uses cookies and gives you control over what you want to activate. You can always change or withdraw your consent later.',
        learnMore: 'Personalize',
      },
      consentModal: {
        title: 'Queremos utilizar algunos servicios',
        description: 'Si continúa navegando por este sitio, acepta el uso de cookies.',
      },
      privacyPolicy: {
        text: 'Haga clic aquí para obtener más información sobre nuestra {privacyPolicy}.',
        name: 'política de cookies',
      },
      poweredBy: 'ver configuración',
      decline: 'Rechazar',
      ok: 'Aceptar',
      purposes: {
        analytics: 'Analytics',
      },
      googleTagManager: {
        description: 'Recopilación de datos sobre las interacciones de los usuarios',
      },
      googleAnalytics: {
        description: 'Recopilación de estadísticas de visitantes',
      },
      hotjar: {
        description: 'Recopilación de datos sobre el comportamiento del usuario a través de la aplicación.',
      },
      tawk: {
        description: 'Herramienta de mensajería',
      },
    },
    de: {
      privacyPolicyUrl: '/data',
      consentNotice: {
        description:
          'This site uses cookies and gives you control over what you want to activate. You can always change or withdraw your consent later.',
        learnMore: 'Personalize',
      },
      consentModal: {
        title: 'Wir möchten einige Dienste nutzen',
        description: 'Wenn Sie diese Website weiterhin durchsuchen, stimmen Sie der Verwendung von Cookies zu.',
      },
      privacyPolicy: {
        text: 'Klicken Sie hier, um mehr über unsere {privacyPolicy} zu erfahren.',
        name: 'Cookie-Richtlinie',
      },
      poweredBy: 'siehe Konfiguration',
      decline: 'Ablehnen',
      ok: 'Akzeptieren',
      purposes: {
        analytics: 'Analytics',
      },
      googleTagManager: {
        description: 'Sammlung von Besucherinteraktionsdaten innerhalb der App',
      },
      googleAnalytics: {
        description: 'Sammlung von Besuchsstatistiken',
      },
      hotjar: {
        description: 'Erfassung der Daten darüber, wie Besucher unsere App nutzen',
      },
      tawk: {
        description: 'Messaging-Tool',
      },
    },
  },
  services: [
    {
      name: 'googleTagManager',
      title: 'Google Tag Manager',
      cookies: [/^_ga(_.*)?/],
      purposes: ['analytics'],
    },
    {
      name: 'googleAnalytics',
      title: 'Google Analytics',
      cookies: [/^_gid(_.*)?/],
      purposes: ['analytics'],
    },
    {
      name: 'hotjar',
      title: 'Hotjar',
      cookies: [/^_hjid(_.*)?/],
      purposes: ['analytics'],
    },
    {
      name: 'tawk',
      title: 'Tawk.to',
      cookies: ['TawkConnectionTime'],
      purposes: ['messaging'],
    },
  ],
};
