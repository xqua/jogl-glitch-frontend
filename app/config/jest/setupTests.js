// Add custom
import '@testing-library/jest-dom/extend-expect';
// add jest-emotion serializer
// import { createSerializer, matchers } from 'jest-emotion';
import { createSerializer, matchers } from '@emotion/jest';
// import * as emotion from 'emotion';
// maybe have to use this line above instead, as it was before emotion 11
import * as emotion from '@emotion/css';

expect.addSnapshotSerializer(createSerializer(emotion));

expect.extend(matchers);
