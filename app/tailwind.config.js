const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  theme: {
    extend: {
      colors: {
        primary: '#2987cd',
        secondary: '#6c757d',
        danger: '#dc3545',
        lightBlue: '#F1F4F8',
      },
      boxShadow: {
        custom: '0px 4px 30px rgba(0, 0, 0, 0.09)',
        custom2: '0px 4px 34px rgba(0, 0, 0, 0.09)',
      },
      screens: {
        xs: '475px',
        ...defaultTheme.screens,
      },
    },
  },
  plugins: [require('@tailwindcss/line-clamp'), require('@tailwindcss/forms')],
};
