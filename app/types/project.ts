import { Proposal } from '.';
import { Challenge } from './challenge';
import { Owner, Geoloc, UsersSm } from './common';
import { Program } from './program';
import { Space } from './space';

export interface Project {
  id: number;
  title: string;
  short_title: string;
  banner_url: string;
  banner_url_sm: string;
  short_description: string;
  creator: Owner;
  status: string;
  skills: string[];
  interests: string[];
  badges: any[];
  geoloc: Geoloc;
  country?: string;
  city?: string;
  address?: string;
  feed_id: number;
  is_private: boolean;
  proposals: Proposal[];
  challenges: Pick<
    Challenge,
    | 'banner_url'
    | 'banner_url_sm'
    | 'id'
    | 'short_description'
    | 'short_description_fr'
    | 'short_title'
    | 'status'
    | 'project_status'
    | 'title'
    | 'title_fr'
  >[];
  affiliated_spaces:
    | Pick<Space, 'banner_url' | 'banner_url_sm' | 'id' | 'short_description' | 'short_title' | 'status' | 'title'>
    | 'status'[];
  programs: Pick<Program, 'id' | 'short_title' | 'title'>[];
  users_sm: UsersSm[];
  claps_count: number;
  followers_count: number;
  saves_count: number;
  members_count: number;
  needs_count: number;
  posts_count: number;
  created_at: Date;
  updated_at: Date;
  is_owner: boolean;
  is_admin: boolean;
  is_member: boolean;
  is_pending: boolean;
  is_reviewer: boolean;
  has_followed: boolean;
  has_saved: boolean;
  documents: any[];
  documents_feed: any[];
  maturity: string;
  is_looking_for_collaborators: boolean;
  description: string;
  desc_elevator_pitch?: string;
  desc_problem_statement?: string;
  desc_objectives?: string;
  desc_state_art?: string;
  desc_progress?: string;
  desc_stakeholder?: string;
  desc_impact_strat?: string;
  desc_ethical_statement?: string;
  desc_sustainability_scalability?: string;
  desc_communication_strat?: string;
  desc_funding?: string;
  desc_contributing?: string;
  reviews_count?: number;
  grant_info?: string;
  has_valid_proposal?: boolean;
}
