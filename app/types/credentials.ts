export interface Credentials {
  authorization: string;
  userId: string;
}
