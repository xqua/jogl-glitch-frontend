export interface Proposal {
  answers: any[];
  funding: string;
  id: number;
  peer_review_id: number;
  project_id: number;
  score: number;
  submitted_at: Date;
  title: string;
  boards: any[];
  is_peer_review_admin: boolean;
  is_validated: boolean;
  summary: string;
}
