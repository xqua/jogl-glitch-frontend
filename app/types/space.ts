import { Document, UsersSm } from './common';

export interface Space {
  id: number;
  title: string;
  title_fr?: string;
  short_title: string;
  banner_url: string;
  banner_url_sm: string;
  logo_url: string;
  logo_url_sm: string;
  status: string;
  users_sm: UsersSm[];
  feed_id: number;
  short_description: string;
  ressources?: string;
  claps_count: number;
  followers_count: number;
  saves_count: number;
  members_count: number;
  needs_count: number;
  projects_count: number;
  challenges_count: number;
  activities_count: number;
  programs_count: number;
  is_owner: boolean;
  is_admin: boolean;
  is_member: boolean;
  is_pending?: boolean;
  has_followed: boolean;
  has_saved: boolean;
  description: string;
  meeting_information: string;
  home_header: string;
  home_info: string;
  documents: Document[];
  contact_email: string;
  enablers?: string;
  show_featured: {
    challenges: boolean;
    needs: boolean;
    programs: boolean;
    projects: boolean;
  }
  selected_tabs: {
    programs: boolean;
    challenges: boolean;
    resources: boolean;
    enablers: boolean;
    faqs: boolean;
  }
  space_type?: string;
  show_associated_users?: boolean;
  show_associated_projects?: boolean;
}
