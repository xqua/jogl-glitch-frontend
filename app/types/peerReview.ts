import { Proposal } from './proposal';

export interface PeerReview {
  proposals: Proposal[];
  banner_url: string;
  banner_url_sm: string;
  created_at: Date;
  deadline: Date;
  description: string;
  feed_id: number;
  followers_count: number;
  has_followed: boolean;
  has_saved: boolean;
  id: number;
  interests: number[];
  is_admin: boolean;
  is_member: boolean;
  is_member_of_parent: boolean;
  maximum_disbursement: number;
  options: any;
  proposals_count: number;
  resource_id: number;
  resource_type: string;
  rules: string;
  saves_count: number;
  score_threshold: number;
  short_title: string;
  skills: string[];
  start: Date;
  stop: Date;
  summary: string;
  template_question_count: number;
  title: string;
  total_funds: number;
  updated_at: Date;
  visibility: 'hidden' | 'open';
}
