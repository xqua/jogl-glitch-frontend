export interface Owner {
  id: number;
  first_name: string;
  last_name: string;
  logo_url: string;
  logo_url_sm: string;
  short_bio: string;
}

export interface Document {
  id: number;
  content_type: string;
  filename: string;
  url_variant: string;
  url: string;
}

export interface Geoloc {
  lat: number;
  lng: number;
}

export interface UsersSm {
  id: number;
  first_name: string;
  last_name: string;
  short_bio: string;
  owner: boolean;
  admin: boolean;
  member: boolean;
  logo_url: string;
}
