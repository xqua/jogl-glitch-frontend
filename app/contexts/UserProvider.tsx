import cookie from 'js-cookie';
import Router from 'next/router';
import React, { createContext, Dispatch, FC, SetStateAction, useCallback, useEffect, useMemo, useState } from 'react';
import Swal from 'sweetalert2';
import { useApi } from 'contexts/apiContext';
import useGet from 'hooks/useGet';
import { Credentials, User } from 'types';

// Context types
interface IUserContext {
  checkUserTokens: () => Promise<unknown>;
  frontLogout: (redirectPath?: string, modalOnly?: boolean) => void;
  signIn: (email: any, password: any) => Promise<unknown>;
  logout: (redirectPath?: any) => Promise<void>;
  isConnected: boolean;
  credentials: Credentials;
  user: User | undefined;
  userData: User | undefined;
  isUserModerator: boolean;
}

// Context, with default value set as undefined
export const UserContext = createContext<IUserContext | undefined>(undefined);
// associated useContext is written in each component

export const frontLogout = (redirectPath = '/signin', modalOnly = false) => {
  // Delete all the auth cookies
  cookie.remove('authorization');
  cookie.remove('userId');
  // to support logging out from all windows
  // This will work with NextJS since it's call in the browser only.
  window.localStorage.setItem('logout', Date.now().toString());
  Router.push(redirectPath);
};

// Provider types
interface ProviderProps {
  credentials: Credentials;
  setCredentials: Dispatch<SetStateAction<Credentials>>;
}

// Provider
const UserProvider: FC<ProviderProps> = ({ children, credentials, setCredentials }) => {
  const api = useApi();
  const [isConnected, setIsConnected] = useState(!!credentials.authorization);
  const { data: user, error: userError, mutate: userMutate } = useGet(
    credentials.userId && `/api/users/${credentials.userId}`,
    {
      revalidateOnFocus: false,
      revalidateOnMount: true,
    }
  );
  const [userData, setUserData] = useState({});
  const [isUserModerator, setIsUserModerator] = useState(false);

  useEffect(() => {
    const getUserData = async () => {
      if (credentials.userId) {
        const userReq = await api.get(`/api/users/${credentials.userId}`);
        setUserData(userReq.data);
      }
    };
    getUserData();
  }, [credentials.userId]);

  // useEffect(() => {
  //   const getUserModerator = async () => {
  //     const getModerator = await api.get('/api/admin/moderator');
  //     setIsUserModerator(getModerator.data.moderator);
  //   };
  //   isConnected && getUserModerator();
  // }, []);

  useEffect(() => {
    if (!!credentials.authorization) {
      setIsConnected(true);
    } else {
      setIsConnected(false);
    }
  }, [credentials.authorization]);

  const logout = useCallback(
    (redirectPath = '/signin') => {
      Swal.fire({ text: 'Logging out..', showConfirmButton: false });
      api
        .delete('/api/auth/sign_out')
        .then(() => {
          frontLogout(redirectPath);
          setIsConnected(false);
          setCredentials({ authorization: '', userId: '' });
          userMutate();
          setUserData({});
          setIsUserModerator(false);
          Swal.fire({ icon: 'success', showConfirmButton: false, timer: 1500 });
        })
        .catch((error) => {
          if (error.response.status === 404) {
            frontLogout(redirectPath);
            setCredentials({ authorization: '', userId: '' });
            setIsConnected(false);
            userMutate();
            setUserData({});
            setIsUserModerator(false);
          }
        });
    },
    [api]
  );

  const checkUserTokens = useCallback(
    () =>
      new Promise((resolve, reject) => {
        // Control if user is connected and always available
        if (credentials.authorization) {
          api
            .get('/api/auth/validate_token')
            .then((res) => {
              // Validating tokens
              if (res.status === 401) {
                // Tokens are not valid
                logout();
                reject(new Error('Tokens are not valid'));
              }
              if (res.status === 200) {
                // Valid tokens !
                resolve();
              }
            })
            .catch((error) => reject(error));
        } else {
          reject();
        }
      }),
    [api, credentials.authorization, logout]
  );

  const signIn = useCallback(
    async (email, password) =>
      new Promise((resolve, reject) => {
        api
          .post('/api/auth/sign_in', { email, password })
          .then((res) => {
            const expires = 7; // In days
            cookie.set('authorization', res.headers.authorization, { expires });
            cookie.set('userId', res.data.id, { expires });
            // Sync login between pages
            window.localStorage.setItem('login', Date.now().toString());
            setCredentials({
              authorization: res.headers.authorization,
              userId: res.data.id,
            });
            userMutate(res.data, false);
            // Eventually redirect to an other page here
            resolve(res.data);
          })
          .catch((error) => {
            // TODO: Comment what this line does
            const { response: { data: { errors = [] } = {} } = {} } = error;
            // console.warn(errors);
            reject(error);
          });
      }),
    [api, setCredentials]
  );

  const providerValue = useMemo(
    () => ({
      checkUserTokens,
      frontLogout,
      signIn,
      logout,
      isConnected,
      credentials,
      user,
      userData,
      isUserModerator,
    }),
    [checkUserTokens, signIn, logout, isConnected, credentials, user, userData, isUserModerator]
  );
  // Values stored here passed on to all children components of the app, wrapped inside the present context in _app.tsx
  return <UserContext.Provider value={providerValue}>{children}</UserContext.Provider>;
};

export default UserProvider;
