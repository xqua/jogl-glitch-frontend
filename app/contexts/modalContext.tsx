import { Close } from '@emotion-icons/material/Close';
import { Router } from 'next/router';
import React, {
  createContext,
  Dispatch,
  ReactNode,
  SetStateAction,
  useCallback,
  useContext,
  useMemo,
  useState,
} from 'react';
import Modal from 'react-modal';
import P from 'components/primitives/P';
import styled from 'utils/styled';
import styles from './modalContext.module.scss';
import tw, { css } from 'twin.macro';

interface ShowModal {
  children: ReactNode;
  title?: string;
  maxWidth?: string;
  showTitle?: boolean;
  showCloseButton?: boolean;
  allowOverflow?: boolean;
  modalClassName?: string;
}

export interface IModalContext {
  setModalChildren: Dispatch<SetStateAction<ReactNode>>;
  setIsOpen: Dispatch<SetStateAction<boolean>>;
  closeModal: (value?: boolean) => void;
  showModal: ({ children, maxWidth, title, showCloseButton, showTitle, modalClassName }: ShowModal) => void;
  isOpen: boolean;
}

// Required as mentioned in the docs.
Modal.setAppElement('#__next');
const ModalContext = createContext<IModalContext | undefined>(undefined);

export function ModalProvider({ children }) {
  const [isOpen, setIsOpen] = useState(false);
  const [modalChildren, setModalChildren] = useState<ReactNode>(
    <div>You haven\&apos;t provided any modalChildren</div>
  );
  const [contentLabel, setContentLabel] = useState(undefined);
  const [showTitle, setShowTitle] = useState<boolean>(true);
  const [allowOverflow, setAllowOverflow] = useState<boolean>(false);
  const [modalTitle, setModalTitle] = useState<undefined | string>(undefined);
  const [showCloseButton, setShowCloseButton] = useState<boolean>(true);
  const [maxWidth, setMaxWith] = useState<string>('30rem');
  const [modalClassName, setModalClassName] = useState<string>('');

  const showModal = useCallback(
    ({
      children,
      maxWidth = '30rem',
      title,
      showCloseButton = true,
      showTitle = true,
      allowOverflow = false,
      modalClassName,
    }: ShowModal) => {
      setModalChildren(children);
      setIsOpen(true);
      setModalTitle(title);
      setShowTitle(showTitle);
      setAllowOverflow(allowOverflow);
      setShowCloseButton(showCloseButton);
      setContentLabel(title);
      setMaxWith(maxWidth);
      setModalClassName(modalClassName);
    },
    []
  );
  const closeModal = (value: boolean = true) => setIsOpen(!value);
  // console.log(modalTitleValues);
  // Memoize value so it doesn't re-render the tree
  const value: IModalContext = useMemo(() => ({ showModal, setModalChildren, isOpen, closeModal }), [
    closeModal,
    showModal,
    setModalChildren,
    isOpen,
  ]);
  // force closing modal when soft changing route
  Router.events.on('routeChangeStart', closeModal);

  return (
    <ModalContext.Provider value={value}>
      <Modal
        isOpen={isOpen}
        contentLabel={contentLabel}
        onRequestClose={closeModal}
        className={styles.modal}
        overlayClassName={styles.overlay}
      >
        <div tw="flex flex-col w-[90vw] sm:w-[80vw]" css={{ maxWidth: `${maxWidth}` }} className={modalClassName}>
          {(showTitle || showCloseButton) && (
            <div tw="flex justify-between items-center p-4 pr-2 border-b border-[lightgrey] relative bg-primary rounded-t-lg">
              <P
                tw="flex-grow text-white font-medium text-center mb-0"
                style={{ fontSize: modalClassName === 'onBoardingModal' ? '2.3rem' : '1.3rem' }}
              >
                {showTitle && modalTitle}
              </P>
              {showCloseButton && (
                <button tw="flex flex-col" onClick={closeModal}>
                  {/* <CloseButton icon={['far', 'times-circle']} /> */}
                  <CloseButton size={modalClassName === 'onBoardingModal' ? 18 : 27} title="Close modal" />
                </button>
              )}
              {/* {modalClassName === 'createSpaceModal' && (
                <img
                  src="/images/banner-beta-feature-blue.png"
                  tw="hidden absolute top-0 left-0 w-1/4 xs:block md:w-[150px]"
                />
              )} */}
            </div>
          )}
          {/* To be able to scroll in the modal content (not it's header), we need wrap modalChildren with a ModalFrame that has a set height and overflow-y:auto, and then by a div with overflow:scroll */}
          <ModalFrame tw="overflow-y-visible">
            {/* Can allow content to overflow for some modals */}
            <div
              tw="flex flex-col p-4"
              css={[
                allowOverflow ? tw`overflow-visible` : tw`overflow-scroll pb-14`,
                css`
                  &::-webkit-scrollbar {
                    display: none;
                  }
                `,
              ]}
            >
              {modalChildren}
            </div>
          </ModalFrame>
        </div>
      </Modal>
      {children}
    </ModalContext.Provider>
  );
}

const CloseButton = styled(Close)`
  color: white;
  &:hover {
    color: lightgrey;
  }
`;

const ModalFrame = styled.div`
  display: flex;
  flex-direction: column;
  overflow-y: auto;
  max-height: calc(95vh - 10rem);
`;

export const useModal = () => useContext(ModalContext);
export default ModalContext;
