import { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { connectStats } from 'react-instantsearch-dom';
import { formatNumber } from 'utils/utils';

interface Props {
  nbHits: number,
}

const ResultsNumberMobile: FC<Props> = ({ nbHits }) => {
  const { t } = useTranslation('common');

  return (
    <div>
      <strong>{formatNumber(nbHits)}</strong> {t('algolia.results')}
    </div>
  );
};

export default connectStats(ResultsNumberMobile);
