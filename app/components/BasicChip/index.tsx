const BasicChip = (props) => (
  <span tw="capitalize rounded px-[6px] py-[3px] w-[fit-content] bg-gray-200 text-gray-800" {...props} />
);

export default BasicChip;
