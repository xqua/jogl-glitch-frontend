import { FC } from 'react';
import styled from 'utils/styled';

// Hide checkbox visually but remain accessible to screen readers.
// Source: https://polished.js.org/docs/#hidevisually
const HiddenCheckbox = styled.input`
  border: 0;
  clip: rect(0 0 0 0);
  clippath: inset(50%);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  white-space: nowrap;
  width: 1px;
  :focus + & {
    box-shadow: 0 0 0 3px pink;
  }
`;

const Label = styled.label`
  box-shadow: ${(p) => p.theme.shadows.light};
  border: ${(p) => (p.checked ? `2px solid #4F4BFF` : '2px solid transparent')};
  border-radius: ${(p) => p.theme.radii.full};
  cursor: pointer;
  font-weight: bold;
  transition: border 0.3s ease-in-out, box-shadow 0.3s ease-in-out;
  white-space: nowrap;
  background: white;
  margin-top: 2px;
  margin-bottom: 3px;
  color: ${(p) => (p.checked ? '#4F4BFF' : p.theme.colors.greys['600'])};
  :hover {
    border: 2px solid #4f4bff;
    box-shadow: ${(p) => p.theme.shadows.md};
    color: #4f4bff;
  }
  /* :focus-within {
    outline: -webkit-focus-ring-color auto 1px !important;
  } */
  @media (max-width: ${(p) => p.theme.breakpoints.md}) {
    &:last-child {
      padding-right: 5rem;
    }
  }
`;

interface Props {
  checked: boolean;
  iconSrc: string;
  onChange: (e: any) => void;
  label: string;
  value: number;
}

const FilterCheckbox: FC<Props> = ({ checked, iconSrc, onChange, label, value }) => {
  return (
    <Label checked={checked}>
      <HiddenCheckbox name={value} type="checkbox" checked={checked} onChange={onChange} />
      <div tw="flex space-x-1 px-4 h-7 text-sm justify-center items-center">
        {iconSrc && (
          <div tw="flex flex-col w-6 h-6 justify-center items-center relative">
            <img src={iconSrc} width="100%" alt="Label icon" />
          </div>
        )}
        {/* {typeof label === 'object' ? t(label.id) : label} */}
        {label}
      </div>
    </Label>
  );
};

export default FilterCheckbox;
