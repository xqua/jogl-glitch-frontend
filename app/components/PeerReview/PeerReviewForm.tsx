import { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import FormDefaultComponent from 'components/Tools/Forms/FormDefaultComponent';
import FormWysiwygComponent from 'components/Tools/Forms/FormWysiwygComponent';
import FormTextAreaComponent from '../Tools/Forms/FormTextAreaComponent';
import FormImgComponent from 'components/Tools/Forms/FormImgComponent';
import FormInterestsComponent from 'components/Tools/Forms/FormInterestsComponent';
import FormSkillsComponent from 'components/Tools/Forms/FormSkillsComponent';
import { PeerReview } from 'types';
import { useRouter } from 'next/router';
import { changesSavedConfAlert, hasPeerReviewDatePassed } from 'utils/utils';
import Button from '../primitives/Button';
import SpinLoader from '../Tools/SpinLoader';
import Link from 'next/link';
import tw from 'twin.macro';

interface Props {
  mode: 'edit' | 'create' | 'edit_about' | 'edit_proposal';
  peerReview: PeerReview;
  sending: boolean;
  hasUpdated: boolean;
  handleChange: (key: any, content: any) => void;
  handleSubmit: () => void;
}

const PeerReviewForm: FC<Props> = ({
  mode,
  peerReview,
  sending = false,
  hasUpdated = false,
  handleChange,
  handleSubmit,
}) => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const urlBack = mode === 'create' ? '/' : `/peer-review/${peerReview.short_title}`;
  const textAction = mode === 'create' ? 'Create' : 'Update';

  // show conf message when peer review has been successfully saved/updated
  hasUpdated &&
    changesSavedConfAlert(t)
      .fire()
      // go back to peer review page if user clicked on conf button
      .then(({ isConfirmed }) => isConfirmed && router.push(urlBack));

  return (
    <form className="peerReviewForm">
      {/* Basic fields */}
      {(mode === 'edit' || mode === 'create') && (
        <>
          <FormDefaultComponent
            id="title"
            content={peerReview.title}
            title={t('entity.info.title')}
            placeholder={t('peerReview.form.title_placeholder')}
            onChange={handleChange}
            mandatory
          />
          <FormDefaultComponent
            id="short_title"
            content={peerReview.short_title}
            title={t('entity.info.short_name')}
            placeholder={t('peerReview.form.short_title_placeholder')}
            onChange={handleChange}
            prepend="#"
            baseUrl={`${process.env.ADDRESS_FRONT}/peer-review/`}
            mandatory
            pattern={/[A-Za-z0-9]/g}
          />
          <FormTextAreaComponent
            content={peerReview.summary}
            id="summary"
            maxChar={500}
            onChange={handleChange}
            rows={3}
            title={t('entity.info.short_description')}
            placeholder={t('peerReview.form.short_description_placeholder')}
            mandatory
          />
        </>
      )}

      {mode === 'edit_about' && (
        <FormWysiwygComponent
          id="description"
          content={peerReview.description}
          title={t('entity.info.description')}
          placeholder={t('peerReview.form.description_placeholder')}
          onChange={handleChange}
          show
        />
      )}

      {mode === 'edit_proposal' && (
        <div tw="mt-4" css={[peerReview.stop && hasPeerReviewDatePassed(peerReview.stop) && tw`opacity-40`]}>
          <FormDefaultComponent
            id="score_threshold"
            type="number"
            content={peerReview.score_threshold}
            title={t('peerReview.form.score_threshold')}
            description={t('peerReview.form.score_threshold_placeholder')}
            placeholder="3.7, 4.5..."
            onChange={handleChange}
            isDisabled={peerReview.stop && hasPeerReviewDatePassed(peerReview.stop)}
          />
        </div>
      )}
      {mode === 'edit' && (
        <>
          <FormInterestsComponent
            content={peerReview.interests}
            title={t('peerReview.form.associatedSDGs')}
            onChange={handleChange}
            mandatory
          />
          <FormSkillsComponent
            id="skills"
            type="keywords"
            placeholder={t('general.skills.placeholder')}
            title={t('peerReview.form.keywords')}
            tooltipMessage={t('peerReview.form.keywords_tooltipMessage')}
            content={peerReview.skills}
            mandatory
            onChange={handleChange}
          />
          <FormImgComponent
            id="banner_url"
            content={peerReview.banner_url}
            title={t('peerReview.info.banner_url')}
            imageUrl={peerReview.banner_url}
            itemId={peerReview.id}
            type="banner"
            itemType="peer_reviews"
            defaultImg="/images/default/default-peer-review.jpg"
            onChange={handleChange}
          />
        </>
      )}

      {/* Form buttons */}
      <div tw="space-x-2 mt-5 mb-3 text-center">
        {mode !== 'edit_about' && mode !== 'edit_proposal' && (
          <Link href={urlBack} passHref>
            <Button btnType="secondary" disabled={sending}>
              {t('user.profile.edit.back')}
            </Button>
          </Link>
        )}
        <Button onClick={handleSubmit} disabled={sending}>
          {sending && <SpinLoader />}
          {t(`entity.form.btn${textAction}`)}
        </Button>
      </div>
    </form>
  );
};
export default PeerReviewForm;
