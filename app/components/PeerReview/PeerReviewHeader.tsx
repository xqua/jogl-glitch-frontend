import React, { FC, useContext } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { Edit } from '@emotion-icons/boxicons-solid/Edit';
import Button from 'components/primitives/Button';
import PeerReviewInfo from 'components/PeerReview/PeerReviewInfo';
// import PeerReviewSkillSdg from 'components/PeerReview/PeerReviewSkillSdg';
import ShareBtns from 'components/Tools/ShareBtns/ShareBtns';
import H1 from 'components/primitives/H2';
import { PeerReview } from 'types';
import Link from 'next/link';
import { ProjectLinkToPeerReview } from '../Project/ProjectLinkToPeerReview';
import { UserContext } from 'contexts/UserProvider';
import { useModal } from '~/contexts/modalContext';
import BtnJoin from '../Tools/BtnJoin';
import { hasPeerReviewDatePassed } from '~/utils/utils';
import PeerReviewSkillSdg from './PeerReviewSkillSdg';

interface Props {
  peerReview: PeerReview;
}

const PeerReviewHeader: FC<Props> = ({ peerReview }) => {
  const { t } = useTranslation('common');
  const user = useContext(UserContext);
  const { showModal, closeModal } = useModal();

  return (
    <header tw="relative w-full">
      <div tw="flex flex-wrap divide-x divide-gray-200 md:flex-nowrap">
        <div tw="hidden md:block">
          <PeerReviewInfo peerReview={peerReview} />
        </div>

        <div tw="px-4 py-4 md:px-8 md:mt-4">
          <div tw="inline-flex items-center">
            <H1 tw="pr-3 font-size[1.8rem] md:font-size[2.3rem]">{peerReview.title}</H1>
            <div tw="hidden md:block">
              <ShareBtns type="peerReview" specialObjId={peerReview.id} />
            </div>
          </div>
          <p tw="mt-4 text-gray-600">{peerReview.summary}</p>

          {/* {(peerReview.interests?.length > 0 || peerReview.skills?.length > 0) && (
            <div tw="hidden md:block mb-6">
              <PeerReviewSkillSdg interests={peerReview.interests} skills={peerReview.skills} />
            </div>
          )} */}

          <div tw="flex flex-wrap gap-3 mb-2">
            {peerReview.is_admin && (
              <Link href={`/peer-review/${peerReview.short_title}/edit`}>
                <Button tw="flex justify-center items-center">
                  <Edit size={23} title="Edit peer review" />
                  {t('entity.form.btnAdmin')}
                </Button>
              </Link>
            )}
            {/* show apply button only if 1) peer_review user is member of parent obj (in case pr admin chose to restrict this),
              2) if we are after start date, 3) if we are before deadline date */}
            {/* !peerReview.is_admin && ( */}
            {user.isConnected &&
              (peerReview.is_restricted_to_parent_members ? peerReview.is_member_of_parent : true) &&
              hasPeerReviewDatePassed(peerReview.start) &&
              !hasPeerReviewDatePassed(peerReview.deadline) && (
                <Button
                  onClick={() => {
                    showModal({
                      children: (
                        <ProjectLinkToPeerReview
                          alreadyPresentProposals={peerReview.proposals}
                          peerReviewId={peerReview.id}
                          peerReviewShortTitle={peerReview.short_title}
                          closeModal={closeModal}
                        />
                      ),
                      title: t('peerReview.form.btnCreateProposal'),
                      maxWidth: '50rem',
                      allowOverflow: true,
                    });
                  }}
                >
                  {t('peerReview.form.btnCreateProposal')}
                </Button>
              )}
            {(peerReview.is_restricted_to_parent_members ? peerReview.is_member_of_parent : true) &&
              !peerReview.is_admin &&
              !hasPeerReviewDatePassed(peerReview.stop) && (
                <BtnJoin
                  joinState={peerReview.is_member || false}
                  itemType="peer_reviews"
                  itemId={peerReview.id}
                  textJoin={t('peerReview.form.btnJoinAsReviewer')}
                  textUnjoin={t('peerReview.form.btnQuitAsReviewer')}
                  hasNoStat
                  // count={members_count}
                  // showMembersModal={showMembersModal}
                />
              )}
            <div tw="md:hidden">
              <ShareBtns type="peerReview" specialObjId={peerReview.id} />
            </div>
          </div>
        </div>
      </div>
      {/* {(interests?.length > 0 || skills?.length > 0) && (
        <div tw="hidden md:block">
          <hr tw="mb-4" />
          <PeerReviewSkillSdg interests={interests} skills={skills} />
        </div>
      )} */}
    </header>
  );
};

export default PeerReviewHeader;
