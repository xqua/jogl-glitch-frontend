import { FC, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import TitleInfo from 'components/Tools/TitleInfo';
import { PeerReview, Program, Space } from 'types';
import { useRouter } from 'next/router';
import { changesSavedConfAlert, hasPeerReviewStarted } from 'utils/utils';
import Button from '../primitives/Button';
import SpinLoader from '../Tools/SpinLoader';
import Link from 'next/link';
import FormWysiwygComponent from '../Tools/Forms/FormWysiwygComponent';
import FormDefaultComponent from '../Tools/Forms/FormDefaultComponent';
import tw, { css } from 'twin.macro';
import FormToggleComponent from '../Tools/Forms/FormToggleComponent';
import useGet from '~/hooks/useGet';
import Swal from 'sweetalert2';
import { useApi } from '~/contexts/apiContext';

interface Props {
  peerReview: PeerReview;
  sending: boolean;
  hasUpdated: boolean;
  handleChange: (key: any, content: any) => void;
  handleSubmit: () => void;
}

const PeerReviewRulesForm: FC<Props> = ({
  peerReview,
  sending = false,
  hasUpdated = false,
  handleChange,
  handleSubmit,
}) => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const api = useApi();
  const [showOpenBtn, setShowOpenBtn] = useState(peerReview.visibility !== 'open');
  const [isAccessibilityBtnDisabled, setIsAccessibilityBtnDisabled] = useState(
    hasPeerReviewStarted(peerReview.proposals.length) && !peerReview.options.restrict_to_parent_members
  );

  const urlBack = `/peer-review/${peerReview.short_title}`;
  const textAction = 'Update';
  const hasStarted = hasPeerReviewStarted(peerReview.proposals.length);
  const peerReviewResource = { type: peerReview?.resource_type?.toLowerCase(), id: peerReview?.resource_id };
  const { data: peer_review_parent } = useGet<Space | Program>(
    `/api/${peerReviewResource.type?.toLowerCase()}s/${peerReviewResource.id}`
  );

  const handleToggleChange = (key, content) => {
    handleChange(key, content === true ? 1 : 0);
  };

  const handleOptionsToggleChange = (key, content) => {
    handleChange(key, { restrict_to_parent_members: !content });
    // disable accessibility toggle if it has started (so they can only change it once to ALL jogl members)
    hasPeerReviewStarted(peerReview.proposals.length) && setIsAccessibilityBtnDisabled(true);
  };
  // const handleVisibilityToggleChange = (key, content) => {
  //   handleChange(key, content === true ? 'open' : 'hidden');
  // };

  // show conf message when peer review has been successfully saved/updated
  hasUpdated &&
    changesSavedConfAlert(t)
      .fire()
      // go back to peer review page if user clicked on conf button
      .then(({ isConfirmed }) => isConfirmed && router.push(urlBack));
  return (
    <form className="peerReviewForm">
      <FormWysiwygComponent
        id="rules"
        title={t('peerReview.rules.title')}
        placeholder={t('peerReview.rules.rules_placeholder')}
        content={peerReview.rules}
        onChange={handleChange}
        mandatory
        show
      />
      <div tw="w-full mb-2">
        <TitleInfo title={t('peerReview.timeline.title')} mandatory />
        <div className="content">
          <p>{t('peerReview.timeline.message')}</p>
          <div tw="flex gap-4 flex-wrap sm:flex-nowrap">
            <FormDefaultComponent
              type="datetime-local"
              id="start"
              content={peerReview.start ? peerReview.start.substr(0, 16) : undefined}
              title={t('peerReview.timeline.submissionStart')}
              onChange={handleChange}
              // minDate={new Date().toISOString().substr(0, 16)}
              maxDate={peerReview.deadline && new Date(peerReview.deadline).toISOString().substr(0, 16)}
            />
            <FormDefaultComponent
              type="datetime-local"
              id="deadline"
              content={peerReview.deadline ? peerReview.deadline.substr(0, 16) : undefined}
              title={t('peerReview.timeline.submissionDeadline')}
              onChange={handleChange}
              minDate={peerReview.start && new Date(peerReview.start).toISOString().substr(0, 16)}
              maxDate={peerReview.stop && new Date(peerReview.stop).toISOString().substr(0, 16)}
            />
            {/* <FormDefaultComponent
              type="datetime-local"
              id="stop"
              content={peerReview.stop ? peerReview.stop.substr(0, 16) : undefined}
              title={t('peerReview.timeline.reviewsEnd')}
              onChange={handleChange}
              minDate={peerReview.deadline && new Date(peerReview.deadline).toISOString().substr(0, 16)}
            /> */}
          </div>
        </div>
      </div>
      <div tw="mb-5">
        {showOpenBtn && (
          <Button
            onClick={() => {
              Swal.fire({
                title: t('peerReview.form.now_open_warnTitle'),
                text: t('peerReview.form.now_open_warnDesc'),
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: t('general.yes'),
              }).then(({ isConfirmed }) => {
                if (isConfirmed) {
                  api.patch(`/api/peer_reviews/${peerReview.id}`, { peer_review: { visibility: 'open' } }).then(() => {
                    Swal.fire({
                      icon: 'success',
                      showConfirmButton: false,
                      text: t('peerReview.form.now_open'),
                      // timer: 2500,
                    });
                    setShowOpenBtn(false);
                  });
                }
              });
            }}
          >
            {t('peerReview.form.now_open_btn')}
          </Button>
        )}
      </div>

      <div tw="w-full mb-4">
        <TitleInfo title={t('peerReview.funding.title')} />
        <div className="content">
          <p>{t('peerReview.funding.message')}</p>
          <div tw="flex gap-4 w-full justify-between flex-wrap md:flex-nowrap" css={hasStarted && tw`opacity-40`}>
            <FormToggleComponent
              id="total_funds"
              title={t('peerReview.funding.has_funding')}
              choice1={t('general.no')}
              choice2={t('general.yes')}
              color1="#27B40E"
              color2="#F9530B"
              isChecked={peerReview.total_funds === null || peerReview.total_funds === 0 ? false : true}
              onChange={handleToggleChange}
              isDisabled={hasStarted}
            />
            <div tw="flex items-center">
              <div
                css={[
                  css`
                  > div { width:inherit!important: }
                `,
                  peerReview.total_funds === 0 && tw`opacity-40`,
                ]}
              >
                <FormDefaultComponent
                  type="number"
                  title={t('peerReview.funding.maximum_disbursement')}
                  content={peerReview.maximum_disbursement === null ? 0 : peerReview.maximum_disbursement}
                  id="maximum_disbursement"
                  onChange={handleChange}
                  isDisabled={hasStarted || peerReview.total_funds === 0}
                />
              </div>
              <span tw="font-bold ml-2">€</span>
            </div>
          </div>
        </div>
      </div>

      <FormToggleComponent
        id="options"
        warningMsg={t('peerReview.form.accessibilityToggleMsg')}
        title={t('peerReview.form.accessibilityTitle')}
        choice1={t('peerReview.form.accessibilityToggleRestrict', {
          resource_type: peer_review_parent?.title,
        })}
        choice2={t('peerReview.form.accessibilityToggleOpen')}
        color1="#27B40E"
        color2="#F9530B"
        isChecked={!peerReview.options.restrict_to_parent_members}
        onChange={handleOptionsToggleChange}
        isDisabled={isAccessibilityBtnDisabled}
      />

      {/* <FormToggleComponent
        id="visibility"
        warningMsg={
          'Once you mark the peer review as "open", it will appear on JOGL and users will be able to submit proposals..'
        }
        title={'Visibility°°'}
        choice1={'Hidden°°'}
        choice2={'Open°°'}
        color1="#27B40E"
        color2="#F9530B"
        isChecked={peerReview.visibility === 'open'}
        onChange={handleVisibilityToggleChange}
        isDisabled={hasPeerReviewStarted(peerReview.proposals.length)}
      /> */}

      {/* Form buttons */}
      <div tw="space-x-2 mt-5 mb-3 text-center">
        <Link href={urlBack} passHref>
          <Button btnType="secondary" disabled={sending}>
            {t('user.profile.edit.back')}
          </Button>
        </Link>
        <Button onClick={handleSubmit} disabled={sending}>
          {sending && <SpinLoader />}
          {t(`entity.form.btn${textAction}`)}
        </Button>
      </div>
    </form>
  );
};
export default PeerReviewRulesForm;
