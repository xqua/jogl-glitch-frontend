import React, { FC } from 'react';
import Grid from '../Grid';
import ProposalCard from '../Proposal/ProposalCard';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';
import { Proposal } from '~/types/proposal';

interface Props {
  proposals: Proposal[];
  peerReviewId: number;
}

const PeerReviewProposals: FC<Props> = ({ proposals, peerReviewId }) => {
  return (
    <div>
      <div tw="flex flex-col py-4 relative">
        {!proposals ? (
          <Loading />
        ) : proposals?.length === 0 ? (
          <NoResults type="proposal" />
        ) : (
          <Grid tw="py-4">
            {proposals
              .filter(({ submitted_at }) => submitted_at) // filter proposals that have not yet been submitted
              .map((proposal, index) => (
                <ProposalCard
                  key={index}
                  id={proposal.id}
                  projectId={proposal.project_id}
                  peerReviewId={peerReviewId}
                />
              ))}
          </Grid>
        )}
      </div>
    </div>
  );
};

export default PeerReviewProposals;
