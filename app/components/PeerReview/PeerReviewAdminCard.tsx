import useTranslation from 'next-translate/useTranslation';
import { FC, useState } from 'react';
import Alert from 'components/Tools/Alert';
import { useApi } from 'contexts/apiContext';
import { PeerReview } from 'types';
import A from '../primitives/A';
import Button from '../primitives/Button';
import SpinLoader from '../Tools/SpinLoader';
import { hasPeerReviewStarted } from '~/utils/utils';

interface Props {
  peerReview: PeerReview;
  callBack: () => void;
}

const PeerReviewAdminCard: FC<Props> = ({ peerReview, callBack }) => {
  const [sending, setSending] = useState('');
  const [error, setError] = useState('');
  const api = useApi();
  const { t } = useTranslation('common');

  const onSuccess = () => {
    setSending('');
    callBack();
  };

  const onError = (type, err) => {
    console.error(`Couldn't ${type} peer review`, err);
    setSending('');
    setError(t('err-'));
  };

  const removePeerReview = () => {
    setSending('remove');
    api
      .delete(`/api/peer_reviews/${peerReview.id}`)
      .then(() => onSuccess())
      .catch((err) => onError('delete', err));
  };

  if (peerReview !== undefined) {
    let imgTodisplay = '/images/default/default-peer-review.jpg';
    if (peerReview.banner_url_sm) {
      imgTodisplay = peerReview.banner_url_sm;
    }

    const bgBanner = {
      backgroundImage: `url(${imgTodisplay})`,
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      border: '1px solid #ced4da',
      borderRadius: '50%',
      height: '50px',
      width: '50px',
    };

    return (
      <div>
        <div tw="flex justify-between py-3" key={peerReview.id}>
          <div tw="flex items-center pb-3 space-x-3 sm:pb-0">
            <div style={{ width: '50px' }}>
              <div style={bgBanner} />
            </div>
            <div tw="flex flex-col">
              <A href={`/peer-review/${peerReview.short_title}`}>{peerReview.title}</A>
              <div tw="flex flex-col text-[93%] pt-1">
                Visibility:&nbsp;
                {peerReview.visibility}
              </div>
            </div>
          </div>
          {!hasPeerReviewStarted(peerReview.proposals.length) && (
            <div tw="flex items-center ml-3">
              <Button
                btnType="danger"
                disabled={sending === 'remove' || peerReview.visibility === 'open'}
                onClick={removePeerReview}
              >
                {sending === 'remove' && <SpinLoader />}
                {t('attach.remove')}
              </Button>
              {error !== '' && <Alert type="danger" message={error} />}
            </div>
          )}
        </div>
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};
export default PeerReviewAdminCard;
