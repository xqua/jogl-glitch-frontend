/* eslint-disable camelcase */
import Link from 'next/link';
import React from 'react';
import ObjectCard from 'components/Cards/ObjectCard';
import H2 from 'components/primitives/H2';
import Title from 'components/primitives/Title';
import { TextWithPlural } from 'utils/managePlurals';
import tw from 'twin.macro';
import { peerReviewStatus, showColoredStatus } from '~/utils/utils';
import useTranslation from 'next-translate/useTranslation';
import { PeerReview, Program, Space } from '~/types';
import useGet from '~/hooks/useGet';

interface Props {
  peerReview: PeerReview;
  source?: 'algolia' | 'api';
  cardFormat?: string;
  width?: string;
  chip?: string;
}
const PeerReviewCard = ({ peerReview, source, cardFormat, width, chip }: Props) => {
  const { t } = useTranslation('common');
  const peeReviewUrl = `/peer-review/${peerReview.short_title}`;
  const peerReviewResource = { type: peerReview?.resource_type.toLowerCase(), id: peerReview?.resource_id };
  const { data: peer_review_parent } = useGet<Space | Program>(
    `/api/${peerReviewResource.type}s/${peerReviewResource.id}`
  );
  return (
    <ObjectCard
      imgUrl={peerReview.banner_url || '/images/default/default-program.jpg'}
      href={peeReviewUrl}
      width={width}
      chip={chip}
      cardFormat={cardFormat}
    >
      {cardFormat === 'showObjType' && (
        <div tw="bg-[#FFA1C3] w-[calc(100% + 32px)] pl-3 flex flex-wrap -mx-4 mt-[-.8rem] mb-[.8rem]">
          <span tw="font-bold px-1 text-sm">{t('entity.card.peer-reviews')}</span>
        </div>
      )}
      {/* Title */}
      <div tw="inline-flex items-center md:h-14">
        <Link href={peeReviewUrl} passHref>
          <Title>
            <H2 tw="word-break[break-word] line-clamp-2 items-center">{peerReview.title}</H2>
          </Title>
        </Link>
      </div>
      <Hr tw="mt-2 pt-4" />
      <div tw="inline-flex flex-wrap mb-1">
        {t('proposal.card.organizedBy')}:&nbsp;
        <Link href={`/${peerReviewResource.type}/${peer_review_parent?.short_title}`} passHref>
          <a tw="text-black font-medium underline hover:(text-black font-bold)">{peer_review_parent?.title}</a>
        </Link>
      </div>
      {showColoredStatus(
        peerReviewStatus(peerReview.visibility, {
          start: peerReview.start,
          deadline: peerReview.deadline,
          stop: peerReview.stop,
        }),
        t
      )}
      <Hr tw="mt-2 pt-4" />
      {/* Description */}
      <div css={[tw`flex-1`, cardFormat === 'compact' ? tw`line-clamp-3` : tw`line-clamp-6`]}>{peerReview.summary}</div>
      {/* Stats */}
      {cardFormat !== 'compact' && (
        <>
          <Hr tw="mt-5 pt-2" />
          <div tw="items-center justify-around space-x-2 flex flex-wrap">
            <CardData
              value={peerReview.proposals_count}
              title={<TextWithPlural type="proposal" count={peerReview.proposals_count} />}
            />
            {peerReview.maximum_disbursement ? (
              <CardData value={peerReview.maximum_disbursement} title={t('peerReview.funding.maximum_disbursement')} />
            ) : (
              <CardData value="0" title={t('peerReview.funding.no_grant')} />
            )}
            {/* <CardData value={challengesCount} title={<TextWithPlural type="challenge" count={challengesCount} />} /> */}
          </div>
        </>
      )}
      {/* Star icon */}
      {/* {(peerReview.has_saved !== undefined || source === 'algolia') && (
        <BtnStar itemType="peer_reviews" itemId={peerReview.id} saveState={peerReview.has_saved} source={source} />
      )} */}
    </ObjectCard>
  );
};

const CardData = ({ value, title }) => (
  <div tw="flex flex-col justify-center items-center">
    <div tw="font-bold font-size[17px] -mb-1">{value}</div>
    <div tw="text-gray-500 font-medium text-sm">{title}</div>
  </div>
);
const Hr = (props) => <div tw="-mx-4 border-0 border-t border-solid border-color[rgba(0, 0, 0, 0.07)]" {...props} />;

export default PeerReviewCard;
