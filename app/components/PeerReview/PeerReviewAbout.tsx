import React, { FC } from 'react';
// import useTranslation from 'next-translate/useTranslation';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';

interface Props {
  description: string;
  maximum_disbursement: number;
}

const PeerReviewAbout: FC<Props> = ({ description, maximum_disbursement }) => {
  // const { t } = useTranslation('common');

  return (
    <div>
      <InfoHtmlComponent content={description} />
      {/* <hr />
      <div tw="flex flex-col justify-start">
        <div tw="flex gap-2 items-center font-bold mb-2">
          {t('peerReview.funding.maximum_disbursement')}:<div tw="italic">{maximum_disbursement}€</div>
        </div>

        <span>{t('peerReview.funding.maximum_disbursement_message')}</span>
      </div> */}
    </div>
  );
};

export default PeerReviewAbout;
