import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { displayObjectUTCDate, peerReviewStatus, showColoredStatus } from '~/utils/utils';
import day from 'dayjs';
import utc from 'dayjs/plugin/utc';
import localizedFormat from 'dayjs/plugin/localizedFormat';
import A from '../primitives/A';
import useGet from '~/hooks/useGet';
import { PeerReview, Program, Space } from '~/types';
import InfoStats from '../Tools/InfoStats';

interface Props {
  peerReview: PeerReview;
}

const PeerReviewInfo: FC<Props> = ({ peerReview }) => {
  const { t } = useTranslation('common');
  day.extend(utc);
  day.extend(localizedFormat);

  const peerReviewResource = { type: peerReview?.resource_type?.toLowerCase(), id: peerReview?.resource_id };
  const proposalsCount = peerReview?.proposals.filter(({ submitted_at }) => submitted_at).length;
  const { data: peer_review_parent } = useGet<Space | Program>(
    `/api/${peerReviewResource.type + 's'}/${peerReviewResource.id}`
  );

  return (
    <div tw="flex-shrink-0 w-full md:p-4 md:w-[26rem]">
      <div tw="flex flex-wrap mt-4 mb-2 justify-around">
        <InfoStats object="peer-review" type="proposal" tab="proposals" count={proposalsCount} />
        {peerReview?.maximum_disbursement ? (
          <div tw="py-1 text-center">
            <div tw="text-2xl font-bold">{peerReview?.maximum_disbursement}€</div>
            <p tw="mb-0">{t('peerReview.funding.maximum_grant_available')}</p>
          </div>
        ) : (
          ''
        )}
      </div>
      <hr tw="mb-4" />
      <div tw="grid gap-4 grid-cols-1">
        <div tw="flex items-center justify-between flex-wrap">
          <div>{t('attach.status')}</div>
          {showColoredStatus(
            peerReviewStatus(peerReview?.visibility, {
              start: peerReview?.start,
              deadline: peerReview?.deadline,
              stop: peerReview?.stop,
            }),
            t
          )}
        </div>
        <div tw="flex items-center justify-between flex-wrap">
          <div>{t('peerReview.info.startedOn')}:</div>
          <span tw="font-bold">{displayObjectUTCDate(peerReview?.start)}</span>
        </div>
        <div tw="flex items-center justify-between flex-wrap">
          <div>{t('peerReview.info.submissionDeadline')}:</div>
          <span tw="font-bold">{displayObjectUTCDate(peerReview?.deadline)}</span>
        </div>
        <div tw="flex items-center justify-between flex-wrap md:flex-nowrap">
          <div>{t('peerReview.info.organizedBy')}:</div>
          <span tw="px-1 font-bold hover:underline">
            <A href={`/${peerReviewResource?.type}/${peer_review_parent?.short_title}`}>{peer_review_parent?.title}</A>
          </span>
        </div>
      </div>
    </div>
  );
};

export default PeerReviewInfo;
