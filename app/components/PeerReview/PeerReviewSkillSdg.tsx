import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Chips from '../Chip/Chips';
import InfoInterestsComponent from '../Tools/Info/InfoInterestsComponent';

interface Props {
  interests: number[];
  skills: string[];
}

const PeerReviewSkillSdg: FC<Props> = ({ interests, skills }) => {
  const { t } = useTranslation('common');

  return (
    <div tw="flex-wrap w-full mt-1 mb-2">
      <div tw="flex flex-col w-full md:justify-between">
        {skills?.length > 0 && (
          <div tw="w-full flex flex-col flex-wrap">
            <div tw="flex mb-2 gap-2">
              <span tw="underline font-bold text-gray-700 sm:no-underline">{t('peerReview.form.keywords')}</span>
              <Chips
                data={skills.map((skill) => ({
                  title: skill,
                  href: `/search/peer-reviews/?refinementList[skills][0]=${skill}`,
                }))}
                type="skills"
                smallChips
                overflowText="seeMore"
                showCount={5}
              />
            </div>
          </div>
        )}

        {interests && (
          <div tw="w-full">
            <InfoInterestsComponent content={interests} title={t('peerReview.form.associatedSDGs')} />
          </div>
        )}
      </div>
    </div>
  );
};

export default PeerReviewSkillSdg;
