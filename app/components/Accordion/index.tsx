import { AngleDown, AngleUp } from '@emotion-icons/fa-solid';
import { Accordion as AccordionReach, AccordionButton, AccordionItem, AccordionPanel } from '@reach/accordion';
import React, { FC, useState } from 'react';
import styled from 'utils/styled';
import Card from 'components/Cards/Card';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import '@reach/accordion/styles.css';
import tw from 'twin.macro';

interface Props {
  values: { title: string; content: string }[];
  hasHtmlContent?: boolean;
}

const Accordion: FC<Props> = ({ values, hasHtmlContent = false }) => {
  const [index, setIndex] = useState(0);

  return (
    <CustomAccordion index={index} onChange={(value) => setIndex(value)}>
      {values.map((value, i) => (
        <AccordionItem key={i}>
          <Card>
            <CustomAccordionButton>
              <div
                tw="flex justify-between space-y-0 font-bold"
                css={[i === index ? tw`text-primary` : tw`text-[rgba(0, 0, 0, 0.7)]`]}
              >
                <span>{value.title}</span>
                <span>
                  {i === index ? (
                    <AngleUp size={15} title="Hide accordion content" />
                  ) : (
                    <AngleDown size={15} title="Show accordion content" />
                  )}
                </span>
              </div>
            </CustomAccordionButton>
            <AccordionPanel>
              {!hasHtmlContent ? value.content : <InfoHtmlComponent content={value.content} />}
            </AccordionPanel>
          </Card>
        </AccordionItem>
      ))}
    </CustomAccordion>
  );
};

const CustomAccordion = styled(AccordionReach)`
  > * + * {
    margin-top: ${(p) => p.theme.space[3]};
  }
`;

const CustomAccordionButton = styled(AccordionButton)`
  width: 100%;
  padding: ${(p) => p.theme.space[2]} 0 0;
  text-align: left;
  cursor: pointer;
`;

export default Accordion;
