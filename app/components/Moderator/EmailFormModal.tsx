import { FC, useState } from 'react';
// import useTranslation from 'next-translate/useTranslation';
import Button from 'components/primitives/Button';
import { useApi } from 'contexts/apiContext';
import { useModal } from 'contexts/modalContext';
import SpinLoader from 'components/Tools/SpinLoader';

interface Props {
  matches: number;
  listIdsSkills: number[];
}
const EmailFormModal: FC<Props> = ({ matches, listIdsSkills }) => {
  const [object, setObject] = useState('');
  const [content, setContent] = useState('');
  const [isSending, setIsSending] = useState(false);
  // const { t } = useTranslation('common');
  const { closeModal } = useModal();
  const api = useApi();

  const handleChange = (e) => {
    if (e.target.name === 'object') setObject(e.target.value);
    setContent(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (confirm(`Are you sure you want to send this email to ${matches} JOGLers?`)) {
      sendEmail();
    } else {
      closeModal();
    }
  };

  const sendEmail = () => {
    const param = {
      content,
      object,
      skills: listIdsSkills,
    };
    if (listIdsSkills?.length !== 0) {
      setIsSending(true);
      api
        .post('api/admin/email_users_with_skills', param)
        .then((res) => {
          // alert(res?.data?.message);
          alert('sent!');
          setIsSending(false);
          closeModal();
        })
        .catch((err) => {
          console.log(`this is a skilled email sending error: ${err}`);
          setIsSending(false);
        });
    }
  };

  const disabledBtns = !content || !object;

  return (
    <section>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="object">Object</label>
          <input
            type="text"
            className="object styledInputTextArea"
            id="object"
            name="object"
            onChange={handleChange}
            required
          />
        </div>
        <div>
          <label htmlFor="content">Email content</label>
          <textarea
            className="message styledInputTextArea"
            style={{ minHeight: '200px' }}
            id="content"
            name="content"
            onChange={handleChange}
            required
          />
        </div>
        <div tw="flex gap-4">
          <Button type="submit" btnType="primary" disabled={disabledBtns}>
            {isSending ? <SpinLoader /> : 'Send email'}
          </Button>
          <Button btnType="secondary" onClick={closeModal}>
            Cancel
          </Button>
        </div>
      </form>
    </section>
  );
};

export default EmailFormModal;
