import { FC, useState } from 'react';
import Button from 'components/primitives/Button';
import { useApi } from 'contexts/apiContext';
import SpinLoader from 'components/Tools/SpinLoader';

interface Props {
  listIdsSkills: number[];
  disable: boolean;
}
const DownloadSkilledUsers: FC<Props> = ({ listIdsSkills, disable }) => {
  const api = useApi();
  const [isSending, setIsSending] = useState(false);

  const handleCSV = () => {
    const param = {
      skills: listIdsSkills,
    };
    setIsSending(true);
    api
      .post('api/admin/users_with_skills.csv', param)
      .then((res) => {
        const data = `text/csv;charset=utf-8,${encodeURIComponent(res.data)}`;
        const link: HTMLElement = document.querySelector('#dlHiddenLink') as HTMLElement;
        link.href = `data:${data}`;
        link.download = `users-with-skills.csv`;
        link.innerHTML = 'download CSV';
        link.click();
        setIsSending(false);
      })
      .catch((err) => {
        console.log(`this is a doc csv download error: ${err}`);
        setIsSending(false);
      });
  };

  return (
    <div tw="flex gap-4">
      <Button btnType="primary" onClick={handleCSV} disabled={disable}>
        {isSending ? <SpinLoader /> : 'Download list'}
      </Button>
      <a href="#" id="dlHiddenLink" tw="visibility[hidden]" />
    </div>
  );
};

export default DownloadSkilledUsers;
