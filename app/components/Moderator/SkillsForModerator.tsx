import { useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import FormSkillsComponent from 'components/Tools/Forms/FormSkillsComponent';
import FormValidator from 'components/Tools/Forms/FormValidator';
import skillsFormRules from './skillsFormRules.json';
import EmailFormModal from '~/components/Moderator/EmailFormModal';
import DownloadSkilledUsers from 'components/Moderator/DownloadSkilledUsers';
import Button from 'components/primitives/Button';
import P from 'components/primitives/P';
import { useApi } from 'contexts/apiContext';
import { useModal } from 'contexts/modalContext';

const SkillsForModerator = () => {
  const validator = new FormValidator(skillsFormRules);
  const [listIdsSkills, setListIdsSkills] = useState([]);
  const [stateValidation, setStateValidation] = useState({});
  const [numberOfMatches, setNumberOfMatches] = useState(0);
  const api = useApi();
  const { t } = useTranslation('common');
  const { showModal } = useModal();

  const handleChange = (key, content) => {
    // We enter handleChange each time we select or make up a skill in dropdown, or delete one
    const skillsName = content?.map(({ label }) => label);
    const skillsId = content?.map(({ skillId }) => skillId);
    // Validator
    const state = {};
    state[key] = skillsName;
    const validation = validator.validate(state);
    if (validation[key] !== undefined) {
      const newStateValidation = {};
      newStateValidation[`valid_${key}`] = validation[key];
      setStateValidation(newStateValidation);
    }
    getCount(skillsId);
  };

  const getCount = (skillsId) => {
    setListIdsSkills(skillsId); // to capture skillsId into array local state so it can later be passed as props
    const param = {
      skills: skillsId,
    };
    if (skillsId?.length !== 0) {
      api
        .post('api/admin/users_with_skills', param)
        .then((res) => setNumberOfMatches(res?.data?.count))
        .catch((err) => console.warn(`this is a matches count error: ${err}`));
    }
    // if all skills inputs are taken away, count needs to go back to 0
    else setNumberOfMatches(0);
  };

  const { valid_skills } = stateValidation || '';
  const disabledBtns = listIdsSkills?.length === 0;

  return (
    <section>
      <form>
        <FormSkillsComponent
          id="skills"
          type="moderator"
          placeholder={t('general.skills.placeholder')}
          title=""
          errorCodeMessage={valid_skills ? valid_skills.message : ''}
          onChange={handleChange}
        />
      </form>
      <P tw="italic text-sm">Number of JOGLers with this skills set: {numberOfMatches}</P>
      <div tw="flex gap-4">
        <Button
          onClick={() => {
            showModal({
              children: <EmailFormModal matches={numberOfMatches} listIdsSkills={listIdsSkills} />,
              title: 'Send an email to skilled JOGLers',
              maxWidth: '50rem',
            });
          }}
          btnType="secondary"
          disabled={disabledBtns}
        >
          Send email
        </Button>
        <DownloadSkilledUsers listIdsSkills={listIdsSkills} disable={disabledBtns} />
      </div>
    </section>
  );
};

export default SkillsForModerator;
