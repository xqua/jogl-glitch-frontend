/* eslint-disable camelcase */
import Link from 'next/link';
import React, { FC } from 'react';
import ObjectCard from 'components/Cards/ObjectCard';
import H2 from 'components/primitives/H2';
import Title from 'components/primitives/Title';
import BtnStar from 'components/Tools/BtnStar';
import { DataSource } from 'types';
import { TextWithPlural } from 'utils/managePlurals';
import Chips from '../Chip/Chips';
import tw from 'twin.macro';

interface Props {
  id: number;
  title: string;
  shortTitle: string;
  short_description: string;
  postsCount?: number;
  members_count: number;
  has_saved?: boolean;
  banner_url?: string;
  width?: string;
  cardFormat?: string;
  source?: DataSource;
  skills?: string[];
}

const CommunityCard: FC<Props> = ({
  id,
  title,
  shortTitle,
  short_description,
  postsCount,
  members_count,
  has_saved,
  banner_url = '/images/default/default-group.jpg',
  width,
  cardFormat,
  source,
  skills,
}) => {
  const groupUrl = `/community/${id}/${shortTitle}`;

  return (
    <ObjectCard imgUrl={banner_url} href={groupUrl} width={width}>
      <div tw="flex justify-between space-x-4">
        <Link href={groupUrl} passHref>
          <Title tw="pr-2">
            <H2
              tw="word-break[break-word] items-center line-clamp-2"
              // css={[cardFormat !== 'compact' && tw`md:text-3xl`]}
            >
              {title}
            </H2>
          </Title>
        </Link>
        {(has_saved !== undefined || source === 'algolia') && (
          <BtnStar itemType="communities" itemId={id} hasStarred={has_saved} hasNoStat source={source} />
        )}
      </div>
      {/* <div style={{ color: "grey", paddingBottom: "5px" }}>
            <span> Last active today </span> <span> Prototyping </span>
          </div> */}
      <div tw="line-clamp-4 flex-1">{short_description}</div>
      {skills && (
        <Chips
          data={skills.map((skill) => ({
            title: skill,
            href: `/search/groups/?refinementList[skills][0]=${skill}`,
          }))}
          overflowLink={`/community/${id}/${shortTitle}`}
          type="skills"
          showCount={3}
          smallChips
        />
      )}
      {cardFormat !== 'compact' && (
        <div tw="flex items-center justify-between space-x-2 flex-wrap">
          <CardData value={members_count} title={<TextWithPlural type="member" count={members_count} />} />
          {postsCount > 0 && <CardData value={postsCount} title={<TextWithPlural type="post" count={postsCount} />} />}
        </div>
      )}
    </ObjectCard>
  );
};

const CardData = ({ value, title }) => (
  <div tw="flex flex-col justify-center items-center">
    <div>{value}</div>
    <div>{title}</div>
  </div>
);

export default CommunityCard;
