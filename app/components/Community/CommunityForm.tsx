/* eslint-disable camelcase */
import { FC, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
// import MembersList from "~/components/Members/MembersList";
/** * Form objects ** */
import FormDefaultComponent from 'components/Tools/Forms/FormDefaultComponent';
import FormImgComponent from 'components/Tools/Forms/FormImgComponent';
import FormInterestsComponent from 'components/Tools/Forms/FormInterestsComponent';
import FormSkillsComponent from 'components/Tools/Forms/FormSkillsComponent';
import FormResourcesComponent from 'components/Tools/Forms/FormResourcesComponent';
import FormTextAreaComponent from 'components/Tools/Forms/FormTextAreaComponent';
import FormToggleComponent from 'components/Tools/Forms/FormToggleComponent';
import FormWysiwygComponent from 'components/Tools/Forms/FormWysiwygComponent';
/** * Validators ** */
import FormValidator from 'components/Tools/Forms/FormValidator';
import communityFormRules from './communityFormRules.json';
import { toAlphaNum } from 'components/Tools/Nickname';
import { Community } from 'types';
import { useRouter } from 'next/router';
import { changesSavedConfAlert } from 'utils/utils';
import SpinLoader from '../Tools/SpinLoader';
import AllowPostingToAllToggle from '../Tools/AllowPostingToAllToggle';
import Button from '../primitives/Button';
// import "./CommunityForm.scss";

interface Props {
  mode: 'edit' | 'create';
  community: Community;
  sending: boolean;
  hasUpdated: boolean;
  handleChange: (key, content) => void;
  handleSubmit: () => void;
}

const CommunityForm: FC<Props> = ({
  mode,
  community,
  sending = false,
  hasUpdated = false,
  handleChange,
  handleSubmit,
}) => {
  const validator = new FormValidator(communityFormRules);
  const [stateValidation, setStateValidation] = useState({});
  const { valid_title, valid_short_title, valid_short_description, valid_interests, valid_skills } =
    stateValidation || '';
  const { t } = useTranslation('common');
  const router = useRouter();
  const urlBack = mode === 'edit' ? `/community/${community.id}/${community.short_title}` : '/search/groups';
  const textAction = mode === 'edit' ? 'Update' : 'Create';

  // show conf message when group has been successfully saved/updated
  hasUpdated &&
    changesSavedConfAlert(t)
      .fire()
      // go back to group page if user clicked on conf button
      .then(({ isConfirmed }) => isConfirmed && router.push(urlBack));

  const generateSlug = (grouTitle) => {
    let proposalShortName = grouTitle.trim();
    proposalShortName = toAlphaNum(proposalShortName);
    return proposalShortName;
  };

  const handleChangeGroup = (key, content) => {
    let proposalShortName;
    if (key === 'title') {
      // generate a shortname when typing a title, and update short_name field with the value
      proposalShortName = generateSlug(content);
      handleChange('short_title', proposalShortName);
    }
    /* Validators start */
    const state = {};
    state[key] = content;
    // Check proposalShortName
    if (key === 'title') {
      state.short_title = proposalShortName;
    }
    const validation = validator.validate(state);
    if (validation[key] !== undefined) {
      const newStateValidation = {};
      newStateValidation[`valid_${key}`] = validation[key];
      // Update short_title too only if title has been changed
      if (key === 'title') {
        newStateValidation.valid_short_title = validation.short_title;
      }
      setStateValidation(newStateValidation);
    }
    /* Validators end */
    handleChange(key, content);
  };

  const handleSubmitGroup = () => {
    /* Validators control before submit */
    let firsterror = true;
    const validation = validator.validate(community);
    if (validation.isValid) {
      handleSubmit();
    } else {
      const newStateValidation = {};
      Object.keys(validation).forEach((key) => {
        if (key !== 'isValid') {
          if (validation[key].isInvalid && firsterror) {
            // if field is invalid and it's the first field that has error
            const element = document.querySelector(`#${key}`); // get element that is not valid
            const y = element.getBoundingClientRect().top + window.pageYOffset - 140; // calculate it's top value and remove 25 of offset
            window.scrollTo({ top: y, behavior: 'smooth' }); // scroll to element to show error
            firsterror = false; // set to false so that it won't scroll to second invalid field and further
          }
          newStateValidation[`valid_${key}`] = validation[key];
        }
      });
      setStateValidation(newStateValidation);
    }
  };

  return (
    <form className="communityForm">
      <FormDefaultComponent
        content={community.title}
        errorCodeMessage={valid_title ? valid_title.message : ''}
        id="title"
        isValid={valid_title ? !valid_title.isInvalid : undefined}
        onChange={handleChangeGroup}
        mandatory
        title={t('entity.info.title')}
        placeholder={t('community.form.title_placeholder')}
      />
      {/* {mode === "create" && */}
      <FormDefaultComponent
        content={community.short_title}
        errorCodeMessage={valid_short_title ? valid_short_title.message : ''}
        id="short_title"
        isValid={valid_short_title ? !valid_short_title.isInvalid : undefined}
        mandatory
        onChange={handleChangeGroup}
        pattern={/[A-Za-z0-9]/g}
        title={t('entity.info.short_name')}
        prepend="#"
        placeholder={t('community.form.short_title_placeholder')}
      />
      <FormTextAreaComponent
        content={community.short_description}
        errorCodeMessage={valid_short_description ? valid_short_description.message : ''}
        id="short_description"
        isValid={valid_short_description ? !valid_short_description.isInvalid : undefined}
        mandatory
        maxChar={340}
        onChange={handleChangeGroup}
        title={t('entity.info.short_description')}
        rows={3}
        placeholder={t('community.form.short_description_placeholder')}
      />
      {mode === 'edit' && (
        <FormWysiwygComponent
          id="description"
          title={t('entity.info.description')}
          placeholder={t('community.form.description_placeholder')}
          content={community.description}
          show
          onChange={handleChangeGroup}
        />
      )}
      {mode === 'edit' && (
        <FormImgComponent
          type="banner"
          id="banner_url"
          imageUrl={community.banner_url}
          itemId={community.id}
          itemType="communities"
          title={t('community.info.banner_url')}
          content={community.banner_url}
          defaultImg="/images/default/default-group.jpg"
          onChange={handleChangeGroup}
        />
      )}
      <FormInterestsComponent
        content={community.interests}
        errorCodeMessage={valid_interests ? valid_interests.message : ''}
        mandatory
        onChange={handleChangeGroup}
      />
      <FormSkillsComponent
        content={community.skills}
        errorCodeMessage={valid_skills ? valid_skills.message : ''}
        id="skills"
        type="group"
        isValid={valid_skills ? !valid_skills.isInvalid : undefined}
        mandatory
        onChange={handleChangeGroup}
        title={t('entity.info.skills')}
        placeholder={t('general.skills.placeholder')}
      />
      <FormResourcesComponent
        content={community.ressources}
        id="ressources"
        type="group"
        mandatory={false}
        onChange={handleChangeGroup}
        title={t('entity.info.resources')}
        placeholder={t('general.resources.placeholder')}
      />
      {mode === 'edit' && (
        <>
          <FormToggleComponent
            id="is_private"
            warningMsg={t('community.info.publicPrivateToggleMsg')}
            title={t('entity.info.is_private')}
            choice1={t('general.public')}
            choice2={t('general.private')}
            color1="#F9530B"
            color2="#27B40E"
            isChecked={community.is_private}
            onChange={handleChangeGroup}
          />
          <AllowPostingToAllToggle feedId={community.feed_id} />
        </>
      )}
      <div tw="space-x-2 mt-10 mb-3 text-center">
        <Link href={urlBack} passHref>
          <Button btnType="secondary" disabled={sending}>
            {t('user.profile.edit.back')}
          </Button>
        </Link>
        <Button disabled={sending} onClick={handleSubmit}>
          {sending && <SpinLoader />}
          {t(`entity.form.btn${textAction}`)}
        </Button>
      </div>
    </form>
  );
};
export default CommunityForm;
