import isPropValid from '@emotion/is-prop-valid';
import { MenuItem, MenuList, MenuButton } from '@reach/menu-button';
import styled from 'utils/styled';

export const Container = styled.header`
  box-shadow: ${(p) => p.theme.shadows.default};
  align-items: center;
  position: fixed;
  z-index: 10;
  background: white;
  width: 100%;
  top: 0;
`;
export const MobileNav = styled.nav`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  height: 100%;
`;
export const DesktopNav = styled.nav`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  height: 100%;
  max-width: 1280px;
  margin: auto;
`;
export const Logo = styled.img`
  height: auto;
  width: 100%;
`;
interface ILangItem {
  selected: boolean;
}
export const LangItem = styled(MenuItem)<ILangItem>`
  ${(p) => p.selected && `color: ${p.theme.colors.primary}`};
`;

export const StyledMenuButton = styled(MenuButton, {
  shouldForwardProp: (prop) => isPropValid(prop),
})`
  background: none;
  color: ${(p) => p.theme.colors.dark};
  border: none;
  padding: 0;
  font: inherit;
  cursor: pointer;
  outline: inherit;
  display: inline-block;
  text-align: left;
  &:hover {
    color: black;
  }
`;

export const DropDownMenu = styled(MenuList)`
  background-color: white;
  border: 1px solid black;
  border-radius: ${(p) => p.theme.radii.default};
  box-shadow: ${(p) => p.theme.shadows.xl};
  border: 1px solid ${(p) => p.theme.colors.greys['200']};
  padding: 0px;
  font-size: 1rem;
  * + * {
    ${(p) => `border-top: 1px solid ${p.theme.colors.greys['200']}`};
  }
  a {
    cursor: pointer;
    transition: opacity 0.1s ease-in-out;
    &:hover {
      text-decoration: none;
    }
  }
`;

export const AMenu = styled.a`
  color: ${(p) => p.theme.colors.greys['800']}!important;
  cursor: pointer;
  transition: opacity 0.1s ease-in-out;
  &:hover {
    text-decoration: none;
    color: black !important;
  }
`;

export const ADropDown = styled.a`
  color: ${(p) => p.theme.colors.greys['800']}!important;
  cursor: pointer;
  transition: opacity 0.1s ease-in-out;
  &:hover {
    text-decoration: none;
  }
`;

export const MobileSideNav = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100vw;
  height: 100vh;
  padding-top: 5rem !important;
  background-color: white;
  will-change: transform;
  transition: transform 0.3s ease-in;
  transform: ${({ open }) => (open ? 'translate3d(0, 0, 0)' : 'translate3d(100%, 0, 0)')};
`;
