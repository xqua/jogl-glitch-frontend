import Link from 'next/link';
import Loading from '../Tools/Loading';
import React, { useContext, useState, useEffect } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { Menu, MenuButton, MenuLink, MenuList } from '@reach/menu-button';
import { useRouter } from 'next/router';
import { Transition } from 'react-transition-group';
import Button from 'components/primitives/Button';
import UserMenu from 'components/User/UserMenu';
import { UserContext } from 'contexts/UserProvider';
import useGet from 'hooks/useGet';
import { Program, Space } from '~/types';
import styled from 'utils/styled';
import { useModal } from 'contexts/modalContext';
import {
  AMenu,
  Container,
  DesktopNav,
  DropDownMenu,
  LangItem,
  Logo,
  MobileNav,
  MobileSideNav,
  StyledMenuButton,
} from './Header.styles';
import HeaderNotifications from './HeaderNotifications';
import setLanguage from 'next-translate/setLanguage';
import { Bars, Check, Globe, Search } from '@emotion-icons/fa-solid';
import { ArrowRight } from '@emotion-icons/bootstrap';
import { Home } from '@emotion-icons/material';
import { ProjectListNeed } from '../Project/ProjectListNeed';

const languages = [
  { id: 'en', title: 'English' },
  { id: 'fr', title: 'Français' },
  { id: 'de', title: 'Deutsch' },
  { id: 'es', title: 'Español' },
];

const Header = () => {
  const router = useRouter();
  const { userData, isConnected } = useContext(UserContext);
  const [isOpen, setOpen] = useState(false);
  const [loading, setLoading] = useState(true);
  // const { data: programs } = useGet<Program[]>('/api/programs?simple=true');
  // const { data: spaces } = useGet<Space[]>('/api/spaces?simple=true');
  const { t } = useTranslation('common');
  const modal = useModal();

  useEffect(() => {
    router.events.on('routeChangeStart', () => setOpen(false));
  }, [router]);
  // force close mobile menu when changing page, as the href of some pages are the same(all search page)

  const gotoSignInPage = () => {
    // don't add the redirectUrl query param if we go to signin page from the homepage (for url esthetics reason)
    if (router.pathname === '/') router.push({ pathname: '/signin' });
    else router.push({ pathname: '/signin', query: { redirectUrl: router.asPath } });
  };

  // const featuredSpaces = [3, 4, 39, 47];

  useEffect(() => {
    setLoading(false);
  }, [isConnected]);

  // const FeaturedObjsDropdown = () => (
  //   <Menu>
  //     <StyledMenuButton>
  //       {t('home.featured')} <span aria-hidden>▾</span>
  //     </StyledMenuButton>

  //     <DropDownMenu>
  //       {programs?.programs ? (
  //         programs?.programs
  //           ?.reverse()
  //           .filter(({ status }) => status !== 'completed')
  //           .map((program, i) => (
  //             // browse through programs (from newest to oldest)
  //             <Link key={i} href={`/program/${program.short_title}`} passHref>
  //               <MenuLink to={`/program/${program.short_title}`}>{program.title}</MenuLink>
  //             </Link>
  //           ))
  //       ) : (
  //         <Loading />
  //       )}
  //       {spaces?.spaces
  //         ?.reverse()
  //         .filter(({ status }) => status !== 'completed')
  //         // only show featured spaces
  //         .filter(({ id }) => featuredSpaces.includes(id))
  //         .map((space, i) => (
  //           // browse through spaces (from newest to oldest)
  //           <Link key={i} href={`/space/${space.short_title}`} passHref>
  //             <MenuLink to={`/space/${space.short_title}`}>{space.title}</MenuLink>
  //           </Link>
  //         ))}
  //     </DropDownMenu>
  //   </Menu>
  // );

  const openCreateSpaceModal = () => {
    modal.showModal({
      children: (
        <div tw="sm:p-6">
          <p tw="text-lg mb-10">{t('space.modalCreate.text')}</p>
          <div tw="flex justify-around">
            <a href="https://www.canva.com/design/DAE-_WpI3bY/u5nq-cTci-wvVpCzk_ZePw/view" target="_blank">
              <Button btnType="secondary" tw="hover:(bg-white opacity-80 text-primary)">
                {t('space.modalCreate.button1')}
              </Button>
            </a>
            <a href="mailto:contact@jogl.io?subject=Request to become a JOGL Member and create a Space">
              <Button btnType="primary" tw="border border-solid border-white hover:opacity-80">
                {t('space.modalCreate.button2-contact')}
              </Button>
            </a>
          </div>
        </div>
      ),
      title: t('space.modalCreate.title'),
      maxWidth: '20rem',
      modalClassName: 'createSpaceModal',
    });
  };

  const openCreateNeedModal = () => {
    modal.showModal({
      children: <ProjectListNeed closeModal={modal.closeModal} />,
      title: `${t('entity.form.btnCreate')} ${t('header.createNeed')}`,
      maxWidth: '50rem',
    });
  };

  return (
    <Container tw="h-16 px-3">
      {/* for users navigating with keyboard, show this so they can skip content*/}
      <div className="skip-links">
        Skip to <a href="#main">content</a> or <a href="#footer">footer</a>
      </div>
      {/* Mobile Nav -- Will only display on mobile breakpoints */}
      <MobileNav tw="flex md:hidden">
        <div id="home-link-mobile">
          <Link href="/" passHref>
            <div tw="h-auto w-20">
              <Logo src="/images/logo.png" alt="JOGL icon" quality={50} />
            </div>
          </Link>
        </div>
        <div tw="flex">
          {isConnected && !loading && (
            <div tw="flex space-x-4 mr-2">
              <HeaderNotifications userId={userData?.id} />
              <UserMenu />
            </div>
          )}
          <Menu>
            <StyledMenuButton tw="z-10 relative ml-3 text-2xl" onClick={() => setOpen(!isOpen)}>
              <Bars size={23} title="Mobile menu" />
            </StyledMenuButton>
            <Transition in={isOpen} timeout={300}>
              {(state) => (
                // state change: exited -> entering -> entered -> exiting -> exited
                <MobileSideNav open={state === 'entering' || state === 'entered'} tw="p-4">
                  <MobileLinksBox tw="space-y-3">
                    <Link href="/" passHref>
                      <AMenu>
                        <Home size={23} title="Home" />
                        {t('program.home.title')}
                      </AMenu>
                    </Link>
                    <div>
                      <Link href="/search/members" passHref>
                        <AMenu>
                          <Search size={15} title="Search website" />
                          {t('general.explore')}
                        </AMenu>
                      </Link>
                    </div>
                    {/* <div tw="pt-3">
                      <FeaturedObjsDropdown />
                    </div> */}
                    {isConnected && (
                      <div tw="pt-3">
                        <CreateObjectsDropdown
                          t={t}
                          openCreateSpaceModal={openCreateSpaceModal}
                          openCreateNeedModal={openCreateNeedModal}
                        />
                      </div>
                    )}
                    <LangDropdown />
                    {!isConnected && <Button onClick={gotoSignInPage}>{t('header.signInUp')}</Button>}
                  </MobileLinksBox>
                </MobileSideNav>
              )}
            </Transition>
          </Menu>
        </div>
      </MobileNav>
      {/* Desktop Nav */}
      <DesktopNav tw="hidden md:flex">
        <div tw="inline-flex items-center md:space-x-4 lg:space-x-5">
          <Link href="/" passHref>
            <AMenu>
              <div tw="h-auto w-24">
                <Logo src="/images/logo.png" alt="JOGL icon" quality={50} />
              </div>
            </AMenu>
          </Link>
          <div id="home-link-desktop">
            <Link href="/" passHref>
              <AMenu>
                <Home size={23} title="Home" />
                {t('program.home.title')}
              </AMenu>
            </Link>
          </div>
          <div id="explore">
            <Link href="/search/members" passHref className="explore">
              <AMenu>
                <Search size={15} title="Search website" />
                {t('general.explore')}
              </AMenu>
            </Link>
          </div>
          {/* <div id="activePrograms">
            <FeaturedObjsDropdown />
          </div> */}
        </div>
        <div tw="flex space-x-4">
          {isConnected && !loading && (
            <CreateObjectsDropdown
              t={t}
              openCreateSpaceModal={openCreateSpaceModal}
              openCreateNeedModal={openCreateNeedModal}
            />
          )}
          {isConnected && !loading && <HeaderNotifications userId={userData?.id} />}
          <LangDropdown />
          {loading ? (
            <Loading />
          ) : isConnected ? (
            <UserMenu />
          ) : (
            <Button onClick={gotoSignInPage}>{t('header.signInUp')}</Button>
          )}
        </div>
      </DesktopNav>
    </Container>
  );
};

const createItem = (t, translateId, text) => (
  <>
    <div tw="pr-4">
      <h5>{t(`header.${translateId}`)}</h5>
      <p tw="mb-0">{text}</p>
    </div>
    <div tw="hidden sm:block">
      <ArrowRight size={20} title="Go" />
    </div>
  </>
);

const CreateObjectsDropdown = ({ t, openCreateSpaceModal, openCreateNeedModal }) => (
  <Menu>
    <MenuButton tw="text-left lg:text-center px-3 py-2 h-[fit-content] text-base transition-colors duration-150 bg-primary text-white border rounded border-solid hocus:bg-[#166098] cursor-pointer mr-2">
      +&nbsp;{t('entity.form.btnCreate')}
    </MenuButton>
    <MenuList tw="text-primary p-3 w-[350px] space-y-2 rounded mt-1">
      <div tw="hover:text-white">
        <MenuLink
          onClick={openCreateNeedModal}
          tw="p-4 flex justify-between items-center rounded bg-[#F9FCFD] border border-gray-300 border-solid hover:(bg-[#eaedf0] no-underline text-primary)"
        >
          {createItem(t, 'createNeed', 'Ask for help, define an opportunity')}
        </MenuLink>
      </div>
      <Link href="/project/create" passHref>
        <MenuLink
          to="/project/create"
          tw="p-4 flex justify-between items-center rounded bg-[#F9FCFD] border border-gray-300 border-solid hover:(bg-[#eaedf0] no-underline text-primary)"
        >
          {createItem(t, 'createProject', 'Showcase what you are working on')}
        </MenuLink>
      </Link>
      <div tw="hover:text-white">
        <MenuLink
          onClick={openCreateSpaceModal}
          tw="p-4 flex justify-between items-center rounded bg-[#F9FCFD] border border-gray-300 border-solid hover:(bg-[#eaedf0] no-underline text-primary)"
        >
          {createItem(t, 'createSpace', 'Animate your own community')}
        </MenuLink>
      </div>
    </MenuList>
  </Menu>
);

const LangDropdown = () => {
  const router = useRouter();
  const { locale } = router;

  const changeLanguage = (newLanguage) => {
    // This is allowed because it is only called on client side.
    // This code has a lot of implications, because normally there's no locale set in the
    // cookies so the prioritized language is the one from your browser if it exists.
    // If your browser language is not supported it will fallback to english.
    // But if you set a locale in the cookies this lang will prioritize over all langs.
    // This is all defined in the custom server in server.js
    // cookie.set('locale', newLanguage, { expires: 365 });

    // Using next-translate new feature setLanguage to update the language globally within the app
    setLanguage(newLanguage);

    // router.push(router.pathname, router.asPath, { locale: newLanguage });
  };

  return (
    <Menu>
      <StyledMenuButton tw="uppercase text-left lg:text-center">
        <Globe size={15} title="Change locale" /> <span tw="-ml-1">{locale}</span> <span aria-hidden>▾</span>
      </StyledMenuButton>
      <DropDownMenu>
        {languages.map((lang, i) => (
          <LangItem
            onClick={() => changeLanguage(lang.id)}
            onSelect={() => changeLanguage(lang.id)}
            selected={locale === lang.id}
            key={i}
          >
            {lang.title} {locale === lang.id && <Check size={16} title="Selected locale" />}
          </LangItem>
        ))}
      </DropDownMenu>
    </Menu>
  );
};

const MobileLinksBox = styled.div`
  display: flex;
  flex-direction: column;
  > * + div,
  > * + button {
    ${(p) => `border-top: 1px solid ${p.theme.colors.greys['400']}!important`};
    padding-top: 0.75rem;
  }
`;

export default React.memo(Header);
