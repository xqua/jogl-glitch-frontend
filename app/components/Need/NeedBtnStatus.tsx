import { useState, FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from 'contexts/apiContext';
import useUser from 'hooks/useUser';
import { logEventToGA } from 'utils/analytics';
import SpinLoader from '../Tools/SpinLoader';
import { Need } from 'types';
import Button from 'components/primitives/Button';

interface Props {
  need?: Need;
}

const NeedBtnStatus: FC<Props> = ({ need }) => {
  const [sending, setSending] = useState(false);
  const api = useApi();
  const { user } = useUser();
  const { t } = useTranslation('common');

  const changeStatus = () => {
    // set new need status
    const newStatus = need.status === undefined || need.status === 'active' ? 'completed' : 'active';
    setSending(true);
    api
      .patch(`/api/needs/${need.id}`, { status: newStatus })
      .then(() => {
        setSending(false);
        // send event to google analytics (only if user is closing the need)
        newStatus === 'completed' &&
          logEventToGA('close need', 'Need', `[${user.id},${need.id}]`, { userId: user?.id, needId: need.id });
        const cancelBtn: HTMLElement = document.querySelector('#cancelBtn') as HTMLElement;
        cancelBtn.click(); // force click on cancel button to go back to normal view
      })
      .catch(() => setSending(false));
  };
  if (need && need.is_owner) {
    return (
      <Button tw="bg-yellow-500 border-yellow-500" onClick={changeStatus} disabled={sending}>
        {need.status === 'completed' && sending && <SpinLoader />}
        {need.status === 'completed' && !sending && t('need.card.reopen')}
        {need.status !== 'completed' && sending && <SpinLoader />}
        {need.status !== 'completed' && !sending && t('need.card.close')}
      </Button>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};

export default NeedBtnStatus;
