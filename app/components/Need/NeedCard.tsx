import Link from 'next/link';
import React from 'react';
import useTranslation from 'next-translate/useTranslation';
import BasicChip from '../BasicChip';
import { useRouter } from 'next/router';
import ObjectCard from 'components/Cards/ObjectCard';
import Chips from 'components/Chip/Chips';
import H2 from 'components/primitives/H2';
import Title from 'components/primitives/Title';
import BtnStar from 'components/Tools/BtnStar';
import ShareBtns from 'components/Tools/ShareBtns/ShareBtns';
import { DataSource, Project } from 'types';
import { ThumbsUp } from '@emotion-icons/fa-solid/';
import NeedDates from './NeedDates';
import Image from 'components/primitives/Image';
import tw from 'twin.macro';
import { logEventToGA } from '~/utils/analytics';
import useUser from '~/hooks/useUser';

interface Props {
  title: string;
  project?: Project;
  hasSaved?: boolean;
  postsCount?: number;
  skills?: string[];
  resources?: string[];
  publishedDate?: string;
  dueDate?: string;
  id: number;
  width?: string;
  cardFormat?: string;
  source?: DataSource;
  status?: string;
  membersCount?: number;
  recommendedIds?: number[];
  chip?: string;
}

const NeedCard = ({
  title,
  project,
  skills = [],
  resources = [],
  publishedDate,
  dueDate,
  hasSaved,
  postsCount = 0,
  id,
  width,
  cardFormat,
  status,
  source,
  membersCount = 0,
  recommendedIds = [],
  chip,
}: Props) => {
  const { t } = useTranslation('common');
  const needUrl = `/need/${id}`;
  const router = useRouter();
  const { user } = useUser();

  return (
    <ObjectCard
      href={needUrl}
      width={width}
      {...(project && cardFormat !== 'compact' && { imgUrl: project.banner_url })}
      // show completed tag if status is completed
      chip={chip && cardFormat === 'showObjType' ? chip : status === 'completed' && t('entity.info.status.completed')}
      cardFormat={cardFormat}
    >
      {cardFormat === 'showObjType' && (
        <div tw="bg-[#9baec8] w-[calc(100% + 32px)] pl-3 flex flex-wrap -mx-4 mt-[-.8rem] mb-[.8rem]">
          <span tw="px-1 space-x-1 font-bold text-sm">{t('need.uppercase')}</span>
        </div>
      )}
      <div tw="flex flex-col justify-between h-full">
        <div>
          {/* Title */}
          <div css={[tw`inline-flex items-center`, cardFormat !== 'compact' && tw`md:h-12`]}>
            <Link href={`/need/${id}`} passHref>
              <Title
                onClick={() =>
                  recommendedIds?.length !== 0 &&
                  logEventToGA('Reco click', 'Recommendation', `[${user.id},${id},"need", ${recommendedIds}]`, {
                    userId: user.id,
                    itemId: id,
                    itemType: 'need',
                    recommendedIds,
                  })
                }
              >
                {/* Add padding-right if card doesn't have project (thus its banner), so title doesn't overlap with position absolute's star icon */}
                <H2
                  css={[
                    tw`word-break[break-word] items-center line-clamp-2 pr-6`,
                    // cardFormat !== 'compact' && project && tw`md:(text-3xl h-16)`,
                    // cardFormat !== 'compact' && project && tw`md:text-3xl`,
                  ]}
                >
                  {title}
                </H2>
              </Title>
            </Link>
          </div>
          {/* Project */}
          {project && (
            <>
              <Hr tw="mt-3 pt-3" />
              <div tw="inline-flex space-x-2 items-center">
                {/* <div tw="w-3!">
                  <Image src="/images/logo.svg" alt="jogl logo rocket" quality={25} tw="w-3!" />
                </div> */}
                <div>
                  {t('needsPage.project')}
                  <Link href={`/project/${project.id}`} passHref>
                    <a tw="text-black font-medium hover:(underline text-black)">{project.title}</a>
                  </Link>
                </div>
              </div>
            </>
          )}
          {/* Skills & resources chips */}
          {cardFormat !== 'compact' && (
            <>
              <Hr tw="mt-2 pt-2" />
              {skills.length !== 0 && (
                <>
                  <p tw="text-gray-400 mb-0 text-sm">{t('need.skills.title')}</p>
                  <Chips
                    data={skills.map((skill) => ({
                      title: skill,
                      href: `/search/needs/?refinementList[skills][0]=${skill}`,
                    }))}
                    overflowLink={`/need/${id}`}
                    type="skills"
                    showCount={2}
                    smallChips
                  />
                </>
              )}
              <div tw="pt-2 pb-4">
                {resources.length !== 0 && (
                  <>
                    <p tw="text-gray-400 mb-0 text-sm">{t('need.resources.title')}</p>
                    <Chips
                      data={resources.map((resource) => ({
                        title: resource,
                        href: `/search/needs/?refinementList[ressources][0]=${resource}`,
                      }))}
                      overflowLink={`/need/${id}`}
                      type="resources"
                      showCount={2}
                      smallChips
                    />
                  </>
                )}
              </div>
              {/* show need publish AND/OR due date if one of them is set */}
              {(publishedDate || dueDate) && (
                <NeedDates publishedDate={publishedDate} dueDate={dueDate} status={status} />
              )}
              {status === 'completed' && !project && <BasicChip>{t('entity.info.status.completed')}</BasicChip>}
            </>
          )}
        </div>
        {cardFormat !== 'compact' && (
          // Stats
          <div tw="flex flex-col">
            {/* <div tw="flex justify-between items-center">
              {membersCount > 1 && (
                <div tw="flex justify-between">
                  {membersCount - 1} <TextWithPlural type="volunteer" count={membersCount - 1} />
                </div>
              )}
              {postsCount > 0 && (
                <div tw="flex justify-between">
                  {postsCount} <TextWithPlural type="post" count={postsCount} />
                </div>
              )}
            </div> */}
            <Hr tw="mt-5 pt-2" />
            {/* Footer (will help and share btns) */}
            <div tw="flex justify-around">
              <span tw="relative z-0 inline-flex rounded-md shadow-sm">
                <button
                  type="button"
                  tw="relative inline-flex items-center px-3 py-2 rounded-md border bg-white text-sm font-medium text-gray-700 focus:(z-10 outline-none ring-1 ring-primary border-primary) border-solid border-gray-300 hover:bg-gray-100"
                  onClick={() => router.push(`/need/${id}`)}
                >
                  <ThumbsUp size={18} title={t('need.help.willHelp')} />
                  <span tw="pl-1">{t('need.help.willHelp')}</span>
                </button>
              </span>
              <ShareBtns type="need" specialObjId={id} />
            </div>
          </div>
        )}
      </div>
      {(hasSaved !== undefined || source === 'algolia') && (
        <BtnStar itemType="needs" itemId={id} hasStarred={hasSaved} hasNoStat source={source} />
      )}
    </ObjectCard>
  );
};

const Hr = (props) => <div tw="-mx-4 border-0 border-t border-solid border-color[rgba(0, 0, 0, 0.07)]" {...props} />;

export default NeedCard;
