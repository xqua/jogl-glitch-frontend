import { Edit } from '@emotion-icons/boxicons-solid/Edit';
import { ExternalLinkAlt } from '@emotion-icons/fa-solid/ExternalLinkAlt';
import Link from 'next/link';
import React, { FC, useContext } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Feed from 'components/Feed/Feed';
import BtnFollow from 'components/Tools/BtnFollow';
import BtnJoin from 'components/Tools/BtnJoin';
import ShareBtns from 'components/Tools/ShareBtns/ShareBtns';
import { UserContext } from 'contexts/UserProvider';
import { NeedDisplayMode } from 'pages/need/[id]';
import BtnStar from '../Tools/BtnStar';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import NeedDates from './NeedDates';
import NeedDelete from './NeedDelete';
import NeedDocsManagement from './NeedDocsManagement';
import NeedWorkers from './NeedWorkers';
import { Need } from 'types';
import BasicChip from '../BasicChip/index';
import Chips from '../Chip/Chips';
import { useApi } from 'contexts/apiContext';
import { useModal } from 'contexts/modalContext';
import Grid from '../Grid';
import UserCard from '../User/UserCard';
import { Menu, MenuItem, MenuButton, MenuList } from '@reach/menu-button';
import H2 from '../primitives/H2';

interface Props {
  changeMode: (newMode: NeedDisplayMode) => void;
  need: Need;
}

const NeedContent: FC<Props> = ({ changeMode = () => console.warn('Missing changeMode function'), need = {} }) => {
  const user = useContext(UserContext);
  const { t } = useTranslation('common');
  const modal = useModal();
  const api = useApi();

  if (need === undefined) {
    // eslint-disable-next-line @rushstack/no-null
    return null;
  }

  const showAllWorkers = () => {
    api.get(`/api/needs/${need.id}/members`).then((res) => {
      // get all need members, then show modal showing them
      modal.showModal({
        children: (
          <Grid tw="py-0 sm:py-2 md:py-4">
            {res.data.members.map((member, i) => (
              <UserCard
                key={i}
                id={member.id}
                firstName={member.first_name}
                lastName={member.last_name}
                nickName={member.nickname}
                shortBio={member.short_bio}
                logoUrl={member.logo_url}
                hasFollowed={member.has_followed}
                mutualCount={member.stats.mutual_count}
                isCompact
              />
            ))}
          </Grid>
        ),
        maxWidth: '61rem',
      });
    });
  };

  return (
    <div className={`needContent need${need.id}`}>
      <div className="needContent--header" tw="flex justify-between">
        <H2 tw="mb-0">{need.title}</H2>
        <div tw="inline-flex">
          <div tw="height[fit-content]">
            <ShareBtns type="need" specialObjId={need.id} />
          </div>
          {(need.is_admin || need.is_owner) && (
            <Menu>
              <MenuButton tw="font-size[.8rem] height[fit-content] text-gray-700 pl-2 hover:bg-gray-300 rounded-sm">
                •••
              </MenuButton>
              <MenuList className="post-manage-dropdown">
                <MenuItem onSelect={() => changeMode('update')}>
                  <Edit size={25} title="Edit need" />

                  {t('feed.object.update')}
                </MenuItem>
                <MenuItem>
                  <NeedDelete needId={need.id} needProjId={need.project.id} />
                </MenuItem>
              </MenuList>
            </Menu>
          )}
        </div>
      </div>
      <div tw="flex flex-wrap mt-3 gap-x-4 gap-y-2">
        {!need.is_owner && (
          <BtnJoin
            itemId={need.id}
            itemType="needs"
            joinState={need.is_member}
            textJoin={t('need.help.willHelp')}
            textUnjoin={t('need.help.stopHelp')}
            count={need.members_count}
            showMembersModal={showAllWorkers}
          />
        )}
        <BtnFollow followState={need.has_followed} itemType="needs" itemId={need.id} count={need.followers_count} />
        <BtnStar itemType="projects" itemId={need.id} hasStarred={need.has_saved} count={need.saves_count} />
      </div>
      <div tw="mt-6">
        {need.status === 'completed' && (
          <BasicChip tw="mb-3 flex flex-col">{t('entity.info.status.completed')}</BasicChip>
        )}
        {t('needsPage.project')}
        <Link href={`/project/${need?.project.id}?t=needs`}>
          <a>
            {need?.project.title}{' '}
            <ExternalLinkAlt size={15} style={{ position: 'relative', top: '-2px' }} title="Go to project" />
          </a>
        </Link>
      </div>
      {/* show need publish AND/OR due date if one of them is set */}
      {(need.created_at || need.end_date) && (
        <NeedDates publishedDate={need.created_at} dueDate={need.end_date} status={need.status} />
      )}
      <span>
        {t('entity.card.by')}
        <Link href={`/user/${need.creator.id}`}>
          <a>{`${need.creator.first_name} ${need.creator.last_name}`}</a>
        </Link>
      </span>
      <div className="needContent--main">
        <InfoHtmlComponent content={need.content} />
        <hr />
        {need.skills.length !== 0 && (
          <div tw="flex flex-col">
            <div tw="flex flex-col">{t('need.skills.title')}</div>
            <Chips
              data={need.skills.map((skill) => ({
                title: skill,
                href: `/search/needs/?refinementList[skills][0]=${skill}`,
              }))}
              type="skills"
              showCount={5}
              overflowText="seeMore"
            />
          </div>
        )}
        {need.ressources.length !== 0 && (
          <div tw="flex flex-col mt-3">
            <div tw="flex flex-col">{t('need.resources.title')}</div>
            <Chips
              data={need.ressources.map((resource) => ({
                title: resource,
                href: `/search/needs/?refinementList[ressources][0]=${resource}`,
              }))}
              type="resources"
              showCount={5}
              overflowText="seeMore"
            />
          </div>
        )}
        <NeedWorkers need={need} mode="details" showAllWorkers={showAllWorkers} />
        {need.documents.length > 0 && <NeedDocsManagement need={need} mode="details" />}
        <div className="needFeed">
          {(user.isConnected || (!user.isConnected && need.posts_count > 0)) && <h6>{t('need.feed.title')}</h6>}
          <Feed feedId={need.feed_id} isAdmin={need.is_owner} allowPosting />
        </div>
      </div>
    </div>
  );
};

export default NeedContent;
