import { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import ReactTooltip from 'react-tooltip';
import Loading from 'components/Tools/Loading';
import NeedWorkersDelete from './NeedWorkersDelete';
import { Need } from 'types';

interface Props {
  need: Need;
  mode: 'update' | 'details';
  showAllWorkers: (e: any) => void | Promise<boolean>;
}

const NeedWorkers: FC<Props> = ({ need = undefined, mode, showAllWorkers }) => {
  const needMembers = need.users_sm;
  const { t } = useTranslation('common');

  return (
    <div className="needWorkers" tw="flex items-center">
      {t('need.card.working')}
      <span className="workers">
        <span className="imgList">
          {needMembers ? (
            needMembers.map((user, index) => {
              const logoUrl = !user.logo_url ? '/images/default/default-user.png' : user.logo_url;
              if (index < 6) {
                return (
                  <>
                    <Link href={`/user/${user.id}`} key={index}>
                      <a>
                        <img
                          className="userImg"
                          src={logoUrl}
                          alt={`${user.first_name} ${user.last_name}`}
                          data-tip={`${user.first_name} ${user.last_name}`}
                          data-for="need_worker"
                        />
                        <ReactTooltip id="need_worker" effect="solid" className="forceTooltipBg" />
                      </a>
                    </Link>
                    {mode === 'update' && need.is_owner && (
                      <NeedWorkersDelete worker={user} itemId={need.id} itemType="needs" />
                    )}
                    {index === 0 && needMembers.length > 1 && (
                      <div tw="self-center pl-2 pr-0 text-xl text-gray-400">|</div>
                    )}
                  </>
                );
              }
              return '';
            })
          ) : (
            <Loading />
          )}
          {need.members_count > 6 && (
            <div
              className="moreMembers"
              onClick={showAllWorkers}
              onKeyUp={(e) =>
                // execute only if it's the 'enter' key
                (e.which === 13 || e.keyCode === 13) && showAllWorkers()
              }
              tabIndex={0}
            >
              +{need.members_count - 6}
            </div>
          )}
        </span>
      </span>
    </div>
  );
};
export default NeedWorkers;
