import { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { Delete } from '@emotion-icons/material/Delete';
import { useApi } from 'contexts/apiContext';
import { useRouter } from 'next/router';

interface Props {
  needId: number;
  needProjId: number;
}

const NeedDelete: FC<Props> = ({ needId, needProjId }) => {
  const api = useApi();
  const router = useRouter();
  const { t } = useTranslation('common');

  const deleteNeed = () => {
    needId &&
      api
        .delete(`api/needs/${needId}`)
        .then(() => {
          // after deletion, show delete confirmation modal + redirect to project page
          alert('Deleted');
          router.push(`/project/${needProjId}?t=needs`);
        })
        .catch((error) => console.error(error));
  };
  return (
    <div onClick={deleteNeed} onKeyUp={(e) => (e.which === 13 || e.keyCode === 13) && deleteNeed()} tabIndex={0}>
      <Delete size={25} title="Delete need" />
      {t('feed.object.delete')}
    </div>
  );
};

export default NeedDelete;
