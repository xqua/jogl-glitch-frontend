import React, { forwardRef } from 'react';
import ReactTooltip from 'react-tooltip';
import tw from 'twin.macro';

type Ref = HTMLDivElement;

interface Props {
  type?: 'skills' | 'resources';
  maxLength?: number;
  children: string | string[];
  smallChips?: boolean;
  hasOpacity?: boolean;
}

// TODO: Explain why we use forwardRef here.
const Chip = forwardRef<Ref, Props>(
  ({ type = 'skill', children, maxLength = 1000, smallChips, hasOpacity = false }, ref) => {
    // add "..." to value if it's length is > than maxLength
    const value = children.length > maxLength ? `${children.slice(0, maxLength)}...` : children;

    return (
      <>
        <div
          ref={ref}
          // add tooltip prop to show chip tooltip, it its length is more than maxLength
          tw="flex flex-col justify-center items-center px-2 whitespace-nowrap rounded-full"
          css={[
            type === 'skills' ? tw`bg-[#E2EEF9] text-[#6e8aa4]` : tw`bg-[#efefef] text-[#7c8085]`,
            // type === 'skills' ? tw`text-[#5F5D5D]` : tw`text-[#5F5D5D]`,
            smallChips ? tw`h-[1.4rem]` : tw`h-[1.8rem]`,
            smallChips ? tw`font-medium` : tw`font-bold`,
            smallChips ? tw`font-size[.875rem]` : tw`text-sm`,
            hasOpacity ? tw`opacity-60` : tw`opacity-100`,
            hasOpacity ? tw`hover:opacity-100` : tw`hover:underline`,
          ]}
          {...(children.length > maxLength && { 'data-tip': children, 'data-for': `skillTooltip${children}` })}
        >
          {value}
        </div>
        <ReactTooltip
          id={`skillTooltip${children}`}
          effect="solid"
          role="tooltip"
          type="dark"
          className="forceTooltipBg"
        />
      </>
    );
  }
);

export default Chip;
