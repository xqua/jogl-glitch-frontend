import withTranslation from 'next-translate/withTranslation';
import { Component } from 'react';
import { applyPattern } from 'components/Tools/Forms/FormChecker';
import TitleInfo from 'components/Tools/TitleInfo';
import InfoMaxCharComponent from '../Info/InfoMaxCharComponent';
import tw from 'twin.macro';

class FormDefaultComponent extends Component {
  static get defaultProps() {
    return {
      beHide: false,
      content: '',
      errorCodeMessage: '',
      id: 'default',
      maxChar: undefined,
      isValid: undefined,
      mandatory: false,
      onChange: (value) => console.warn(`onChange doesn't exist to update ${value}`),
      pattern: undefined,
      placeholder: '',
      title: undefined,
      tooltipMessage: undefined,
      type: 'text',
      isDisabled: false,
      description: '',
      baseUrl: '',
    };
  }

  handleChange(event) {
    let { id, value } = event.target;
    const { content, pattern } = this.props;
    value = applyPattern(value, content, pattern);
    this.props.onChange(id, value);
  }

  render() {
    const {
      content,
      errorCodeMessage,
      id,
      isValid,
      mandatory,
      placeholder,
      prepend,
      title,
      tooltipMessage,
      type,
      minDate,
      maxDate,
      maxChar,
      isDisabled,
      description,
      baseUrl,
    } = this.props;
    const { t } = this.props.i18n;
    return (
      <div tw="w-full">
        {title && (
          <TitleInfo
            title={title}
            id={id}
            mandatory={mandatory}
            {...(tooltipMessage && { tooltipMessage: tooltipMessage })}
          />
        )}
        {description && <p tw="italic mb-0">{description}</p>}
        {prepend && baseUrl && <span tw="text-[.95rem]">{baseUrl.substr(8) + content}</span>}
        <div tw="mb-4 flex flex-col rounded-md shadow-sm">
          <div tw="flex">
            {prepend && (
              <span tw="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-100 text-gray-500">
                {prepend}
              </span>
            )}
            <input
              type={type}
              name={id}
              id={id}
              tw="flex-1 min-w-0 block w-full px-3 py-[0.4rem]"
              css={[
                prepend ? tw`rounded-r-md` : tw`rounded-md`,
                errorCodeMessage
                  ? tw`border-danger focus:(ring-danger border-danger)`
                  : tw`border-gray-300 focus:(ring-primary border-primary)`,
              ]}
              className={`form-control ${isValid !== undefined ? (isValid ? 'is-valid' : 'is-invalid') : ''}`}
              placeholder={placeholder}
              value={content === null ? '' : content}
              onChange={this.handleChange.bind(this)}
              // if input is date and has a minDate, disable selecting all days before minDate
              min={minDate && minDate}
              max={maxDate && maxDate}
              disabled={isDisabled}
            />
          </div>
          <div tw="inline-flex justify-between w-full">
            {errorCodeMessage ? <div tw="text-danger">{t(errorCodeMessage)}</div> : ''}
            <InfoMaxCharComponent content={content} maxChar={maxChar} />
          </div>
        </div>
      </div>
    );
  }
}

export default withTranslation(FormDefaultComponent, 'common');
