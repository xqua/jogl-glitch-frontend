import Router, { useRouter } from 'next/router';
import useTranslation from 'next-translate/useTranslation';
import { ReactNode, useEffect, useState } from 'react';
import Alert from 'components/Tools/Alert';
import { useApi } from 'contexts/apiContext';
import SpinLoader from 'components/Tools/SpinLoader';
import Button from '~/components/primitives/Button';
import useUserData from '~/hooks/useUserData';

const FormChangePwd = () => {
  const [password, setPassword] = useState('');
  const [passwordConfirmation, setPasswordConfirmation] = useState('');
  const [error, setError] = useState('');
  const [fireRedirect, setFireRedirect] = useState(false);
  const [token, setToken] = useState('');
  const [sending, setSending] = useState(false);
  const [success, setSuccess] = useState(false);
  const [errorMessage, setErrorMessage] = useState<string | ReactNode>(error);
  const { t } = useTranslation('common');
  const router = useRouter();
  const { userData } = useUserData();
  const api = useApi();

  const checkPassword = (pwd, pwdConfirm) => {
    if (pwd !== pwdConfirm) {
      setError('err-4001');
      return false;
    }
    if (pwd.length < 8) {
      setError('err-4002');
      return false;
    }
    return true;
  };

  // Get user token if user is connected
  useEffect(() => {
    userData && api.get('/api/auth/password/token').then((res) => setToken(res.data));
  }, []);

  // If we get token from the url param, set it
  useEffect(() => {
    setToken(router.query.token);
  }, [router.query.token]);

  const handleChange = (e) => {
    setError('');
    switch (e.target.name) {
      case 'password':
        setPassword(e.target.value);
        break;
      case 'password_confirmation':
        setPasswordConfirmation(e.target.value);
        break;
      default:
        console.error(`The input with name=${e.target.name} is not handled`);
        break;
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const headers = {
      'access-token': router.query['access-token'],
      client: router.query.client,
      uid: router.query.uid,
    };
    if (checkPassword(password, passwordConfirmation)) {
      setError('');
      setSending(true);
      api
        .post(
          `/api/auth/password/update?reset_password_token=${token}`,
          { password, password_confirmation: passwordConfirmation },
          { headers }
        )
        .then(() => {
          setSending(false);
          setSuccess(true);
          setTimeout(() => {
            setFireRedirect(true);
          }, 3500);
        })
        .catch((err) => {
          setError(err.response.data.error || 'Invalid token');
          setSending(false);
        });
    }
  };

  useEffect(() => {
    if (fireRedirect) {
      Router.push('/signin');
    }
  }, [fireRedirect]);

  useEffect(() => {
    setErrorMessage(error.includes('err-') ? t(error) : error);
  }, [error]);

  return (
    <form onSubmit={handleSubmit} className="newPwdForm">
      <div tw="mb-4">
        <label className="form-check-label" htmlFor="password">
          {t('auth.newPwd.pwd')}
        </label>
        <input
          type="password"
          name="password"
          id="password"
          className="form-control styledInputAuth"
          placeholder={t('auth.newPwd.pwd_placeholder')}
          onChange={handleChange}
        />
      </div>
      <div tw="mb-4">
        <label className="form-check-label" htmlFor="password_confirmation">
          {t('auth.newPwd.pwdConfirm')}
        </label>
        <input
          type="password"
          name="password_confirmation"
          id="password_confirmation"
          className="form-control styledInputAuth"
          placeholder={t('auth.newPwd.pwd_placeholder')}
          onChange={handleChange}
        />
      </div>
      {error !== '' && <Alert type="danger" message={errorMessage} />}
      {success && <Alert type="success" message={t('info-4001')} />}

      <Button disabled={!!sending} type="submit" tw="flex">
        {sending && <SpinLoader />}
        {t('auth.newPwd.btnConfirm')}
      </Button>
    </form>
  );
};

export default FormChangePwd;
