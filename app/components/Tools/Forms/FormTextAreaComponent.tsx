import withTranslation from 'next-translate/withTranslation';
import { Component } from 'react';
import InfoMaxCharComponent from 'components/Tools/Info/InfoMaxCharComponent';
import TitleInfo from 'components/Tools/TitleInfo';
import tw from 'twin.macro';

class FormTextAreaComponent extends Component {
  static get defaultProps() {
    return {
      errorCodeMessage: '',
      id: 'default',
      isValid: undefined,
      onChange: (value) => console.warn(`onChange doesn't exist to update ${value}`),
      placeholder: '',
      maxChar: undefined,
      mandatory: false,
      title: 'Title',
      supportLinks: false,
    };
  }

  handleChange(event) {
    this.props.onChange(event.target.id, event.target.value);
  }

  render() {
    const {
      content,
      errorCodeMessage,
      id,
      isValid,
      maxChar,
      mandatory,
      placeholder,
      title,
      rows = 5,
      supportLinks = false,
    } = this.props;
    const { t } = this.props.i18n;

    return (
      <div tw="mb-4">
        <div tw="inline-flex flex-wrap">
          <div tw="mr-2">
            <TitleInfo title={title} mandatory={mandatory} />
          </div>
          <div tw="italic">
            ({supportLinks && `${t('form.supportLinks')}, `}
            {maxChar || '340'} {t('form.maxChar')})
          </div>
        </div>
        <textarea
          className={`form-control ${isValid !== undefined ? (isValid ? 'is-valid' : 'is-invalid') : ''}`}
          id={id}
          name={id}
          placeholder={placeholder}
          value={content === null ? '' : content}
          rows={rows}
          tw="shadow-sm block w-full rounded-md sm:border"
          css={[
            errorCodeMessage
              ? tw`border-danger focus:(ring-danger border-danger)`
              : tw`border-gray-300 focus:(ring-primary border-primary)`,
          ]}
          onChange={this.handleChange.bind(this)}
        />
        <div css={[errorCodeMessage && tw`inline-flex justify-between w-full`]}>
          {errorCodeMessage && <div className="invalid-feedback">{t(errorCodeMessage)}</div>}
          <InfoMaxCharComponent content={content} maxChar={maxChar} />
        </div>
      </div>
    );
  }
}

export default withTranslation(FormTextAreaComponent, 'common');
