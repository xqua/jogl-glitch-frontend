import useTranslation from 'next-translate/useTranslation';
import AsyncCreatableSelect from 'react-select/async-creatable';
import algoliasearch from 'algoliasearch/lite';
import TitleInfo from 'components/Tools/TitleInfo';
// import "./FormSkillsComponent.scss";

const FormSkillsComponent = ({
  content = [],
  errorCodeMessage = '',
  id = 'skills',
  mandatory = false,
  type = 'default',
  onChange = (value, skills) => console.warn(`onChange doesn't exist to update ${value} of ${skills}`),
  placeholder = '',
  title = 'Title',
  tooltipMessage = '',
}) => {
  const { t } = useTranslation('common');

  const handleChange = (skillsList) => {
    // convert skillsList array into an array with just the skills labels
    const skills = skillsList?.map(({ label }) => label);
    // send the whole initial array (value, label, skillId) only for the moderator dashboard feature
    type === 'moderator' ? onChange(id, skillsList) : onChange(id, skills);
  };

  const appId = process.env.ALGOLIA_APP_ID;
  const token = process.env.ALGOLIA_TOKEN;

  const client = algoliasearch(appId, token);
  const index = client.initIndex('Skill'); // Skill
  let algoliaSkills = [];

  const fetchAlgolia = (resolve, value) => {
    index
      .search(value, {
        attributesToRetrieve: ['objectID', 'skill_name'], // skill_name & skill id
        hitsPerPage: 5,
      })
      .then((content) => {
        if (content) {
          algoliaSkills = content.hits;
        } else {
          algoliaSkills = [''];
        }
        algoliaSkills = algoliaSkills.map(({ objectID, skill_name }) => {
          return { value: skill_name, label: skill_name, skillId: objectID };
        }); // skill_name & skill id
        resolve(algoliaSkills);
      });
  };
  const loadOptions = (inputValue) =>
    new Promise((resolve) => {
      fetchAlgolia(resolve, inputValue);
    });

  const currentSkills = content.map((skill) => {
    if (skill.skill_name) {
      return { label: skill.skill_name, value: skill.skill_name };
    }
    return { label: skill, value: skill };
  });

  const customStyles = {
    loadingIndicator: (provided) => ({
      ...provided,
      display: 'none',
    }),
  };

  return (
    <div className="formSkills">
      <TitleInfo title={title} mandatory={mandatory} tooltipMessage={tooltipMessage} />

      <div className="content skill-container" id="skills">
        <label tw="text-gray-400" className="form-check-label" htmlFor={`show${id}`}>
          {t(`help.skills.${type}`)}
        </label>
        <AsyncCreatableSelect
          name={id}
          cacheOptions
          isMulti
          defaultValue={currentSkills && currentSkills}
          defaultOptions={false}
          components={{ DropdownIndicator: null, IndicatorSeparator: null }}
          formatCreateLabel={(inputValue) => `${t('general.add')} "${inputValue}"`}
          blurInputOnSelect={false} // force keeping focus when selecting option (for mobile)
          noOptionsMessage={() => null}
          loadOptions={loadOptions}
          onChange={handleChange}
          placeholder={placeholder}
          // allowCreateWhileLoading={true}
          styles={customStyles}
          // getOptionValue={option => option['id']}
        />
        {errorCodeMessage && <div className="invalid-feedback">{t(errorCodeMessage)}</div>}
      </div>
    </div>
  );
};

export default FormSkillsComponent;
