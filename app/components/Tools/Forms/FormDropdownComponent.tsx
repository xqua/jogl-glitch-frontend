import TitleInfo from 'components/Tools/TitleInfo';
import useTranslation from 'next-translate/useTranslation';
import Select from 'react-select';
import { FC, useEffect, useRef } from 'react';

interface Props {
  id: number;
  title: any;
  content: string;
  options: any[];
  onChange?: (id: number, value: string | number) => void;
  mandatory?: boolean;
  type?: string;
  warningMsg?: string;
  errorCodeMessage?: string;
  placeholder?: string;
  isSearchable?: boolean;
  tooltipMessage?: string;
}

const FormDropdownComponent: FC<Props> = ({
  id,
  title,
  content = '',
  options = [],
  onChange = (id, value) => console.warn(`onChange doesn't exist to update ${value} of ${id}`),
  mandatory = false,
  type = '',
  warningMsg,
  errorCodeMessage,
  placeholder,
  isSearchable = false,
  tooltipMessage,
}) => {
  const { t } = useTranslation('common');
  const handleChange = (key, content) => {
    onChange(content.name, key.value);
  };
  let selectRef = useRef();

  const optionTranslation = (option) =>
    id === 'name' || id === 'birth_date' // if id of form is name or birth_date, just display option
      ? option
      : id === 'gender' // if dropdown is a user category, show them
      ? t(`user.profile.edit.gender.${option}`)
      : id === 'category' // if dropdown is a user category, show them
      ? t(`user.profile.edit.select.${option}`)
      : type === 'challenge' // if dropdown comes from challenge, display special challenge status translation
      ? t(`challenge.info.status_${option}`)
      : id === 'maturity' // if dropdown comes from project maturity field
      ? t(`project.maturity.${option}`)
      : id === 'space_type' // if dropdown comes from space type field
      ? t(`space.type.${option}`)
      : // else display default status translation
        t(`entity.info.status.${option}`);

  const optionsList = options.map((option) => {
    return {
      value: option,
      label: optionTranslation(option),
    };
  });
  const customStyles = {
    option: (provided) => ({
      ...provided,
      padding: '6px 10px',
    }),
  };

  // to not soft select first element of list, use workaround mentioned in this comment,
  // waiting for PR to be merged: https://github.com/JedWatson/react-select/pull/4080#issuecomment-781565207
  useEffect(() => {
    if (selectRef) {
      selectRef.select.getNextFocusedOption = () => null;
    }
  }, [selectRef, selectRef.select]);

  return (
    <div className="formDropdown">
      <TitleInfo title={title} mandatory={mandatory} tooltipMessage={tooltipMessage} />
      <div className="content">
        {warningMsg && content === 'draft' && (
          /* display warning message if applicable */
          <span tw="text-red-500 italic">{warningMsg}</span>
        )}
        <Select
          name={id}
          id={id}
          options={optionsList}
          styles={customStyles}
          isSearchable={isSearchable}
          menuShouldScrollIntoView={true} // force scroll into view
          placeholder={placeholder || t('user.profile.edit.select.default')}
          defaultValue={content && { value: content, label: optionTranslation(content) }}
          noOptionsMessage={() => null}
          ref={(ref) => (selectRef = ref)}
          onChange={handleChange}
        />
      </div>
      {errorCodeMessage && (
        <div className="invalid-feedback" style={{ display: 'block' }}>
          {t(errorCodeMessage)}
        </div>
      )}
    </div>
  );
};
export default FormDropdownComponent;
