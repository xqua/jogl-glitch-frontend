import { FC, ReactNode } from 'react';
import tw from 'twin.macro';

interface Props {
  message: string | ReactNode;
  type: string;
}

const Alert: FC<Props> = ({ message, type }) => (
  <div
    tw="rounded-md p-4 mb-4"
    role="alert"
    css={[
      type === 'success'
        ? tw`bg-green-100`
        : type === 'danger'
        ? tw`bg-red-100`
        : type === 'warning'
        ? tw`bg-yellow-100`
        : tw`bg-blue-100`,
    ]}
  >
    <span tw="text-gray-700">{message}</span>
  </div>
);

export default Alert;
