import Axios from 'axios';
import React, { FC, useEffect, useState } from 'react';
import styled from 'utils/styled';
import { PatchCheck, PatchCheckFill } from '@emotion-icons/bootstrap';
import { EmotionIconBase } from '@emotion-icons/emotion-icon';
import { useApi } from 'contexts/apiContext';
import { ItemType } from 'types';
import ReactTooltip from 'react-tooltip';
import useUser from 'hooks/useUser';
import SpinLoader from './SpinLoader';
import { logEventToGA } from 'utils/analytics';

interface Props {
  reviewState: boolean;
  itemId: number;
  itemType: ItemType;
  refresh?: () => void;
}

const BookmarkStyleWrapper = styled.div`
  ${EmotionIconBase} {
    opacity: 0.8;
    cursor: pointer;
    color: #2987cd;
    width: 22px;
    height: 22px;
  }
`;

const reviewStateIcon = {
  unreviewd: <PatchCheck title="Unreview" />,
  reviewd: <PatchCheckFill title="Review" />,
};

const BtnReview: FC<Props> = ({ reviewState: propsReviewState = false, itemId, itemType, refresh }) => {
  const [reviewState, setReviewState] = useState(propsReviewState);
  const [action, setAction] = useState<'review' | 'unreview'>(propsReviewState ? 'unreview' : 'review');
  const [icon, setIcon] = useState<'reviewd' | 'unreviewd'>(reviewState ? 'reviewd' : 'unreviewd');
  const [sending, setSending] = useState(false);
  const api = useApi();
  const { user } = useUser();
  useEffect(() => {
    const axiosSource = Axios.CancelToken.source();
    // This will prevent to setReviewState on unmounted BtnReview
    return () => axiosSource.cancel();
  }, [api, itemId, itemType, user]);

  useEffect(() => {
    setAction(reviewState ? 'unreview' : 'review');
  }, [reviewState]);

  const changeStateReview = (e) => {
    if (itemId && itemType && ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which)) {
      // if function is launched via keypress, execute only if it's the 'enter' key
      setSending(true);
      if (action === 'review') {
        api
          .put(`/api/${itemType}/${itemId}/review`)
          .then(() => {
            // send event to google analytics
            logEventToGA(action, 'Button', `[${user.id},${itemId},${itemType}]`, { userId: user.id, itemId, itemType });
            setReviewState(true);
            setSending(false);
          })
          .catch(() => setSending(false));
      } else {
        api.delete(`/api/${itemType}/${itemId}/review`).then(() => {
          setReviewState(false);
          setSending(false);
        });
      }
      refresh && refresh();
    }
  };
  return (
    <>
      {sending ? (
        <SpinLoader />
      ) : (
        <>
          <a
            tabIndex={0}
            onMouseEnter={() => setIcon(reviewState ? 'unreviewd' : 'reviewd')}
            onMouseLeave={() => setIcon(reviewState ? 'reviewd' : 'unreviewd')}
            onClick={changeStateReview}
            onKeyUp={changeStateReview}
            data-tip={action}
            data-for="review"
            aria-describedby={`review ${itemType}`}
          >
            <BookmarkStyleWrapper>{reviewStateIcon[icon]}</BookmarkStyleWrapper>
          </a>
          <ReactTooltip id="review" delayHide={300} effect="solid" role="tooltip" />
        </>
      )}
    </>
  );
};

export default BtnReview;
