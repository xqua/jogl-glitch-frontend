import React, { FC, FormEvent, useEffect, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { Move } from '@emotion-icons/boxicons-regular';
import tw, { theme } from 'twin.macro';
import { ReactSortable } from 'react-sortablejs';
import Button from 'components/primitives/Button';
import H2 from 'components/primitives/H2';
import { useApi } from 'contexts/apiContext';
import useGet from 'hooks/useGet';
import { Faq } from 'types';
import Card from 'components/Cards/Card';
import FormDefaultComponent from './Forms/FormDefaultComponent';
import FormWysiwygComponent from './Forms/FormWysiwygComponent';
import Loading from './Loading';
import Alert from 'components/Tools/Alert';
import SpinLoader from './SpinLoader';
import FormTextAreaComponent from './Forms/FormTextAreaComponent';

const ManageFaq = ({ itemType, itemId, isQuestion = false }) => {
  const { t } = useTranslation('common');
  const { data: faqList, mutate: faqListMutate } = useGet<{ documents: Faq[] }>(`/api/${itemType}/${itemId}/faq`);
  const [showFaqCreate, setShowFaqCreate] = useState(false);
  const addFaq = () => {
    setShowFaqCreate(!showFaqCreate);
  };

  const api = useApi();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [requestFailed, setRequestFailed] = useState<boolean>(false);
  const [faqListState, setFaqListState] = useState<Faq[]>();
  const [isDnDEvent, setIsDnDEvent] = useState<boolean>(false);

  const areFaqsEqual = (arr1, arr2) => {
    if (!arr1 || !arr2) {
      return false;
    }

    if (arr1.length !== arr2.length) {
      return false;
    }

    for (let i = 0; i < arr1.length; i++) {
      if (arr1[i].id !== arr2[i].id || arr1[i].title !== arr2[i].title || arr1[i].content !== arr2[i].content) {
        return false;
      }
    }
    return true;
  };

  const updateFaqPosition = async (faqs, isDnDEvent) => {
    if (!isDnDEvent) setIsLoading(true);

    Promise.all<Faq>(
      faqs.map(async (faq, i) => {
        if (faq.id && faq.position !== i) {
          const updatedFaq = { faq: { ...faq, position: i } };
          const response = await api.patch(`/api/${itemType}/${itemId}/faq/${faq.id}`, updatedFaq);
          return response.data.documents.find(({ id }) => id === faq.id);
        } else {
          return faq;
        }
      })
    )
      .then((res) => {
        setRequestFailed(false);
        if (!isDnDEvent) {
          setFaqListState(res);
          setIsLoading(false);
        }
      })
      .catch((err) => {
        console.error(err);
        setRequestFailed(true);
        setFaqListState(faqList?.documents);
        setIsLoading(false);
      });
  };

  useEffect(() => {
    if (faqListState && !areFaqsEqual(faqList?.documents, faqListState)) {
      setIsDnDEvent(true);
      updateFaqPosition(faqListState, true);
    }
  }, [faqListState]);

  useEffect(() => {
    if (faqList?.documents && !isDnDEvent && !areFaqsEqual(faqList?.documents, faqListState)) {
      updateFaqPosition(faqList?.documents, false);
    }
  }, [faqList?.documents]);

  return (
    <div tw="flex flex-col space-y-3">
      <H2>{!isQuestion ? t('faq.title') : 'Questions'}</H2>
      {requestFailed && <Alert type="danger" message={t('err-')} />}
      <Button onClick={addFaq}>{t('general.add')}</Button>
      {showFaqCreate && (
        <FaqFormCard
          mode="create"
          itemType={itemType}
          itemId={itemId}
          isQuestion={isQuestion}
          onCreate={(newFaq: Faq) => {
            faqListMutate({ data: { documents: [...faqList.documents, newFaq] } }, false);
            setShowFaqCreate(false);
          }}
          isLoading={isLoading}
          setIsLoading={setIsLoading}
          setRequestFailed={setRequestFailed}
        />
      )}
      <div tw="flex flex-col space-y-3 pt-2">
        {!faqList?.documents ? (
          <Loading />
        ) : isLoading ? (
          <Loading />
        ) : (
          faqListState && (
            <ReactSortable list={faqListState} setList={setFaqListState}>
              {faqListState.map((item, i) => (
                <div key={item.id} tw="flex flex-col pt-3">
                  <FaqFormCard
                    value={item}
                    position={i + 1}
                    itemType={itemType}
                    itemId={itemId}
                    isQuestion={isQuestion}
                    mode="edit"
                    onUpdate={(updatedFaq: Faq) => {
                      faqListMutate(
                        {
                          data: {
                            documents: faqList.documents.map((faq) => {
                              if (faq.id === updatedFaq.id) return updatedFaq;
                              return faq;
                            }),
                          },
                        },
                        false
                      );
                    }}
                    onDelete={(faqId: number) => {
                      faqListMutate(
                        {
                          // removes the faq which has the same id as faqId
                          data: {
                            documents: faqList.documents.filter((faq) => faq.id !== faqId),
                          },
                        },
                        false
                      );
                    }}
                    isLoading={isLoading}
                    setIsLoading={setIsLoading}
                    setRequestFailed={setRequestFailed}
                  />
                </div>
              ))}
            </ReactSortable>
          )
        )}
      </div>
    </div>
  );
};
interface IFaqFormCard {
  value?: Faq;
  position: number;
  mode: 'create' | 'edit';
  itemType: string;
  itemId: number;
  onCreate?: (newFaq: Faq) => void;
  onUpdate?: (updatedFaq: Faq) => void;
  onDelete?: (faqId: number) => void;
  isLoading: boolean;
  setIsLoading: (state: boolean) => void;
  setRequestFailed: (state: boolean) => void;
  isQuestion: boolean;
}

const FaqFormCard: FC<IFaqFormCard> = ({
  mode,
  position,
  itemType,
  itemId,
  isQuestion,
  value = { title: '', content: isQuestion ? '‎' : '', id: undefined, position: undefined },
  onCreate,
  onUpdate,
  onDelete,
  isLoading,
  setIsLoading,
  setRequestFailed,
}) => {
  const api = useApi();
  const { t } = useTranslation('common');
  const [showSubmitBtn, setShowSubmitBtn] = useState(false);
  const [faq, setFaq] = useState<{ faq: Faq }>({
    faq: {
      id: value.id,
      title: value.title,
      content: value.content,
      position: value.position,
    },
  });
  // handle every changes to the faq
  const handleChange: (key: number, content: string) => void = (key, content) => {
    setFaq((prevFaq) => ({ faq: { ...prevFaq.faq, [key]: content } }));
    setShowSubmitBtn(content !== faq.faq.title || content !== faq.faq.content);
  };
  // handle when submitting (update, or create) a faq
  const handleSubmit: (event: FormEvent<HTMLFormElement>) => void = (e) => {
    e.preventDefault();
    if (mode === 'edit') {
      setIsLoading(true);
      api
        .patch(`/api/${itemType}/${itemId}/faq/${faq.faq.id}`, faq)
        .then((_res) => {
          setRequestFailed(false);
          setIsLoading(false);
          onUpdate(faq.faq); // call onUpdate function
          setShowSubmitBtn(false); // hide update button
        })
        .catch((err) => {
          console.error(err);
          setRequestFailed(true);
          setIsLoading(false);
        });
    }
    if (mode === 'create') {
      setIsLoading(true);
      api
        .post(`/api/${itemType}/${itemId}/faq`, faq)
        .then((_res) => {
          setRequestFailed(false);
          setIsLoading(false);
          onCreate(faq.faq); // call onCreate function
          setFaq({
            faq: {
              title: '',
              content: '',
              id: undefined,
              position: undefined,
            },
          }); // reset faq fields
        })
        .catch((err) => {
          console.error(err);
          setRequestFailed(true);
          setIsLoading(false);
        });
    }
  };
  // handle when deleting on faq
  const handleDelete = () => {
    setIsLoading(true);
    api
      .delete(`/api/${itemType}/${itemId}/faq/${faq.faq.id}`)
      .then((_res) => {
        setRequestFailed(false);
        setIsLoading(false);
        onDelete(faq.faq.id); // call onDelete function
      })
      .catch((err) => {
        console.error(err);
        setRequestFailed(true);
        setIsLoading(false);
      });
  };

  const tranlationId = mode === 'create' ? 'entity.form.btnCreate' : 'entity.form.btnUpdate';

  return (
    <Card tw="pt-2">
      <div tw="flex flex-col justify-between flex-row">
        {mode !== 'create' && (
          <>
            <p tw="mb-0 rounded-full w-8 h-8 bg-gray-300 text-xl flex items-center justify-center">{position}</p>
            <Move size={25} title="Drag and drop the FAQ" color={theme`colors.secondary`} tw="cursor-move" />
          </>
        )}
      </div>
      <form onSubmit={handleSubmit}>
        <div tw="flex flex-col justify-between md:flex-row">
          <div tw="flex flex-col space-y-4 w-full pr-0 md:pr-5">
            <div tw="flex flex-col mt-0!">
              <FormDefaultComponent
                id="title"
                content={faq.faq.title}
                title={`${t('faq.question')}${isQuestion ? '*' : ''}`}
                placeholder={t('faq.question')}
                onChange={handleChange}
              />
            </div>
            {!isQuestion ? (
              <div tw="flex flex-col">
                <FormWysiwygComponent
                  id="content"
                  content={faq.faq.content}
                  title={t('faq.answer')}
                  placeholder={t('faq.answer')}
                  onChange={handleChange}
                  show
                />
              </div>
            ) : (
              <div tw="flex flex-col" css={isQuestion && tw`mt-0!`}>
                <FormTextAreaComponent
                  content={faq.faq.content}
                  id="content"
                  maxChar={340}
                  onChange={handleChange}
                  rows={2}
                  title={t('faq.additionalDetails')}
                  placeholder={t('faq.additionalDetails')}
                />
              </div>
            )}
          </div>
          <div tw="flex flex-col justify-center items-end">
            <div tw="flex md:flex-col justify-between space-x-2 md:(space-y-2 space-x-0) mt-auto! mb-auto!">
              {(showSubmitBtn || mode === 'create') && (
                <Button type="submit" width="100%" disabled={isLoading}>
                  <>
                    {isLoading && <SpinLoader />}
                    {t(tranlationId)}
                  </>
                </Button>
              )}
              {mode !== 'create' && (
                <Button type="button" btnType="danger" onClick={handleDelete} width="100%">
                  {t('feed.object.delete')}
                </Button>
              )}
            </div>
          </div>
        </div>
      </form>
    </Card>
  );
};

export default ManageFaq;
