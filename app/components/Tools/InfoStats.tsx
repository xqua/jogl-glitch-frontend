import { FC } from 'react';
import A from '../primitives/A';
import { useRouter } from 'next/router';
import { TextWithPlural } from 'utils/managePlurals';

interface Props {
  type: string;
  object: string;
  tab: string;
  count: number;
}

const InfoStats: FC<Props> = ({ object, type, tab, count }) => {
  const router = useRouter();
  const objectId = object === 'project' || object === 'user' ? router.query.id : router.query.short_title;

  return (
    <div tw="flex flex-col justify-between px-2 py-1 text-center hover:opacity-80">
      <A href={`/${object}/${objectId}?tab=${tab}`} shallow noStyle scroll={false}>
        <div tw="text-2xl font-bold">{count}</div>
        <TextWithPlural type={type} count={count} />
      </A>
    </div>
  );
};

export default InfoStats;
