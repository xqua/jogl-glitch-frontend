import useGet from 'hooks/useGet';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from 'contexts/apiContext';
import { confAlert } from 'utils/utils';
import { Post } from 'types';
import FormToggleComponent from 'components/Tools/Forms/FormToggleComponent';
import { FC, useState } from 'react';

const AllowPostingToAllToggle: FC<{ feedId: number }> = ({ feedId }) => {
  const { t } = useTranslation('common');
  const { data: dataFeed } = useGet<{
    allow_posting_to_all: boolean;
    posts: Post[];
  }>(`/api/feeds/${feedId}?items=1`);
  const [allowPosting, setAllowPosting] = useState(dataFeed?.[0]?.allow_posting_to_all);
  const api = useApi();

  const handleCheckedChange = () => {
    setAllowPosting(!allowPosting);
    api.patch(`/api/feeds/${feedId}`, { allowPostingToAll: !allowPosting }).then(() => {
      // show conf alert on api call success
      confAlert.fire({ icon: 'success', title: t('entity.form.confModal.saved') });
    });
  };

  return (
    <FormToggleComponent
      id="allow_posting"
      title={t('entity.form.allowPostingToAll.title')}
      warningMsg={t('entity.form.allowPostingToAll.warningMsg')}
      choice2={t('general.yes')}
      choice1={t('general.no')}
      isChecked={!allowPosting}
      onChange={handleCheckedChange}
    />
  );
};

export default AllowPostingToAllToggle;
