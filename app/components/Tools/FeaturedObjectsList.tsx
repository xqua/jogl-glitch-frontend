import useGet from '~/hooks/useGet';
import Link from 'next/link';
import { Program, Space, Project, PeerReview } from '~/types';
import ObjectCard from '~/components/Cards/ObjectCard';
import Title from '~/components/primitives/Title';
import H2 from '~/components/primitives/H2';
import useTranslation from 'next-translate/useTranslation';

const FeaturedObjectsList = () => {
  const { t } = useTranslation('common');
  const prod = process.env.ADDRESS_FRONT === 'https://app.jogl.io';
  // const { data: epidemiumProgram } = useGet<Program>(`/api/programs/${!prod ? 2 : 11}`);
  // const { data: jogs } = useGet<Space>(`/api/spaces/${!prod ? 1 : 133}`);
  const { data: sopBio1 } = useGet<PeerReview>(`/api/peer_reviews/${!prod ? 1 : 4}`);
  const { data: afterIgem } = useGet<Space>(`/api/spaces/${!prod ? 1 : 4}`);
  const { data: africaAMR } = useGet<Space>(`/api/spaces/${!prod ? 1 : 39}`);
  const { data: synbioBrasil } = useGet<Space>(`/api/spaces/${!prod ? 1 : 3}`);
  const { data: covidProgram } = useGet<Program>(`/api/programs/${!prod ? 1 : 2}`);
  // const { data: friendzymes } = useGet<Space>(`/api/spaces/${!prod ? 1 : 47}`);
  const { data: basicRespirator } = useGet<Project>(`/api/projects/${!prod ? 24 : 212}`);
  // const { data: coronaDet } = useGet<Project>(`/api/projects/${!prod ? 1 : 181}`);
  // const { data: covidTest } = useGet<Project>(`/api/projects/${!prod ? 1 : 163}`);
  // hard-coded slides objects for the featured objects section
  const featuredSlides = [
    // { objectType: 'program', content: epidemiumProgram },
    // { objectType: 'space', content: jogs },
    { objectType: 'peer-review', content: sopBio1 },
    { objectType: 'space', content: afterIgem },
    { objectType: 'space', content: africaAMR },
    { objectType: 'space', content: synbioBrasil },
    { objectType: 'program', content: covidProgram },
    // { objectType: 'space', content: friendzymes },
    { objectType: 'project', content: basicRespirator },
    // { objectType: 'project', content: coronaDet },
    // { objectType: 'project', content: covidTest },
  ];
  const getObjectUrl = (objectType, objectId, shortTitle) => {
    if (objectType === 'project') return `/${objectType}/${objectId}/${shortTitle}`;
    else return `/${objectType}/${shortTitle}`;
  };
  return (
    <>
      <h4 tw="hidden md:block">{t('home.featured')}</h4>
      <div tw="flex flex-wrap justify-center gap-3">
        {featuredSlides?.map((slide, i) => (
          <ObjectCard
            // imgUrl={slide.content?.banner_url || `/images/default/default-${slide.objectType}.jpg`}
            href={getObjectUrl(slide.objectType, slide.content?.id, slide.content?.short_title)}
            hrefNewTab
            isFeaturedCarousel
            // chip={slide.objectType}
            key={i}
            width="295px"
          >
            <div tw="flex flex-wrap gap-x-3 items-center">
              <Link href={getObjectUrl(slide.objectType, slide.content?.id, slide.content?.short_title)} passHref>
                <Title>
                  <H2 tw="word-break[break-word] line-clamp-2 items-center">{slide.content?.title}</H2>
                </Title>
              </Link>
              <div tw="capitalize color[#4d4d4d] w-[fit-content] h-[fit-content] capitalize rounded bg-gray-200 bg-opacity-80 px-1">
                {slide.objectType !== 'peer-review' ? slide.objectType : 'Call for proposals'}
              </div>
            </div>
            <p tw="text-gray-600 text-sm my-1 line-clamp-4 md:line-clamp-2">
              {slide.content?.short_description || slide.content?.summary}
            </p>
          </ObjectCard>
        ))}
      </div>
    </>
  );
};

export default FeaturedObjectsList;
