import React, { FC, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import SpinLoader from './SpinLoader';
import { useApi } from 'contexts/apiContext';
import { useModal } from 'contexts/modalContext';
import Alert from 'components/Tools/Alert';
import Button from 'components/primitives/Button';
import P from 'components/primitives/P';
import { Delete } from '@emotion-icons/material';
import tw from 'twin.macro';

interface Props {
  userId: number;
}

// The present BtnDeleteUser tool so far only exists for JOGL moderators to delete an unappropriate user.

const BtnDeleteUser: FC<Props> = ({ userId }) => {
  const { showModal, closeModal } = useModal();
  const { t } = useTranslation('common');
  const [deleteState, setDeleteState] = useState(false);
  const [isSending, setIsSending] = useState(false);

  const UserDeleteModal = () => {
    const api = useApi();
    const [errors, setErrors] = useState();

    const errorMessage = errors?.includes('err-') ? t(errors) : errors;

    const deleteAccount = (event) => {
      event.preventDefault();
      setIsSending(true);
      // delete account forever without even archiving
      api
        .delete(`/api/users/${userId}`)
        .then(() => {
          closeModal();
          alert(`JOGL User with id ${userId} was permanently deleted.`);
          setDeleteState((prevState) => !prevState);
          setIsSending(false);
        })
        .catch((err) => {
          setErrors(err.toString());
          console.error(`Couldn't DELETE /api/users/:userId`, err);
          setIsSending(false);
        });
    };

    return (
      <>
        {errors && <Alert type="danger" message={errorMessage} />}
        <P tw="font-semibold mb-0 font-size[1.1rem]">Are you sure you want to delete this JOGL account?</P>
        <P tw="italic font-size[.9rem]">This is a permanent deletion.</P>
        <div tw="flex space-x-3 pt-3">
          <Button btnType="danger" onClick={deleteAccount} disabled={isSending}>
            {t('general.yes')}
          </Button>
          <Button onClick={closeModal}>{t('general.no')}</Button>
        </div>
      </>
    );
  };

  const text = deleteState ? 'Deleted' : 'Delete';
  return (
    <span tw="relative z-0 inline-flex shadow-sm rounded-md" id="deleteBtn">
      <button
        type="button"
        css={[
          tw`relative inline-flex items-center p-2 text-sm font-medium text-black bg-white border border-solid rounded-md hover:bg-gray-100 border-danger`,
          deleteState && tw`opacity-40`,
        ]}
        onClick={() => {
          showModal({
            children: <UserDeleteModal />,
            title: 'Delete JOGL account',
            maxWidth: '30rem',
          });
        }}
        disabled={isSending || deleteState}
      >
        {isSending ? <SpinLoader /> : <Delete size={22} title={text} css={[tw`text-black`]} />}
        {text}
      </button>
    </span>
  );
};

export default BtnDeleteUser;
