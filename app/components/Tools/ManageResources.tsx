import React, { FC, FormEvent, useEffect, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { Move } from '@emotion-icons/boxicons-regular';
import { theme } from 'twin.macro';
import { ReactSortable } from 'react-sortablejs';
import Button from 'components/primitives/Button';
import H2 from 'components/primitives/H2';
import { useApi } from 'contexts/apiContext';
import useGet from 'hooks/useGet';
import { Resource } from 'types';
import Card from 'components/Cards/Card';
import FormDefaultComponent from './Forms/FormDefaultComponent';
import FormWysiwygComponent from './Forms/FormWysiwygComponent';
import Loading from './Loading';
import Alert from 'components/Tools/Alert';

const ManageResources = ({ itemType, itemId }) => {
  const { t } = useTranslation('common');
  const { data: resourceList, mutate: resourceListMutate } = useGet<Resource[]>(
    `/api/${itemType}/${itemId}/resources`
  );
  const [showResourceCreate, setShowResourceCreate] = useState(false);
  const addResource = () => {
    setShowResourceCreate(!showResourceCreate);
  };

  const api = useApi();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [requestFailed, setRequestFailed] = useState<boolean>(false);
  const [resourceListState, setResourceListState] = useState<Resource[]>();
  const [isDnDEvent, setIsDnDEvent] = useState<boolean>(false);

  const areResourcesEqual = (arr1, arr2) => {
    if (!arr1 || !arr2) {
      return false;
    }

    if (arr1.length !== arr2.length) {
      return false;
    }

    for (let i = 0; i < arr1.length; i++) {
      if (
        arr1[i].id !== arr2[i].id ||
        arr1[i].title !== arr2[i].title ||
        arr1[i].content !== arr2[i].content
      ) {
        return false;
      }
    }
    return true;
  };

  const updateResourcePosition = async (resources, isDnDEvent) => {
    if (!isDnDEvent) setIsLoading(true);

    Promise.all<Resource>(
      resources.map(async (resource, i) => {
        if (resource.id && resource.position !== i) {
          const updatedResource = { resource: { ...resource, position: i } };
          const response = await api.patch(
            `/api/${itemType}/${itemId}/resources/${resource.id}`,
            updatedResource
          );
          return response.data.find(({ id }) => id === resource.id);
        } else {
          return resource;
        }
      })
    )
      .then((res) => {
        setRequestFailed(false);
        if (!isDnDEvent) {
          setResourceListState(res);
          setIsLoading(false);
        }
      })
      .catch((err) => {
        console.error(err);
        setRequestFailed(true);
        setResourceListState(resourceList);
        setIsLoading(false);
      });
  };

  useEffect(() => {
    if (
      resourceListState &&
      !areResourcesEqual(resourceList, resourceListState)
    ) {
      setIsDnDEvent(true);
      updateResourcePosition(resourceListState, true);
    }
  }, [resourceListState]);

  useEffect(() => {
    if (
      resourceList &&
      !isDnDEvent &&
      !areResourcesEqual(resourceList, resourceListState)
    ) {
      updateResourcePosition(resourceList, false);
    }
  }, [resourceList]);

  return (
    <div tw="flex flex-col space-y-3">
      <H2>{t('program.resources.title')}</H2>
      {requestFailed && <Alert type="danger" message={t('err-')} />}
      <Button onClick={addResource}>{t('general.add')}</Button>
      {showResourceCreate && (
        <ResourceFormCard
          mode="create"
          itemType={itemType}
          itemId={itemId}
          onCreate={(newResource: Resource) =>
            resourceListMutate({ data: [...resourceList, newResource] }, false)
          }
          setIsLoading={setIsLoading}
          setRequestFailed={setRequestFailed}
        />
      )}
      <div tw="flex flex-col space-y-3 pt-2">
        {!resourceList ? (
          <Loading />
        ) : isLoading ? (
          <Loading />
        ) : (
          resourceListState && (
            <ReactSortable
              list={resourceListState}
              setList={setResourceListState}
            >
              {resourceListState.map((value, i) => (
                <div key={value.id} tw="flex flex-col pt-3">
                  <p tw="pl-2 mb-0 text-3xl">{i + 1}</p>
                  <ResourceFormCard
                    value={value}
                    itemType={itemType}
                    itemId={itemId}
                    mode="edit"
                    onUpdate={(updatedResource: Resource) => {
                      resourceListMutate(
                        {
                          data: resourceList.map((resource) => {
                            if (resource.id === updatedResource.id)
                              return updatedResource;
                            return resource;
                          }),
                        },
                        false
                      );
                    }}
                    onDelete={(resourceId: number) => {
                      resourceListMutate(
                        {
                          // removes the resource which has the same id as resourceId
                          data: resourceList.filter(
                            (resource) => resource.id !== resourceId
                          ),
                        },
                        false
                      );
                    }}
                    setIsLoading={setIsLoading}
                    setRequestFailed={setRequestFailed}
                  />
                </div>
              ))}
            </ReactSortable>
          )
        )}
      </div>
    </div>
  );
};
interface IResourceFormCard {
  value?: Resource;
  mode: 'create' | 'edit';
  itemType: string;
  itemId: number;
  onCreate?: (newResource: Resource) => void;
  onUpdate?: (updatedResource: Resource) => void;
  onDelete?: (resourceId: number) => void;
  setIsLoading: (state: boolean) => void;
  setRequestFailed: (state: boolean) => void;
}
const ResourceFormCard: FC<IResourceFormCard> = ({
  value = { title: '', content: '', id: undefined, position: undefined },
  mode,
  itemType,
  itemId,
  onCreate,
  onUpdate,
  onDelete,
  setIsLoading,
  setRequestFailed,
}) => {
  const api = useApi();
  const { t } = useTranslation('common');
  const [showSubmitBtn, setShowSubmitBtn] = useState(false);
  const [resource, setResource] = useState<{ resource: Resource }>({
    resource: {
      id: value.id,
      title: value.title,
      content: value.content,
      position: value.position,
    },
  });
  // handle every changes to the resource
  const handleChange: (key: number, content: string) => void = (
    key,
    content
  ) => {
    setResource((prevResource) => ({
      resource: { ...prevResource.resource, [key]: content },
    }));
    setShowSubmitBtn(
      content !== resource.resource.title ||
        content !== resource.resource.content
    );
  };
  // handle when submitting (update, or create), an resource
  const handleSubmit: (event: FormEvent<HTMLFormElement>) => void = (e) => {
    e.preventDefault();
    if (mode === 'edit') {
      setIsLoading(true);
      api
        .patch(
          `/api/${itemType}/${itemId}/resources/${resource.resource.id}`,
          resource
        )
        .then((_res) => {
          setRequestFailed(false);
          setIsLoading(false);
          onUpdate(resource.resource); // call onUpdate function
          setShowSubmitBtn(false); // hide update button
        })
        .catch((err) => {
          console.error(err);
          setRequestFailed(true);
          setIsLoading(false);
        });
    }
    if (mode === 'create') {
      setIsLoading(true);
      api
        .post(`/api/${itemType}/${itemId}/resources`, resource)
        .then((_res) => {
          setRequestFailed(false);
          setIsLoading(false);
          onCreate(resource.resource); // call onCreate function
          setResource({
            resource: {
              title: '',
              content: '',
              id: undefined,
              position: undefined,
            },
          }); // reset resource fields
        })
        .catch((err) => {
          console.error(err);
          setRequestFailed(true);
          setIsLoading(false);
        });
    }
  };
  // handle when deleting on resource
  const handleDelete = () => {
    setIsLoading(true);
    api
      .delete(`/api/${itemType}/${itemId}/resources/${resource.resource.id}`)
      .then((_res) => {
        setRequestFailed(false);
        setIsLoading(false);
        onDelete(resource.resource.id); // call onDelete function
      })
      .catch((err) => {
        console.error(err);
        setRequestFailed(true);
        setIsLoading(false);
      });
  };
  const translationId =
    mode === 'create' ? 'entity.form.btnCreate' : 'entity.form.btnUpdate';

  return (
    <Card>
      <form onSubmit={handleSubmit}>
        <div tw="flex flex-col justify-between md:flex-row">
          <div tw="flex flex-col space-y-4 w-full pr-0 md:pr-5">
            <Move
              size={25}
              title="Drag and drop the resource"
              color={theme`colors.secondary`}
              tw="cursor-move self-end md:hidden"
            />
            <div tw="flex flex-col mt-0!">
              <FormDefaultComponent
                id="title"
                content={resource.resource.title}
                title={t('entity.info.title')}
                placeholder={t('entity.info.title')}
                onChange={handleChange}
              />
            </div>
            <div tw="flex flex-col">
              <FormWysiwygComponent
                id="content"
                content={resource.resource.content}
                title={t('program.resources.content')}
                placeholder={t('program.resources.content_placeholder')}
                onChange={handleChange}
                show
              />
            </div>
          </div>
          <div tw="flex flex-col justify-center space-y-2 items-end">
            {mode !== 'create' && (
              <Move
                size={25}
                title="Drag and drop the resource"
                color={theme`colors.secondary`}
                tw="hidden cursor-move md:block"
              />
            )}
            <div tw="flex flex-col justify-between space-y-2 mt-auto! mb-auto!">
              {(showSubmitBtn || mode === 'create') && (
                <Button type="submit" width="100%">
                  {t(translationId)}
                </Button>
              )}
              {mode !== 'create' && (
                <Button btnType="danger" onClick={handleDelete} width="100%">
                  {t('feed.object.delete')}
                </Button>
              )}
            </div>
          </div>
        </div>
      </form>
    </Card>
  );
};

export default ManageResources;
