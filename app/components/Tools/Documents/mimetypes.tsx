import {
  FileAudio,
  FileImage,
  FileVideo,
  FilePdf,
  FileAlt,
  FileCode,
  FileArchive,
  FileWord,
  FilePowerpoint,
  FileExcel,
  File,
} from '@emotion-icons/fa-regular';
import { EmotionIconBase } from '@emotion-icons/emotion-icon';
import styled from 'utils/styled';

export const IconStyleWrapper = styled.div`
  ${EmotionIconBase} {
    width: ${(props: { size: number }) => props.size}px;
    height: ${(props: { size: number }) => props.size}px;
  }
`;

const mapping = [
  // Images
  [<FileImage />, /^image\//],
  // Audio
  [<FileAudio />, /^audio\//],
  // Video
  [<FileVideo />, /^video\//],
  // Documents
  [<FilePdf />, 'application/pdf'],
  [<FileAlt />, 'text/plain'],
  [<FileCode />, ['text/html', 'text/javascript']],
  // Archives
  [
    <FileArchive />,
    [
      /^application\/x-(g?tar|xz|compress|bzip2|g?zip)$/,
      /^application\/x-(7z|rar|zip)-compressed$/,
      /^application\/(zip|gzip|tar)$/,
    ],
  ],
  // Word
  [
    <FileWord />,
    [
      /ms-?word/,
      'application/vnd.oasis.opendocument.text',
      'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    ],
  ],
  // Powerpoint
  [<FilePowerpoint />, [/ms-?powerpoint/, 'application/vnd.openxmlformats-officedocument.presentationml.presentation']],
  // Excel
  [<FileExcel />, [/ms-?excel/, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']],
  // Default, misc
  [<File />],
];

function match(mimetype, cond) {
  if (Array.isArray(cond)) {
    return cond.reduce((v, c) => v || match(mimetype, c), false);
  }
  if (cond instanceof RegExp) {
    return cond.test(mimetype);
  }
  if (cond === undefined) {
    return true;
  }
  return mimetype === cond;
}

const cache = {};

function resolve(mimetype) {
  if (cache[mimetype]) {
    return cache[mimetype];
  }

  for (let i = 0; i < mapping.length; i++) {
    if (match(mimetype, mapping[i][1])) {
      cache[mimetype] = mapping[i][0];
      return mapping[i][0];
    }
  }
}

function mimetype2fa(mimetype: any, size: number = 20) {
  let icon = resolve(mimetype);

  return <IconStyleWrapper size={size}>{icon}</IconStyleWrapper>;
}

export default mimetype2fa;
