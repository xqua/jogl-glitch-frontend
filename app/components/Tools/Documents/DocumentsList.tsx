import { FC } from 'react';
import { ItemType } from 'types';
import DocumentCard from './DocumentCard';
import DocumentCardFeed from './DocumentCardFeed';

interface Props {
  documents: any[];
  cardType: 'feed' | 'cards';
  postId?: number;
  itemType?: ItemType;
  itemId?: number;
  isAdmin?: boolean;
  isEditing?: boolean;
  showDeleteIcon?: boolean;
  refresh?: () => void;
}
const DocumentsList: FC<Props> = ({
  documents = [],
  isAdmin = false,
  cardType,
  itemId,
  isEditing = false,
  itemType,
  postId,
  showDeleteIcon = true,
  refresh,
}) => {
  if (cardType === 'feed' && documents.length !== 0)
    return (
      <div tw="inline-flex flex-wrap">
        {documents.map((document, index) => {
          return (
            <DocumentCardFeed document={document} key={index} isEditing={isEditing} postId={postId} refresh={refresh} />
          );
        })}
      </div>
    );
  return documents.map((document, index) => {
    return (
      <DocumentCard
        document={document}
        key={index}
        itemId={itemId}
        isAdmin={isAdmin}
        itemType={itemType}
        refresh={refresh}
        showDeleteIcon={showDeleteIcon}
      />
    );
  });
};

export default DocumentsList;
