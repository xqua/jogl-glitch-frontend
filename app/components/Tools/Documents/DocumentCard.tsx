import { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { Times } from '@emotion-icons/fa-solid/Times';
import mimetype2fa from './mimetypes';
import { useApi } from 'contexts/apiContext';
import ReactTooltip from 'react-tooltip';
import { ItemType } from 'types';
import styled from 'utils/styled';
import Card from '~/components/Cards/Card';

interface Props {
  document: { content_type: string; filename: string; url: string; id: number };
  isAdmin: boolean;
  itemId: number;
  itemType: ItemType;
  showDeleteIcon?: boolean;
  refresh: () => void;
}

const DeleteIcon = styled(Times)`
  font-size: 25px;
  position: absolute;
  top: -10px;
  right: -15px;
  color: red;
  &:hover {
    cursor: pointer;
    opacity: 0.7;
  }
`;

const DocumentCard: FC<Props> = ({
  document = {
    content_type: 'image/jpg',
    filename: 'MyImage.jpg',
    url: 'https://something.com/image.jpg',
  },
  itemId,
  itemType,
  isAdmin,
  showDeleteIcon,
  refresh,
}) => {
  const api = useApi();
  const { t } = useTranslation('common');

  const deleteDoc = () => {
    api
      .delete(`/api/${itemType}/${itemId}/documents/${document.id}`)
      .then((res) => {
        if (res.status === 200) {
          refresh();
        }
      })
      .catch((err) => {
        console.error(`Couldn't DELETE ${itemType} with itemId=${itemId}`, err);
      });
  };

  return (
    <Card tw="text-center width[160px] m-4 ml-0 relative">
      <h6 tw="break-words">{document.filename}</h6>
      {mimetype2fa(document.content_type, 29)}
      <a
        target="_blank"
        rel="noopener noreferrer"
        href={document.url}
        download={document.url}
        tw="mt-3 bg-gray-200 py-2 text-gray-700 rounded hover:(opacity-90 text-gray-700 no-underline)"
      >
        {t('document.download')}
      </a>
      {/* restrict deletion of documents only to admin */}
      {showDeleteIcon && isAdmin && (
        <>
          <DeleteIcon
            onClick={deleteDoc}
            onKeyUp={(e) => (e.which === 13 || e.keyCode === 13) && deleteDoc()}
            data-tip={t('general.remove')}
            data-for="document_delete"
            // show/hide tooltip on element focus/blur
            onFocus={(e) => ReactTooltip.show(e.target)}
            onBlur={(e) => ReactTooltip.hide(e.target)}
            tabIndex={0}
            size={25}
            title="Delete file"
          />
          <ReactTooltip id="document_delete" effect="solid" />
        </>
      )}
    </Card>
  );
};
export default DocumentCard;
