import { FC } from 'react';
import { QuestionCircle } from '@emotion-icons/fa-solid/QuestionCircle';
import ReactTooltip from 'react-tooltip';
import tw from 'twin.macro';

interface Props {
  title: string;
  id?: string;
  mandatory?: boolean;
  tooltipMessage?: string;
  isFormSubpartTitle?: boolean;
}

const TitleInfo: FC<Props> = ({ title, id, mandatory, tooltipMessage, isFormSubpartTitle = false }) => (
  <label htmlFor={id} tw="flex items-center" className="titleInfo">
    <span
      css={[
        tw`text-[1rem] font-bold text-gray-700 underline sm:no-underline`,
        isFormSubpartTitle && tw`mb-1 text-lg no-underline text-primary`,
      ]}
    >
      {title}
    </span>
    {mandatory && <div tw="text-danger contents">&nbsp;*</div>}
    {tooltipMessage && (
      <div tw="pl-2">
        <QuestionCircle
          size={16}
          title="More information"
          data-tip={tooltipMessage}
          data-for="bannerTooltip"
          tabIndex={0}
          onFocus={(e) => ReactTooltip.show(e.target)}
          onBlur={(e) => ReactTooltip.hide(e.target)}
        />
        <ReactTooltip
          id="bannerTooltip"
          effect="solid"
          type="dark"
          className="solid-tooltip"
          overridePosition={({ left, top }, _e, _t, node) => {
            return {
              top,
              left: typeof node === 'string' ? left : Math.max(left, 20),
            };
          }}
        />
      </div>
    )}
  </label>
);

export default TitleInfo;
