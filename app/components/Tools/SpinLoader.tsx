import { FC } from 'react';
import { LoaderAlt } from '@emotion-icons/boxicons-regular';

interface Props {
  size?: string;
}
const SpinLoader: FC<Props> = ({ size = '18' }) => (
  <LoaderAlt size={size} tw="animate-spin mr-2" title="Loading spinner" />
);
export default SpinLoader;
