import Select from 'react-select';
import { useState } from 'react';
import { useApi } from 'contexts/apiContext';
import useTranslation from 'next-translate/useTranslation';
import SpinLoader from './SpinLoader';

const DropdownRole = ({
  actualRole = 'member',
  callBack = () => {
    console.warn('Missing callback function');
  },
  onRoleChanged = () => {
    console.warn('Missing function');
  },
  itemId = undefined,
  itemType = undefined,
  listRole = [],
  member = undefined,
  isDisabled = false,
  setNewRole,
  showPlaceholder = false,
}) => {
  const [sending, setSending] = useState(false);
  const api = useApi();
  const { t } = useTranslation('common');

  const handleChange = (key) => {
    if ((itemId || itemType || member.id) && !Array.isArray(member)) {
      const jsonToSend = {
        user_id: member.id,
        previous_role: actualRole,
        new_role: key.value.toLowerCase(),
      };
      setSending(true);
      api
        .post(`/api/${itemType}/${itemId}/members`, jsonToSend)
        .then(() => {
          setSending(false);
          callBack();
          onRoleChanged();
        })
        .catch((err) => {
          console.error(`Couldn't POST ${itemType} with itemId=${itemId} members`, err);
          setSending(false);
        });
    } else if (Array.isArray(member)) {
      setNewRole(key);
    }
  };

  return (
    <>
      <Select
        options={listRole.map((role) => ({
          value: role,
          label: t(`member.role.${role}`),
        }))}
        isSearchable={false}
        isDisabled={!isDisabled || sending}
        menuShouldScrollIntoView={true} // force scroll into view
        {...(!showPlaceholder && {
          defaultValue: {
            value: actualRole,
            label: t(`member.role.${actualRole}`),
          },
        })}
        onChange={handleChange}
        placeholder={t('member.role.changeRole')}
      />
      {sending && <SpinLoader />}
    </>
  );
};
export default DropdownRole;
