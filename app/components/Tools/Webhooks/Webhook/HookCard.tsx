import { Edit } from '@emotion-icons/boxicons-solid/Edit';
import { Delete } from '@emotion-icons/material/Delete';
import withTranslation from 'next-translate/withTranslation';
import { Component } from 'react';
import { withApi } from 'contexts/apiContext';
import { UserContext } from 'contexts/UserProvider';
import HookForm from './HookForm';
// import "./HookCard.scss";

interface Props {
  mode: 'display' | 'update';
  hook: any;
  refresh: () => void;
  hookProjectId: number;
  api: any;
  onDelete: (id: number) => void;
}

interface State {
  hook: any;
  error: string;
  mode: 'display' | 'update';
  uploading: boolean;
}
class HookCard extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      mode: this.props.mode,
      hook: this.props.hook,
      error: undefined,
      uploading: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  static get defaultProps() {
    return {
      hook: undefined,
      refresh: () => console.warn('Missing refresh function'),
    };
  }

  changeMode(newMode) {
    if (newMode) {
      this.setState({ mode: newMode });
      this.props.refresh();
    }
  }

  cancel() {
    this.setState({ hook: this.props.hook });
    this.changeMode('display');
  }

  handleChange(key, content) {
    const updateHook = this.state.hook;
    updateHook[key] = content;
    const hook_params = {
      hook_url: key === 'hook_url' ? content : this.state.hook.hook_params.hook_url,
      channel: key === 'channel' ? content : this.state.hook.hook_params.channel,
      username: key === 'username' ? content : this.state.hook.hook_params.username,
    };
    updateHook.hook_params = hook_params;
    if (key === 'trigger_post') updateHook.trigger_post = !content;
    if (key === 'trigger_need') updateHook.trigger_need = !content;
    if (key === 'trigger_member') updateHook.trigger_member = !content;
    this.setState({ hook: updateHook, error: '' });
  }

  handleSubmit() {
    const { hookProjectId } = this.props;
    const { hook } = this.state;
    this.setState({ uploading: true });
    const { api } = this.props;
    api.patch(`/api/projects/${hookProjectId}/hooks/${hook.id}`, { hook }).then((res) => {
      this.changeMode('display');
      this.props.refresh();
    });
  }

  deleteHook() {
    const { hookProjectId, api } = this.props;
    const { hook } = this.state;
    this.props.onDelete(hook.id);
    // this.setState({ hook: this.props.hook });
    // this.props.refresh();
  }

  render() {
    const { mode, hook } = this.state;
    const { t } = this.props.i18n;

    if (hook === undefined) {
      // eslint-disable-next-line @rushstack/no-null
      return null;
    }
    return (
      <div className="hookCard">
        {mode === 'display' && (
          <>
            <div className="hookContent">
              <div className="hookContent--header">
                <h6>
                  {t('hook.parameters.channel')}
                  <span className="channel">{hook.hook_params.channel}</span>
                </h6>
                <span className="hookAction">
                  <Edit
                    size={22}
                    title="Edit hook"
                    onClick={() => this.changeMode('update')}
                    onKeyUp={(e) => (e.which === 13 || e.keyCode === 13) && this.changeMode('update')}
                    tabIndex={0}
                  />
                  <button onClick={this.deleteHook.bind(this)} tabIndex={0}>
                    <Delete size={22} title="Delete hook" />
                  </button>
                </span>
              </div>
              <p className="hookContent--triggers">
                <span>{t('hook.triggerOn')}</span>
                :&nbsp;
                {hook.trigger_post && <span>{t('general.post', { count: 1 })}</span>}
                {hook.trigger_need && <span>{t('general.need', { count: 1 })}</span>}
                {hook.trigger_member && <span>{t('general.member', { count: 1 })}</span>}
              </p>
            </div>
          </>
        )}
        {mode === 'update' && (
          <HookForm
            action="update"
            cancel={() => this.cancel()}
            hook={hook}
            handleChange={this.handleChange}
            handleSubmit={this.handleSubmit}
          />
        )}
      </div>
    );
  }
}
HookCard.contextType = UserContext;
export default withApi(withTranslation(HookCard, 'common'));
