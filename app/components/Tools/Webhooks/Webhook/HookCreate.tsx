import withTranslation from 'next-translate/withTranslation';
import { Component } from 'react';
import { withApi } from 'contexts/apiContext';
import { UserContext } from 'contexts/UserProvider';
import HookForm from './HookForm';
import Button from '~/components/primitives/Button';

class HookCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCreating: false,
      hook: {
        hook_type: 'Slack',
        trigger_need: true,
        trigger_post: true,
        trigger_member: true,
        trigger_project: true,
        hook_params: {
          hook_url: '',
          channel: '',
          username: '',
        },
      },
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  static get defaultProps() {
    return {
      refresh: () => console.warn('Missing refresh function'),
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.hook) {
      this.setState({ hook: nextProps.hook });
    }
  }

  handleChange(key, content) {
    const updateHook = this.state.hook;
    updateHook[key] = content;
    // var newHook = {
    //   "hook_type": updateHook.hook_type,
    //   "hook_type": updateHook.hook_type,
    //   "trigger_need": updateHook.trigger_need,
    //   "trigger_post": updateHook.trigger_post,
    //   "trigger_member": updateHook.trigger_member,
    //   "trigger_project": true,
    //   "hook_params": {
    //     "hook_url": key === "hook_url" ? content : updateHook.hook_url,
    //     "channel": key === "hook_channel" ? content : updateHook.hook_channel,
    //     "username": key === "hook_username" ? content : updateHook.hook_username,
    //   }
    // }
    // updateHook["hook_params"] = hook_params;

    if (key === 'trigger_post') updateHook.trigger_post = !content;
    if (key === 'trigger_need') updateHook.trigger_need = !content;
    if (key === 'trigger_member') updateHook.trigger_member = !content;
    const hook_params = {
      hook_url: key === 'hook_url' ? content : this.state.hook.hook_url,
      channel: key === 'channel' ? content : this.state.hook.channel,
      username: key === 'username' ? content : this.state.hook.username,
    };
    updateHook.hook_params = hook_params;
    this.setState({ hook: updateHook });
  }

  handleSubmit() {
    const { projectId } = this.props;
    const { hook } = this.state;
    if (!projectId) {
    } else {
      // hook["project_id"] = projectId;
      const { api } = this.props;
      api
        .post(`/api/projects/${projectId}/hooks`, { hook })
        .then(() => {
          this.refresh();
        })
        .catch((err) => {
          console.error(`Couldn't POST project ${projectId} hooks`, err);
        });
    }
  }

  changeDisplay() {
    this.setState({ isCreating: !this.state.isCreating });
  }

  refresh() {
    this.setState({
      isCreating: false,
      hook: {
        hook_type: 'Slack',
        trigger_need: true,
        trigger_post: true,
        trigger_member: true,
        trigger_project: true,
        hook_params: {
          hook_url: '',
          channel: '',
          username: '',
        },
      },
    });
    this.props.refresh();
  }

  render() {
    const { isCreating, hook } = this.state;
    const { t } = this.props.i18n;
    if (hook === undefined) {
      return 'OK';
    }
    if (isCreating) {
      return (
        <div className="hookCard isCreating">
          {/* {error &&
              <div className="alert alert-danger" role="alert">
                {t("err-")}
              </div>
            } */}
          <HookForm
            action="create"
            cancel={this.changeDisplay.bind(this)}
            hook={hook}
            handleChange={this.handleChange}
            handleSubmit={this.handleSubmit}
          />
        </div>
      );
    }
    return (
      <div className="hookCreate justButton">
        <Button onClick={() => this.changeDisplay()}>{t('hook.addHook')}</Button>
      </div>
    );
  }
}
HookCreate.contextType = UserContext;
export default withApi(withTranslation(HookCreate, 'common'));
