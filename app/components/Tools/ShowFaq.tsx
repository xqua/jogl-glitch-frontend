import React, { FC, memo } from 'react';
import useTranslation from 'next-translate/useTranslation';
import H2 from '~/components/primitives/H2';
import Accordion from '../Accordion';

interface Props {
  faqList: any;
}
const ShowFaq: FC<Props> = ({ faqList }) => {
  const { t } = useTranslation('common');
  return (
    <div tw="flex flex-col space-y-3">
      <H2>{t('faq.fullTitle')}</H2>
      {faqList && <Accordion values={faqList.documents} hasHtmlContent />}
    </div>
  );
};

export default memo(ShowFaq);
