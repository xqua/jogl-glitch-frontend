import { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';

interface Props {
  type?:
    | 'group'
    | 'challenge'
    | 'project'
    | 'program'
    | 'need'
    | 'followings'
    | 'post'
    | 'board'
    | 'star'
    | 'resources'
    | 'activity'
    | 'proposal'
    | 'peer_review';
}
const NoResults: FC<Props> = ({ type = 'title' }) => {
  const { t } = useTranslation('common');
  return <p tw="font-medium text-[#C5CED5] text-2xl">{t(`list.noResult.${type}`)}</p>;
};

export default NoResults;
