import TitleInfo from 'components/Tools/TitleInfo';
// import "./InfoDefaultComponent.scss";

export default function InfoDefaultComponent({ title = '', content, prepend = '', containsHtml = false }) {
  if (content) {
    return (
      <div className="infoDefault">
        {title && <TitleInfo title={title} />}
        <div className="content">
          {!containsHtml ? prepend + content : <div dangerouslySetInnerHTML={{ __html: content }} />}
        </div>
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
}
