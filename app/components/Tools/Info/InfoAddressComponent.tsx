import TitleInfo from 'components/Tools/TitleInfo';
// import "./InfoAddressComponent.scss";

export default function InfoAddressComponent({ title, address, city, country }) {
  if (address || city || country) {
    return (
      <div className="infoAddress">
        {title && <TitleInfo title={title} />}
        <div className="content">
          <span id="full_address">
            {city && country ? `${city}, ` : city && city}
            {country}
          </span>
        </div>
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
}
