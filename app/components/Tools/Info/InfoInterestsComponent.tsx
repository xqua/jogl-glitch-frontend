import React, { useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import TitleInfo from '~/components/Tools/TitleInfo';
import ReactTooltip from 'react-tooltip';
import Link from 'next/link';
import { defaultSdgsInterests } from '~/utils/utils';
import tw from 'twin.macro';

export default React.memo(function InfoInterestsComponent({ title = 'Title', content = [] }) {
  const { t } = useTranslation('common');
  // get sdgs infos array from external function
  const defaultInterests = defaultSdgsInterests(t);
  const [showHidden, setShowHidden] = useState(false);

  if (content.length > 0) {
    return (
      <div className="infoInterests">
        {/* {title && <div tw="text-secondary text-right">{title}</div>} */}
        {/* {title && <div tw="text-secondary">{title}</div>} */}
        <TitleInfo title={title} />
        <div tw="flex flex-wrap gap-2">
          {/* <div tw="flex flex-wrap gap-2 justify-end"> */}
          {content
            .sort((a, b) => a - b) // sort sdgs by asc order
            .map((interestId, index) => {
              const interest = defaultInterests.find((el) => el.value === interestId);
              return (
                <div key={index} css={index > 3 && !showHidden && tw`hidden`}>
                  <Link href={`/search/projects/?refinementList[interests][0]=${interestId}`} passHref>
                    <div
                      tw="font-bold inline-flex items-center text-white px-2 py-1"
                      css={{ backgroundColor: `${interest.color}` }}
                      data-for={`sdg-${interestId}`}
                      data-tip={t(`sdg.description.${interestId}`)}
                    >
                      <span style={{ fontSize: '1.15rem' }}>{interestId}</span>
                      <img
                        style={{ width: '48px', height: '24px', objectFit: 'contain', paddingRight: '2px' }}
                        src={`/images/interests/Interest-${interestId}-icon.png`}
                      />
                      <span style={{ fontSize: '.85rem' }}>{interest.label}</span>
                      <ReactTooltip
                        id={`sdg-${interestId}`}
                        effect="solid"
                        type="info"
                        className="solid-tooltip"
                        arrowColor="transparent"
                      />
                    </div>
                  </Link>
                  <ReactTooltip id={`sdg-${interestId}`} effect="solid" type="info" className="solid-tooltip" />
                </div>
              );
            })}
          {content.length > 4 && (
            <div
              tw="cursor-pointer px-2 py-1 border border-solid border-gray-400 hover:bg-gray-200"
              onClick={() => setShowHidden((prevState) => !prevState)}
            >
              {showHidden ? t('general.showless') : t('general.showmore')}
            </div>
          )}
        </div>
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
});
