import React, { FC } from 'react';
import { Program } from 'types';
import InfoStats from 'components/Tools/InfoStats';
import useTranslation from 'next-translate/useTranslation';
import { displayObjectDate } from 'utils/utils';
import useGet from '~/hooks/useGet';

interface Props {
  program: Program;
}

const ProgramInfo: FC<Props> = ({ program }) => {
  const { t } = useTranslation('common');
  const { data: dataExternalLink } = useGet<{ url: string; icon_url: string }[]>(`/api/programs/${program.id}/links`);

  return (
    <div tw="flex-shrink-0 md:p-4 md:w-80 md:pt-6">
      <div tw="flex flex-wrap mb-2 justify-around md:mt-0">
        <InfoStats object="program" type="project" tab="projects" count={program.projects_count} />
        <InfoStats object="program" type="need" tab="needs" count={program.needs_count} />
        <InfoStats object="program" type="member" tab="members" count={program.members_count} />
      </div>
      <hr tw="mb-4" />
      <div tw="grid gap-4 grid-cols-1 items-center text-center">
        <div tw="flex items-center justify-between flex-wrap">
          <div>{t('attach.status')}</div>
          <div tw="inline-flex space-x-2 items-center">
            <div
              tw="rounded-full h-2 w-2"
              // style={{ backgroundColor: program.status !== 'draft' ? statusStep[1] : 'grey' }}
            />
            <div
              // style={{ color: program.status !== 'draft' ? statusStep[1] : 'grey' }}
              tw="flex w-[fit-content] rounded-md items-center"
              data-tip={t('entity.info.status.title')}
              data-for="challengeCard_status"
            >
              <div tw="font-medium">{t(`entity.info.status.${program.status}`)}</div>
            </div>
            {/* {program.status !== 'draft' && <span tw="text-gray-500">({statusStep[0]}/4)</span>} */}
          </div>
        </div>

        {program.launch_date && (
          <div tw="flex items-center justify-between flex-wrap">
            <div>{t('entity.info.launch_date')}:</div>
            <span tw="font-bold">{displayObjectDate(program.launch_date, 'LL')}</span>
          </div>
        )}

        {dataExternalLink && dataExternalLink?.length !== 0 && (
          <div tw="flex items-center justify-between flex-wrap">
            <div>{t('general.externalLink.findUs')}</div>
            <div tw="flex flex-wrap gap-3">
              {[...dataExternalLink].map((link, i) => (
                <a tw="items-center" key={i} href={link.url} target="_blank">
                  <img tw="w-10 hover:opacity-80" src={link.icon_url} />
                </a>
              ))}
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default ProgramInfo;
