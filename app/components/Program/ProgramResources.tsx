import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import H2 from 'components/primitives/H2';
import useGet from 'hooks/useGet';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';

interface Props {
  programId: number;
}

const ProgramResources: FC<Props> = ({ programId }) => {
  const { data: resourcesList } = useGet(`/api/programs/${programId}/resources`);
  const { t } = useTranslation('common');
  return (
    <div tw="flex flex-col">
      {!resourcesList ? (
        <Loading />
      ) : resourcesList?.length === 0 ? (
        <NoResults type="resources" />
      ) : (
        <div tw="flex flex-col space-y-7">
          <H2 tw="px-4 md:pl-0">{t('program.resources.title')}</H2>
          {[...resourcesList].map((resource, i) => (
            <div>
              <h4 tw="pb-2 px-4 md:pl-0">{resource.title}</h4>
              <div tw="flex flex-col bg-white px-4 md:pl-0">
                <InfoHtmlComponent content={resource.content} />
              </div>
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default ProgramResources;
