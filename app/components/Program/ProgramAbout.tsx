import useTranslation from 'next-translate/useTranslation';
import React, { FC } from 'react';
import H2 from 'components/primitives/H2';
// import styled from 'utils/styled';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import ShowBoards from '../Tools/ShowBoards';
import { Program } from '~/types';

interface Props {
  program: Program;
}

const ProgramAbout: FC<Props> = ({ program }) => {
  const { t } = useTranslation('common');

  return (
    <div tw="max-w-3xl">
      {/* program full description */}
      <InfoHtmlComponent content={program.description} />
      <hr tw="mt-10" />
      {/* Program Boards */}
      <div tw="mb-10">
        <ShowBoards objectType="programs" objectId={program.id} />
      </div>
      {/* supporters logos bloc */}
      <H2>{t('program.supporters')}</H2>
      <InfoHtmlComponent content={program.enablers} />
      <a href="mailto:hello@jogl.io">{t('program.supporters_interested')}</a>
    </div>
  );
};

export default ProgramAbout;
