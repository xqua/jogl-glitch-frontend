/* eslint-disable camelcase */
import { Edit } from '@emotion-icons/fa-solid/Edit';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import React, { FC } from 'react';
import H1 from 'components/primitives/H1';
import Button from '../primitives/Button';
import { Program } from 'types';
import ShareBtns from '../Tools/ShareBtns/ShareBtns';
import BtnFollow from 'components/Tools/BtnFollow';
import ProgramInfo from './ProgramInfo';
import BtnJoin from '../Tools/BtnJoin';
import { useRouter } from 'next/router';

interface Props {
  program: Program;
  lang: string;
}

const ProgramHeader: FC<Props> = ({ program, lang = 'en' }) => {
  const { t } = useTranslation('common');
  const router = useRouter();

  const { id, title, title_fr, short_description_fr, short_description } = program;

  return (
    <header tw="relative w-full">
      <div tw="flex flex-wrap divide-x divide-gray-200 md:flex-nowrap">
        <div tw="hidden md:block">
          <ProgramInfo program={program} />
        </div>

        <div tw="px-4 py-4 md:px-8 md:pt-6">
          <div tw="inline-flex items-center mb-1">
            <H1 tw="pr-3 font-size[1.8rem] md:font-size[2.3rem]">{(lang === 'fr' && title_fr) || title}</H1>
            <div tw="hidden md:block">
              <ShareBtns type="program" specialObjId={id} />
            </div>
          </div>
          <p tw="mt-4 text-gray-600">{(lang === 'fr' && short_description_fr) || short_description}</p>

          <div tw="flex flex-wrap gap-3 mb-2">
            {program.is_admin && (
              <Link href={`/program/${program.short_title}/edit`} passHref>
                <Button tw="flex justify-center items-center">
                  <Edit size={18} title="Edit program" />
                  {t('entity.form.btnAdmin')}
                </Button>
              </Link>
            )}
            {!program?.is_owner && (
              <BtnJoin
                joinState={program.is_member}
                itemType="programs"
                itemId={program.id}
                count={program.members_count}
                showMembersModal={() =>
                  router.push(`/program/${router.query.short_title}?tab=members`, undefined, { shallow: true })
                }
              />
            )}
            <BtnFollow
              followState={program.has_followed}
              itemType="programs"
              itemId={program.id}
              count={program.followers_count}
            />
            <div tw="md:hidden">
              <ShareBtns type="program" specialObjId={id} />
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default ProgramHeader;
