/* eslint-disable camelcase */
import Link from 'next/link';
import React from 'react';
import ObjectCard from 'components/Cards/ObjectCard';
import H2 from 'components/primitives/H2';
import Title from 'components/primitives/Title';
import BtnStar from 'components/Tools/BtnStar';
import { TextWithPlural } from 'utils/managePlurals';
import { useRouter } from 'next/router';
import tw from 'twin.macro';
import useTranslation from 'next-translate/useTranslation';

interface Props {
  id: number;
  title: string;
  title_fr?: string;
  short_title: string;
  short_description: string;
  short_description_fr?: string;
  membersCount?: number;
  projectsCount?: number;
  challengesCount?: number;
  has_saved?: boolean;
  banner_url: string;
  source?: 'algolia' | 'api';
  cardFormat?: string;
  width?: string;
  chip?: string;
}
const ProgramCard = ({
  id,
  title,
  title_fr,
  short_title,
  short_description,
  short_description_fr,
  membersCount,
  projectsCount,
  challengesCount,
  has_saved,
  banner_url = '/images/default/default-program.jpg',
  source,
  cardFormat,
  width,
  chip,
}: Props) => {
  const router = useRouter();
  const { locale } = router;
  const { t } = useTranslation('common');
  const programUrl = `/program/${short_title}`;
  return (
    <ObjectCard imgUrl={banner_url} href={programUrl} width={width} chip={chip} cardFormat={cardFormat}>
      {cardFormat === 'showObjType' && (
        <div tw="bg-[#CEDE7C] w-[calc(100% + 32px)] pl-3 flex flex-wrap -mx-4 mt-[-.8rem] mb-[.8rem]">
          <span tw="px-1 space-x-1 font-bold text-sm">{t('program.title')}</span>
        </div>
      )}
      {/* Title */}
      <div tw="inline-flex items-center md:h-14">
        <Link href={programUrl} passHref>
          <Title>
            <H2 tw="word-break[break-word] line-clamp-2 items-center">{(locale === 'fr' && title_fr) || title}</H2>
          </Title>
        </Link>
      </div>
      <Hr tw="mt-2 pt-4" />
      {/* Description */}
      <div css={[tw`flex-1`, cardFormat === 'compact' ? tw`line-clamp-3` : tw`line-clamp-6`]}>
        {(locale === 'fr' && short_description_fr) || short_description}
      </div>
      {/* Stats */}
      {cardFormat !== 'compact' && (
        <>
          <Hr tw="mt-5 pt-2" />
          <div tw="items-center justify-around space-x-2 flex flex-wrap">
            <CardData value={membersCount} title={<TextWithPlural type="member" count={membersCount} />} />
            <CardData value={projectsCount} title={<TextWithPlural type="project" count={projectsCount} />} />
            {/* <CardData value={challengesCount} title={<TextWithPlural type="challenge" count={challengesCount} />} /> */}
          </div>
        </>
      )}
      {/* Star icon */}
      {(has_saved !== undefined || source === 'algolia') && (
        <BtnStar itemType="programs" itemId={id} hasStarred={has_saved} hasNoStat source={source} />
      )}
    </ObjectCard>
  );
};

const CardData = ({ value, title }) => (
  <div tw="flex flex-col justify-center items-center">
    <div tw="font-bold font-size[17px] -mb-1">{value}</div>
    <div tw="text-gray-500 font-medium text-sm">{title}</div>
  </div>
);
const Hr = (props) => <div tw="-mx-4 border-0 border-t border-solid border-color[rgba(0, 0, 0, 0.07)]" {...props} />;

export default ProgramCard;
