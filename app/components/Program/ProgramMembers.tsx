import useTranslation from 'next-translate/useTranslation';
import React, { FC, useState } from 'react';
import QuickSearchBar from 'components/Tools/QuickSearchBar';
import useChallenges from 'hooks/useChallenges';
import useUserData from 'hooks/useUserData';
import styled from 'utils/styled';
import Filters from '../Filters';
import Grid from '../Grid';
import A from '../primitives/A';
import Button from '../primitives/Button';
import SpinLoader from '../Tools/SpinLoader';
import UserCard from '../User/UserCard';
import useInfiniteLoading from 'hooks/useInfiniteLoading';

const OverflowGradient = styled.div`
  width: 3rem;
  height: 100%;
  position: absolute;
  right: 0;
  ${(p) => `background: linear-gradient(269.82deg, ${p.theme.colors.lightBlue} 50.95%, rgba(241, 244, 248, 0) 134.37%)`}
`;

interface Props {
  programId: number;
  customChalName: string;
}

const ProgramMembers: FC<Props> = ({ programId, customChalName }) => {
  const membersPerQuery = 24; // number of members we get per query calls (make it 3 to test locally)
  const [membersEndpoints, setMembersEndpoint] = useState(
    `/api/programs/${programId}/members?items=${membersPerQuery}`
  );
  const { data: dataMembers, response, error, size, setSize } = useInfiniteLoading(
    (index) => `${membersEndpoints}&page=${index + 1}`
  );
  const { dataChallenges, challengesError } = useChallenges('programs', programId);
  const [selectedChallengeFilterId, setSelectedChallengeFilterId] = useState(undefined);
  const { userData } = useUserData();
  const { t } = useTranslation('common');
  const members = dataMembers ? [].concat(...dataMembers?.map((d) => d.members)) : [];
  const isLoadingInitialData = !dataMembers && !error;
  const isLoadingMore =
    isLoadingInitialData || (size > 0 && dataMembers && typeof dataMembers[size - 1] === 'undefined');
  const isEmpty = dataMembers?.[0]?.members.length === 0;
  const isReachingEnd =
    isEmpty || (dataMembers && dataMembers[dataMembers.length - 1]?.members.length < membersPerQuery);

  const onFilterChange = (e) => {
    const id = e.target.name;
    const itemsNb = members.length > membersPerQuery ? members.length : membersPerQuery;
    if (id) {
      // if we select one of the challenge
      setSelectedChallengeFilterId(Number(id));
      setMembersEndpoint(`/api/challenges/${id}/members?items=${itemsNb}`); // get members from selected challenge instead of program
      // setHasLoadOnce(false); // set to false so it set new members of only selected challenge
    } else {
      // if we select "All challenges"
      setSelectedChallengeFilterId(undefined);
      setMembersEndpoint(`/api/programs/${programId}/members?items=${itemsNb}`); // default end point, getting default number of members from program api
    }
  };
  return (
    <>
      {!userData && ( // if user is not connected
        <div tw="flex flex-col space-x-2 pb-4">
          <A href="/signin">
            {t('header.signIn')} {t('program.signinCta.members')}
          </A>
        </div>
      )}
      <div tw="flex flex-col relative">
        {/* Filters of members by program/challenges */}
        {dataChallenges?.length > 0 && (
          <div tw="flex flex-col relative overflow-hidden">
            <OverflowGradient tw="md:hidden" />
            <Filters
              resetButtonLabel="challenge.list_all"
              content={dataChallenges
                // filter to hide draft challenges
                ?.filter(({ status }) => status !== 'draft')
                .map(({ title, id }) => ({
                  title,
                  id,
                }))}
              onChange={(e) => onFilterChange(e)}
              isError={challengesError}
              errorMessage="Could not get challenges filters"
              selectedId={selectedChallengeFilterId}
              customWording={customChalName}
            />
          </div>
        )}
        {/* Members grid/list */}
        {members && (
          <>
            {/* Search bar to quickly find members (show if more than 30 members) */}
            {members.length > 30 && <QuickSearchBar members={members} />}
            <Grid tw="py-4">
              {members?.map((member, i) => (
                <UserCard
                  key={i}
                  id={member.id}
                  firstName={member.first_name}
                  lastName={member.last_name}
                  nickName={member.nickname}
                  shortBio={member.short_bio}
                  skills={member.skills}
                  resources={member.ressources}
                  status={member.status}
                  lastActive={member.current_sign_in_at}
                  logoUrl={member.logo_url}
                  hasFollowed={member.has_followed}
                  affiliation={member.affiliation}
                  projectsCount={member.stats?.projects_count}
                  followersCount={member.stats?.followers_count}
                  spacesCount={member.stats?.spaces_count}
                  mutualCount={member.stats.mutual_count}
                  role={member.owner ? 'leader' : !member.owner && member.admin && 'admin'}
                />
              ))}
            </Grid>
            <div tw="flex flex-col self-center pt-4">
              <Button onClick={() => setSize(size + 1)} disabled={isLoadingMore || isReachingEnd}>
                {isLoadingMore && <SpinLoader />}
                {isLoadingMore ? t('general.loading') : !isReachingEnd ? t('general.load') : t('general.noMoreResults')}
              </Button>
            </div>
          </>
        )}
      </div>
    </>
  );
};

export default ProgramMembers;
