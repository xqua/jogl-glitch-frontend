import useTranslation from 'next-translate/useTranslation';
import Grid from '../Grid';
import Button from '../primitives/Button';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';
import useGet from 'hooks/useGet';
import { Challenge, PeerReview } from 'types';
// import ActivityCard from '../Challenge/ActivityCard';
import Link from 'next/link';
import PeerReviewCard from '../PeerReview/PeerReviewCard';
import ChallengeCard from '../Challenge/ChallengeCard';

const ProgramActivities = ({ programId, programShortTitle, isAdmin }) => {
  const { t } = useTranslation('common');
  const { data: dataChallenges } = useGet<{ challenges: Challenge[] }>(`/api/programs/${programId}/challenges`);
  const { data: dataPeerReviews } = useGet<PeerReview[]>(`/api/programs/${programId}/peer_reviews`);

  return (
    <>
      {/* <P>{t('general.attached_challenges_explanation', { challenge_wording: t('general.activities') })}</P> */}
      {isAdmin && (
        <div tw="mt-4">
          <Link href={`/program/${programShortTitle}/edit?t=activities`} passHref>
            <Button btnType="secondary">
              {t('challenge.create.title', { challenge_wording: t('general.activity_1') })}
            </Button>
          </Link>
        </div>
      )}
      {/* Grid showing all activities */}
      <div tw="flex flex-col py-4 relative">
        {!dataChallenges || !dataPeerReviews ? (
          <Loading />
        ) : dataChallenges?.challenges.length === 0 && dataPeerReviews?.length === 0 ? (
          <NoResults type="activity" />
        ) : (
          <Grid tw="py-4">
            {/* Peer reviews */}
            {dataPeerReviews
              ?.filter(({ visibility }) => visibility !== 'hidden')
              // don't display hidden ones
              .map((peerReview) => (
                <PeerReviewCard key={peerReview.id} peerReview={peerReview} chip={t('peerReview.title')} />
                // <ActivityCard
                //   key={index}
                //   banner_url={peerReview.banner_url || '/images/default/default-challenge.jpg'}
                //   title={peerReview.title}
                //   short_description={peerReview.summary}
                //   link={`/peer-review/${peerReview.short_title}`}
                //   custom_type={t('peerReview.title')}
                // />
              ))}
            {/* Challenges */}
            {dataChallenges.challenges
              ?.filter(({ status }) => status !== 'draft')
              // don't display draft challenges
              .map((challenge, index) => (
                <ChallengeCard
                  key={index}
                  id={challenge.id}
                  banner_url={challenge.banner_url || '/images/default/default-challenge.jpg'}
                  short_title={challenge.short_title}
                  title={challenge.title}
                  title_fr={challenge.title_fr}
                  short_description={challenge.short_description}
                  short_description_fr={challenge.short_description_fr}
                  membersCount={challenge.members_count}
                  needsCount={challenge.needs_count}
                  clapsCount={challenge.claps_count}
                  projectsCount={challenge.projects_count}
                  space={challenge.space}
                  status={challenge.status}
                  has_saved={challenge.has_saved}
                  customType={challenge.custom_type}
                  createdAt={challenge.created_at}
                />
              ))}
          </Grid>
        )}
      </div>
    </>
  );
};

export default ProgramActivities;
