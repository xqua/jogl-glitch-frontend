import Link from 'next/link';
import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from 'contexts/apiContext';
import styled from 'utils/styled';
import Card from 'components/Cards/Card';
import Button from '../primitives/Button';
import H2 from '../primitives/H2';

interface Props {
  icon: string;
  title: string;
  shortTitle: string;
  status?: string;
  projectId?: number;
  projectChallengeId?: number;
  onChallengeDelete?: (id: number) => void;
  isEditCard?: boolean;
}

const ChallengeMiniCard: FC<Props> = ({
  icon,
  title,
  shortTitle,
  status = 'pending',
  projectChallengeId,
  projectId,
  onChallengeDelete,
  isEditCard = false,
}) => {
  const api = useApi();
  const { t } = useTranslation('common');
  const cancelParticipation = () => {
    api
      .delete(`/api/challenges/${projectChallengeId}/projects/${projectId}`)
      .then(() => onChallengeDelete(projectChallengeId))
      .catch((err) => console.error(`Couldn't remove project of challenge with id=${projectChallengeId}`, err));
  };

  return (
    <CustomCard>
      <div tw="flex flex-col h-full w-56 justify-between">
        <div>
          <Link href={`/challenge/${shortTitle}`}>
            <a>
              <div tw="flex flex-col justify-center items-center">
                {icon && (
                  <img
                    src={icon}
                    width="100%"
                    alt="Challenge Icon"
                    style={{ objectFit: 'cover', maxHeight: '110px' }}
                  />
                )}
                <div tw="flex flex-col pt-4 text-2xl font-black letter-spacing[-1pX]">
                  <H2 tw="line-clamp-2 hover:underline mb-0">{title}</H2>
                </div>
              </div>
            </a>
          </Link>
          <div
            tw="flex flex-col text-center w-[fit-content] px-2 self-center rounded-sm text-white font-bold mx-auto"
            style={{ color: status === 'pending' ? '#dea500' : '#008b2b' }}
          >
            {t(`challenge.acceptState.${status}`)}
          </div>
        </div>
        {status && isEditCard && (
          <div tw="flex justify-around mt-3">
            <Button btnType="danger" onClick={cancelParticipation}>
              {t('attach.remove')}
            </Button>
          </div>
        )}
      </div>
    </CustomCard>
  );
};

const CustomCard = styled(Card)`
  border: 1px solid lightgray;
  transition: box-shadow 0.3s ease-in-out;
  a {
    color: #38424f;
    text-decoration: none;
  }
`;

export default ChallengeMiniCard;
