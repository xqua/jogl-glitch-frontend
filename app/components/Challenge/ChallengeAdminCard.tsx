import React, { FC, ReactNode, useEffect, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Alert from 'components/Tools/Alert';
import { useApi } from 'contexts/apiContext';
import { Challenge } from 'types';
import A from '../primitives/A';
import Button from '../primitives/Button';
import SpinLoader from '../Tools/SpinLoader';
import { isGroupNotChallenge } from '~/utils/utils';

interface Props {
  challenge: Challenge;
  parentType: 'programs' | 'spaces';
  parentId: number;
  callBack: () => void;
  showChalType?: boolean;
}

const ChallengeAdminCard: FC<Props> = ({ challenge, parentType, parentId, callBack, showChalType = false }) => {
  const [sending, setSending] = useState<'remove' | 'accepted' | undefined>(undefined);
  const [error, setError] = useState<string | ReactNode>(undefined);
  const [imgToDisplay, setImgToDisplay] = useState('/images/default/default-challenge.jpg');
  const api = useApi();
  const { t } = useTranslation('common');
  const route = `/api/${parentType}/${parentId}/challenges/${challenge.id}`;

  const onSuccess = () => {
    setSending(undefined);
    callBack();
  };

  const onError = (err) => {
    console.error(err);
    setSending(undefined);
    setError(t('err-'));
  };

  const acceptChallenge = () => {
    setSending('accepted');
    api
      .post(route, { status: 'accepted' })
      .then(() => onSuccess())
      .catch((err) => onError(err));
  };

  const removeChallenge = () => {
    setSending('remove');
    api
      .delete(route)
      .then(() => onSuccess())
      .catch((err) => onError(err));
  };

  const isChallengePending = false;

  useEffect(() => {
    challenge.logo_url && setImgToDisplay(challenge.logo_url);
  }, [challenge.logo_url]);

  const bgLogo = {
    backgroundImage: `url(${imgToDisplay})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    border: '1px solid #ced4da',
    borderRadius: '50%',
    height: '50px',
    width: '50px',
  };

  if (challenge) {
    return (
      <div tw="flex flex-col justify-between py-3 sm:flex-row" key={challenge.id}>
        <div tw="flex items-center pb-3 space-x-3 sm:pb-0">
          <div style={{ width: '50px' }}>
            <div style={bgLogo} />
          </div>
          <div tw="flex flex-col">
            <span>
              <A href={`/challenge/${challenge.short_title}`}>{challenge.title}</A>
              {showChalType && <span tw="text-[90%]">{` (${challenge.custom_type})`}</span>}
            </span>
            <div tw="flex flex-col text-[85%] pt-1">
              {t('attach.status')}
              {isGroupNotChallenge(challenge.created_at, challenge.custom_type)
                ? t(`entity.info.status.${challenge.status}`)
                : t(`challenge.info.status_${challenge.status}`)}
            </div>
          </div>
        </div>
        <div tw="flex items-center justify-between sm:justify-end">
          <div tw="flex flex-col pr-8">
            {t('attach.members')}
            {challenge.members_count}
          </div>
          {isChallengePending ? ( // if challenge status is pending, display the accept/reject buttons
            <div tw="space-x-2">
              <Button
                tw="text-green-500 border-green-500 bg-white hover:(text-white bg-green-500)"
                disabled={sending === 'accepted'}
                onClick={acceptChallenge}
              >
                {sending && <SpinLoader />}
                {t('general.accept')}
              </Button>
              <Button
                tw="text-danger border-danger bg-white hover:(text-white bg-danger)"
                disabled={sending === 'remove'}
                onClick={removeChallenge}
              >
                {sending === 'remove' && <SpinLoader />}
                {t('general.reject')}
              </Button>
            </div>
          ) : (
            <Button btnType="danger" disabled={sending === 'remove'} onClick={removeChallenge} tw="ml-3">
              {sending === 'remove' && <SpinLoader />}
              {t('general.remove')}
            </Button>
          )}
          {error && <Alert type="danger" message={error} />}
        </div>
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};
export default ChallengeAdminCard;
