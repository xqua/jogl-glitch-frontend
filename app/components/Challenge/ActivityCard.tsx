/* eslint-disable camelcase */
import Link from 'next/link';
import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import ObjectCard from 'components/Cards/ObjectCard';
import H2 from 'components/primitives/H2';
import Title from 'components/primitives/Title';
import { Challenge } from 'types';
import ReactTooltip from 'react-tooltip';

export type ChallengeType = Pick<
  Challenge,
  'title' | 'title_fr' | 'short_description' | 'short_description_fr' | 'banner_url' | 'custom_type' | 'status'
>;
const ActivityCard: FC<ChallengeType> = ({
  title,
  title_fr,
  link,
  short_description,
  short_description_fr,
  banner_url = '/images/default/default-challenge.jpg',
  custom_type,
  status,
}) => {
  const router = useRouter();
  const { locale } = router;
  const { t } = useTranslation('common');
  const customType =
    custom_type === 'Group' ? t('challenge.title') : custom_type === 'event' ? t('space.event.title') : custom_type;

  return (
    // hrefNewTab indicates that clicking link on the objectCard banner will open it in new tab
    <ObjectCard imgUrl={banner_url} href={link} chip={customType} hrefNewTab={custom_type === 'event'}>
      {/* Title */}
      <Link href={link} passHref>
        <Title target={custom_type === 'event' && '_blank'}>
          <H2 tw="word-break[break-word]">{(locale === 'fr' && title_fr) || title}</H2>
        </Title>
      </Link>
      {status === 'draft' && (
        <div tw="inline-flex space-x-2 items-center">
          <div tw="rounded-full h-2 w-2" style={{ backgroundColor: 'grey' }} />
          <div
            style={{ color: 'grey' }}
            tw="flex w-[fit-content] rounded-md items-center"
            data-tip={t('entity.info.status.title')}
            data-for="spaceCard_status"
          >
            <div tw="font-medium">{t(`entity.info.status.draft`)}</div>
          </div>
          <ReactTooltip id="spaceCard_status" effect="solid" />
        </div>
      )}
      {/* Description */}
      <div tw="line-clamp-4 flex-1 mt-4">{(locale === 'fr' && short_description_fr) || short_description}</div>
    </ObjectCard>
  );
};

export default ActivityCard;
