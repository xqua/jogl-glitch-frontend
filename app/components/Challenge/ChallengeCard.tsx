/* eslint-disable camelcase */
import Link from 'next/link';
import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import ObjectCard from 'components/Cards/ObjectCard';
import H2 from 'components/primitives/H2';
import Title from 'components/primitives/Title';
import BtnStar from 'components/Tools/BtnStar';
import { DataSource, Program, Space } from 'types';
import { TextWithPlural } from 'utils/managePlurals';
import { statusToStep } from './ChallengeStatus';
import ReactTooltip from 'react-tooltip';
import { isGroupNotChallenge } from '~/utils/utils';

export type Challenge = {
  id: number;
  title: string;
  title_fr?: string;
  short_title: string;
  short_description: string;
  short_description_fr?: string;
  membersCount: number;
  projectsCount: number;
  program?: Pick<Program, 'id' | 'short_title' | 'title' | 'title_fr'>;
  space?: Pick<Space, 'id' | 'short_title' | 'title' | 'title_fr'>;
  needsCount: number;
  has_saved?: boolean;
  banner_url: string;
  status: string;
  customType?: string;
  createdAt?: Date;
  cardFormat?: string;
  chip?: string;
};
interface Props extends Challenge {
  width?: string;
  source?: DataSource;
}
const ChallengeCard: FC<Props> = ({
  id,
  title,
  title_fr,
  short_title,
  short_description,
  short_description_fr,
  membersCount,
  projectsCount,
  program,
  space,
  needsCount = 0,
  has_saved,
  banner_url = '/images/default/default-challenge.jpg',
  width,
  source,
  status,
  customType,
  createdAt,
  cardFormat,
  chip,
}) => {
  const router = useRouter();
  const { locale } = router;
  const { t } = useTranslation('common');
  const challengeUrl = `/challenge/${short_title}`;
  const statusStep = statusToStep(status);
  const showChip = chip && cardFormat === 'showObjType';

  return (
    <ObjectCard
      imgUrl={banner_url}
      href={challengeUrl}
      width={width}
      // customize the chip depending on conditions
      chip={
        showChip
          ? chip
          : customType !== undefined && cardFormat !== 'showObjType'
          ? customType !== ''
            ? customType
            : t('challenge.title')
          : ''
      }
      cardFormat={cardFormat}
    >
      {cardFormat === 'showObjType' && (
        <div tw="bg-[#A1A3FF] w-[calc(100% + 32px)] pl-3 flex flex-wrap -mx-4 mt-[-.8rem] mb-[.8rem] text-sm">
          <span tw="font-bold px-1">{t('challenge.title')}</span>
          {customType !== 'Group' && <span>({customType})</span>}
        </div>
      )}
      {/* Title */}
      <div tw="inline-flex items-center md:h-14">
        <Link href={challengeUrl} passHref>
          <Title>
            {/* <H2 tw="word-break[break-word] text-2xl flex items-center md:(text-3xl h-16)"> */}
            <H2 tw="word-break[break-word] line-clamp-2 items-center">{(locale === 'fr' && title_fr) || title}</H2>
          </Title>
        </Link>
      </div>
      {/* <div style={{ color: "grey", paddingBottom: "5px" }}>
        <span> Last active today </span> <span> Prototyping </span>
      </div> */}
      <Hr tw="mt-2 pt-2" />
      {/* Challenge's program */}
      {program &&
      program.id !== -1 && ( // if program id is !== -1 (meaning challenge is not attached to a program), display program name and link
          <div tw="inline-flex space-x-2 items-center">
            {/* <div tw="w-3!">
              <Image src="/images/logo.svg" alt="jogl logo rocket" quality={25} tw="w-3!" />
            </div> */}
            <div>
              {t('entity.info.program_title')}
              &nbsp;
              <Link href={`/program/${program.short_title}`} passHref>
                <a tw="text-black font-medium hover:(underline text-black)">
                  {(locale === 'fr' && program.title_fr) || program.title}
                </a>
              </Link>
            </div>
          </div>
        )}
      {/* Challenge's space */}
      {space &&
      space.id !== -1 && ( // if space id is !== -1 (meaning challenge is not attached to a space), display space name and link
          <div tw="inline-flex space-x-2 mt-0 items-center">
            {/* <div tw="w-3!">
              <Image src="/images/logo.svg" alt="jogl logo rocket" quality={25} tw="w-3!" />
            </div> */}
            <div>
              {t('space.title')}:&nbsp;
              <Link href={`/space/${space.short_title}`} passHref>
                <a tw="text-black font-medium hover:(underline text-black)">
                  {(locale === 'fr' && space.title_fr) || space.title}
                </a>
              </Link>
            </div>
          </div>
        )}
      {/* Status */}
      {(status === 'draft' || status === 'completed') && (
        <div tw="inline-flex space-x-2 items-center">
          <div
            tw="rounded-full h-2 w-2"
            style={{
              backgroundColor: status !== 'draft' ? statusStep?.[1] : 'grey',
            }}
          />
          <div
            style={{ color: status !== 'draft' ? statusStep?.[1] : 'grey' }}
            tw="flex w-[fit-content] rounded-md items-center"
            data-tip={t('entity.info.status.title')}
            data-for="challengeCard_status"
          >
            <div tw="font-medium">
              {isGroupNotChallenge(createdAt, customType)
                ? t(`entity.info.status.${status}`)
                : t(`challenge.info.status_${status}`)}
            </div>
          </div>
          <ReactTooltip id="challengeCard_status" effect="solid" />
        </div>
      )}

      <Hr tw="mt-2 pt-6" />
      {/* Description */}
      <div tw="line-clamp-4 flex-1">{(locale === 'fr' && short_description_fr) || short_description}</div>
      <Hr tw="mt-5 pt-2" />
      {/* Stats */}
      <div tw="items-center justify-around space-x-2 flex flex-wrap">
        <CardData value={membersCount} title={<TextWithPlural type="member" count={membersCount} />} />
        <CardData value={projectsCount} title={<TextWithPlural type="project" count={projectsCount} />} />
        <CardData value={needsCount} title={<TextWithPlural type="need" count={needsCount} />} />
      </div>
      {/* Star icon */}
      {(has_saved !== undefined || source === 'algolia') && (
        <BtnStar itemType="challenges" itemId={id} hasStarred={has_saved} hasNoStat source={source} />
      )}
    </ObjectCard>
  );
};

const CardData = ({ value, title }) => (
  <div tw="flex flex-col justify-center items-center">
    <div tw="font-bold font-size[17px] -mb-1">{value}</div>
    <div tw="text-gray-500 font-medium text-sm">{title}</div>
  </div>
);
const Hr = (props) => <div tw="-mx-4 border-0 border-t border-solid border-[rgba(0, 0, 0, 0.07)]" {...props} />;

export default ChallengeCard;
