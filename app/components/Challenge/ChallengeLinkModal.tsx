import React, { useState, useMemo, FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from 'contexts/apiContext';
import useGet from 'hooks/useGet';
import Button from 'components/primitives/Button';
import Alert from 'components/Tools/Alert';
import Loading from '../Tools/Loading';
import SpinLoader from '../Tools/SpinLoader';

interface PropsModal {
  alreadyPresentChallenges: any[];
  objectId: number;
  objectType: 'programs' | 'spaces';
  mutateChallenges: (data?: any, shouldRevalidate?: boolean) => Promise<any>;
  closeModal: () => void;
}
export const ChallengeLinkModal: FC<PropsModal> = ({
  alreadyPresentChallenges,
  objectId,
  objectType,
  mutateChallenges,
  closeModal,
}) => {
  const { data: dataChallengesMine, error } = useGet('/api/challenges/mine');
  const [selectedChallenge, setSelectedChallenge] = useState(null);
  const [sending, setSending] = useState(false);
  const [requestSent, setRequestSent] = useState(false);
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);
  const api = useApi();
  const { t } = useTranslation('common');

  // Filter the challenges that are already in this challenge so you don't add it twice!
  const filteredChallenges = useMemo(() => {
    if (dataChallengesMine) {
      return dataChallengesMine
        .filter(({ program }) => program.id === -1) // check that does not belong to another program
        .filter(({ space }) => space.id === -1) // and space
        .filter((challengeMine) => {
          // Check if my challenge is found in alreadyPresentChallenges
          const isMyChallengeAlreadyPresent = alreadyPresentChallenges.find((alreadyPresentChallenge) => {
            return alreadyPresentChallenge.id === challengeMine.id;
          });
          // We keep only the ones that are not present
          return !isMyChallengeAlreadyPresent;
        });
    }
  }, [dataChallengesMine, alreadyPresentChallenges]);

  const onSubmit = async (e) => {
    e.preventDefault();
    setSending(true);
    // Link this challenge to the program then mutate the cache of the challenges from the parent prop
    if ((selectedChallenge as { id: number })?.id) {
      await api
        .put(`/api/${objectType}/${objectId}/challenges/${(selectedChallenge as { id: number }).id}`)
        .catch(() =>
          console.error(`Could not PUT/link objectId=${objectId} with challenge challengeId=${selectedChallenge.id}`)
        );
      setSending(false);
      setRequestSent(true);
      setIsButtonDisabled(true);
      mutateChallenges({ challenges: [...alreadyPresentChallenges, selectedChallenge] });
      setTimeout(() => {
        // close modal after 3.5sec
        closeModal();
      }, 3500);
    }
  };
  const onChallengeSelect = (e) => {
    setSelectedChallenge(filteredChallenges.find((item) => item.id === parseInt(e.target.value)));
    setIsButtonDisabled(false);
  };

  return (
    <div>
      {!filteredChallenges ? (
        <Loading />
      ) : filteredChallenges?.length > 0 ? (
        <form style={{ textAlign: 'left' }}>
          {filteredChallenges.map((challenge, index) => (
            <div className="form-check" key={index} style={{ height: '50px' }}>
              <input
                type="radio"
                className="form-check-input"
                name="exampleRadios"
                id={`challenge-${index}`}
                value={challenge.id}
                onChange={onChallengeSelect}
              />
              <label className="form-check-label" htmlFor={`challenge-${index}`}>
                {challenge.title}
              </label>
            </div>
          ))}

          <div className="btnZone">
            <Button type="submit" disabled={isButtonDisabled || sending} onClick={onSubmit} tw="mb-2">
              <>
                {sending && <SpinLoader />}
                {t('attach.challenge.btnSend')}
              </>
            </Button>
            {requestSent && <Alert type="success" message={t('attach.challenge.btnSendEnded')} />}
          </div>
        </form>
      ) : (
        <div className="noChallenge" style={{ textAlign: 'center' }}>
          {t('attach.challenge.noChallenge')}
        </div>
      )}
    </div>
  );
};
