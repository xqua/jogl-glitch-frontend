import Link from 'next/link';
import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
// import { displayObjectDate, isGroupNotChallenge } from 'utils/utils';
import ReactTooltip from 'react-tooltip';
import { statusToStep } from './ChallengeStatus';
import { Challenge } from 'types';
import InfoStats from 'components/Tools/InfoStats';
import useGet from '~/hooks/useGet';

interface Props {
  challenge: Challenge;
}

const ChallengeInfo: FC<Props> = ({ challenge }) => {
  const { t } = useTranslation('common');
  const { data: dataExternalLink } = useGet<{ url: string; icon_url: string }[]>(
    `/api/challenges/${challenge.id}/links`
  );

  const statusStep = statusToStep(challenge.status);

  // const getDaysLeft = (endDate) => {
  //   const now = new Date();
  //   const end = new Date(endDate);
  //   const daysLeft = (end - now) / 1000 / 60 / 60 / 24;
  //   return Math.ceil(daysLeft <= 0 ? 0 : daysLeft);
  // };

  return (
    <div tw="flex-shrink-0 md:p-4 md:w-80 md:pt-6">
      <div tw="flex flex-wrap mb-2 justify-around md:mt-0">
        <InfoStats object="challenge" type="project" tab="projects" count={challenge.projects_count} />
        <InfoStats object="challenge" type="member" tab="members" count={challenge.members_count} />
        <InfoStats object="challenge" type="need" tab="needs" count={challenge.needs_count} />
      </div>
      <hr tw="mb-4" />
      <div tw="grid gap-4 grid-cols-1 items-center text-center">
        {challenge.status !== 'archived' && (
          <div tw="flex items-center justify-between flex-wrap">
            <div>{t('attach.status')}</div>
            <div tw="inline-flex space-x-2 items-center">
              <div
                tw="rounded-full h-2 w-2"
                style={{ backgroundColor: challenge.status !== 'draft' ? statusStep[1] : 'grey' }}
              />
              <div
                style={{ color: challenge.status !== 'draft' ? statusStep[1] : 'grey' }}
                tw="flex w-[fit-content] rounded-md items-center"
                data-tip={t('entity.info.status.title')}
                data-for="challengeCard_status"
              >
                <div tw="font-medium">
                  {challenge.status !== 'draft' || challenge.status !== 'completed' || challenge.status !== 'archived'
                    ? t(`entity.info.status.active`)
                    : t(`entity.info.status.${challenge.status}`)}
                  {/* {isGroupNotChallenge(challenge.created_at, challenge.custom_type)
                    ? t(`entity.info.status.${challenge.status}`)
                    : t(`challenge.info.status_${challenge.status}`)} */}
                </div>
              </div>
              {/* {challenge.status !== 'draft' && !isGroupNotChallenge(challenge.created_at, challenge.custom_type) && (
                <span tw="text-gray-500">({statusStep[0]}/4)</span>
              )} */}
              <ReactTooltip id="challengeCard_status" effect="solid" />
            </div>
          </div>
        )}

        {/* {challenge.launch_date && (
          <div tw="flex items-center justify-between flex-wrap">
            <div>{t('entity.info.launch_date')}:</div>
            <span tw="font-bold">{displayObjectDate(challenge.launch_date, 'LL')}</span>
          </div>
        )}
        {challenge.final_date && (
          <div tw="flex items-center justify-between flex-wrap">
            <div>{t('entity.info.final_date')}:</div>
            <span tw="font-bold">
              {displayObjectDate(challenge.final_date, 'LL')}
              {getDaysLeft(challenge.final_date) > 0 && (
                <span>
                  &nbsp;({getDaysLeft(challenge.final_date)}
                  &nbsp;
                  {t('program.due.days')}
                  {getDaysLeft(challenge.final_date) > 1 ? 's' : ''}
                  {t('program.due.left')})
                </span>
              )}
            </span>
          </div>
        )}
        {challenge.end_date && (
          <div tw="flex items-center justify-between flex-wrap">
            <div>{t('entity.info.end_date')}:</div>
            <span tw="font-bold">{displayObjectDate(challenge.end_date, 'LL')}</span>
          </div>
        )} */}
        {challenge.program.id !== -1 && ( // if program id is !== -1 (meaning challenge is not attached to a program), display program name and link
          <div tw="flex items-center justify-between flex-wrap">
            <div>{t('entity.info.program_title')}</div>
            <span tw="font-bold text-left">
              <Link href={`/program/${challenge.program.short_title}`}>
                <a>{challenge.program.title}</a>
              </Link>
            </span>
          </div>
        )}
        {challenge.space.id !== -1 && ( // if space id is !== -1 (meaning challenge is not attached to a space), display space name and link
          <div tw="flex items-center justify-between flex-wrap">
            <div>{t('space.title')}</div>
            <span tw="font-bold text-left">
              <Link href={`/space/${challenge.space.short_title}`}>
                <a>{challenge.space.title}</a>
              </Link>
            </span>
          </div>
        )}
      </div>
      {dataExternalLink && dataExternalLink?.length !== 0 && (
        <>
          <hr tw="mb-4" />
          <div tw="grid gap-4 grid-cols-1 items-center text-center">
            <div tw="flex items-center justify-between flex-wrap">
              <div>{t('general.externalLink.findUs')}</div>
              <div tw="flex flex-wrap gap-3">
                {[...dataExternalLink].map((link, i) => (
                  <a tw="items-center" key={i} href={link.url} target="_blank">
                    <img tw="w-10 hover:opacity-80" src={link.icon_url} />
                  </a>
                ))}
              </div>
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default ChallengeInfo;
