// set array of step numbers and colors depending on status value
export const statusToStep: any = (status) => {
  switch (status) {
    case 'soon':
      return [0, '#A4B6C5'];
    case 'accepting':
      // return [1, '#1CB5AC'];
      return [1, '#1CB5AC'];
    case 'evaluating':
      // return [2, '#4F4BFF'];
      return [2, '#1CB5AC'];
    case 'active':
      // return [3, '#DE3E96'];
      return [3, '#1CB5AC'];
    case 'completed':
      // return [4, '#643CC6'];
      return [4, '#1CB5AC'];
    default:
      return undefined;
  }
};
