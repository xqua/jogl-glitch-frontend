import { Edit } from '@emotion-icons/boxicons-solid/Edit';
import { FileEarmarkPlus } from '@emotion-icons/bootstrap/FileEarmarkPlus';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { FC, useContext } from 'react';
import useTranslation from 'next-translate/useTranslation';
import H1 from 'components/primitives/H1';
import { useModal } from 'contexts/modalContext';
import { UserContext } from 'contexts/UserProvider';
import useGet from 'hooks/useGet';
import { Challenge, Project } from 'types';
import Button from '../primitives/Button';
import { ProjectLinkModal } from '../Project/ProjectLinkModal';
import BtnFollow from 'components/Tools/BtnFollow';
import BtnJoin from '../Tools/BtnJoin';
import ShareBtns from '../Tools/ShareBtns/ShareBtns';
import ChallengeInfo from './ChallengeInfo';
import { confAlert, isGroupNotChallenge } from '~/utils/utils';
import ReactTooltip from 'react-tooltip';
import { useApi } from '~/contexts/apiContext';
// import InfoSkillSdg from 'components/Tools/Info/InfoSkillSdg';

interface Props {
  challenge: Challenge;
  lang: string;
}

const ChallengeHeader: FC<Props> = ({ challenge, lang = 'en' }) => {
  const { t } = useTranslation('common');
  const user = useContext(UserContext);
  const router = useRouter();
  const api = useApi();
  const { showModal, closeModal } = useModal();
  const { data: projectsData, mutate: mutateProjects } = useGet<{
    projects: Project[];
  }>(`/api/challenges/${challenge.id}/projects`);

  const {
    has_followed,
    id,
    is_member,
    is_owner,
    members_count,
    followers_count,
    status,
    title,
    title_fr,
    program,
    space,
    short_description,
    short_description_fr,
    is_admin,
    short_title,
  } = challenge;

  const isMemberOfSpace = space.id !== -1 ? challenge.is_member_of_space : true;
  const isMember = is_member || is_admin || isMemberOfSpace;

  return (
    <header tw="relative w-full">
      <div tw="flex flex-wrap divide-x divide-gray-200 md:flex-nowrap">
        <div tw="hidden md:block">
          <ChallengeInfo challenge={challenge} />
        </div>
        <div tw="px-4 py-4 md:px-8 md:pt-6">
          <div tw="inline-flex items-center mb-1">
            <H1 tw="pr-3 font-size[1.8rem] md:font-size[2.3rem]">{(lang === 'fr' && title_fr) || title}</H1>
            <div tw="hidden md:block">
              <ShareBtns type="challenge" specialObjId={id} />
            </div>
          </div>
          <p tw="mt-4 text-gray-600">{(lang === 'fr' && short_description_fr) || short_description}</p>

          <div tw="flex flex-wrap gap-3 mb-2">
            {is_admin && (
              <Link href={`/challenge/${short_title}/edit`}>
                <Button tw="flex justify-center items-center">
                  <Edit size={23} title="Edit challenge" />
                  {t('entity.form.btnAdmin')}
                </Button>
              </Link>
            )}
            {/* show button if: user is connected, status is accepting OR it's a group so can't have accepting status, is attached to space or program, is member of space (if attached) */}
            {user.isConnected &&
              projectsData?.projects &&
              (program.id !== -1 || space.id !== -1) &&
              (status === 'accepting' || isGroupNotChallenge(challenge.created_at, challenge.custom_type)) &&
              isMemberOfSpace && (
                <div
                  {...(!isMember && {
                    'data-tip': 'You first need to be a member of the group or the parent space.',
                    'data-for': 'cantAddExplanation',
                  })}
                >
                  <Button
                    tw="flex justify-center items-center"
                    onClick={() => {
                      showModal({
                        children: (
                          <ProjectLinkModal
                            alreadyPresentProjects={projectsData.projects}
                            challengeId={id}
                            programId={program.id}
                            spaceId={space.id}
                            mutateProjects={mutateProjects}
                            closeModal={closeModal}
                            isMember={is_member}
                            hasFollowed={has_followed}
                          />
                        ),
                        title: t('attach.project.title'),
                        maxWidth: '50rem',
                        allowOverflow: true,
                      });
                    }}
                    disabled={!isMember}
                  >
                    <FileEarmarkPlus size={23} title="Add my project" />
                    {t('attach.project.title')}
                  </Button>
                  <ReactTooltip
                    id="cantAddExplanation"
                    effect="solid"
                    role="tooltip"
                    type="dark"
                    className="forceTooltipBg"
                  />
                </div>
              )}
            {!is_owner && ( // show join button only if we are not owner of challenge
              <BtnJoin
                joinState={is_member}
                itemType="challenges"
                itemId={id}
                count={members_count}
                showMembersModal={() =>
                  router.push(`/challenge/${router.query.short_title}?tab=members`, undefined, { shallow: true })
                }
                onJoin={() => {
                  if (space.id !== -1) {
                    api.put(`/api/spaces/${space.id}/join`); // join parent space when joining challenge/group
                    api.put(`/api/spaces/${space.id}/follow`); // follow parent space when joining challenge/group
                    confAlert.fire({
                      icon: 'success',
                      title: 'By joining this group, you also requested to become a member of its parent space.',
                    });
                  }
                }}
              />
            )}
            <BtnFollow followState={has_followed} itemType="challenges" itemId={challenge.id} count={followers_count} />
            <div tw="md:hidden">
              <ShareBtns type="challenge" specialObjId={id} />
            </div>
          </div>
        </div>
      </div>
      {/* {(interests?.length > 0 || skills?.length > 0) && (
        <div>
          <hr tw="mb-4" />
          <div tw="hidden md:block">
            <InfoSkillSdg interests={interests} skills={skills} />
          </div>
        </div>
      )} */}
    </header>
  );
};

export default ChallengeHeader;
