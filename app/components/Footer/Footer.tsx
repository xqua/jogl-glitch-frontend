import React from 'react';
import Link from 'next/link';
import useTranslation from 'next-translate/useTranslation';
import P from '../primitives/P';
import Image from 'components/primitives/Image';
import Button from '../primitives/Button';
import { Twitter, Facebook, Linkedin, Medium, Gitlab, Instagram, Slack } from '@emotion-icons/fa-brands';
import { css } from 'twin.macro';

const Footer = () => {
  const { t } = useTranslation('common');
  return (
    <footer tw="bg-gray-100" id="footer">
      <h2 id="footerHeading" tw="sr-only">
        Footer
      </h2>
      <div tw="px-1 sm:px-4 xl:px-0">
        <div tw="max-w-7xl mx-auto py-12 px-4 sm:px-6 lg:(pt-10 pb-5 px-0)">
          <div tw="xl:(grid grid-cols-3 gap-8)">
            <div tw="space-y-8 mb-10 xl:(col-span-1 mb-0)">
              <div
                css={[
                  css`
                    > div > div {
                      width: 100px !important;
                    }
                  `,
                ]}
              >
                <Image src="/images/logo_single.svg" alt="jogl logo rocket" quality={50} tw="h-[100px]! w-[100px]!" />
              </div>
              <p tw="text-gray-500 text-base text-justify sm:text-left">{t('footer.JOGLdescription')}</p>
              <div tw="flex flex-wrap gap-6">
                <a href="https://twitter.com/justonegiantlab" target="_blank" rel="noopener">
                  <Twitter size={25} title="Twitter icon" tw="text-gray-500 hover:color[#00aced]" />
                </a>
                <a href="https://www.facebook.com/justonegiantlab/" target="_blank" rel="noopener">
                  <Facebook size={25} title="Facebook icon" tw="text-gray-500 hover:color[#0077e2]" />
                </a>
                <a href="https://www.linkedin.com/company/jogl/about/" target="_blank" rel="noopener">
                  <Linkedin size={25} title="Linkedin icon" tw="text-gray-500 hover:color[#0277b5]" />
                </a>
                <a href="https://www.instagram.com/justonegiantlab/" target="_blank" rel="noopener">
                  <Instagram size={25} title="Instagram icon" tw="text-gray-500 hover:color[#c43670]" />
                </a>
                <a href="https://gitlab.com/JOGL/JOGL" target="_blank" rel="noopener">
                  <Gitlab size={25} title="Gitlab icon" tw="text-gray-500 hover:color[#fc6d26]" />
                </a>
                <a href="https://medium.com/justonegiantlab/latest" target="_blank" rel="noopener">
                  <Medium size={25} title="medium icon" tw="text-gray-500 hover:text-black" />
                </a>
                <a
                  href="https://join.slack.com/t/joglcommunity/shared_invite/zt-enxkz2hi-8SGgQNUcps1oQpQ0tooo8Q"
                  target="_blank"
                  rel="noopener"
                >
                  <Slack size={25} title="medium icon" tw="text-gray-500 hover:text-black" />
                </a>
              </div>
            </div>
            <nav tw="grid grid-cols-2">
              <div>
                <h3 tw="text-xl text-gray-600">About</h3>
                <div tw="mt-4 space-y-4">
                  <a href="https://jogl.io" target="_blank" rel="noopener" tw="block text-gray-600">
                    {t('footer.aboutJOGL')}
                  </a>
                  <Link href="/ethics-pledge" passHref>
                    <a tw="block text-gray-600">{t('footer.ethicsPledge')}</a>
                  </Link>
                  <Link href="/terms" passHref>
                    <a tw="block text-gray-600">{t('footer.termsConditions')}</a>
                  </Link>
                  <Link href="/data" passHref>
                    <a tw="block text-gray-600">{t('footer.data')}</a>
                  </Link>
                </div>
              </div>
              <div>
                <h3 tw="text-xl text-gray-600">Links</h3>
                <div tw="mt-4 space-y-4">
                  <a href="https://gitlab.com/JOGL/JOGL" target="_blank" rel="noopener" tw="block text-gray-600">
                    {t('footer.contribute')}
                  </a>
                  <a href="https://jogl.io/opportunities" target="_blank" rel="noopener" tw="block text-gray-600">
                    {t('footer.jobs')}
                  </a>
                  <a href="https://jogl.tawk.help/" target="_blank" rel="noopener" tw="block text-gray-600">
                    {t('faq.title')}
                  </a>
                  <a
                    href="https://gitlab.com/JOGL/frontend-v0.1/-/blob/master/CHANGELOG.md"
                    target="_blank"
                    rel="noopener"
                    tw="block text-gray-600"
                  >
                    Changelog
                  </a>
                  <a
                    href="https://www.canva.com/design/DAEqvWEqkr0/wExrqVtmfOYzKsEkgAmH4Q/view"
                    target="_blank"
                    rel="noopener"
                    tw="block text-gray-600"
                  >
                    {t('user.profile.welcomeKit')}
                  </a>
                </div>
              </div>
            </nav>
            <div tw="mt-12 xl:mt-0">
              <h3 tw="text-xl text-gray-600">Contact</h3>
              {/* <p>Centre de Recherches Interdisciplinaires (CRI) - 8 bis rue Charles V, 75004, Paris</p> */}
              <div className="contact">
                <div tw="inline-flex space-x-2 mt-1">
                  <a href="mailto:hello@jogl.io">
                    <Button>{t('footer.contactUs')}</Button>
                  </a>
                  <a href="mailto:press@jogl.io">
                    <Button btnType="secondary">{t('footer.pressContact')}</Button>
                  </a>
                </div>
              </div>
              <P tw="pt-3 font-size[93%]">
                {t('footer.bugOrRequest.text')}
                &nbsp;
                <a href="https://gitlab.com/JOGL/JOGL/-/issues/new" target="_blank" rel="noopener">
                  {t('footer.bugOrRequest.link')}
                </a>
              </P>
              <hr />
              <div tw="flex flex-col">
                {t('footer.donate.text')}
                <Link href="/donate" passHref>
                  <Button tw="mt-4">{t('footer.donate.btn')}</Button>
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div tw="mt-3 border-0 border-solid border-t border-gray-200 pt-2 pb-5">
          <div tw="text-base text-gray-400 text-center">
            <p tw="md:mb-0">Just One Giant Lab - {new Date().getFullYear()} | v0.10.4</p>
            <p>{t('footer.CC')}</p>
          </div>
          <div tw="flex justify-center">
            {/* <Button tw="bg-yellow-500 border-yellow-500">
              <a href="mailto:support@jogl.io" target="_blank" rel="noopener">
                {t('footer.help')}
              </a>
            </Button> */}
            <a href="https://www.algolia.com/" target="_blank" rel="noopener">
              <Image src="/images/search-by-algolia.svg" alt="Search by algolia" tw="(w-[168px] h-[24px]!)" />
            </a>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default React.memo(Footer);
