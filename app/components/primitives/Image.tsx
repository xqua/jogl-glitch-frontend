import NextImage from 'next/image';
import { styled } from 'twin.macro';

const BaseImage = styled(NextImage)`
  object-fit: contain;
  position: relative !important;
  height: 100% !important;
`;
const ImgContainer = styled.div`
  position: relative !important;
  width: 100%;
  > div {
    position: unset !important;
    height: 100%;
  }
`;

const Image = (props) => {
  const { ...rest } = props;
  return (
    <ImgContainer>
      <BaseImage {...rest} layout="fill" />
    </ImgContainer>
  );
};

export default Image;
