import { FC } from 'react';
import tw, { css, styled } from 'twin.macro';

interface Props {
  btnType: 'secondary' | 'primary' | 'danger';
  width: string;
  disabled: boolean;
}
const Container: FC<Props> = styled.button(({ btnType = 'primary', width, disabled }) => [
  tw`px-3 py-2 text-base`,
  tw`transition-colors duration-150`,
  btnType === 'secondary' ? tw`bg-white` : btnType === 'danger' ? tw`bg-danger` : tw`bg-primary`,
  btnType === 'secondary' ? tw`text-primary` : tw`text-white`,
  tw`border rounded border-solid focus:(outline-none border-blue-300)`,
  btnType === 'danger' ? tw`border-danger` : tw`border-primary`,
  // btnType === 'secondary' && !disabled && tw`hocus:(text-white bg-primary)`,
  btnType === 'primary' && !disabled && tw`hocus:(bg-[#166098] border-[#166098])`,
  btnType === 'secondary' && !disabled && tw`hocus:(text-[#166098] border-[#166098])`,
  btnType === 'danger' && !disabled && tw`hocus:bg-red-700`,
  // btnType !== 'secondary' && !disabled && tw`hover:opacity[.95]`,
  disabled ? tw`opacity-50 cursor-not-allowed` : tw`cursor-pointer`,
  css`
    width: ${width || 'fit-content'};
  `,
]);
export default function Button(props) {
  return <Container {...props} type={props.type || 'button'} />;
}
