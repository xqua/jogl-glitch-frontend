import tw from 'twin.macro';

const FakeLink = tw.span`underline text-primary cursor-pointer hover:no-underline`;
export default FakeLink;
