import React, { FC, Fragment } from 'react';
import { Project } from 'types';
import InfoStats from 'components/Tools/InfoStats';
import useTranslation from 'next-translate/useTranslation';
import useGet from '~/hooks/useGet';
// import { displayObjectDate, renderOwnerNames } from '~/utils/utils';
import Link from 'next/link';
import ReactTooltip from 'react-tooltip';

interface Props {
  project: Project;
}

const ProjectInfo: FC<Props> = ({ project }) => {
  const { t } = useTranslation('common');
  const { data: dataExternalLink } = useGet<{ url: string; icon_url: string }[]>(`/api/projects/${project.id}/links`);
  const activeChallenges = project?.challenges.filter(({ project_status }) => project_status !== 'pending');
  const activeSpaces = project?.affiliated_spaces.filter((challenge) => challenge[0].affiliation_status !== 'pending');

  return (
    <div tw="flex-shrink-0 md:p-4 md:w-80 md:pt-6">
      <div tw="flex flex-wrap mb-2 justify-around md:mt-0">
        <InfoStats object="project" type="member" tab="members" count={project.members_count} />
        <InfoStats object="project" type="need" tab="needs" count={project.needs_count} />
      </div>
      <hr tw="mb-4" />
      <div tw="grid gap-4 grid-cols-1 items-center text-center">
        <div tw="flex items-center justify-between flex-wrap">
          <div>{t('entity.info.status.title')}:</div>
          <span tw="font-bold">{t(`entity.info.status.${project.status}`)}</span>
        </div>
        {project.maturity && (
          <div tw="flex items-center justify-between flex-wrap">
            <div>{t('project.maturity.title')}:</div>
            <span
              tw="font-bold"
              data-tip={t(`project.maturity.tooltip.${project.maturity}`)}
              data-for="maturity_tooltip"
              tabIndex={0}
            >
              {t(`project.maturity.${project.maturity}`)}
            </span>
            <ReactTooltip id="maturity_tooltip" className="solid-tooltip" />
          </div>
        )}
        {activeChallenges?.length !== 0 && (
          <div tw="flex items-center justify-between flex-wrap">
            <div>{t('project.info.participatingToChallenges')}</div>
            <div tw="text-left">
              {activeChallenges.map((challenge, index) => {
                const rowLen = activeChallenges.length;
                return (
                  <Fragment key={index}>
                    <Link href={`/challenge/${challenge.short_title}`} passHref>
                      <a tw="font-bold">{challenge.title}</a>
                    </Link>
                    {/* add comma, except for last item */}
                    {rowLen !== index + 1 && <span>, </span>}
                  </Fragment>
                );
              })}
            </div>
          </div>
        )}
        {activeSpaces?.length !== 0 && (
          <div tw="flex items-center justify-between flex-wrap">
            <div>{t('project.info.affiliatedToSpaces')}</div>
            <div tw="text-left">
              {activeSpaces?.length !== 0 &&
                activeSpaces.map((space, index) => {
                  return (
                    <Fragment key={index}>
                      <Link href={`/space/${space[0].short_title}`} passHref>
                        <a tw="font-bold">{space[0].title}</a>
                      </Link>
                      {/* add comma, except for last item */}
                      {activeSpaces.length !== index + 1 && <span>, </span>}
                    </Fragment>
                  );
                })}
            </div>
          </div>
        )}
        {dataExternalLink && dataExternalLink?.length !== 0 && (
          <div tw="flex items-center justify-between flex-wrap">
            <div>{t('general.externalLink.findUs')}</div>
            <div tw="flex flex-wrap gap-3">
              {[...dataExternalLink].map((link, i) => (
                <a tw="items-center" key={i} href={link.url} target="_blank">
                  <img tw="w-10 hover:opacity-80" src={link.icon_url} />
                </a>
              ))}
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default ProjectInfo;
