import React, { useState, useMemo, FC, useEffect } from 'react';
import useTranslation from 'next-translate/useTranslation';
import useGet from 'hooks/useGet';
import Button from '../primitives/Button';
import Loading from '../Tools/Loading';
import SpinLoader from '../Tools/SpinLoader';
import Select from 'react-select';
import NeedCreate from '../Need/NeedCreate';

interface PropsModal {
  closeModal: () => void;
}

export const ProjectListNeed: FC<PropsModal> = ({ closeModal }) => {
  const { data: dataProjectsMine, error } = useGet('/api/projects/mine');
  const [selectedProject, setSelectedProject] = useState(null);
  const [sending, setSending] = useState(false);
  const [requestSent, setRequestSent] = useState(false);
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);
  const { t } = useTranslation('common');

  // Filter the projects that are already in this challenge so you don't add it twice!
  const filteredProjects = useMemo(() => {
    return dataProjectsMine?.filter((p) => p.is_admin);
  }, [dataProjectsMine]);

  useEffect(() => {
    filteredProjects?.length !== 0 && setIsButtonDisabled(false);
    filteredProjects?.length !== 0 && setSelectedProject(filteredProjects?.[0]);
  }, [filteredProjects]);

  const onSubmit = (e) => {
    e.preventDefault();
    setSending(true);
    // Link this project to the challenge then mutate the cache of the projects from the parent prop
    if ((selectedProject as { id: number })?.id) {
      setSending(false);
      setRequestSent(true);
      setIsButtonDisabled(true);
    }
  };

  const onProjectSelect = (project) => {
    setSelectedProject(filteredProjects.find((item) => item.title === project.value));
    setIsButtonDisabled(false);
  };

  const optionsList = filteredProjects?.map((option) => {
    return {
      value: option.title,
      label: option.title,
    };
  });

  return (
    <div>
      {!requestSent && (
        <div tw="pb-32">
          <div tw="flex justify-between mb-2">
            <span>First, select a project</span>
            <a href="/project/create" target="_blank">
              {t('general.createProject')}
            </a>
          </div>
          {!filteredProjects ? (
            <Loading />
          ) : filteredProjects?.length > 0 && dataProjectsMine?.filter((project) => project.is_admin).length > 0 ? (
            <>
              <Select
                options={optionsList}
                menuShouldScrollIntoView={true} // force scroll into view
                defaultValue={filteredProjects.length !== 0 && optionsList[0]}
                noOptionsMessage={() => null}
                onChange={onProjectSelect}
              />
              <div className="btnZone">
                <Button type="submit" disabled={isButtonDisabled || sending} onClick={onSubmit} tw="mb-2">
                  <>
                    {sending && <SpinLoader />}
                    {t('entity.form.btnNext')}
                  </>
                </Button>
              </div>
            </>
          ) : (
            <div className="noProject" style={{ textAlign: 'center' }}>
              {t('attach.project.noProject')}
            </div>
          )}
        </div>
      )}
      {requestSent && <NeedCreate projectId={(selectedProject as { id: number })?.id} refresh={closeModal} noButton />}
    </div>
  );
};
