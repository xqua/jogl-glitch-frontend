import Grid from '../Grid';
import ProjectCard from './ProjectCard';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';

export default function ProjectRecommended({ listProjects, cardFormat = undefined, recommendedIds = [] }) {
  return (
    <Grid tw="pt-3">
      {!listProjects ? (
        <Loading />
      ) : listProjects.length === 0 ? (
        <NoResults type="project" />
      ) : (
        listProjects.map((project, i) => (
          <ProjectCard
            key={i}
            id={project.targetable_node?.project.id}
            title={project.targetable_node?.project.title}
            shortTitle={project.targetable_node?.project.short_title}
            short_description={project.targetable_node?.project.short_description}
            members_count={project.targetable_node?.project.members_count}
            needs_count={project.targetable_node?.project.needs_count}
            clapsCount={project.targetable_node?.project.claps_count}
            postsCount={project.targetable_node?.project.posts_count}
            has_saved={project.targetable_node?.project.has_saved}
            reviewsCount={project.targetable_node?.project.reviews_count}
            banner_url={project.targetable_node?.project.banner_url || '/images/default/default-project.jpg'}
            // show skills only if cardFormat is not compact
            {...(cardFormat !== 'compact' && { skills: project.targetable_node?.project.skills })}
            cardFormat={cardFormat}
            recommendedIds={recommendedIds}
          />
        ))
      )}
    </Grid>
  );
}
