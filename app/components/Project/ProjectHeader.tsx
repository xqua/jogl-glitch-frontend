/* eslint-disable camelcase */
import { Edit } from '@emotion-icons/fa-solid/Edit';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import React, { FC } from 'react';
import H1 from 'components/primitives/H1';
import Button from '../primitives/Button';
import { Project } from 'types';
import ShareBtns from '../Tools/ShareBtns/ShareBtns';
import BtnFollow from 'components/Tools/BtnFollow';
import ProjectInfo from './ProjectInfo';
import BtnJoin from '../Tools/BtnJoin';
import { useRouter } from 'next/router';
import { linkify } from '~/utils/utils';
import InfoDefaultComponent from '../Tools/Info/InfoDefaultComponent';
// import BtnReview from '../Tools/BtnReview';
// import useUserData from '~/hooks/useUserData';
import { PatchCheckFill } from '@emotion-icons/bootstrap';
import ReactTooltip from 'react-tooltip';
import { theme } from 'twin.macro';

interface Props {
  project: Project;
}

const ProjectHeader: FC<Props> = ({ project }) => {
  const { t } = useTranslation('common');
  const router = useRouter();
  // const { userData } = useUserData();

  return (
    <>
      <header tw="relative w-full">
        <div tw="flex flex-wrap divide-x divide-gray-200 md:flex-nowrap">
          <div tw="hidden md:block">
            <ProjectInfo project={project} />
          </div>

          <div tw="px-4 py-4 md:px-8 md:pt-6 w-full">
            <div tw="inline-flex items-center mb-1">
              <H1 tw="pr-3 font-size[1.8rem] md:font-size[2.3rem]">{project?.title}</H1>
              <div tw="flex">
                {/* {userData &&
              project.is_reviewer && ( // show review button only to JOGL global reviewer
                  <BtnReview itemType="projects" itemId={project?.id} reviewState={project?.reviews_count > 0} />
                )} */}
                {((project.reviews_count > 0 && !project.is_reviewer) || project.has_valid_proposal) && (
                  // show reviewed badge if project has been reviewed
                  <div tw="inline-flex items-center">
                    <PatchCheckFill
                      size="24"
                      title="About reviewed project"
                      data-tip={t('project.info.reviewedTooltip')}
                      data-for="reviewed_project"
                      color={theme`colors.primary`}
                    />
                    <ReactTooltip id="reviewed_project" effect="solid" type="dark" className="forceTooltipBg" />
                  </div>
                )}
              </div>
              <div tw="hidden md:block ml-3">
                <ShareBtns type="project" specialObjId={project?.id} />
              </div>
            </div>
            <div tw="text-gray-600 mt-4">
              <InfoDefaultComponent content={linkify(project?.short_description)} containsHtml />
            </div>
            {/* {(project.interests?.length > 0 || project.skills?.length > 0) && (
              <div tw="hidden md:block">
                <ProjectSkillSdg interests={project.interests} skills={project.skills} />
              </div>
            )} */}

            <div tw="flex flex-wrap gap-3 mb-2">
              {project.is_admin && (
                <Link href={`/project/${project.id}/edit`} passHref>
                  <Button tw="flex justify-center items-center">
                    <Edit size={18} title="Edit project" />
                    {t('entity.form.btnAdmin')}
                  </Button>
                </Link>
              )}
              {!project?.is_owner && (
                <BtnJoin
                  joinState={project.is_pending ? 'pending' : project.is_member || project.is_admin}
                  isPrivate={project.is_private}
                  itemType="projects"
                  itemId={project.id}
                  count={project.members_count}
                  showMembersModal={() =>
                    router.push(`/project/${router.query.id}?tab=members`, undefined, { shallow: true })
                  }
                />
              )}
              <BtnFollow
                followState={project.has_followed}
                itemType="projects"
                itemId={project.id}
                count={project.followers_count}
              />
              <div tw="md:hidden">
                <ShareBtns type="project" specialObjId={project?.id} />
              </div>
            </div>
          </div>
        </div>
      </header>
      {/* {(project.interests?.length > 0 || project.skills?.length > 0) && (
        <div tw="hidden md:block mb-2 border-t border-solid border-gray-200 px-4">
          <ProjectSkillSdg interests={project.interests} skills={project.skills} />
        </div>
      )} */}
    </>
  );
};

export default React.memo(ProjectHeader);
