import useTranslation from 'next-translate/useTranslation';
import { FC, useState } from 'react';
import Alert from 'components/Tools/Alert';
import { useApi } from 'contexts/apiContext';
import { Project } from 'types';
import A from '../primitives/A';
import Button from '../primitives/Button';
import SpinLoader from '../Tools/SpinLoader';

interface Props {
  project: Project;
  parentId?: number;
  parentType: 'challenges' | 'spaces';
  callBack: () => void;
}

const ProjectAdminCard: FC<Props> = ({ project, parentType, parentId, callBack }) => {
  const [sending, setSending] = useState('');
  const [error, setError] = useState('');
  const api = useApi();
  const { t } = useTranslation('common');

  // have different api route depending on parentType
  const route =
    parentType === 'challenges'
      ? `/api/challenges/${parentId}/projects/${project.id}`
      : `/api/projects/${project.id}/affiliations/spaces/${parentId}`;

  const onSuccess = () => {
    setSending('');
    callBack();
    parentType === 'spaces' && api.patch(`/api/spaces/${parentId}`, { space: { id: parentId } });
  };

  const onError = (type, err) => {
    console.error(`Couldn't ${type} ${parentType} with parentId=${parentId}`, err);
    setSending('');
    setError(t('err-'));
  };

  const acceptProject = () => {
    setSending('accepted');
    parentType === 'challenges'
      ? api
          .post(route, { status: 'accepted' })
          .then(() => onSuccess())
          .catch((err) => onError('post', err))
      : api
          .patch(route, { affiliations: { status: 'accepted' } })
          .then(() => onSuccess())
          .catch((err) => onError('post', err));
  };

  const removeProject = () => {
    setSending('remove');
    api
      .delete(route)
      .then(() => onSuccess())
      .catch((err) => onError('delete', err));
  };

  const isProjectPending =
    parentType === 'challenges'
      ? project.challenges.find((obj) => obj.challenge_id === parentId)?.project_status === 'pending'
      : project.affiliated_spaces?.find((obj) => obj[0].id === parentId)?.[0].affiliation_status === 'pending';

  if (project !== undefined) {
    let imgTodisplay = '/images/default/default-project.jpg';
    if (project.banner_url_sm) {
      imgTodisplay = project.banner_url_sm;
    }

    const bgBanner = {
      backgroundImage: `url(${imgTodisplay})`,
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      border: '1px solid #ced4da',
      borderRadius: '50%',
      height: '50px',
      width: '50px',
    };

    return (
      <div>
        <div tw="flex justify-between" key={project.id}>
          <div tw="flex items-center pb-3 space-x-3 sm:pb-0">
            <div style={{ width: '50px' }}>
              <div style={bgBanner} />
            </div>
            <div tw="flex flex-col">
              <div>
                <A href={`/project/${project.id}/${project.short_title}`}>{project.title}</A>
                {project.status === 'draft' && ` (${project.status})`}
              </div>
              <div tw="flex flex-col text-[93%] pt-1">
                {t('attach.members')}
                {project.members_count}
              </div>
            </div>
          </div>
          <div tw="flex items-center ml-3">
            {isProjectPending ? ( // if project status is pending, display the accept/reject buttons
              <div tw="flex flex-col space-y-2 xs:(flex-row space-x-2 space-y-0)">
                <Button
                  tw="text-green-500 border-green-500 bg-white hover:(text-white bg-green-500)"
                  disabled={sending === 'accepted'}
                  onClick={acceptProject}
                >
                  {sending === 'accepted' && <SpinLoader />}
                  {t('general.accept')}
                </Button>
                <Button
                  tw="text-danger border-danger bg-white hover:(text-white bg-danger)"
                  disabled={sending === 'remove'}
                  onClick={removeProject}
                >
                  {sending === 'remove' && <SpinLoader />}
                  {t('general.reject')}
                </Button>
              </div>
            ) : (
              // else display the remove button
              <Button btnType="danger" disabled={sending === 'remove'} onClick={removeProject}>
                {sending === 'remove' && <SpinLoader />}
                {t('attach.remove')}
              </Button>
            )}
            {error !== '' && <Alert type="danger" message={error} />}
          </div>
        </div>
        <hr />
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};
export default ProjectAdminCard;
