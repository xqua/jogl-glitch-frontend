import Link from 'next/link';
import React, { FC } from 'react';
import ObjectCard from 'components/Cards/ObjectCard';
import H2 from 'components/primitives/H2';
import Title from 'components/primitives/Title';
import BtnStar from 'components/Tools/BtnStar';
import { DataSource } from 'types';
import { TextWithPlural } from 'utils/managePlurals';
import Chips from '../Chip/Chips';
import { theme } from 'twin.macro';
import { PatchCheckFill } from '@emotion-icons/bootstrap';
import useTranslation from 'next-translate/useTranslation';
import ReactTooltip from 'react-tooltip';
import tw from 'twin.macro';
import { logEventToGA } from '~/utils/analytics';
import useUser from '~/hooks/useUser';

interface Props {
  id: number;
  title: string;
  shortTitle: string;
  short_description: string;
  postsCount?: number;
  followersCount?: number;
  savesCount?: number;
  members_count: number;
  needs_count: number;
  has_saved?: boolean;
  banner_url: string;
  width?: string;
  cardFormat?: string;
  source?: DataSource;
  skills?: string[];
  reviewsCount?: number;
  chip?: string;
  status?: string;
  recommendedIds?: number[];
}
const ProjectCard: FC<Props> = ({
  id,
  title,
  shortTitle = undefined,
  short_description,
  postsCount = 0,
  followersCount,
  savesCount = 0,
  members_count,
  needs_count,
  has_saved,
  banner_url = '/images/default/default-project.jpg',
  width,
  cardFormat,
  source,
  skills,
  reviewsCount = 0,
  chip,
  status,
  recommendedIds = [],
}) => {
  const projUrl = `/project/${id}/${shortTitle}`;
  const { t } = useTranslation('common');
  const { user } = useUser();
  return (
    <ObjectCard imgUrl={banner_url} href={projUrl} width={width} chip={chip} cardFormat={cardFormat}>
      {cardFormat === 'showObjType' && (
        <div tw="bg-[#A9CCF7] w-[calc(100% + 32px)] pl-3 flex flex-wrap -mx-4 mt-[-.8rem] mb-[.8rem]">
          <span tw="px-1 space-x-1 font-bold text-sm">{t('project.title')}</span>
        </div>
      )}
      <div css={[tw`inline-flex items-center`, cardFormat !== 'compact' && tw`md:h-14`]}>
        <Link href={projUrl} passHref>
          <Title
            onClick={() =>
              recommendedIds?.length !== 0 &&
              logEventToGA('Reco click', 'Recommendation', `[${user.id},${id},"user", ${recommendedIds}]`, {
                userId: user.id,
                itemId: id,
                itemType: 'user',
                recommendedIds,
              })
            }
          >
            <H2
              css={[
                tw`word-break[break-word] items-center line-clamp-2`,
                // cardFormat !== 'compact' && tw`md:text-3xl`,
              ]}
            >
              {title}
            </H2>
          </Title>
        </Link>
        {reviewsCount === 1 && (
          <div tw="max-width[20px] pt-1 pl-2">
            <PatchCheckFill
              size="15"
              title="About reviewed project"
              data-tip={t('project.info.reviewedTooltip')}
              data-for="reviewed_project"
              color={theme`colors.primary`}
            />
            <ReactTooltip id="reviewed_project" effect="solid" type="dark" className="forceTooltipBg" />
          </div>
        )}
      </div>
      <Hr tw="mt-2 pt-4" />
      {/* Status */}
      {status === 'draft' && (
        <div tw="inline-flex space-x-2 items-center mb-2">
          <div tw="rounded-full h-2 w-2" style={{ backgroundColor: 'grey' }} />
          <div
            style={{ color: 'grey' }}
            tw="flex w-[fit-content] rounded-md items-center"
            data-tip={t('entity.info.status.title')}
            data-for="spaceCard_status"
          >
            <div tw="font-medium">{t(`entity.info.status.draft`)}</div>
          </div>
          <ReactTooltip id="spaceCard_status" effect="solid" />
        </div>
      )}
      {/* <div style={{ color: "grey", paddingBottom: "5px" }}>
        <span> Last active today </span> <span> Prototyping </span>
      </div> */}
      {/* Description */}
      <div css={[tw`flex-1 line-clamp-4`, cardFormat !== 'compact' ? tw`mb-8` : tw`mb-2`]}>{short_description}</div>
      {/* <div css={[tw`h-20 line-clamp-4`, cardFormat !== 'compact' ? tw`mb-8` : tw`mb-2`]}>{short_description}</div> */}
      {/* SKills */}
      {skills && (
        <>
          <p tw="text-gray-400 mb-0 text-sm">{t('peerReview.form.keywords')}</p>
          <Chips
            data={skills.map((skill) => ({
              title: skill,
              href: `/search/projects/?refinementList[skills][0]=${skill}`,
            }))}
            overflowLink={`/project/${id}/${shortTitle}`}
            type="skills"
            showCount={3}
            smallChips
          />
        </>
      )}
      {/* Stats */}
      {cardFormat !== 'compact' && (
        <>
          <Hr tw="mt-3 pt-2" />
          <div tw="items-center justify-around space-x-2 flex flex-wrap">
            <CardData value={members_count} title={<TextWithPlural type="member" count={members_count} />} />
            {/* <CardData value={savesCount} title={<TextWithPlural type="star" count={savesCount} />} /> */}
            <CardData value={needs_count} title={<TextWithPlural type="need" count={needs_count} />} />
            {/* {postsCount > 0 && ( */}
            <CardData value={postsCount} title={<TextWithPlural type="post" count={postsCount} />} />
            {/* )} */}
          </div>
        </>
      )}
      {/* Star icon */}
      {(has_saved !== undefined || source === 'algolia') && (
        <BtnStar itemType="projects" itemId={id} hasStarred={has_saved} hasNoStat source={source} />
      )}
    </ObjectCard>
  );
};

const CardData = ({ value, title }) => (
  <div tw="flex flex-col justify-center items-center">
    <div tw="font-bold font-size[17px] -mb-1">{value}</div>
    <div tw="text-gray-500 font-medium text-sm">{title}</div>
  </div>
);
const Hr = (props) => <div tw="-mx-4 border-0 border-t border-solid border-color[rgba(0, 0, 0, 0.07)]" {...props} />;

export default ProjectCard;
