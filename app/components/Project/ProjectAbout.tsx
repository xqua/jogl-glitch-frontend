import React, { FC } from 'react';
import { Project } from 'types';
import useTranslation from 'next-translate/useTranslation';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import InfoDefaultComponent from '../Tools/Info/InfoDefaultComponent';
import { linkify } from '~/utils/utils';

interface Props {
  project: Project;
}

const ProjectAbout: FC<Props> = ({ project }) => {
  const { t } = useTranslation('common');
  return (
    <div tw="flex flex-col">
      {/* display full description or short description conditionally */}
      {project.description && project.description !== '<p><br></p>' ? (
        <InfoHtmlComponent content={project.description} />
      ) : (
        <InfoDefaultComponent content={linkify(project.short_description)} containsHtml />
      )}
      {project.desc_elevator_pitch && <h4>{t('entity.info.desc_elevator_pitch')}</h4>}
      <InfoHtmlComponent content={project.desc_elevator_pitch} />
      {project.desc_contributing && <h4>{t('entity.info.desc_contributing')}</h4>}
      <InfoHtmlComponent content={project.desc_contributing} />
      {project.desc_problem_statement && <h4>{t('entity.info.desc_problem_statement')}</h4>}
      <InfoHtmlComponent content={project.desc_problem_statement} />
      {project.desc_objectives && <h4>{t('entity.info.desc_objectives')}</h4>}
      <InfoHtmlComponent content={project.desc_objectives} />
      {project.desc_state_art && <h4>{t('entity.info.desc_state_art')}</h4>}
      <InfoHtmlComponent content={project.desc_state_art} />
      {project.desc_progress && <h4>{t('entity.info.desc_progress')}</h4>}
      <InfoHtmlComponent content={project.desc_progress} />
      {project.desc_stakeholder && <h4>{t('entity.info.desc_stakeholder')}</h4>}
      <InfoHtmlComponent content={project.desc_stakeholder} />
      {project.desc_impact_strat && <h4>{t('entity.info.desc_impact_strat')}</h4>}
      <InfoHtmlComponent content={project.desc_impact_strat} />
      {project.desc_ethical_statement && <h4>{t('entity.info.desc_ethical_statement')}</h4>}
      <InfoHtmlComponent content={project.desc_ethical_statement} />
      {project.desc_sustainability_scalability && <h4>{t('entity.info.desc_sustainability_scalability')}</h4>}
      <InfoHtmlComponent content={project.desc_sustainability_scalability} />
      {project.desc_communication_strat && <h4>{t('entity.info.desc_communication_strat')}</h4>}
      <InfoHtmlComponent content={project.desc_communication_strat} />
      {project.desc_funding && <h4>{t('entity.info.desc_funding')}</h4>}
      <InfoHtmlComponent content={project.desc_funding} />
      {/* For now, always render the timeline, but eventually only render it if a project has a timeline */}
      {/* {true && (
      <div tw="flez flex-col mt-4">
        <H2 textAlign="center">Project Timeline</H2>
        <VerticalTimeline
          className={css`
            .vertical-timeline-element-subtitle {
              margin-top: 5px !important; //needed to override about's h4 styling
              font-size: 1.2rem;
            }
            &::before {
              background-color: rgba(87, 87, 87, 0.344);
            }
          `}
        >
          {testTimelineData.map((el) => {
            // map through all phases/periods
            const phase = testTimelineData.find(({ id }) => id === el.id); // set phase/period object
            return (
              <> */}
      {/* Phase box */}
      {/* <TimelinePhaseBox color={phase.color}>
                  {phase.title}
                  <br />
                  {`${phase.date_start} - ${phase.date_end}`}
                </TimelinePhaseBox> */}
      {/* map through phase moments/events */}
      {/* {phase.moments.map((el, idx) => (
                  <VerticalTimelineElement
                    key={idx} // change the key when timeline is added to API
                    className="vertical-timeline-element--work"
                    contentStyle={{ borderTop: `3px solid ${phase.color}` }}
                    contentArrowStyle={{ borderRight: `7px solid ${phase.color}` }}
                    date={el.date}
                    iconStyle={{ background: phase.color, color: '#fff' }}
                    // icon={''} (no icon for now)
                  >
                    <h3 className="vertical-timeline-element-title">{el.title}</h3>
                    <p>{el.description}</p>
                  </VerticalTimelineElement>
                ))}
              </>
            );
          })}
        </VerticalTimeline>
      </div>
    )} */}
    </div>
  );
};

export default ProjectAbout;
