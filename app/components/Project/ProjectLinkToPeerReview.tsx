import React, { useState, useMemo, FC, useEffect } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from 'contexts/apiContext';
import useGet from 'hooks/useGet';
import Alert from '../Tools/Alert';
import Button from '../primitives/Button';
import Loading from '../Tools/Loading';
import SpinLoader from '../Tools/SpinLoader';
import Select from 'react-select';
import { useRouter } from 'next/router';

interface PropsModal {
  alreadyPresentProposals: any[];
  peerReviewId: number;
  peerReviewShortTitle: string;
  closeModal: () => void;
}

export const ProjectLinkToPeerReview: FC<PropsModal> = ({
  alreadyPresentProposals,
  peerReviewId,
  peerReviewShortTitle,
  closeModal,
}) => {
  const { data: dataProjectsMine } = useGet('/api/projects/mine');
  const [selectedProjectId, setSelectedProjectId] = useState(null);
  const [sending, setSending] = useState(false);
  const [requestSent, setRequestSent] = useState(false);
  const [hasChecked, setHasChecked] = useState(false);
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);
  const api = useApi();
  const { t } = useTranslation('common');
  const router = useRouter();

  // Filter the projects that are already linked to an proposal, so you can't link it twice to an proposal!
  const filteredProjects = useMemo(() => {
    if (dataProjectsMine) {
      return dataProjectsMine
        .filter((projectMine) => {
          // Check if my project is found in alreadyPresentProposals
          const isMyProjectAlreadyPresent = alreadyPresentProposals.find((proposal) => {
            return proposal.project_id === projectMine.id;
          });
          // We keep only the ones that are not present
          return !isMyProjectAlreadyPresent;
        })
        .filter((p) => p.is_admin && p.status !== 'draft');
    }
    return undefined;
  }, [dataProjectsMine, alreadyPresentProposals]);

  // if user has projects, set submit button to not disabled, so they can directly submit it without needing to select it in the dropdown
  useEffect(() => {
    filteredProjects?.length !== 0 && hasChecked && setIsButtonDisabled(false);
    filteredProjects?.length !== 0 && !selectedProjectId && setSelectedProjectId(filteredProjects?.[0]?.id);
  }, [filteredProjects, hasChecked]);

  const onSubmit = async (e) => {
    e.preventDefault();
    setSending(true);
    // Create proposal that is linked to the selected project
    if (selectedProjectId) {
      const res = await api
        .post('/api/proposals/', {
          peer_review_id: peerReviewId,
          project_id: selectedProjectId,
        })
        .catch(() =>
          console.error(
            `Could not POST peerReview with id=${peerReviewId} with the linked project with projectId=${selectedProjectId}`
          )
        );
      setSending(false);
      setRequestSent(true);
      setIsButtonDisabled(true);
      // Redirect to proposal edit page
      alert(t('peerReview.form.submittedAlert'));
      router.push(`/proposal/${res?.data?.id}/edit`);
    }
  };

  const onProjectSelect = (project) => {
    setSelectedProjectId(filteredProjects.find((item) => item.title === project.value).id);
    hasChecked && setIsButtonDisabled(false);
  };

  const handleCheckBoxChange = (e) => {
    setHasChecked(true);
    selectedProjectId && setIsButtonDisabled(false);
  };

  const optionsList = filteredProjects?.map((option) => {
    return {
      value: option.title,
      label: option.title,
    };
  });

  return (
    <div>
      <div tw="flex flex-col mb-7">
        <a href="/project/create" target="_blank" tw="right-4 absolute">
          {t('general.createProject')}
        </a>
      </div>
      {!filteredProjects ? (
        <Loading />
      ) : filteredProjects.length > 0 && dataProjectsMine?.filter((project) => project.is_admin).length > 0 ? (
        <>
          <Select
            options={optionsList}
            menuShouldScrollIntoView={true} // force scroll into view
            // if user has projects to add, make first of them the default selected option
            defaultValue={filteredProjects.length !== 0 && optionsList[0]}
            noOptionsMessage={() => null}
            onChange={onProjectSelect}
          />
          <div tw="mt-4">
            <input
              type="radio"
              id="read"
              name="read"
              value="read"
              onChange={handleCheckBoxChange}
              checked={hasChecked}
            />
            <label htmlFor="read">
              {t('proposal.form.readAcceptPeerReviewRules')}&nbsp;
              <a target="_blank" href={`/peer-review/${peerReviewShortTitle}?t=rules`}>
                {t('proposal.form.theRulesLink')}
              </a>
            </label>
          </div>
          <div className="btnZone">
            <Button type="submit" disabled={isButtonDisabled || sending} onClick={onSubmit} tw="mb-4">
              <>
                {sending && <SpinLoader />}
                {t('entity.form.btnCreate')}
              </>
            </Button>
            {requestSent && <Alert type="success" message={t('attach.project.success')} />}
          </div>
        </>
      ) : (
        <div className="noProject">{t('attach.project.noProject')}</div>
      )}
    </div>
  );
};
