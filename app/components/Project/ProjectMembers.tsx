import { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import useUserData from 'hooks/useUserData';
import Grid from '../Grid';
import UserCard from '../User/UserCard';
import A from '../primitives/A';
import QuickSearchBar from '../Tools/QuickSearchBar';
import Loading from '../Tools/Loading';
import Button from '../primitives/Button';
import SpinLoader from '../Tools/SpinLoader';
import useInfiniteLoading from 'hooks/useInfiniteLoading';

interface Props {
  projectId: number;
}

const ProjectMembers: FC<Props> = ({ projectId }) => {
  const membersPerQuery = 24; // number of members we get per query calls (make it 3 to test locally)
  const { userData } = useUserData();
  const { t } = useTranslation('common');
  const { data: dataMembers, error, size, setSize } = useInfiniteLoading(
    (index) => `/api/projects/${projectId}/members?items=${membersPerQuery}&page=${index + 1}`
  );

  const members = dataMembers ? [].concat(...dataMembers?.map((d) => d.members)) : [];
  const isLoadingInitialData = !dataMembers && !error;
  const isLoadingMore =
    isLoadingInitialData || (size > 0 && dataMembers && typeof dataMembers[size - 1] === 'undefined');
  const isEmpty = dataMembers?.[0]?.members.length === 0;
  const isReachingEnd =
    isEmpty || (dataMembers && dataMembers[dataMembers.length - 1]?.members.length < membersPerQuery);

  return (
    <>
      {!userData && ( // if user is not connected
        <div tw="flex flex-col space-x-2 pb-4">
          <A href="/signin">
            {t('header.signIn')} {t('program.signinCta.members')}
          </A>
        </div>
      )}
      <div tw="flex flex-col relative">
        {/* Members grid/list */}
        {members && (
          <>
            {/* Search bar to quickly find members (show if more than 30 members) */}
            {members.length > 30 && <QuickSearchBar members={members} />}
            {/* list */}
            <Grid tw="pb-4">
              {members.length === 0 ? (
                <Loading />
              ) : (
                members
                  .sort((a, b) => {
                    // Sort by owner first
                    if (a.owner && b.owner) return 0;
                    if (a.owner && !b.owner) return -1;
                    if (!a.owner && b.owner) return 1;

                    // Then by admin
                    if (a.admin && b.admin) return 0;
                    if (a.admin && !b.admin) return -1;
                    if (!a.admin && b.admin) return 1;
                    // Leave members at the end
                  })
                  ?.map((member, i) => (
                    <UserCard
                      key={i}
                      id={member.id}
                      firstName={member.first_name}
                      lastName={member.last_name}
                      nickName={member.nickname}
                      shortBio={member.short_bio}
                      skills={member.skills}
                      resources={member.ressources}
                      status={member.status}
                      lastActive={member.current_sign_in_at}
                      logoUrl={member.logo_url}
                      hasFollowed={member.has_followed}
                      affiliation={member.affiliation}
                      projectsCount={member.stats?.projects_count}
                      followersCount={member.stats?.followers_count}
                      spacesCount={member.stats?.spaces_count}
                      mutualCount={member.stats?.mutual_count}
                      role={member.owner ? 'leader' : !member.owner && member.admin && 'admin'}
                    />
                  ))
              )}
            </Grid>
            {members?.length >= membersPerQuery && (
              <div tw="flex flex-col self-center pt-4">
                <Button onClick={() => setSize(size + 1)} disabled={isLoadingMore || isReachingEnd}>
                  {isLoadingMore && <SpinLoader />}
                  {isLoadingMore
                    ? t('general.loading')
                    : !isReachingEnd
                    ? t('general.load')
                    : t('general.noMoreResults')}
                </Button>
              </div>
            )}
          </>
        )}
      </div>
    </>
  );
};

export default ProjectMembers;
