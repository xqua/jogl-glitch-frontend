import Link from 'next/link';
import React, { FC, Fragment, memo } from 'react';
import useTranslation from 'next-translate/useTranslation';
import H2 from 'components/primitives/H2';
import { useModal } from 'contexts/modalContext';
import useGet from 'hooks/useGet';
import useUserData from 'hooks/useUserData';
import { DataSource } from 'types';
import { TextWithPlural } from 'utils/managePlurals';
import ObjectCard from 'components/Cards/ObjectCard';
import Chips from '../Chip/Chips';
import Title from '../primitives/Title';
import BtnFollow from '../Tools/BtnFollow';
// import BtnDeleteUser from '../Tools/BtnDeleteUser';
import Image from 'components/primitives/Image';
import tw, { css } from 'twin.macro';
import A from '../primitives/A';
import { useApi } from '~/contexts/apiContext';
import Button from '../primitives/Button';
import { openShareConnectionsModal } from '~/utils/utils';
import { logEventToGA } from '~/utils/analytics';

interface Props {
  id: number;
  firstName: string;
  lastName: string;
  nickName?: string;
  shortBio: string;
  skills?: string[];
  resources?: string[];
  status?: string;
  lastActive?: string;
  logoUrl: string;
  hasFollowed?: boolean;
  source?: DataSource;
  role?: string;
  projectsCount?: number;
  followersCount?: number;
  spacesCount?: number;
  mutualCount?: number;
  isCompact?: boolean;
  affiliation?: string;
  showRelationNotRole?: boolean;
  recommendedIds?: number[];
}
const UserCard: FC<Props> = ({
  id,
  firstName = 'First name',
  lastName = 'Last name',
  nickName = '',
  shortBio = '– –',
  skills = [],
  resources = [],
  status,
  logoUrl = '/images/default/default-user.png',
  hasFollowed = false,
  lastActive,
  source,
  role,
  mutualCount,
  projectsCount,
  followersCount = 0,
  spacesCount = 0,
  isCompact = false,
  affiliation = '',
  showRelationNotRole = false,
  recommendedIds = [],
}) => {
  const { userData } = useUserData();
  const api = useApi();
  const { t } = useTranslation('common');
  const { showModal } = useModal();
  const { data: affiliations } = useGet(`api/users/${id}/affiliations`);
  // all affiliation that are not pending
  const currentAffiliations = affiliations?.parents.filter((affiliate) => affiliate.status !== 'pending');

  const recordRecommendationClick = () =>
    recommendedIds?.length !== 0 &&
    logEventToGA('Reco click', 'Recommendation', `[${userData.id},${id},"user", ${recommendedIds}]`, {
      userId: userData.id,
      itemId: id,
      itemType: 'user',
      recommendedIds,
    });

  const openSkillsResourcesModal = () => {
    showModal({
      children: (
        <>
          {skills.length !== 0 && (
            <>
              <p tw="mb-0 md:text-lg">{t('user.profile.skills')}</p>
              <Chips
                data={skills.map((skill) => ({
                  title: skill,
                  href: `/search/members/?refinementList[skills][0]=${skill}`,
                }))}
                type="skills"
                smallChips
              />
            </>
          )}
          {resources.length !== 0 && (
            <>
              <p tw="mb-0 pt-2 md:text-lg">{t('user.profile.resources')}</p>
              <Chips
                data={resources.map((resource) => ({
                  title: resource,
                  href: `/search/members/?refinementList[ressources][0]=${resource}`,
                }))}
                type="resources"
                smallChips
              />
            </>
          )}
          <div tw="self-center mt-5">
            <Link href={`/user/${id}`} passHref>
              <Button>{t('userCard.btnProfile')}</Button>
            </Link>
          </div>
        </>
      ),
      title: `${t('user.profile.skills')} & ${t('user.profile.resources')}`,
    });
  };

  const userUrl = `/user/${id}/${nickName}`;
  if (!isCompact) {
    return (
      <ObjectCard tw="justify-between">
        <div>
          <div tw="-mx-4 -mt-4 height[96px]">
            {/* Top shape */}
            {!role ? (
              <img src="/images/userCardShapeMember.svg" tw="w-full height[96px]" loading="lazy" />
            ) : showRelationNotRole && role === 'member' ? (
              <img src="/images/userCardShapeMember2.svg" tw="w-full height[96px]" loading="lazy" />
            ) : (
              <img
                src={`/images/userCardShape${role[0].toUpperCase() + role.slice(1)}.svg`}
                tw="w-full height[96px]"
                loading="lazy"
              />
            )}
          </div>
          {role && (
            <div tw="absolute left-3 color[#4d4d4d] capitalize rounded top-3 bg-white bg-opacity-80 px-1 box-shadow[0px 4px 15px rgb(0 0 0 / 9%)]">
              {role}
            </div>
          )}
          {/* Profile img */}
          <A href={userUrl} noStyle>
            <div
              css={[
                tw`w-24 h-24 mx-auto mt-[-5.5rem]`,
                css`
                  img {
                    ${tw`(object-cover rounded-full w-24 h-24 border-white bg-white border-4 border-solid hover:border-width[2px])!`};
                  }
                `,
              ]}
            >
              <Image src={logoUrl} priority />
            </div>
          </A>
          {/* Name */}
          <Link href={userUrl} passHref>
            <Title>
              <H2 tw="word-break[break-word] text-center my-3">
                {firstName} {lastName}
              </H2>
            </Title>
          </Link>
          {/* Stats */}
          <div tw="items-center justify-around space-x-2 flex flex-wrap">
            <CardData value={followersCount} title={<TextWithPlural type="follower" count={followersCount} />} />
            <CardData value={projectsCount} title={<TextWithPlural type="project" count={projectsCount} />} />
            <CardData value={spacesCount} title={<TextWithPlural type="space" count={spacesCount} />} />
          </div>
          <Hr tw="mt-2 pt-3" />
          {/* Affiliation */}
          <div className="content" tw="inline-flex line-height[18px]">
            {/* if user has affiliation, display them */}
            <span tw="underline">{t('user.profile.affiliation')}</span>:&nbsp;
            <div>
              {affiliations?.parents.length !== 0 && currentAffiliations?.length !== 0
                ? currentAffiliations?.map((affiliate, i) => {
                    const rowLen = currentAffiliations.length;
                    return (
                      <Fragment key={i}>
                        <Link href={`/${affiliate.parent_type.toLowerCase()}/${affiliate.short_title}`} passHref>
                          <a tw="text-black font-medium hover:(underline text-black)">{affiliate.title}</a>
                        </Link>
                        {/* add comma, except for last item */}
                        {rowLen !== i + 1 && <span>, </span>}
                      </Fragment>
                    );
                  })
                : affiliation
                ? affiliation
                : '– –'}
            </div>
          </div>
          <Hr tw="mt-2 pt-2" />
          {/* Bio */}
          <div tw="line-clamp-2 my-1 break-all h-[2.85rem]">{shortBio || '– –'}</div>
          {(skills.length !== 0 || resources.length !== 0) && (
            <>
              <Hr tw="mt-2 pt-2" />
              <p tw="text-gray-400 mb-0 text-sm">
                {t('user.profile.skills')} & {t('user.profile.resources')}
              </p>
              {/* SKills & resources */}
              <div tw="inline-flex flex-wrap gap-1">
                {skills.length !== 0 && (
                  <Chips
                    data={skills.map((skill) => ({
                      title: skill,
                      href: `/search/members/?refinementList[skills][0]=${skill}`,
                    }))}
                    type="skills"
                    showCount={2}
                    smallChips
                    hideOverFlow
                  />
                )}
                {resources.length !== 0 && (
                  <Chips
                    data={resources.map((resource) => ({
                      title: resource,
                      href: `/search/members/?refinementList[ressources][0]=${resource}`,
                    }))}
                    type="resources"
                    showCount={2}
                    smallChips
                    hideOverFlow
                  />
                )}
                {(skills.length > 2 || resources.length > 2) && (
                  <button
                    tw="bg-[#efefef] h-[1.4rem] justify-center items-center px-2 hover:bg-gray-200 rounded-full text-[.5rem]"
                    onClick={openSkillsResourcesModal}
                  >
                    •••
                  </button>
                )}
              </div>
            </>
          )}
        </div>
        <div>
          <Hr tw="mt-2 pt-3" />
          {/* Shared connections */}
          {mutualCount !== undefined && (
            <div
              tw="flex flex-col items-center text-sm mb-3 cursor-pointer hover:opacity-80"
              // open modal only if count is more than 0, and you're not the user
              onClick={() => mutualCount > 0 && userData?.id !== id && openShareConnectionsModal(id, api, t, showModal)}
            >
              <div tw="inline-flex">
                <div tw="rounded-full h-3 w-3 flex border-gray-600 border-solid border-2"></div>
                <div tw="rounded-full h-3 w-3 flex border-gray-600 border-solid border-2 -ml-1.5"></div>
              </div>
              {userData?.id !== id ? mutualCount : '∞'} <TextWithPlural type="mutualConnection" count={mutualCount} />
            </div>
          )}
          <div tw="flex justify-center gap-4">
            {/* Follow button */}
            {hasFollowed !== undefined && (
              <div tw="flex justify-center">
                <BtnFollow
                  followState={hasFollowed}
                  itemType="users"
                  itemId={id}
                  hasNoStat
                  dataComesFromAlgolia={source === 'algolia'}
                  isDisabled={userData?.id === id}
                  tw="width[fit-content]"
                />
              </div>
            )}
            {/* Delete button, appears only on search/members and if is JOGL Moderator */}
            {/* {isModerator && source === 'algolia' && userData?.id !== id && <BtnDeleteUser userData.id={id} />} */}
          </div>
        </div>
      </ObjectCard>
    );
  }
  // Compact card
  return (
    <div tw="items-center flex shadow-custom rounded-xl pr-3 h-[8rem] w-[300px]">
      {/* Left shape */}
      <img src="/images/userCardShapeLeft.svg" loading="lazy" tw="height[inherit] w-[54px]" />
      {/* Profile img */}
      <A href={userUrl} noStyle>
        <div
          css={[
            tw`w-16 h-16 ml-[-2.7rem]`,
            css`
              img {
                ${tw`(object-cover rounded-full w-16 h-16 border-white bg-white border-4 border-solid hover:opacity-90)!`};
              }
            `,
          ]}
          onClick={recordRecommendationClick}
        >
          <Image src={logoUrl} priority />
        </div>
      </A>
      <div tw="flex flex-col flex-1 ml-2 items-start">
        {/* Name and follow button */}
        <div tw="flex justify-between w-full">
          <Link href={userUrl} passHref>
            <Title onClick={recordRecommendationClick}>
              <H2 tw="word-break[break-word] mr-3 text-left">
                {firstName} {lastName}
              </H2>
            </Title>
          </Link>
          {hasFollowed !== undefined && (
            <div tw="flex justify-center height[fit-content]">
              <BtnFollow
                followState={hasFollowed}
                itemType="users"
                itemId={id}
                hasNoStat
                dataComesFromAlgolia={source === 'algolia'}
                isDisabled={userData?.id === id}
                tw="width[fit-content] height[fit-content]"
              />
            </div>
          )}
        </div>
        {/* Bio */}
        <div tw="line-clamp-2 my-1 break-all">{shortBio || '– –'}</div>
      </div>
    </div>
  );
};

const CardData = ({ value, title }) => (
  <div tw="flex flex-col justify-center items-center">
    <div tw="font-bold font-size[16px] -mb-1">{value}</div>
    <div tw="text-gray-500 font-medium text-sm">{title}</div>
  </div>
);
const Hr = (props) => <div tw="-mx-4 border-0 border-t border-solid border-color[rgba(0, 0, 0, 0.07)]" {...props} />;

export default memo(UserCard);
