/* eslint-disable camelcase */
import { useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { toAlphaNum } from 'components/Tools/Nickname';
/** * Form objects ** */
import FormDefaultComponent from 'components/Tools/Forms/FormDefaultComponent';
import FormImgComponent from 'components/Tools/Forms/FormImgComponent';
import FormInterestsComponent from 'components/Tools/Forms/FormInterestsComponent';
import FormSkillsComponent from 'components/Tools/Forms/FormSkillsComponent';
import FormResourcesComponent from 'components/Tools/Forms/FormResourcesComponent';
import FormToggleComponent from 'components/Tools/Forms/FormToggleComponent';
import FormWysiwygComponent from '../Tools/Forms/FormWysiwygComponent';
/** * Validators ** */
import FormValidator from 'components/Tools/Forms/FormValidator';
import userProfileFormRules from 'components/User/userProfileFormRules.json';
import { useApi } from 'contexts/apiContext';
import ManageExternalLink from 'components/Tools/ManageExternalLink';
import Button from 'components/primitives/Button';
import { useModal } from 'contexts/modalContext';
import Select, { createFilter } from 'react-select';
import { FixedSizeList as List } from 'react-window';
import FormDropdownComponent from 'components/Tools/Forms/FormDropdownComponent';
import TitleInfo from 'components/Tools/TitleInfo';
import { changesSavedConfAlert } from 'utils/utils';
import getCountriesAndCitiesList from 'utils/getCountriesAndCitiesList';
import SpinLoader from 'components/Tools/SpinLoader';
import { logEventToGA } from '~/utils/analytics';
import tw from 'twin.macro';
import FormTextAreaComponent from '../Tools/Forms/FormTextAreaComponent';

const UserForm = ({ user: userProp, inCompleteProfile = false }) => {
  const validator = new FormValidator(userProfileFormRules);
  const [user, setUser] = useState(userProp); // contain all user fields (to update fields as user changes them)
  // updatedUser will contain only the updated fields (always populate with nickname, for it to be valid for backend)
  const [updatedUser, setUpdatedUser] = useState({ nickname: user.nickname });
  const [stateValidation, setStateValidation] = useState({});
  const [isSending, setIsSending] = useState(false);
  const { showModal } = useModal();
  const api = useApi();
  const router = useRouter();
  const { t } = useTranslation('common');
  const [countriesCitiesData, setCountriesCitiesData] = useState([]);
  const [countryNotInList, setCountryNotInList] = useState(false);
  const [countriesList, setCountriesList] = useState([]);
  const [citiesList, setCitiesList] = useState([]);

  getCountriesAndCitiesList(
    countriesCitiesData,
    setCountriesCitiesData,
    countryNotInList,
    setCountryNotInList,
    setCountriesList,
    setCitiesList,
    user?.country
  );

  const MenuList = ({ children, maxHeight }) => {
    return (
      <List height={maxHeight} itemCount={children.length} itemSize={30}>
        {({ index, style }) => <div style={style}>{children[index]}</div>}
      </List>
    );
  };

  const handleChange = (key, content) => {
    setUpdatedUser((prevUpdatedUser) => ({ ...prevUpdatedUser, [key]: content })); // set an object containing only the fields/inputs that are updated by user
    setUser((prevUser) => ({ ...prevUser, [key]: content }));
    /* Validators start */
    const state = {};
    if (key === 'nickname') {
      state[key] = toAlphaNum(content);
    } else state[key] = content;
    const validation = validator.validate(state);
    if (validation[key] !== undefined) {
      const newStateValidation = {};
      newStateValidation[`valid_${key}`] = validation[key];
      setStateValidation(newStateValidation);
    }
    /* Validators end */
  };

  const handleCountryChange = (key, content) => {
    if (content.action === 'set-value' || content.action === 'select-option') {
      setUser((prevUser) => ({ ...prevUser, ['country']: key.value }));
      setUpdatedUser((prevUpdatedUser) => ({ ...prevUpdatedUser, ['country']: key.value })); // set an object containing only the fields/inputs that are updated by user
    }
    if (content.action === 'clear') {
      setUser((prevUser) => ({ ...prevUser, ['country']: '' }));
      setUpdatedUser((prevUpdatedUser) => ({ ...prevUpdatedUser, ['country']: '' })); // set an object containing only the fields/inputs that are updated by user
    }
    /* Validators start */
    const state = {};
    state['country'] = key ? key.value : undefined;
    const validation = validator.validate(state);
    if (validation['country'] !== undefined) {
      const newStateValidation = {};
      newStateValidation[`valid_country`] = validation['country'];
      setStateValidation(newStateValidation);
    }
  };

  const handleCityChange = (key, content) => {
    if (content.action === 'set-value' || content.action === 'select-option') {
      setUser((prevUser) => ({ ...prevUser, ['city']: key.value }));
      setUpdatedUser((prevUpdatedUser) => ({ ...prevUpdatedUser, ['city']: key.value })); // set an object containing only the fields/inputs that are updated by user
    }
    if (content.action === 'clear') {
      setUser((prevUser) => ({ ...prevUser, ['city']: '' }));
      setUpdatedUser((prevUpdatedUser) => ({ ...prevUpdatedUser, ['city']: '' })); // set an object containing only the fields/inputs that are updated by user
    }
  };

  // handling change of year separately, cause we're saving it in a different format than how it's shown in frontend
  const handleChangeYear = (key, content) => {
    const formattedBirthDate = new Date(content, 0) // make date be January 1 of the selected year (where content is the selected year)
      .toLocaleDateString('en-US'); // transform date into a YYYY-MM-DD format
    // we do this because for now we only need the year, but maybe in the future we'll ask the user the full date
    setUser((prevUser) => ({ ...prevUser, ['birth_date']: formattedBirthDate }));
    // TODO: have only one place where we manage need content
    setUpdatedUser((prevUpdatedUser) => ({ ...prevUpdatedUser, ['birth_date']: formattedBirthDate })); // set an object containing only the fields/inputs that are updated by user
    /* Validators start */
    const state = {};
    state['birth_date'] = content.toString(); // as value is a number, transform it into a string, needed for the form validation
    const validation = validator.validate(state);
    if (validation['birth_date'] !== undefined) {
      const newStateValidation = {};
      newStateValidation[`valid_birth_date`] = validation['birth_date'];
      setStateValidation(newStateValidation);
    }
    /* Validators end */
  };

  const handleSubmit = (event) => {
    const isNewUser = router.query?.launch_onboarding === 'true';
    isNewUser && inCompleteProfile && localStorage.setItem('isOnboarding', 'true');
    event?.preventDefault();
    /* Validators control before submit */
    const validation = validator.validate(user);
    if (validation.isValid) {
      setIsSending(true);
      api
        .patch(`/api/users/${user.id}`, { user: updatedUser })
        .then(() => {
          setIsSending(false);
          // if we are in user edit page
          if (!inCompleteProfile) {
            setUpdatedUser({ nickname: user.nickname }); // reset updated user value
            // show conf message when user has been successfully saved/updated
            changesSavedConfAlert(t)
              .fire()
              // go back to user page if they clicked on conf button
              .then(({ isConfirmed }) => isConfirmed && router.push(`/user/${user.id}/${user.nickname}`));
          } else {
            // else, if we are in complete profile page
            logEventToGA('completed profile', 'User', `[${user.id}]`, { userId: user.id }); // send event to google analytics
            router.push({ pathname: `/user/${user.id}`, query: { new_user: isNewUser } });
          }
        })
        .catch((err) => {
          setIsSending(false);
          console.error(`Couldn't PATCH user id=${user.id}`, err);
        });
    } else {
      setIsSending(false);
      let firstError = true;
      const newStateValidation = {};
      Object.keys(validation).forEach((key) => {
        // map through all the object of validation
        if (key !== 'isValid') {
          if (validation[key].isInvalid && firstError) {
            // if field is invalid and it's the first field that has error
            const element = document.querySelector(`#${key}`); // get element that is not valid
            const y = element.getBoundingClientRect().top + window.pageYOffset - 100; // calculate it's top value and remove 25 of offset
            window.scrollTo({ top: y, behavior: 'smooth' }); // scroll to element to show error
            firstError = false; // set to false so that it won't scroll to second invalid field and further
          }
          newStateValidation[`valid_${key}`] = validation[key];
        }
      });
      setStateValidation(newStateValidation);
    }
  };

  // const handleCancel = () => {
  //   localStorage.setItem('sawOnboardingFeature', 'true');
  //   localStorage.setItem('isOnboarding', 'false');
  //   router.push('/');
  // };

  // List of compulsary fields
  const disabledBtns =
    !user?.first_name ||
    !user?.last_name ||
    !user?.nickname ||
    !user?.birth_date ||
    !user?.gender ||
    !user?.short_bio ||
    !user?.country ||
    !user?.affiliation ||
    user?.interests.length === 0 ||
    user?.skills?.length === 0;

  const {
    valid_first_name,
    valid_last_name,
    valid_nickname,
    valid_category,
    valid_country,
    valid_affiliation,
    valid_interests,
    valid_skills,
    valid_short_bio,
    valid_birth_date,
    valid_gender,
  } = stateValidation || '';

  const countriesSelectStyles = {
    option: (provided) => ({
      ...provided,
      padding: '4px 12px',
      cursor: 'pointer',
    }),
    control: (provided) => ({
      ...provided,
      // different border style depending if value is empty or not (validation)
      border: valid_country
        ? valid_country.isInvalid
          ? '1px solid #dc3545'
          : !valid_country.isInvalid && '1px solid #28a745'
        : '1px solid lightgrey',
    }),
  };
  const citiesSelectStyles = {
    option: (provided) => ({
      ...provided,
      padding: '4px 12px',
      cursor: 'pointer',
    }),
  };
  const yearsList = [];
  const currentYear = new Date().getFullYear();
  for (let y = currentYear; y >= 1900; y--) {
    yearsList.push(y);
  }

  return (
    <form>
      {/* 1. Personal info */}
      {inCompleteProfile && (
        <TitleInfo
          isFormSubpartTitle
          title={t('user.profile.personal_infos')}
          tooltipMessage={t('user.profile.personal_infos_tooltip')}
        />
      )}
      <div tw="rounded-md" css={[inCompleteProfile && tw`p-4 mb-8 border border-gray-200 border-solid`]}>
        {/* First infos: img left, other core infos right */}
        <div tw="flex flex-col w-full md:flex-row">
          <div>
            <FormImgComponent
              itemId={user?.id}
              fileTypes={['image/jpeg', 'image/png', 'image/jpg']}
              maxSizeFile={4194304}
              itemType="users"
              type="avatar"
              id="logo_url"
              title={t('user.profile.logo_url')}
              imageUrl={user?.logo_url}
              defaultImg="/images/default/default-user.png"
              onChange={handleChange}
            />
          </div>
          <div tw="flex-1 md:ml-8">
            <div tw="grid grid-cols-1 md:(grid-cols-3 gap-x-6)">
              <FormDefaultComponent
                id="first_name"
                placeholder={t('user.profile.firstname_placeholder')}
                title={t('signUp.firstname')}
                content={user?.first_name}
                onChange={handleChange}
                errorCodeMessage={valid_first_name ? valid_first_name.message : ''}
                isValid={valid_first_name ? !valid_first_name.isInvalid : undefined}
                mandatory
              />
              <FormDefaultComponent
                id="last_name"
                placeholder={t('user.profile.lastname_placeholder')}
                title={t('user.profile.lastname')}
                content={user?.last_name}
                onChange={handleChange}
                errorCodeMessage={valid_last_name ? valid_last_name.message : ''}
                isValid={valid_last_name ? !valid_last_name.isInvalid : undefined}
                mandatory
              />
              <FormDefaultComponent
                id="nickname"
                placeholder={t('user.profile.nickname_placeholder')}
                title={t('user.profile.nickname')}
                content={user?.nickname}
                onChange={handleChange}
                prepend="@"
                mandatory
                errorCodeMessage={valid_nickname ? valid_nickname.message : ''}
                isValid={valid_nickname ? !valid_nickname.isInvalid : undefined}
                pattern={/[A-Za-z0-9]/g}
              />
            </div>
            <FormDefaultComponent
              id="short_bio"
              placeholder={t('user.profile.bioShort_placeholder')}
              title={t('user.profile.bioShort')}
              tooltipMessage={t('user.profile.bioShort_tooltip')}
              content={user?.short_bio}
              maxChar={140}
              mandatory
              errorCodeMessage={valid_short_bio ? valid_short_bio.message : ''}
              isValid={valid_short_bio ? !valid_short_bio.isInvalid : undefined}
              onChange={handleChange}
            />
          </div>
        </div>
        {/* <FormWysiwygComponent
          id="bio"
          placeholder={t('user.profile.bio_placeholder')}
          title={t('user.profile.bio')}
          content={user?.bio}
          onChange={handleChange}
          show
        /> */}
        <FormTextAreaComponent
          id="bio"
          placeholder={t('user.profile.bio_placeholder')}
          title={t('user.profile.bio')}
          content={user?.bio}
          rows={7}
          supportLinks
          onChange={handleChange}
        />
        <div tw="grid grid-cols-1" css={[!inCompleteProfile && tw`md:(grid-cols-2 gap-x-6)`]}>
          {!inCompleteProfile && (
            <FormDropdownComponent
              id="category"
              title={t('user.profile.category')}
              content={user.category}
              placeholder={t('user.profile.category_placeholder')}
              mandatory
              errorCodeMessage={valid_category ? valid_category.message : ''}
              options={['fulltime_worker', 'fulltime_student', 'freelance', 'entrepreneur', 'retired']}
              onChange={handleChange}
            />
          )}
          <FormDefaultComponent
            id="affiliation"
            placeholder={t('user.profile.affiliation_placeholder')}
            title={t('user.profile.affiliation')}
            content={user?.affiliation}
            mandatory
            errorCodeMessage={valid_affiliation ? valid_affiliation.message : ''}
            isValid={valid_affiliation ? !valid_affiliation.isInvalid : undefined}
            onChange={handleChange}
          />
        </div>
        {/* Country and city */}
        <div tw="grid grid-cols-1 md:(grid-cols-2 gap-x-6)">
          <div className="formDropdown">
            <TitleInfo mandatory title={t('general.country')} tooltipMessage={t('general.country_tooltip')} />
            {countriesList.length !== 0 ? ( // show dropdown only if countriesList is set (cause it can be long to be set)
              <>
                <div className="content">
                  <Select
                    name="country"
                    id="country"
                    defaultValue={user?.country && { label: user?.country, value: user?.country }}
                    options={countriesList}
                    placeholder={t('general.country_placeholder')}
                    filterOption={createFilter({ ignoreAccents: false })} // this line greatly improves performance
                    components={{ MenuList }}
                    noOptionsMessage={() => null}
                    onChange={handleCountryChange}
                    styles={countriesSelectStyles}
                    isClearable
                  />
                  {user?.country && countryNotInList && (
                    <div tw="flex flex-col text-danger">
                      {/* ask user to re-select their country if it's not part of the new countries list we have (wrong spelling for ex) */}
                      {t('general.country_notInList')}
                    </div>
                  )}
                  {valid_country?.isInvalid && (
                    <div tw="flex flex-col text-danger">{t(valid_country?.message || '')}</div>
                  )}
                </div>
              </>
            ) : (
              <SpinLoader />
            )}
          </div>
          {user?.country && !countryNotInList && (
            <div className="formDropdown">
              <TitleInfo title={t('general.city')} tooltipMessage={t('general.city_tooltip')} />
              <div className="content">
                <Select
                  name="city"
                  defaultValue={user?.city && { label: user?.city, value: user?.city }}
                  options={citiesList}
                  placeholder={t('general.city_placeholder')}
                  filterOption={createFilter({ ignoreAccents: citiesList?.length > 2150 ? false : true })} // ignore accents for countries that have over 2150 cities, because it greatly improves performance!
                  components={{ MenuList }}
                  noOptionsMessage={() => null}
                  onChange={handleCityChange}
                  styles={citiesSelectStyles}
                  isClearable
                />
              </div>
            </div>
          )}
        </div>
        <div tw="grid grid-cols-1 md:(grid-cols-2 gap-x-6)">
          <FormDropdownComponent
            id="birth_date"
            title={t('user.profile.birth_date')}
            content={user?.birth_date && parseInt(user?.birth_date?.substr(0, 4))} // if user has set his birth year already, transform it into a number and only take the year from the value YYYY-MM-DD
            placeholder={t('user.profile.birth_date_placeholder')}
            options={yearsList}
            errorCodeMessage={valid_birth_date ? valid_birth_date.message : ''}
            mandatory
            isSearchable
            tooltipMessage={t('user.profile.birth_date_tooltip')}
            onChange={handleChangeYear}
          />
          <FormDropdownComponent
            id="gender"
            title={t('user.profile.gender')}
            content={user?.gender}
            placeholder={t('user.profile.gender_placeholder')}
            options={['male', 'female', 'other', 'prefer_not_to_say']}
            errorCodeMessage={valid_gender ? valid_gender.message : ''}
            mandatory
            tooltipMessage={t('user.profile.gender_tooltip')}
            onChange={handleChange}
          />
        </div>
      </div>
      {/* 2/ Complementary info */}
      {inCompleteProfile && (
        <TitleInfo
          isFormSubpartTitle
          title={t('user.profile.additional_infos')}
          tooltipMessage={t('user.profile.additional_infos_tooltip')}
        />
      )}
      <div tw="rounded-md" css={[inCompleteProfile && tw`p-4 border border-gray-200 border-solid`]}>
        {!inCompleteProfile && (
          <Button
            tw="mt-3 mb-5"
            onClick={() => {
              showModal({
                children: <ManageExternalLink itemType="users" itemId={user.id} showTitle={false} />,
                title: t('general.externalLink.addView'),
                maxWidth: '70rem',
              });
            }}
          >
            {t('general.externalLink.addView')}
          </Button>
        )}
        <FormInterestsComponent
          content={user?.interests}
          onChange={handleChange}
          errorCodeMessage={valid_interests ? valid_interests.message : ''}
          mandatory
        />
        <FormSkillsComponent
          id="skills"
          type="user"
          placeholder={t('general.skills.placeholder')}
          title={t('user.profile.skills')}
          tooltipMessage={t('user.profile.skills_tooltip')}
          content={user?.skills}
          mandatory
          errorCodeMessage={valid_skills ? valid_skills.message : ''}
          onChange={handleChange}
        />
        <FormResourcesComponent
          id="ressources"
          type="user"
          placeholder={t('general.resources.placeholder')}
          title={t('user.profile.resources')}
          tooltipMessage={t('user.profile.resources_tooltip')}
          content={user?.ressources}
          onChange={handleChange}
        />

        {!inCompleteProfile && (
          <>
            <FormToggleComponent
              id="can_contact"
              title={t('user.profile.canContact')}
              choice1={t('general.no')}
              choice2={t('general.yes')}
              isChecked={user.can_contact || user.can_contact === null}
              onChange={handleChange}
            />
            <FormToggleComponent
              id="mail_newsletter"
              title={t('signUp.mail_newsletter')}
              choice1={t('general.no')}
              choice2={t('general.yes')}
              isChecked={user.mail_newsletter}
              onChange={handleChange}
            />
          </>
        )}
      </div>
      <div tw="space-x-2 mt-10 text-center">
        {!inCompleteProfile && (
          <Link href={`/user/${user.id}/${user.nickname}`} passHref>
            <Button btnType="secondary">{t('user.profile.edit.back')}</Button>
          </Link>
        )}
        <Button onClick={handleSubmit} disabled={isSending || disabledBtns}>
          {isSending && <SpinLoader />}
          {inCompleteProfile ? t('onBoarding.btns.next') : t('user.profile.edit.submit')}
        </Button>
      </div>
    </form>
  );
};

export default UserForm;
