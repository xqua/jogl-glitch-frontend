import { FolderShared } from '@emotion-icons/material';
import { Star, HelpCircle, UserCircle, Cog, DonateHeart } from '@emotion-icons/boxicons-solid';
import { SignOutAlt, BookOpen } from '@emotion-icons/fa-solid';
import { ArrowRepeat } from '@emotion-icons/bootstrap';
import { Menu, MenuItem, MenuLink } from '@reach/menu-button';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useContext } from 'react';
import { useModal } from 'contexts/modalContext';
import { UserContext } from 'contexts/UserProvider';
import useGet from 'hooks/useGet';
import Loading from '../Tools/Loading';
import { BtnUserMenu, Container, UserDropDownMenu } from './UserMenu.styles';
import UserObjects from './UserObjects';

const UserMenu: React.FC = () => {
  const userContext = useContext(UserContext);
  const router = useRouter();
  const { showModal } = useModal();
  const user = userContext.userData;
  const { t } = useTranslation('common');

  if (user) {
    return (
      <Container>
        <Menu>
          <BtnUserMenu style={{ backgroundImage: `url(${user?.logo_url_sm})` }} />
          <UserDropDownMenu>
            <Link href={`/user/${user?.id}/${user?.nickname}`} passHref>
              <MenuLink to={`/user/${user?.id}/${user?.nickname}`}>
                <UserCircle size={20} title="User profile" />
                {t('menu.profile.profile')}
              </MenuLink>
            </Link>
            <MenuItem
              onSelect={() => {
                showModal({
                  children: <UserObjects userId={user.id} userStats={user.stats} isUser />,
                  title: t('user.objects.title'),
                  maxWidth: '70rem',
                });
              }}
            >
              <FolderShared size={20} title="My portfolio" />
              {t('user.objects.title')}
            </MenuItem>
            <MenuItem
              onSelect={() => {
                showModal({
                  children: <SavedObjectsModal user={user} />,
                  title: t('user.profile.starred_items'),
                  maxWidth: '70rem',
                });
              }}
            >
              <Star size={20} title="Starred items" />
              {t('user.profile.starred_items')}
            </MenuItem>
            <MenuItem
              onSelect={() => {
                router.push(`/user/${user?.id}`); // go to user profile where the actual flow begins
                localStorage.setItem('isOnboarding', 'true'); // set onboarding to true
              }}
            >
              <ArrowRepeat size={20} title="Onboarding" />
              {t('onBoarding.btns.takeAppTour')}
            </MenuItem>
            <MenuLink
              as="a"
              href="https://www.canva.com/design/DAEqvWEqkr0/wExrqVtmfOYzKsEkgAmH4Q/view"
              target="_blank"
              rel="noopener noreferrer"
            >
              <BookOpen size={20} title="Welcome Kit" />
              {t('user.profile.welcomeKit')}
            </MenuLink>
            <MenuLink as="a" href="/donate" target="_blank" rel="noopener noreferrer">
              <DonateHeart size={20} title="Donate" />
              {t('footer.donate.btn')}
            </MenuLink>
            <Link href={`/user/${user?.id}/edit?t=admin`} passHref>
              <MenuLink to={`/user/${user?.id}/edit?t=admin`}>
                <Cog size={20} title="Settings" />
                {t('menu.profile.settings')}
              </MenuLink>
            </Link>
            <MenuItem onSelect={() => (window.location.href = 'mailto:support@jogl.io')}>
              <HelpCircle size={20} title="JOGL Support" />
              {t('menu.profile.help')}
            </MenuItem>
            <MenuItem onSelect={() => userContext.logout('/')}>
              <SignOutAlt size={20} title="Log out" />
              {t('menu.profile.logout')}
            </MenuItem>
          </UserDropDownMenu>
        </Menu>
      </Container>
    );
  } else {
    return <Loading />;
  }
};

const SavedObjectsModal = ({ user }) => {
  const { data: savedObjects } = useGet(`/api/users/saved_objects`);
  return <UserObjects list={savedObjects} userId={user.id} isUser />;
};

export default UserMenu;
