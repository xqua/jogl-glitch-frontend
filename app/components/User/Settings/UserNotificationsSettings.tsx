import Axios from 'axios';
import React, { Fragment, useContext, useEffect, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import FormToggleComponent from 'components/Tools/Forms/FormToggleComponent';
// import P from 'components/primitives/P';
import { useApi } from 'contexts/apiContext';
import { UserContext } from 'contexts/UserProvider';
import { confAlert } from 'utils/utils';
import Loading from 'components/Tools/Loading';

const UserNotificationsSettings = () => {
  const [notificationsSettings, setNotificationsSettings] = useState();
  const api = useApi();
  const user = useContext(UserContext);
  const { t } = useTranslation('common');

  useEffect(() => {
    const axiosSource = Axios.CancelToken.source();
    if (user) {
      const fetchSettings = async () => {
        const res = await api.get(`/api/notifications/settings`, { cancelToken: axiosSource.token }).catch((err) => {
          if (!Axios.isCancel(err)) {
            console.error("Couldn't GET settings", err);
          }
        });
        res?.data?.settings && setNotificationsSettings(res.data.settings);
      };
      fetchSettings();
    }
    return () => {
      axiosSource.cancel();
    };
  }, [api, user]);

  const saveSettings = () => {
    if (notificationsSettings) {
      api
        .post(`/api/notifications/settings`, { settings: notificationsSettings })
        .then(() => confAlert.fire({ icon: 'success', title: t('entity.form.confModal.saved') })); // show confirmation alert
    }
  };

  const isDisabled = (category, delivery_method) => {
    if (notificationsSettings) {
      if (!notificationsSettings.enabled) {
        return true;
      } else {
        if (delivery_method) {
          if (category) {
            if (!notificationsSettings.categories[category].enabled) {
              return true;
            } else {
              if (!notificationsSettings.delivery_methods[delivery_method].enabled) {
                return true;
              }
            }
          }
        }
      }
      return false;
    }
  };

  const handleChange = (category, delivery_method, isDisabled) => {
    var tempSettings = { ...notificationsSettings };
    if (!isDisabled) {
      if (category) {
        if (delivery_method) {
          const isCategoryMailEnabled = tempSettings.categories[category].delivery_methods[delivery_method].enabled;
          tempSettings.categories[category].delivery_methods[delivery_method].enabled = !isCategoryMailEnabled;
        } else {
          const isCategoryNotifEnabled = tempSettings.categories[category].enabled;
          tempSettings.categories[category].enabled = !isCategoryNotifEnabled;
        }
      } else {
        if (delivery_method) {
          const isGlobalMailEnabled = tempSettings.delivery_methods[delivery_method].enabled;
          tempSettings.delivery_methods[delivery_method].enabled = !isGlobalMailEnabled;
        } else {
          const isGlobalNotifEnabled = tempSettings.enabled;
          tempSettings.enabled = !isGlobalNotifEnabled;
        }
      }
      setNotificationsSettings(tempSettings);
      saveSettings();
    }
  };

  const notifCategories = notificationsSettings?.categories;
  const isGeneralSiteNotifEnabled = notificationsSettings?.enabled;
  const isGeneralEmailNotifEnabled = notificationsSettings?.delivery_methods.email.enabled;

  return (
    <div tw="flex flex-col pt-4 pb-7">
      <h3>{t('settings.notifications.name')}</h3>
      <p tw="text-gray-700">{t('settings.notifications.details')}</p>

      <div tw="grid items-center pt-5 gridTemplateColumns[minmax(130px, 1fr) 85px 85px] sm:gridTemplateColumns[minmax(130px, 1fr) 105px 105px] md:gridTemplateColumns[385px 105px 105px]">
        {/* table header */}
        <div tw="flex flex-col font-bold text-lg">{t('settings.notifications.type')}</div>
        <div tw="flex flex-col text-right font-bold text-lg">{t('settings.notifications.jogl')}</div>
        <div tw="flex flex-col text-right font-bold text-lg">{t('settings.notifications.email')}</div>

        {/* table first row (global settings) */}
        <div tw="flex flex-col pt-4 pb-5">
          <div tw="flex flex-col font-semibold">{t('settings.notifications.all.title')}</div>
          <div tw="flex flex-col text-gray-700">{t('settings.notifications.all.details')}</div>
        </div>
        <div tw="flex flex-col pt-4 pb-5">
          <FormToggleComponent
            toggleType="notif"
            id="notifG"
            isChecked={isGeneralSiteNotifEnabled}
            color1="orange"
            onChange={() => handleChange(undefined, undefined, undefined)}
          />
        </div>
        <div tw="flex flex-col pt-4 pb-5">
          <FormToggleComponent
            toggleType="notif"
            id="mailG"
            isChecked={isGeneralEmailNotifEnabled}
            color1="orange"
            isDisabled={isDisabled(undefined, 'email')}
            onChange={() => handleChange(undefined, 'email', isDisabled(undefined, 'email'))}
          />
        </div>

        {/* other table rows (one for each detailed notification setting) */}
        {notificationsSettings ? (
          Object.keys(notifCategories)
            // temporary remove "notification" category which are notif that don't fit in other categories (as we don't have for now)
            // also remove "space" category as it's not yet ready/functional + program/administration ones
            .filter(
              (category) =>
                category !== 'notification' &&
                category !== 'space' &&
                category !== 'administration' &&
                category !== 'program' &&
                category !== 'recsys'
            )
            .map((category, i) => {
              const isNotifChecked = notifCategories[category].enabled;
              const isMailChecked = notifCategories[category].delivery_methods.email.enabled;
              return (
                <Fragment key={i}>
                  <div tw="flex flex-col py-2 pr-0 md:pr-4">
                    <div tw="flex flex-col font-semibold">{t(`settings.notifications.${category}.title`)}</div>
                    <div tw="flex flex-col text-gray-700">{t(`settings.notifications.${category}.details`)}</div>
                  </div>
                  <FormToggleComponent
                    toggleType="notif"
                    id="notifC"
                    isChecked={isNotifChecked}
                    isDisabled={isDisabled(category, undefined)}
                    onChange={() => handleChange(category, undefined, isDisabled(category, undefined))}
                  />
                  <FormToggleComponent
                    toggleType="notif"
                    id="mailC"
                    isChecked={isMailChecked}
                    isDisabled={isDisabled(category, 'email')}
                    onChange={() => handleChange(category, 'email', isDisabled(category, 'email'))}
                  />
                </Fragment>
              );
            })
        ) : (
          <Loading />
        )}
        {/* Show only recsys settings */}
        {notificationsSettings ? (
          Object.keys(notifCategories)
            .filter((category) => category === 'recsys')
            .map((category, i) => {
              const isMailChecked = notifCategories[category].delivery_methods.email.enabled;
              return (
                <Fragment key={i}>
                  <div tw="flex flex-col py-2 pr-0 md:pr-4">
                    <div tw="flex flex-col font-semibold">{t(`settings.notifications.${category}.title`)}</div>
                    <div tw="flex flex-col text-gray-700">{t(`settings.notifications.${category}.details`)}</div>
                  </div>
                  <FormToggleComponent
                    toggleType="notif"
                    id="notifC"
                    isChecked={false}
                    isDisabled={true}
                    onChange={() => handleChange(category, undefined, isDisabled(category, undefined))}
                  />
                  <FormToggleComponent
                    toggleType="notif"
                    id="mailC"
                    isChecked={isMailChecked}
                    isDisabled={isDisabled(category, 'email')}
                    onChange={() => handleChange(category, 'email', isDisabled(category, 'email'))}
                  />
                </Fragment>
              );
            })
        ) : (
          <Loading />
        )}
      </div>
    </div>
  );
};

export default UserNotificationsSettings;
