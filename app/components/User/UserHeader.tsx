/* eslint-disable camelcase */
import { Edit } from '@emotion-icons/fa-solid/Edit';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import React, { FC, useEffect, useState } from 'react';
import H1 from 'components/primitives/H1';
import Button from '../primitives/Button';
import { User } from 'types';
import BtnFollow from 'components/Tools/BtnFollow';
import UserInfo from './UserInfo';
import { PaperPlane } from '@emotion-icons/boxicons-regular';
import { useModal } from '~/contexts/modalContext';
import useUserData from '~/hooks/useUserData';
import Chips from '../Chip/Chips';
import BtnDeleteUser from '../Tools/BtnDeleteUser';
import ReactGA from 'react-ga';
import { ContactForm } from '../Tools/ContactForm';
import ShareBtns from '../Tools/ShareBtns/ShareBtns';
import { useApi } from '~/contexts/apiContext';
import InfoInterestsComponent from '~/components/Tools/Info/InfoInterestsComponent';

interface Props {
  user: User;
}

const UserHeader: FC<Props> = ({ user }) => {
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();
  const { userData } = useUserData();
  const [isModerator, setIsModerator] = useState(false);
  const api = useApi();

  const getUserModerator = async () => {
    const getModerator = await api.get('/api/admin/moderator');
    setIsModerator(getModerator.data.moderator);
  };
  useEffect(() => {
    userData && getUserModerator();
  }, [userData]);

  const showUserProfilePicture = () => {
    showModal({
      children: (
        <div tw="flex flex-col">
          <img src={user?.logo_url} />
        </div>
      ),
      showCloseButton: true,
      maxWidth: '30rem',
    });
  };

  const LogoImgComponent = () => (
    <img
      src={user.logo_url || 'images/default/default-user.png'}
      alt="user logo"
      tw="mx-auto w-20 h-20 self-center object-cover bg-white rounded-full shadow-custom sm:(w-28 h-28 mt-5) md:(w-36 h-36) cursor-pointer"
      onClick={showUserProfilePicture}
    />
  );

  const openSkillsResourcesModal = () => {
    showModal({
      children: (
        <>
          {user.skills.length !== 0 && (
            <>
              <p tw="mb-0 md:text-lg">{t('user.profile.skills')}</p>
              <Chips
                data={user.skills.map((skill) => ({
                  title: skill,
                  href: `/search/members/?refinementList[skills][0]=${skill}`,
                }))}
                type="skills"
                smallChips
              />
            </>
          )}
          {user.ressources.length !== 0 && (
            <>
              <p tw="mb-0 pt-2 md:text-lg">{t('user.profile.resources')}</p>
              <Chips
                data={user.ressources.map((resource) => ({
                  title: resource,
                  href: `/search/members/?refinementList[ressources][0]=${resource}`,
                }))}
                type="resources"
                smallChips
              />
            </>
          )}
        </>
      ),
      title: `${t('user.profile.skills')} & ${t('user.profile.resources')}`,
    });
  };

  const showSkillsAndResourcesChips = () => (
    <>
      <div tw="flex flex-col space-x-0 sm:(flex-row space-x-5) mb-2">
        {/* <div tw="flex flex-col space-x-0 mb-2 w-full"> */}
        {user?.skills.length !== 0 && (
          <div tw="flex flex-col">
            <div tw="flex flex-col text-secondary">{t('user.profile.skills')}</div>
            <Chips
              data={user?.skills.map((skill) => ({
                title: skill,
                href: `/search/members/?refinementList[skills][0]=${skill}`,
              }))}
              overflowLink={`/user/${user?.id}?tab=about`}
              type="skills"
              showCount={4}
              hideOverFlow
              onOverflowClick={openSkillsResourcesModal}
            />
          </div>
        )}
        {user?.ressources.length !== 0 && (
          <div tw="flex flex-col">
            <div tw="flex flex-col text-secondary">{t('user.profile.resources')}</div>
            <div tw="inline-flex flex-wrap">
              <Chips
                data={user?.ressources.map((resource) => ({
                  title: resource,
                  href: `/search/members/?refinementList[ressources][0]=${resource}`,
                }))}
                overflowLink={`/user/${user?.id}?tab=about`}
                type="resources"
                showCount={4}
                hideOverFlow
                onOverflowClick={openSkillsResourcesModal}
              />
            </div>
          </div>
        )}
      </div>
      {/* <div tw="w-full">
        <InfoInterestsComponent title={t('user.profile.interests')} content={user.interests} />
      </div> */}
      {/* <InfoInterestsComponent title={t('user.profile.interests')} content={user.interests} /> */}
    </>
  );

  return (
    <>
      <header tw="relative w-full">
        <div tw="flex flex-wrap divide-x divide-gray-300 md:flex-nowrap">
          <div tw="hidden md:block">
            <LogoImgComponent />
            <UserInfo user={user} />
          </div>

          <div tw="px-4 py-4 md:px-8 md:pt-6">
            <div tw="inline-flex items-center">
              <div tw="md:hidden">
                <LogoImgComponent />
              </div>
              <div tw="pl-3 md:pl-0">
                <div tw="inline-flex items-center">
                  <H1 tw="font-size[1.8rem] md:font-size[2.3rem]">{`${user?.first_name} ${user?.last_name}`}</H1>
                  <div tw="hidden md:block pl-3">
                    <ShareBtns type="user" specialObjId={user?.id} />
                  </div>
                </div>
                <p tw="text-gray-400 mb-0">{`@${user?.nickname}`}</p>
              </div>
            </div>
            <p tw="mt-4 text-gray-600">{user?.short_bio || user?.bio}</p>
            <div tw="hidden md:(flex flex-col) mb-3">{showSkillsAndResourcesChips()}</div>
            <div tw="flex flex-wrap gap-3 mb-2">
              {userData && userData.id === user?.id && (
                <Link href={`/user/${user.id}/edit`} passHref>
                  <Button tw="flex justify-center items-center">
                    <Edit size={18} title="Edit user" />
                    {t('entity.form.btnAdmin')}
                  </Button>
                </Link>
              )}
              {(!userData || (userData && userData.id !== user?.id)) && (
                <BtnFollow
                  followState={user?.has_followed}
                  itemType="users"
                  itemId={user?.id}
                  count={user?.stats.followers_count}
                />
              )}
              <div tw="md:hidden">
                <ShareBtns type="user" specialObjId={user?.id} />
              </div>
              {/* show contact button if user is connected, that he's not the viewed user, and that viewed user wants to be contacted */}
              {userData && userData.id !== user?.id && user?.can_contact !== false && (
                <span tw="relative z-0 inline-flex shadow-sm rounded-md">
                  <button
                    type="button"
                    tw="relative inline-flex items-center p-2 rounded-md border border-solid border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-100 focus:(z-10 outline-none ring-1 ring-primary border-primary)"
                    onClick={() => {
                      ReactGA.modalview('/send-message');
                      showModal({
                        children: <ContactForm itemId={user?.id} closeModal={closeModal} />,
                        title: t('user.contactModal.title', { userFullName: `${user?.first_name} ${user?.last_name}` }),
                      });
                    }}
                  >
                    <PaperPlane size={18} title={t('user.btn.contact')} />
                    {t('user.btn.contact')}
                  </button>
                </span>
              )}
              {isModerator && userData?.id !== user?.id && <BtnDeleteUser userId={user?.id} />}
            </div>
          </div>
        </div>
      </header>
      {/* <div tw="hidden md:flex border-t border-solid border-gray-200 px-4">{showSkillsAndResourcesChips()}</div> */}
    </>
  );
};

export default UserHeader;
