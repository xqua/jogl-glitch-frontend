import React, { useEffect, useState } from 'react';
import useGet from 'hooks/useGet';
import ChallengeCard from '../Challenge/ChallengeCard';
import Grid from '../Grid';
import ProgramCard from '../Program/ProgramCard';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';
import SpaceCard from '../Space/SpaceCard';
import PeerReviewCard from '../PeerReview/PeerReviewCard';
import ProposalCard from '../Proposal/ProposalCard';
import ProjectCard from '../Project/ProjectCard';
import useTranslation from 'next-translate/useTranslation';
import NeedCard from '../Need/NeedCard';
import UserCard from './UserCard';
import tw from 'twin.macro';
import { Disclosure, DisclosureButton, DisclosurePanel } from '@reach/disclosure';

const UserObjects = ({ userId, userStats, isUser, list }) => {
  const objectsRoute = `/api/users/${userId}/objects`;
  const { t } = useTranslation('common');
  const [showObjType, setShowObjType] = useState('all');
  const [showRoleType, setShowRoleType] = useState('all');
  const [showMoreFilters, setShowMoreFilters] = useState(false);
  const { data: projects } = useGet(!userStats || userStats?.projects_count !== 0 ? `${objectsRoute}/projects` : '');
  const { data: challenges } = useGet(
    !userStats || userStats?.challenges_count !== 0 ? `${objectsRoute}/challenges` : ''
  );
  const { data: programs } = useGet(!userStats || userStats?.programs_count !== 0 ? `${objectsRoute}/programs` : '');
  const { data: spaces } = useGet(!userStats || userStats?.spaces_count !== 0 ? `${objectsRoute}/spaces` : '');
  const { data: peerReviews } = useGet(
    !userStats || userStats?.peer_reviews_count !== 0 ? `${objectsRoute}/peer_reviews` : ''
  );
  const { data: proposals } = useGet(!userStats || userStats?.proposals_count !== 0 ? `${objectsRoute}/proposals` : '');
  const { data: needs } = useGet(!userStats || userStats?.needs_count !== 0 ? `${objectsRoute}/needs` : '');

  const userProjects = list ? list.projects : projects;
  const userChallenges = list ? list.challenges : challenges;
  const userPrograms = list ? list.programs : programs;
  const userSpaces = list ? list.spaces : spaces;
  const userPeerReviews = list ? list.peer_reviews : peerReviews;
  const userProposals = list ? [] : proposals;
  const userNeeds = list ? list.needs : needs;
  const userUsers = list?.users;

  const [noResults, setNoResults] = useState(false);
  useEffect(() => {
    setNoResults(
      // if user has no object, set no result to true
      userProjects?.length === 0 &&
        userChallenges?.length === 0 &&
        userPrograms?.length === 0 &&
        userSpaces?.length === 0 &&
        userPeerReviews?.length === 0 &&
        userProposals?.length === 0
    );
  }, []);

  if (noResults) return <NoResults />;

  const tabs = [
    { value: 'all', translationId: 'general.all' },
    ...(userSpaces?.length > 0 ? [{ value: 'spaces', translationId: 'general.spaces' }] : []),
    ...(userChallenges?.length > 0 ? [{ value: 'challenges', translationId: 'general.challenges' }] : []),
    ...(userProjects?.length > 0 ? [{ value: 'projects', translationId: 'user.profile.tab.projects' }] : []),
    ...(userNeeds?.length > 0 ? [{ value: 'needs', translationId: 'entity.tab.needs' }] : []),
    ...(userUsers?.length > 0 ? [{ value: 'users', translationId: 'entity.card.members' }] : []),
    ...(userPeerReviews?.length > 0 ? [{ value: 'peer-reviews', translationId: 'general.peerReviews' }] : []),
    ...(userPrograms?.length > 0 ? [{ value: 'programs', translationId: 'general.programs' }] : []),
    ...(userProposals?.length > 0 ? [{ value: 'proposals', translationId: 'general.proposal_other' }] : []),
  ];

  const roles_of = [
    { value: 'all', label: 'Any role' },
    { value: 'admin', label: "I'm admin of" },
    { value: 'member', label: "I'm member of" },
  ];

  return (
    <section>
      <div tw="flex flex-col relative">
        {/* Show loading indicator while projects or spaces are loading */}
        <div tw="overflow-x-scroll flex items-start gap-3 mb-3 mt-2">
          {tabs.map(({ value, translationId }) => (
            <div
              key={value}
              onClick={() => setShowObjType(value)}
              tw="cursor-pointer px-4 py-1 rounded-full hocus:(bg-gray-200) min-w-[max-content] border border-solid border-gray-200"
              css={showObjType === value ? tw`bg-gray-200` : tw`text-gray-600 bg-white`}
            >
              {t(translationId)}
            </div>
          ))}
        </div>
        {/* Show roles filter only if you're the user, and we're showing portfolio */}
        {isUser && userStats && (
          <>
            <div
              tw="text-left w-[fit-content] cursor-pointer hover:opacity-80"
              onClick={() => setShowMoreFilters((prevState) => !prevState)}
            >
              {!showMoreFilters ? 'More filters ▾' : 'Less filters ▴'}
            </div>
            {showMoreFilters && (
              <div tw="flex gap-3 mb-4">
                {roles_of.map(({ value, label }) => (
                  <div
                    key={value}
                    onClick={() => setShowRoleType(value)}
                    tw="cursor-pointer px-4 py-1 rounded-full hocus:(bg-gray-200) min-w-[max-content] border border-solid border-gray-200"
                    css={showRoleType === value ? tw`bg-gray-200` : tw`text-gray-600 bg-white`}
                  >
                    {label}
                  </div>
                ))}
              </div>
            )}
          </>
        )}
        {((!userProjects && userStats?.projects_count !== 0) || (!userSpaces && userStats?.spaces_count !== 0)) && (
          <Loading />
        )}
        <Grid tw="py-4">
          {(showObjType === 'all' || showObjType === 'spaces') &&
            userSpaces
              ?.filter(({ status }) => (!isUser ? status !== 'draft' : true)) // don't show draft ones to other users
              ?.filter(({ is_admin }) =>
                showRoleType === 'all' ? true : showRoleType === 'admin' ? is_admin : !is_admin
              ) // filter objects you're admin or member of
              .map((space, i) => (
                <SpaceCard
                  key={i}
                  id={space.id}
                  short_title={space.short_title}
                  title={space.title}
                  title_fr={space.title_fr}
                  short_description={space.short_description}
                  short_description_fr={space.short_description_fr}
                  membersCount={space.members_count}
                  activitiesCount={space.activities_count + space.challenges_count + space.programs_count}
                  has_saved={space.has_saved}
                  projectsCount={space.projects_count}
                  spaceType={space.space_type}
                  status={space.status}
                  banner_url={space.banner_url || '/images/default/default-space.jpg'}
                  cardFormat="showObjType"
                  chip={space.is_admin ? t('member.role.admin') : space.is_member && t('member.role.member')}
                />
              ))}
          {(showObjType === 'all' || showObjType === 'challenges') &&
            userChallenges
              ?.filter(({ status }) => (!isUser ? status !== 'draft' : true)) // don't show draft ones to other users
              ?.filter(({ is_admin }) =>
                showRoleType === 'all' ? true : showRoleType === 'admin' ? is_admin : !is_admin
              ) // filter objects you're admin or member of
              .map((challenge, i) => (
                <ChallengeCard
                  key={i}
                  id={challenge.id}
                  short_title={challenge.short_title}
                  title={challenge.title}
                  title_fr={challenge.title_fr}
                  short_description={challenge.short_description}
                  short_description_fr={challenge.short_description_fr}
                  membersCount={challenge.members_count}
                  needsCount={challenge.needs_count}
                  has_saved={challenge.has_saved}
                  status={challenge.status}
                  program={challenge.program}
                  space={challenge.space}
                  customType={challenge.custom_type}
                  projectsCount={challenge.projects_count}
                  createdAt={challenge.created_at}
                  banner_url={challenge.banner_url || '/images/default/default-challenge.jpg'}
                  chip={challenge.is_admin ? t('member.role.admin') : challenge.is_member && t('member.role.member')}
                  cardFormat="showObjType"
                />
              ))}
          {(showObjType === 'all' || showObjType === 'projects') &&
            userProjects
              ?.filter(({ status }) => (!isUser ? status !== 'draft' : true)) // don't show draft ones to other users
              ?.filter(({ is_admin }) =>
                showRoleType === 'all' ? true : showRoleType === 'admin' ? is_admin : !is_admin
              ) // filter objects you're admin or member of
              .map((project, i) => (
                <ProjectCard
                  key={i}
                  id={project.id}
                  title={project.title}
                  shortTitle={project.short_title}
                  short_description={project.short_description}
                  members_count={project.members_count}
                  needs_count={project.needs_count}
                  followersCount={project.followers_count}
                  savesCount={project.saves_count}
                  postsCount={project.posts_count}
                  has_saved={project.has_saved}
                  reviewsCount={project.reviews_count}
                  banner_url={project.banner_url || '/images/default/default-project.jpg'}
                  skills={project.skills}
                  status={project.status}
                  chip={project.is_admin ? t('member.role.admin') : project.is_member && t('member.role.member')}
                  cardFormat="showObjType"
                />
              ))}
          {(showObjType === 'all' || showObjType === 'needs') &&
            userNeeds
              ?.filter(({ is_owner }) =>
                showRoleType === 'all' ? true : showRoleType === 'admin' ? is_owner : !is_owner
              ) // filter objects you're admin or member of
              .map((need, i) => (
                <NeedCard
                  key={i}
                  title={need.title}
                  project={need.project}
                  skills={need.skills}
                  resources={need.ressources}
                  hasSaved={need.has_saved}
                  id={need.id}
                  postsCount={need.posts_count}
                  publishedDate={need.created_at}
                  membersCount={need.members_count}
                  dueDate={need.end_date}
                  status={need.status}
                  cardFormat="showObjType"
                  chip={need.is_owner ? t('member.role.admin') : need.is_member ? t('member.role.member') : ' '}
                />
              ))}
          {(showObjType === 'all' || showObjType === 'users') &&
            userUsers?.map((user, i) => (
              <UserCard
                key={i}
                id={user.id}
                firstName={user.first_name}
                lastName={user.last_name}
                nickName={user.nickname}
                shortBio={user.short_bio}
                logoUrl={user.logo_url}
                hasFollowed={user.has_followed}
                skills={user.skills}
                resources={user.ressources}
                affiliation={user.affiliation}
                projectsCount={user.stats?.projects_count}
                followersCount={user.stats?.followers_count}
                spacesCount={user.stats?.spaces_count}
                mutualCount={user.stats.mutual_count}
              />
            ))}
          {(showObjType === 'all' || showObjType === 'peer-reviews') &&
            userPeerReviews
              ?.filter(({ visibility }) => (!isUser ? visibility === 'open' : true)) // don't show non-visible ones to other users
              ?.filter(({ is_admin }) =>
                showRoleType === 'all' ? true : showRoleType === 'admin' ? is_admin : !is_admin
              ) // filter objects you're admin or member of
              .map((peerReview, i) => (
                <PeerReviewCard
                  key={i}
                  peerReview={peerReview}
                  chip={
                    peerReview.is_admin ? t('member.role.admin') : peerReview.is_member && t('member.role.reviewer')
                  }
                  cardFormat="showObjType"
                />
              ))}
          {(showObjType === 'all' || showObjType === 'proposals') &&
            userProposals
              ?.filter(({ submitted_at }) => (!isUser ? submitted_at : true)) // don't show non-submitted ones to other users
              ?.filter(({ is_admin }) =>
                showRoleType === 'all' ? true : showRoleType === 'admin' ? is_admin : !is_admin
              ) // filter objects you're admin or member of
              .map((proposal, i) => (
                <ProposalCard
                  key={i}
                  id={proposal.id}
                  projectId={proposal.project_id}
                  peerReviewId={proposal.peer_review_id}
                  chip={proposal.is_admin ? t('member.role.admin') : proposal.is_member && t('member.role.member')}
                  cardFormat="showObjType"
                />
              ))}
          {(showObjType === 'all' || showObjType === 'programs') &&
            userPrograms
              ?.filter(({ status }) => (!isUser ? status !== 'draft' : true)) // don't show draft ones to other users
              ?.filter(({ is_admin }) =>
                showRoleType === 'all' ? true : showRoleType === 'admin' ? is_admin : !is_admin
              ) // filter objects you're admin or member of
              .map((program, i) => (
                <ProgramCard
                  key={i}
                  id={program.id}
                  short_title={program.short_title}
                  title={program.title}
                  title_fr={program.title_fr}
                  short_description={program.short_description}
                  short_description_fr={program.short_description_fr}
                  membersCount={program.members_count}
                  projectsCount={program.projects_count}
                  challengesCount={program.challenges_count}
                  has_saved={program.has_saved}
                  banner_url={program.banner_url || '/images/default/default-program.jpg'}
                  cardFormat="showObjType"
                  chip={program.is_admin ? t('member.role.admin') : program.is_member && t('member.role.member')}
                />
              ))}
        </Grid>
      </div>
    </section>
  );
};

export default UserObjects;
