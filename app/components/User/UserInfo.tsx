import React, { FC, Fragment } from 'react';
import { User } from 'types';
import useTranslation from 'next-translate/useTranslation';
import useGet from '~/hooks/useGet';
import { TextWithPlural } from '~/utils/managePlurals';
import { useModal } from '~/contexts/modalContext';
import ListFollowers from '../Tools/ListFollowers';
import Grid from '../Grid';
import { useApi } from '~/contexts/apiContext';
import UserCard from './UserCard';
import Link from 'next/link';
import InfoStats from '../Tools/InfoStats';

interface Props {
  user: User;
}

const UserInfo: FC<Props> = ({ user }) => {
  const { t } = useTranslation('common');
  const api = useApi();
  const { data: dataExternalLink } = useGet<{ url: string; icon_url: string }[]>(`/api/users/${user.id}/links`);
  // const { userData } = useUserData();
  const { showModal, closeModal } = useModal();
  const { data: affiliations } = useGet(`api/users/${user.id}/affiliations`);
  // const userContext = useContext(UserContext);

  const openFollowersModal = (e) => {
    user?.stats.followers_count && // open modal only if user has followers
    ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which) && // if function is launched via keypress, execute only if it's the 'enter' key
      showModal({
        children: <ListFollowers itemId={user?.id} itemType="users" />,
        title: t('entity.tab.followers'),
        maxWidth: '70rem',
      });
  };

  const showMutualConnections = () => {
    // get all user's mutual connections, then show modal displaying them
    api.get(`/api/users/${user?.id}/mutual`).then((res) => {
      showModal({
        children: (
          <Grid tw="py-0 sm:py-2 md:py-4">
            {res.data.map((member, i) => (
              <UserCard
                key={i}
                id={member.id}
                firstName={member.first_name}
                lastName={member.last_name}
                nickName={member.nickname}
                shortBio={member.short_bio}
                logoUrl={member.logo_url}
                hasFollowed={member.has_followed}
                mutualCount={member.stats.mutual_count}
                isCompact
              />
            ))}
          </Grid>
        ),
        maxWidth: '61rem',
        title: t('general.mutualConnection', { count: 2 }),
      });
    });
  };

  const userAffiliations = () =>
    (user.affiliation || affiliations?.parents.length !== 0) && (
      <div tw="flex items-center justify-between flex-wrap">
        <div>{t('user.profile.affiliation')}:</div>
        <div>
          {/* if user has affiliation, display them */}
          {affiliations?.parents.length !== 0 &&
            affiliations?.parents
              .filter((affiliate) => affiliate.status !== 'pending')
              .map((affiliate, i) => {
                return (
                  <Fragment key={i}>
                    <Link href={`/${affiliate.parent_type.toLowerCase()}/${affiliate.short_title}`}>
                      <a>{affiliate.title}</a>
                    </Link>
                    {/* add comma, except for last item */}
                    {(affiliations?.parents.length !== i + 1 ||
                      (affiliations?.parents.length === i + 1 && user.affiliation)) &&
                      ', '}
                  </Fragment>
                );
              })}
          {/* show other user affiliation (with non JOGL objects/entities)  */}
          {user.affiliation}
        </div>
      </div>
    );

  return (
    <div tw="flex-shrink-0 md:p-4 md:w-80 md:(pt-3 mt-0) mb-4 md:mb-0">
      <div tw="flex flex-wrap gap-4 mb-2 justify-around md:mt-0">
        <div
          tw="flex items-center flex-col justify-between py-1 text-center hover:opacity-80 cursor-pointer"
          tabIndex={0}
          onClick={openFollowersModal}
          onKeyUp={openFollowersModal}
        >
          <div tw="text-2xl font-bold">{user?.stats.followers_count}</div>
          <TextWithPlural type="follower" count={user?.stats.followers_count || 0} />
        </div>

        <InfoStats object="user" type="project" tab="collection" count={user.stats.projects_count} />
        <InfoStats object="user" type="space" tab="collection" count={user.stats.spaces_count} />
      </div>

      <hr tw="mb-4" />

      {userAffiliations()}
      <div tw="grid gap-4 grid-cols-1 items-center text-center mt-4">
        {user.stats.mutual_count > 0 && (
          // <div tw="flex items-center space-x-2 md:(space-x-0 justify-between) flex-wrap mt-4 md:mt-0 capitalize">
          <div tw="flex items-center justify-between flex-wrap capitalize">
            <TextWithPlural type="mutualConnection" count={2} />:
            <div tw="font-bold hover:(cursor-pointer underline)" onClick={showMutualConnections}>
              {user.stats.mutual_count}&nbsp;users
            </div>
          </div>
        )}
      </div>
      {dataExternalLink && dataExternalLink?.length !== 0 && (
        <>
          <hr tw="mb-4" />
          <div tw="grid grid-cols-1 items-center text-center mt-4">
            <div tw="flex flex-wrap gap-3">
              {[...dataExternalLink].map((link, i) => (
                <a tw="items-center" key={i} href={link.url} target="_blank">
                  <img tw="w-10 hover:opacity-80" src={link.icon_url} />
                </a>
              ))}
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default UserInfo;
