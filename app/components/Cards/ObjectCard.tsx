import Link from 'next/link';
import React, { FC, ReactNode } from 'react';
import Image from 'components/primitives/Image';
import tw from 'twin.macro';
import ReactTooltip from 'react-tooltip';

interface CardProps {
  imgUrl?: string;
  href?: string;
  children: ReactNode;
  containerStyle?: any;
  width?: string;
  mawWidth?: string;
  chip?: string;
  hrefNewTab?: boolean;
  isFeaturedCarousel?: boolean;
  cardFormat?: string;
}

const ObjectCard: FC<CardProps> = ({
  imgUrl,
  children,
  href,
  width = '300px',
  mawWidth = '',
  chip,
  hrefNewTab = false,
  isFeaturedCarousel = false,
  cardFormat,
  ...props
}) => {
  return (
    <div
      {...props.containerStyle}
      style={{ width }}
      // style={mawWidth ? { 'max-width': mawWidth, width: width } : { width }}
      css={[
        tw`relative flex flex-col overflow-hidden bg-white shadow-custom rounded-xl`,
        isFeaturedCarousel && tw`mx-1`,
      ]}
    >
      {/* if card has image and image is also a link */}
      {imgUrl && href && (
        <div tw="relative">
          <Link href={href}>
            <a target={hrefNewTab && '_blank'}>
              <div tw="relative">
                <div tw="bg-gray-200 w-full h-full rounded-t-2xl absolute" />
                <Image
                  src={imgUrl}
                  // priority
                  // blurDataURL="data:image/jpeg;base64,/9j/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAAGAAoDASIAAhEBAxEB/8QAFgABAQEAAAAAAAAAAAAAAAAAAAYH/8QAHhAAAQMFAQEAAAAAAAAAAAAAAQMFEQACBAYhE0L/xAAVAQEBAAAAAAAAAAAAAAAAAAAAAf/EABoRAAEFAQAAAAAAAAAAAAAAAAABAhEiYTH/2gAMAwEAAhEDEQA/AN12fX9ldHcZjc/luSxVE78XHSJ81QIN9q4+hd0ciBVmJilKruxgalZ1T//Z"
                  // placeholder="blur"
                  tw="w-full object-cover rounded-tl-xl rounded-tr-xl h-24! bg-white hover:opacity-95"
                />
              </div>
            </a>
          </Link>
          {chip && (
            <>
              <div
                tw="absolute top-3 left-3 color[#4d4d4d] capitalize rounded bg-white bg-opacity-80 px-1 box-shadow[0px 4px 15px rgb(0 0 0 / 9%)]"
                {...(cardFormat === 'showObjType' && { 'data-tip': `I'm ${chip} of`, 'data-for': `roleChip${href}` })}
              >
                {chip}
              </div>
              <ReactTooltip
                id={`roleChip${href}`}
                effect="solid"
                role="tooltip"
                type="dark"
                className="forceTooltipBg"
              />
            </>
          )}
        </div>
      )}
      {/* Card content */}
      <div
        css={[
          tw`flex flex-col flex-1`,
          !props.noPadding && tw`px-4 py-3`,
          imgUrl && tw`border-0 border-t border-gray-300 border-solid`,
          // props.noPadding ? tw`px-0` : tw`px-4`,
        ]}
        {...props}
      >
        {children}
      </div>
    </div>
  );
};
export default ObjectCard;
