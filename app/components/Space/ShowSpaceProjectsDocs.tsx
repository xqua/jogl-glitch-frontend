import useGet from '~/hooks/useGet';
import A from '../primitives/A';
import DocumentsList from '../Tools/Documents/DocumentsList';

const ShowSpaceProjectsDocs = ({ itemId }) => {
  const { data: projects } = useGet(`/api/spaces/${itemId}/projects`);

  return (
    <>
      <h4 tw="mt-20 mb-5">List of documents from the space's projects</h4>
      {projects?.projects.map((project) => (
        <>
          {project.documents?.length !== 0 && (
            <>
              <A href={`/project/${project.id}`}>{project.title}</A>
              <div tw="flex gap-3 flex-wrap">
                <DocumentsList
                  documents={project?.documents}
                  itemId={project?.id}
                  isAdmin={project?.is_admin}
                  itemType="spaces"
                  cardType="cards"
                />
              </div>
            </>
          )}
        </>
      ))}
    </>
  );
};

export default ShowSpaceProjectsDocs;
