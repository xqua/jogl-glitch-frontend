import { useRouter } from 'next/router';
import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
// import ChallengeMiniCard from 'components/Challenge/ChallengeMiniCard';
import PostDisplay from 'components/Feed/Posts/PostDisplay';
import H2 from 'components/primitives/H2';
import useGet from 'hooks/useGet';
import useUserData from 'hooks/useUserData';
import { Challenge, Need, Post, Space } from 'types';
import Carousel from '../Carousel';
import NeedCard from '../Need/NeedCard';
import A from '../primitives/A';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';
import ChallengeCard from '../Challenge/ChallengeCard';
// import ProgramCard from '../Program/ProgramCard';
import P from '../primitives/P';

interface Props {
  space: Space;
  shortDescription: string;
  isAdmin?: boolean;
  posts?: Post;
}

const SpaceHome: FC<Props> = ({ space, shortDescription, isAdmin = false, posts }) => {
  const router = useRouter();
  const { userData } = useUserData();
  const spaceId = space.id;
  const meetingInfo = space.meeting_information;
  const { data: dataNeeds } = space.show_featured.needs
    ? useGet<{ needs: Need[] }>(`/api/spaces/${spaceId}/needs?items=5`)
    : { needs: [] };
  const { t } = useTranslation('common');

  return (
    <div tw="flex flex-col space-y-3 relative">
      <div tw="flex flex-col">
        <H2>{t('space.home.shortIntro')}</H2>
        <P tw="font-size[17.5px]">{shortDescription}</P>
      </div>
      {/* Posts bloc */}
      <div tw="flex flex-col pb-8">
        <div tw="flex justify-between items-center">
          <H2>{t('program.announcements')}</H2>
          <A href={`/space/${router.query.short_title}?tab=news`} shallow scroll={false}>
            {t('program.seeAll')}
          </A>
        </div>
        {!posts ? (
          <Loading />
        ) : posts?.length === 0 ? (
          <NoResults type="post" />
        ) : (
          <Carousel>
            {[...posts?.posts].map((post, i) => (
              <PostDisplay
                post={post}
                key={i}
                feedId={space.feed_id}
                user={userData}
                isAdmin={isAdmin}
                cardNoComments
                width="18.4rem"
              />
            ))}
          </Carousel>
        )}
      </div>
      {/* Challenges section */}
      {/* {space.show_featured.challenges && (
        <div tw="flex flex-col pb-8">
          <div tw="flex justify-between items-center">
            <H2>{t('general.activities')}</H2>
            <A href={`/space/${router.query.short_title}?tab=challenges`} shallow scroll={false}>
              {t('program.seeAll')}
            </A>
          </div>
          {!challenges ? (
            <Loading />
          ) : challenges?.length === 0 ? (
            <NoResults type="activity" />
          ) : (
            <Carousel spaceX={12}>
              {challenges
                ?.filter(({ status }) => status !== 'draft')
                .map((challenge, index) => (
                  <ChallengeCard
                    key={index}
                    id={challenge.id}
                    banner_url={challenge.banner_url || '/images/default/default-challenge.jpg'}
                    short_title={challenge.short_title}
                    title={challenge.title}
                    title_fr={challenge.title_fr}
                    short_description={challenge.short_description}
                    short_description_fr={challenge.short_description_fr}
                    membersCount={challenge.members_count}
                    needsCount={challenge.needs_count}
                    clapsCount={challenge.claps_count}
                    projectsCount={challenge.projects_count}
                    status={challenge.status}
                    has_saved={challenge.has_saved}
                    space={challenge.space}
                    createdAt={challenge.created_at}
                    width="18.4rem"
                  />
                ))}
            </Carousel>
          )}
        </div>
      )} */}
      {/* Needs section */}
      {space.show_featured.needs && (
        <div tw="flex flex-col pb-8">
          <div tw="flex justify-between items-center">
            <H2>{t('program.latestNeeds')}</H2>
            <A href={`/space/${router.query.short_title}?tab=needs`} shallow scroll={false}>
              {t('program.seeAll')}
            </A>
          </div>
          {!dataNeeds?.needs ? (
            <Loading />
          ) : dataNeeds?.needs?.length === 0 ? (
            <NoResults type="need" />
          ) : (
            <Carousel>
              {[...dataNeeds.needs].map((need, i) => (
                <NeedCard
                  key={i}
                  title={need.title}
                  project={need.project}
                  skills={need.skills}
                  resources={need.ressources}
                  hasSaved={need.has_saved}
                  id={need.id}
                  postsCount={need.posts_count}
                  membersCount={need.members_count}
                  publishedDate={need.created_at}
                  dueDate={need.end_date}
                  status={need.status}
                  width="18.4rem"
                />
              ))}
            </Carousel>
          )}
          <div tw="flex flex-col pt-4">
            <A href="/search/needs">{t('program.needsMatch')}</A>
          </div>
        </div>
      )}
      <div tw="flex flex-col">
        <H2>{space.home_header}</H2>
        <InfoHtmlComponent content={space.home_info} />
      </div>
      {/* Meeting info bloc */}
      {meetingInfo && (
        <div tw="flex bg-white shadow border-2 border-[lightgray] rounded-2xl p-3 xl:hidden">
          <H2>{t('program.meeting_information')}</H2>
          <InfoHtmlComponent content={meetingInfo} />
        </div>
      )}
    </div>
  );
};
export default SpaceHome;
