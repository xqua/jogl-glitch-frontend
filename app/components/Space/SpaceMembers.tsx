import useTranslation from 'next-translate/useTranslation';
import React, { FC, useState } from 'react';
import QuickSearchBar from 'components/Tools/QuickSearchBar';
import Router from 'next/router';
import useUserData from 'hooks/useUserData';
import Grid from '../Grid';
import A from '../primitives/A';
import Button from '../primitives/Button';
import SpinLoader from '../Tools/SpinLoader';
import UserCard from '../User/UserCard';
import useInfiniteLoading from 'hooks/useInfiniteLoading';
import { useApi } from 'contexts/apiContext';
import useGet from 'hooks/useGet';
import FakeLink from '../primitives/FakeLink';
import H2 from 'components/primitives/H2';
import InviteMember from '../Tools/InviteMember';

interface Props {
  spaceId: number;
  isMember: boolean;
  isAdmin: boolean;
}

const SpaceMembers: FC<Props> = ({ spaceId, isMember, isAdmin }) => {
  const membersPerQuery = 24; // number of members we get per query calls (make it 3 to test locally)
  const [membersEndpoints, setMembersEndpoint] = useState(
    `/api/spaces/${spaceId}/people?items=${membersPerQuery}&sort=roles`
  );
  const { userData } = useUserData();
  const api = useApi();
  const { t } = useTranslation('common');
  const { data: dataMembers, response, error, size, setSize } = useInfiniteLoading(
    (index) => `${membersEndpoints}&page=${index + 1}`
  );

  const members = dataMembers ? [].concat(...dataMembers?.map((d) => d.people)) : [];
  const isLoadingInitialData = !dataMembers && !error;
  const isLoadingMore =
    isLoadingInitialData || (size > 0 && dataMembers && typeof dataMembers[size - 1] === 'undefined');
  const isEmpty = dataMembers?.[0]?.people.length === 0;
  const isReachingEnd =
    isEmpty || (dataMembers && dataMembers[dataMembers.length - 1]?.people.length < membersPerQuery);

  const affiliationRoute = `/api/users/${userData?.id}/affiliations/spaces/${spaceId}`;
  const { data: affiliationStatus } = useGet(affiliationRoute);
  const requestUserAffiliation = () => {
    api.post(affiliationRoute).then(() => alert('Your affiliation request is pending.'));
  };
  const removeAffiliation = (wasPending: boolean) => {
    api.delete(affiliationRoute).then(() => {
      alert(wasPending ? 'Your request has been cancelled.' : 'Your are not affiliated to the space anymore');
      Router.reload(window.location.pathname);
    });
  };
  return (
    <>
      {!userData && ( // if user is not connected
        <div tw="flex flex-col space-x-2 pb-4">
          <A href="/signin">
            {t('header.signIn')} {t('program.signinCta.members')}
          </A>
        </div>
      )}
      <div tw="flex flex-col relative">
        {/* Filters of members by space/challenges */}
        {/* <div tw="flex flex-col relative">
          <OverflowGradient display={[undefined, undefined, 'none']} />
          <Filters
            resetButtonLabel="challenge.list_all"
            content={dataChallenges
              // filter to hide draft challenges
              ?.filter(({ status }) => status !== 'draft')
              .map(({ title, id }) => ({
                title,
                id,
              }))}
            onChange={(e) => onFilterChange(e)}
            isError={challengesError}
            errorMessage="Could not get challenges filters"
            selectedId={selectedChallengeFilterId}
          />
        </div> */}
        {/* Members grid/list */}
        <span>
          {(isMember || isAdmin) && !affiliationStatus && (
            <FakeLink onClick={requestUserAffiliation}>Request affiliation</FakeLink>
          )}
          {(isMember || isAdmin) && affiliationStatus?.status === 'pending' && 'Your affiliation request is pending. '}
          {(isMember || isAdmin) && affiliationStatus?.status === 'accepted' && 'You are affiliated to this space. '}
          {(isMember || isAdmin) && affiliationStatus && (
            <FakeLink onClick={() => removeAffiliation(affiliationStatus?.status === 'pending')}>
              {affiliationStatus?.status === 'accepted' ? 'Remove my affiliation' : 'Cancel request'}
            </FakeLink>
          )}
        </span>
        {members && (
          <>
            {/* Search bar to quickly find members (show if more than 30 members) */}
            {members.length > 30 && <QuickSearchBar members={members} />}
            <Grid tw="py-4">
              {members?.map((member, i) => (
                <UserCard
                  key={i}
                  id={member.id}
                  firstName={member.first_name}
                  lastName={member.last_name}
                  nickName={member.nickname}
                  shortBio={member.short_bio}
                  skills={member.skills}
                  resources={member.ressources}
                  status={member.status}
                  lastActive={member.current_sign_in_at}
                  logoUrl={member.logo_url}
                  hasFollowed={member.has_followed}
                  affiliation={member.affiliation}
                  mutualCount={member.stats.mutual_count}
                  projectsCount={member.stats.projects_count}
                  followersCount={member.stats.followers_count}
                  spacesCount={member.stats.spaces_count}
                  role={member.relation}
                  showRelationNotRole
                />
              ))}
            </Grid>
            <div tw="flex flex-col self-center pt-4">
              <Button onClick={() => setSize(size + 1)} disabled={isLoadingMore || isReachingEnd}>
                {isLoadingMore && <SpinLoader />}
                {isLoadingMore ? t('general.loading') : !isReachingEnd ? t('general.load') : t('general.noMoreResults')}
              </Button>
            </div>
          </>
        )}
        {/* Invite someone component */}
        {isAdmin && (
          <div tw="flex flex-col mt-8">
            <H2>{t('member.invite.general')}</H2>
            <InviteMember itemType="spaces" itemId={spaceId} />
          </div>
        )}
      </div>
    </>
  );
};

export default SpaceMembers;
