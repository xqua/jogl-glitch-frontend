import { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import H2 from 'components/primitives/H2';
import { Document } from 'types';
import DocumentsList from '../Tools/Documents/DocumentsList';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import Loading from '../Tools/Loading';

interface Props {
  documents: Document[];
  isAdmin: boolean;
  spaceId: number;
  resources: any;
}

const SpaceResources: FC<Props> = ({ spaceId, isAdmin, documents, resources }) => {
  const { t } = useTranslation('common');
  return (
    <div tw="flex flex-col">
      {!resources ? (
        <Loading />
      ) : resources?.length === 0 ? (
        ''
      ) : (
        <div tw="flex flex-col space-y-7">
          <H2 tw="px-4 md:pl-0">{t('program.resources.title')}</H2>
          {[...resources].map((resource, i) => (
            <div>
              <h4 tw="pb-2 px-4 md:pl-0">{resource.title}</h4>
              <div tw="flex flex-col bg-white px-4 md:pl-0">
                <InfoHtmlComponent content={resource.content} />
              </div>
            </div>
          ))}
        </div>
      )}
      {documents.length !== 0 && (
        <div tw="px-4 md:px-0">
          <H2 tw="pt-6">{t('entity.tab.documents')}</H2>
          <DocumentsList
            documents={documents}
            itemId={spaceId}
            isAdmin={isAdmin}
            itemType="spaces"
            cardType="cards"
            showDeleteIcon={false}
          />
        </div>
      )}
    </div>
  );
};

export default SpaceResources;
