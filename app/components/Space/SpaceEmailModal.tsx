import { FC, useState } from 'react';
import Button from 'components/primitives/Button';
import { useApi } from 'contexts/apiContext';
import { useModal } from 'contexts/modalContext';
import SpinLoader from 'components/Tools/SpinLoader';

interface Props {
  spaceId: number;
}
const SpaceEmailModal: FC<Props> = ({ spaceId }) => {
  const [subject, setSubject] = useState('');
  const [content, setContent] = useState('');
  const [isSending, setIsSending] = useState(false);
  const { closeModal } = useModal();
  const api = useApi();

  const sendEmail = () => {
    const param = {
      content,
      subject,
    };
    setIsSending(true);
    api
      .post(`api/spaces/${spaceId}/email-members`, param)
      .then((res) => {
        alert('sent!');
        setIsSending(false);
        closeModal();
      })
      .catch((err) => {
        console.log(`this is a skilled email sending error: ${err}`);
        setIsSending(false);
      });
  };

  const handleChange = (e) => {
    if (e.target.name === 'subject') setSubject(e.target.value);
    setContent(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (confirm(`Are you sure you want to send this email?`)) {
      sendEmail();
    } else {
      closeModal();
    }
  };

  const disabledBtns = !content || !subject;

  return (
    <section>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="subject">Subject</label>
          <input
            type="text"
            className="object styledInputTextArea"
            id="subject"
            name="subject"
            onChange={handleChange}
            required
          />
        </div>
        <div>
          <label htmlFor="content">Email content</label>
          <textarea
            className="message styledInputTextArea"
            style={{ minHeight: '200px' }}
            id="content"
            name="content"
            onChange={handleChange}
            required
          />
        </div>
        <div tw="flex gap-4">
          <Button type="submit" btnType="primary" disabled={disabledBtns}>
            {isSending ? <SpinLoader /> : 'Send email'}
          </Button>
          <Button btnType="secondary" onClick={closeModal}>
            Cancel
          </Button>
        </div>
      </form>
    </section>
  );
};

export default SpaceEmailModal;
