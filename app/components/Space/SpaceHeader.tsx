/* eslint-disable camelcase */
import { Edit } from '@emotion-icons/fa-solid/Edit';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import React, { FC } from 'react';
import H1 from 'components/primitives/H1';
import Button from '../primitives/Button';
import { Space } from 'types';
import ShareBtns from '../Tools/ShareBtns/ShareBtns';
import BtnFollow from 'components/Tools/BtnFollow';
import SpaceInfo from './SpaceInfo';
import BtnJoin from '../Tools/BtnJoin';
import { useRouter } from 'next/router';
import useProjects from '~/hooks/useProjects';
import { useModal } from '~/contexts/modalContext';
import ProjectLinkToSpace from '../Project/ProjectLinkToSpace';
import ReactTooltip from 'react-tooltip';
// import Swal from 'sweetalert2';

interface Props {
  space: Space;
}

const SpaceHeader: FC<Props> = ({ space }) => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const { dataProjects } = useProjects('spaces', space?.id);
  const { showModal, closeModal } = useModal();

  const { id, title, short_description } = space;

  const LogoImgComponent = () => (
    <img
      src={space.logo_url || 'images/logo_single.svg'}
      alt="space logo"
      tw="mx-auto w-20 h-20 self-center object-contain bg-white rounded-full shadow-custom sm:(w-28 h-28 mt-5) md:(w-36 h-36)"
    />
  );

  return (
    <header tw="relative w-full">
      <div tw="flex flex-wrap divide-x divide-gray-200 md:flex-nowrap">
        <div tw="hidden md:block">
          <LogoImgComponent />
          <SpaceInfo space={space} />
        </div>

        <div tw="px-4 py-4 md:px-8 md:pt-6">
          <div tw="inline-flex items-center mb-1">
            <div tw="md:hidden">
              <LogoImgComponent />
            </div>
            <H1 tw="pr-3 pl-3 font-size[1.8rem] md:(font-size[2.3rem] pl-0)">{title}</H1>
            <div tw="hidden md:block">
              <ShareBtns type="space" specialObjId={id} />
            </div>
          </div>
          <p tw="mt-4 text-gray-600">{short_description}</p>

          <div tw="flex flex-wrap gap-3 mb-2">
            {/* Edit */}
            {space.is_admin && (
              <Link href={`/space/${space.short_title}/edit`} passHref>
                <Button tw="flex justify-center items-center">
                  <Edit size={18} title="Edit space" />
                  {t('entity.form.btnAdmin')}
                </Button>
              </Link>
            )}
            {/* Add project btn */}
            <div
              {...(!space.is_member && {
                'data-tip': 'You first need to join the space to do that.',
                'data-for': 'cantAddExplanation',
              })}
            >
              <Button
                onClick={() => {
                  showModal({
                    children: (
                      <ProjectLinkToSpace
                        alreadyPresentProjects={dataProjects && dataProjects[0]?.projects}
                        spaceId={space.id}
                        hasFollowed={space.has_followed}
                        closeModal={closeModal}
                      />
                    ),
                    title: t('attach.project.title'),
                    maxWidth: '50rem',
                    allowOverflow: true,
                  });
                }}
                disabled={!space.is_member}
              >
                {t('attach.project.title')}
              </Button>
              <ReactTooltip
                id="cantAddExplanation"
                effect="solid"
                role="tooltip"
                type="dark"
                className="forceTooltipBg"
              />
            </div>
            {/* Join btn */}
            {!space?.is_owner && (
              <BtnJoin
                joinState={space.is_pending ? 'pending' : space.is_member}
                isPrivate
                itemType="spaces"
                itemId={space.id}
                count={space.members_count}
                showMembersModal={() =>
                  router.push(`/space/${router.query.short_title}?tab=members`, undefined, { shallow: true })
                }
                // onJoin={() => {
                //   Swal.fire({
                //     title: 'po',
                //     text:
                //       "By joining this space, you agree that it's admins are able to contact you via email through the platform’s communication feature. Your address will still be hidden from administrators.",
                //     icon: 'warning',
                //     showCancelButton: true,
                //     confirmButtonColor: '#3085d6',
                //     cancelButtonColor: '#d33',
                //     confirmButtonText: t('general.yes'),
                //   }).then(({ isConfirmed }) => {
                //     if (isConfirmed) {
                //       Swal.fire({
                //         icon: 'success',
                //         showConfirmButton: false,
                //         text: 'Your request to join the space is pending the approval of moderators, stay stuned!',
                //       });
                //     }
                //   });
                // }}
              />
            )}
            {/* Follow btn */}
            <BtnFollow
              followState={space.has_followed}
              itemType="spaces"
              itemId={space.id}
              count={space.followers_count}
            />
            <div tw="md:hidden">
              <ShareBtns type="space" specialObjId={id} />
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default SpaceHeader;
