import useTranslation from 'next-translate/useTranslation';
import React, { FC, ReactNode, useState } from 'react';
import Alert from 'components/Tools/Alert';
import { useApi } from 'contexts/apiContext';
import { Event } from 'types';
import Button from '../primitives/Button';
import SpinLoader from '../Tools/SpinLoader';

interface Props {
  event: Event;
  serviceId: number;
  callBack: () => void;
}

const SpaceEventAdminCard: FC<Props> = ({ event, serviceId, callBack }) => {
  const [sending, setSending] = useState<'remove' | undefined>(undefined);
  const [error, setError] = useState<string | ReactNode>(undefined);
  const api = useApi();
  const { t } = useTranslation('common');

  const removeEvent = (): void => {
    setSending('remove');
    api
      .delete(`/api/services/${serviceId}/activities/${event.id}`)
      .then(() => {
        setSending(undefined);
        callBack();
      })
      .catch((err) => {
        console.error(err);
        setSending(undefined);
        setError(t('err-'));
      });
  };

  const bgLogo = {
    backgroundImage: `url(${event.logo || '/images/default/default-challenge.jpg'})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    border: '1px solid #ced4da',
    borderRadius: '50%',
    height: '50px',
    width: '50px',
  };

  if (event) {
    return (
      <div>
        <div tw="flex py-3 justify-between" key={event.id}>
          <div tw="flex items-center pb-3 space-x-3 sm:pb-0">
            <div style={{ width: '50px' }}>
              <div style={bgLogo} />
            </div>
            <div tw="flex flex-col">
              <a href={event.url} target="_blank">
                {event.name}
              </a>
            </div>
          </div>
          <div tw="flex items-center ">
            <Button btnType="danger" disabled={sending === 'remove'} onClick={removeEvent} tw="ml-3">
              {sending === 'remove' && <SpinLoader />}
              {t('general.remove')}
            </Button>
            {error && <Alert type="danger" message={error} />}
          </div>
        </div>
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};
export default SpaceEventAdminCard;
