import useTranslation from 'next-translate/useTranslation';
import Grid from '../Grid';
import Button from '../primitives/Button';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';
import useGet from 'hooks/useGet';
import { Challenge, Event, PeerReview } from 'types';
import ActivityCard from '../Challenge/ActivityCard';
// import { useEffect, useState } from 'react';
// import { useApi } from 'contexts/apiContext';
import Link from 'next/link';

const SpaceActivities = ({ spaceId, spaceShortTitle, isAdmin }) => {
  const { t } = useTranslation('common');
  // const [affiliatedPrograms, setAffiliatedPrograms] = useState([]);
  // const api = useApi();
  const { data: dataChallenges } = useGet<{ challenges: Challenge[] }>(`/api/spaces/${spaceId}/challenges`);
  const { data: dataPeerReviews } = useGet<PeerReview[]>(`/api/spaces/${spaceId}/peer_reviews`);
  const { data: eventsData } = useGet<{ events: Event[] }>(`/api/spaces/${spaceId}/activities`);

  // const getAffiliatedPrograms = () => {
  //   setAffiliatedPrograms([]);
  //   api.get(`api/spaces/${spaceId}/affiliated/programs`).then((res) => {
  //     res.data.affiliates.programs.map((program) => {
  //       api.get(`/api/programs/${program.affiliate_id}`).then((res) => {
  //         setAffiliatedPrograms((affiliatedPrograms) => [
  //           ...affiliatedPrograms,
  //           {
  //             title: res.data.title,
  //             banner_url: res.data.banner_url,
  //             short_description: res.data.short_description,
  //             short_title: res.data.short_title,
  //             id: program.affiliate_id,
  //           },
  //         ]);
  //       });
  //     });
  //   });
  // };

  // useEffect(() => {
  //   getAffiliatedPrograms();
  // }, []);

  return (
    <>
      {/* <P>{t('general.attached_challenges_explanation', { challenge_wording: t('general.activities') })}</P> */}
      {isAdmin && (
        <div tw="mt-4">
          <Link href={`/space/${spaceShortTitle}/edit?t=activities`} passHref>
            <Button btnType="secondary">
              {t('challenge.create.title', { challenge_wording: t('general.activity_1') })}
            </Button>
          </Link>
        </div>
      )}
      {/* Grid showing all activities */}
      <div tw="flex flex-col py-4 relative">
        {!dataChallenges || !dataPeerReviews || !eventsData ? (
          <Loading />
        ) : dataChallenges?.challenges.length === 0 &&
          eventsData?.length === 0 &&
          // affiliatedPrograms?.length === 0 &&
          dataPeerReviews?.length === 0 ? (
          <NoResults type="activity" />
        ) : (
          <Grid tw="py-4">
            {/* Challenges */}
            {dataChallenges.challenges
              .filter(({ status }) => (!isAdmin ? status !== 'draft' : true)) // don't display draft challenges (except for admin)
              // .filter(({ status }) => (spaceId === 133 ? true : !isAdmin ? status !== 'draft' : true)) // don't display draft challenges (except for admin) (exception for space 133 on prod)
              .map((challenge, index) => (
                <ActivityCard
                  key={index}
                  banner_url={challenge.banner_url || '/images/default/default-challenge.jpg'}
                  link={`/challenge/${challenge.short_title}`}
                  title={challenge.title}
                  title_fr={challenge.title_fr}
                  short_description={challenge.short_description}
                  short_description_fr={challenge.short_description_fr}
                  // custom_type={t('challenge.title')}
                  custom_type={challenge.custom_type}
                  status={challenge.status}
                />
              ))}
            {/* Peer reviews */}
            {dataPeerReviews
              ?.filter(({ visibility }) => visibility !== 'hidden')
              // don't display hidden ones
              .map((peerReview, index) => (
                <ActivityCard
                  key={index}
                  banner_url={peerReview.banner_url || '/images/default/default-challenge.jpg'}
                  title={peerReview.title}
                  short_description={peerReview.summary}
                  link={`/peer-review/${peerReview.short_title}`}
                  custom_type={t('peerReview.title')}
                />
              ))}
            {/* Events */}
            {eventsData?.map((event, index) => (
              <ActivityCard
                key={index}
                banner_url={event.logo || '/images/default/default-challenge.jpg'}
                link={event.url}
                title={event.name}
                short_description={event.description}
                custom_type="event"
              />
            ))}
            {/* Programs */}
            {/* {affiliatedPrograms?.map((program, index) => (
              <ActivityCard
                key={index}
                banner_url={program.banner_url || '/images/default/default-challenge.jpg'}
                title={program.title}
                link={`/program/${program.short_title}`}
                short_description={program.short_description}
                custom_type={t('program.title')}
              />
            ))} */}
          </Grid>
        )}
      </div>
    </>
  );
};

export default SpaceActivities;
