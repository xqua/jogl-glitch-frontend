import React, { FC } from 'react';
import { Space } from 'types';
import InfoStats from 'components/Tools/InfoStats';
import useTranslation from 'next-translate/useTranslation';
import useGet from '~/hooks/useGet';

interface Props {
  space: Space;
}

const SpaceInfo: FC<Props> = ({ space }) => {
  const { t } = useTranslation('common');
  const { data: dataExternalLink } = useGet<{ url: string; icon_url: string }[]>(`/api/spaces/${space.id}/links`);

  return (
    <div tw="flex-shrink-0 md:p-4 md:w-80 md:pt-6">
      <div tw="flex flex-wrap mb-2 justify-around md:mt-0">
        <InfoStats object="space" type="project" tab="projects" count={space.projects_count} />
        <InfoStats object="space" type="member" tab="members" count={space.members_count} />
        <InfoStats
          object="space"
          type="activity"
          tab="activities"
          count={space.activities_count + space.challenges_count}
        />
      </div>
      {((dataExternalLink && dataExternalLink?.length !== 0) || space.status === 'draft') && <hr tw="mb-4" />}
      {space.status === 'draft' && (
        <div tw="flex items-center justify-between flex-wrap mb-4">
          <div>{t('attach.status')}</div>
          <div tw="inline-flex space-x-2 items-center">
            <div tw="rounded-full h-2 w-2" style={{ backgroundColor: 'grey' }} />
            <div style={{ color: 'grey' }} tw="flex w-[fit-content] rounded-md items-center">
              <div tw="font-medium">{t(`entity.info.status.draft`)}</div>
            </div>
          </div>
        </div>
      )}
      {dataExternalLink && dataExternalLink?.length !== 0 && (
        <div tw="grid gap-4 grid-cols-1 items-center text-center">
          <div tw="flex items-center justify-between flex-wrap">
            <div>{t('general.externalLink.findUs')}</div>
            <div tw="flex flex-wrap gap-3">
              {[...dataExternalLink].map((link, i) => (
                <a tw="items-center" key={i} href={link.url} target="_blank">
                  <img tw="w-10 hover:opacity-80" src={link.icon_url} />
                </a>
              ))}
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default SpaceInfo;
