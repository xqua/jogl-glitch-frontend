/* eslint-disable camelcase */
import Link from 'next/link';
import React from 'react';
import { useRouter } from 'next/router';
import ObjectCard from 'components/Cards/ObjectCard';
import H2 from 'components/primitives/H2';
import Title from 'components/primitives/Title';
import BtnStar from 'components/Tools/BtnStar';
import { TextWithPlural } from 'utils/managePlurals';
import useTranslation from 'next-translate/useTranslation';
import ReactTooltip from 'react-tooltip';

interface Props {
  id: number;
  title: string;
  title_fr?: string;
  short_title: string;
  short_description: string;
  short_description_fr?: string;
  membersCount?: number;
  projectsCount?: number;
  activitiesCount?: number;
  has_saved?: boolean;
  banner_url: string;
  width?: string;
  source?: 'algolia' | 'api';
  cardFormat?: string;
  spaceType: string;
  status?: string;
  chip: string;
}
const SpaceCard = ({
  id,
  title,
  title_fr,
  short_title,
  short_description,
  short_description_fr,
  membersCount,
  projectsCount,
  activitiesCount,
  has_saved,
  banner_url = '/images/default/default-space.jpg',
  width,
  source,
  cardFormat,
  spaceType,
  status,
  chip,
}: Props) => {
  const router = useRouter();
  const { locale } = router;
  const spaceUrl = `/space/${short_title}`;
  const { t } = useTranslation('common');
  return (
    // <ObjectCard imgUrl={banner_url} href={spaceUrl} width={width} chip={spaceType}>
    <ObjectCard imgUrl={banner_url} href={spaceUrl} width={width} chip={chip} cardFormat={cardFormat}>
      {cardFormat === 'showObjType' && (
        <div tw="bg-[#F5E56C] w-[calc(100% + 32px)] pl-3 flex flex-wrap -mx-4 mt-[-.8rem] mb-[.8rem] text-sm">
          <span tw="font-bold px-1">{t('space.title')}</span>
          <span>({t(`space.type.${spaceType}`)})</span>
        </div>
      )}
      {/* Title */}
      <div tw="inline-flex items-center md:h-14">
        <Link href={spaceUrl} passHref>
          <Title>
            <H2 tw="word-break[break-word] line-clamp-2 items-center">{(locale === 'fr' && title_fr) || title}</H2>
          </Title>
        </Link>
      </div>
      <Hr tw="mt-2 pt-4" />
      {/* Status */}
      {status === 'draft' && (
        <div tw="inline-flex space-x-2 items-center mb-2">
          <div tw="rounded-full h-2 w-2" style={{ backgroundColor: 'grey' }} />
          <div
            style={{ color: 'grey' }}
            tw="flex w-[fit-content] rounded-md items-center"
            data-tip={t('entity.info.status.title')}
            data-for="spaceCard_status"
          >
            <div tw="font-medium">{t(`entity.info.status.draft`)}</div>
          </div>
          <ReactTooltip id="spaceCard_status" effect="solid" />
        </div>
      )}
      {/* Description */}
      <div tw="line-clamp-6 flex-1 color[#343a40]">
        {(locale === 'fr' && short_description_fr) || short_description}
      </div>
      {/* Stats */}
      {cardFormat !== 'compact' && (
        <>
          <Hr tw="mt-5 pt-2" />
          <div tw="items-center justify-around space-x-2 flex flex-wrap">
            <CardData value={membersCount} title={<TextWithPlural type="member" count={membersCount} />} />
            {/* <CardData value={activitiesCount} title={<TextWithPlural type="activity" count={activitiesCount} />} /> */}
            <CardData value={projectsCount} title={<TextWithPlural type="project" count={projectsCount} />} />
            {/* <CardData value={clapsCount} title={<TextWithPlural type="clap" count={clapsCount}/>} /> */}
          </div>
        </>
      )}
      {/* Star icon */}
      {(has_saved !== undefined || source === 'algolia') && (
        <BtnStar itemType="spaces" itemId={id} hasStarred={has_saved} hasNoStat source={source} />
      )}
    </ObjectCard>
  );
};

const CardData = ({ value, title }) => (
  <div tw="flex flex-col justify-center items-center">
    <div tw="font-bold font-size[17px] -mb-1">{value}</div>
    <div tw="text-gray-500 font-medium text-sm">{title}</div>
  </div>
);
const Hr = (props) => <div tw="-mx-4 border-0 border-t border-solid border-color[rgba(0, 0, 0, 0.07)]" {...props} />;

export default SpaceCard;
