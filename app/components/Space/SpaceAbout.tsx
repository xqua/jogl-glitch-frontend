import React, { FC } from 'react';
import H2 from '../primitives/H2';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import ShowBoards from '../Tools/ShowBoards';

interface Props {
  description: string;
  spaceId: number;
  enablers: string;
}

const SpaceAbout: FC<Props> = ({ description, spaceId, enablers }) => {
  return (
    <>
      {/* space short intro */}
      {/* <H2>{t('space.home.shortIntro')}</H2>
      <P tw="font-size[17.5px]">{shortDescription}</P> */}
      {/* space full description */}
      {/* <H2 pt={5}>{t('general.tab.about')}</H2> */}
      <div tw="max-w-3xl pb-4 md:pb-0">
        {/* <div tw="md:(background[#f3f4f699] rounded p-6 max-width[76ch])"> */}
        <InfoHtmlComponent content={description} />
        <hr />
        {/* display space dates info, and status */}
        {/* <div tw="flex pt-2">
          <strong>{t('attach.status')}</strong>&nbsp;
          {t(`entity.info.status.${status}`)}
        </div> */}
        {/* Boards */}
        <div tw="mt-10 mb-3">
          <ShowBoards objectType="spaces" objectId={spaceId} />
        </div>
        {enablers && (
          <>
            <hr tw="mt-8 pb-2" />
            <H2>Partners & sponsors</H2>
            <InfoHtmlComponent content={enablers} />
          </>
        )}
      </div>
    </>
  );
};
export default SpaceAbout;
