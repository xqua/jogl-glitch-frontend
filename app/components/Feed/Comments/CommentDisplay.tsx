import React, { FC, Fragment, useState, useEffect } from 'react';
import Link from 'next/link';
import useTranslation from 'next-translate/useTranslation';
import { Edit } from '@emotion-icons/boxicons-solid/Edit';
import { Link as LinkIcon, Flag } from '@emotion-icons/fa-solid';
import CommentUpdate from './CommentUpdate';
import PostDelete from 'components/Feed/Posts/PostDelete';
import { linkify, reportContent, displayObjectDate, copyLink } from 'utils/utils';
import { useApi } from 'contexts/apiContext';
import { useRouter } from 'next/router';
import { Post, User } from 'types';
import { Menu, MenuItem, MenuButton, MenuList } from '@reach/menu-button';

interface Props {
  comments?: Post['comments'];
  user: User;
  isAdmin: boolean;
  postId: number;
  refresh: () => void;
  isSingle: boolean;
}

const CommentDisplay: FC<Props> = ({
  comments = undefined,
  user,
  isAdmin = false,
  postId,
  refresh,
  isSingle = false,
}) => {
  const [showMoreComm, setShowMoreComm] = useState(false);
  const [edit, setEdit] = useState(false);
  const [commentKey, setCommentKey] = useState();
  const [commentKey2, setCommentKey2] = useState();
  const [commentIdToHighlight, setCommentIdToHighlight] = useState();
  const api = useApi();
  const router = useRouter();
  const { t } = useTranslation('common');

  // if url has a hash (link to a comment), get it
  useEffect(() => {
    setCommentIdToHighlight(
      typeof window !== 'undefined' && router.asPath.match(/#([a-z0-9-]+)/gi)
        ? router.asPath.match(/#([a-z0-9-]+)/gi)[0]
        : window.location.hash
    );
  }, []);

  const showMoreComments = () => {
    setShowMoreComm(!showMoreComm);
  };

  const viewMore = (commentKey) => {
    setCommentKey2(commentKey);
  };

  const isEdit = (commentKey) => {
    setCommentKey(commentKey);
    setEdit(!edit);
  };

  return (
    <div className="commentDisplay">
      <div className="actionBox">
        {comments && (
          <div className="commentList">
            {comments
              .sort((a, b) => a.id - b.id) // sort comments by id, so have most recent at bottom
              .map((comment, index) => {
                var userImg = comment.creator.logo_url ? comment.creator.logo_url : '/images/default/default-user.png';
                const userImgStyle = { backgroundImage: 'url(' + userImg + ')' };
                const ownerId = comment.creator.id;
                const contentWithLinks = linkify(comment.content);
                var commentClassName;
                if (!showMoreComm && !isSingle) {
                  // dynamically hide 5th comment and more (unless isSingle is true (single post page for ex), thus showing all of them)
                  commentClassName = index > 3 ? 'comment hidden' : 'comment';
                } else commentClassName = 'comment'; // show all comments on "show more" click
                var maxChar = 280; // maximum character in a post to be displayed by default;
                // var isLongText = !viewMore && comment.content.length > maxChar
                var isLongText = comment.content.length > maxChar && index !== commentKey2;
                return (
                  <Fragment key={index}>
                    {/* Display a "show more" link when there are more than 4 comments, and when we still haven't clicked on it */}
                    {index === 4
                      ? !showMoreComm &&
                        !isSingle && ( // unless isSingle is true
                          <span className="showMore" onClick={() => showMoreComments()}>
                            {t('general.showmore')}
                          </span>
                        )
                      : ''}
                    <div
                      className={`${commentClassName} ${
                        commentIdToHighlight === `#comment-${comment.id}` && 'highlight'
                      }`}
                      id={`comment-${comment.id}`}
                    >
                      <div className="topContent">
                        <div className="topBar">
                          <div className="left">
                            <div className="userImgContainer">
                              <Link href={'/user/' + ownerId}>
                                <a>
                                  <div className="userImg" style={userImgStyle}></div>
                                </a>
                              </Link>
                            </div>
                            <div className="comment-content">
                              <div tw="flex justify-between">
                                {/* align-self-start */}
                                <div className="comment-author">
                                  <Link href={'/user/' + ownerId}>
                                    <a>{`${comment.creator.first_name} ${comment.creator.last_name}`}</a>
                                  </Link>
                                </div>
                                {/* Manage dropdown menu */}
                                <Menu>
                                  <MenuButton tw="font-size[.8rem] height[fit-content] text-gray-700 px-1 hover:bg-gray-300 rounded-sm">
                                    •••
                                  </MenuButton>
                                  <MenuList className="post-manage-dropdown">
                                    {user && user.id === ownerId && (
                                      <>
                                        {index === commentKey ? (
                                          !edit ? (
                                            <MenuItem onSelect={() => isEdit(index)}>
                                              <Edit size={23} /> {t('feed.object.update')}
                                            </MenuItem>
                                          ) : (
                                            ''
                                          )
                                        ) : (
                                          <MenuItem onSelect={() => isEdit(index)}>
                                            <Edit size={23} />
                                            {t('feed.object.update')}
                                          </MenuItem>
                                        )}
                                        <MenuItem>
                                          <PostDelete
                                            postId={postId}
                                            commentId={comment.id}
                                            type="comment"
                                            origin="self"
                                            refresh={refresh}
                                          />
                                        </MenuItem>
                                      </>
                                    )}
                                    {user &&
                                    user.id !== ownerId &&
                                    isAdmin && ( // if user is admin and it's NOT his post
                                        // set origin to other (not your post)
                                        <MenuItem>
                                          <PostDelete
                                            postId={postId}
                                            commentId={comment.id}
                                            type="comment"
                                            origin="other"
                                            refresh={refresh}
                                          />
                                        </MenuItem>
                                      )}
                                    <MenuItem onSelect={() => copyLink(postId, 'comment', comment.id, t)}>
                                      {/* on click, launch copyLink function */}
                                      <LinkIcon size={20} title="Copy post's link" />
                                      {t('feed.object.comment.copyLink')}
                                    </MenuItem>
                                    {user && user.id !== ownerId && (
                                      <MenuItem onSelect={() => reportContent('comment', postId, comment.id, api, t)}>
                                        {' '}
                                        {/* on click, launch report function (hide if user is owner */}
                                        <Flag size={20} title="Report comment" />
                                        {t('feed.object.report')}
                                      </MenuItem>
                                    )}
                                  </MenuList>
                                </Menu>
                              </div>
                              {edit && index === commentKey ? ( // if comment is being edited, show comment edition component
                                <CommentUpdate
                                  postId={postId}
                                  commentId={comment.id}
                                  content={comment.content}
                                  closeOrCancelEdit={() => isEdit(index)}
                                  index={index}
                                  refresh={refresh}
                                  user={user}
                                />
                              ) : (
                                // else show comment edition component
                                <div className={`commentTextContainer ${isLongText ? 'hideText' : ''}`}>
                                  {' '}
                                  {/* add hideText class to hide text if it's too long */}
                                  <div className="text extra" dangerouslySetInnerHTML={{ __html: contentWithLinks }} />
                                  {isLongText && ( // show "view more" link if text is too long
                                    <div className="viewMore" onClick={() => viewMore(index)}>
                                      ...
                                      {t('general.showmore')}
                                    </div>
                                  )}
                                </div>
                              )}
                            </div>
                          </div>
                        </div>
                        <div className="comment-actions">
                          <div className="comment-date">{displayObjectDate(comment.created_at)}</div>
                        </div>
                      </div>
                    </div>
                  </Fragment>
                );
              })}
          </div>
        )}
      </div>
    </div>
  );
};
export default CommentDisplay;
