import { FC } from 'react';
import { useState } from 'react';
import FormComment from './FormComment';
import { findMentions, noHTMLMentions, transformMentions } from 'components/Feed/Mentions';
import { useApi } from 'contexts/apiContext';
import { logEventToGA } from 'utils/analytics';
import { User } from 'types';

interface Props {
  postId: number;
  content: string;
  commentId: number;
  user: User;
  refresh: () => void;
  closeOrCancelEdit: ((n: number) => void) | (() => void); // doubt on that one
  index: number;
}

const CommentUpdate: FC<Props> = ({
  postId,
  content: contentProp = '',
  commentId = 1,
  user = '',
  refresh,
  closeOrCancelEdit,
  index,
}) => {
  const [content, setContent] = useState(noHTMLMentions(contentProp));
  const api = useApi();
  const [uploading, setUploading] = useState(false);
  const handleChange = (newContent) => {
    setContent(newContent);
  };
  const handleSubmit = () => {
    const mentions = findMentions(content);
    const contentNoMentions = transformMentions(content);
    const updateJson = {
      comment: {
        content: contentNoMentions,
      },
    };
    if (mentions) {
      updateJson.comment.mentions = mentions;
    }
    setUploading(true);
    api
      .patch(`/api/posts/${postId}/comment/${commentId}`, updateJson)
      .then(() => {
        // record event to Google Analytics
        logEventToGA('comment update', 'Post', `[${user?.id},${postId},${commentId}]`, {
          commentId,
          userId: user?.id,
          postId,
        });
        closeOrCancelEdit(); // close comment update box after update
        refresh();
        setUploading(false);
      })
      .catch((err) => {
        console.error(`Couldn't PATCH post comment in post with id=${postId}`, err);
        setUploading(false);
      });
  };
  return (
    <FormComment
      action="update"
      content={content}
      handleChange={handleChange}
      handleSubmit={handleSubmit}
      cancelEdit={() => closeOrCancelEdit(index)}
      index={index}
      uploading={uploading}
      user={user}
    />
  );
};

export default CommentUpdate;
