import { FC, Dispatch, SetStateAction, useEffect, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import PostDisplay from 'components/Feed/Posts/PostDisplay';
import PostCreate from './Posts/PostCreate';
import Alert from 'components/Tools/Alert';
import useUserData from 'hooks/useUserData';
import Button from '../primitives/Button';
import SpinLoader from '../Tools/SpinLoader';
import PostFakeLoader from './Posts/PostFakeLoader';
import useInfiniteLoading from 'hooks/useInfiniteLoading';
import { Post } from 'types';
interface Props {
  allowPosting: boolean;
  feedId: number | string;
  isAdmin: boolean;
  needToJoinMsg?: boolean;
}
const Feed: FC<Props> = ({
  allowPosting: allowPostingProp = false,
  feedId,
  isAdmin = false,
  needToJoinMsg = 'undefined',
}) => {
  const { userData } = useUserData();
  const { t } = useTranslation('common');
  const itemsPerQuery = 5; // number of users per query calls
  const { data: dataFeed, response, error, mutate, size, setSize } = useInfiniteLoading<{
    allow_posting_to_all: boolean;
    posts: Post[];
  }>((index) => `/api/feeds/${feedId}?items=${itemsPerQuery}&page=${index + 1}&order=desc`);

  const totalNbOfPosts = parseInt(response?.[0].headers['total-count']); // get it from response headers
  const generalFeedPosts = dataFeed ? [].concat(...dataFeed) : [];
  // calculate differently currentNbOfPosts depending if feed comes from "/feed" page, or any other object
  const currentNbOfPosts =
    feedId !== 'all' ? dataFeed?.reduce((a, b) => a + b.posts.length, 0) : generalFeedPosts?.length; // get current number of loaded posts
  const isLoadingInitialData = !dataFeed && !error;
  const isLoadingMore = isLoadingInitialData || (size > 0 && dataFeed && typeof dataFeed[size - 1] === 'undefined');
  const isEmpty = dataFeed?.length === 0;
  const isReachingEnd = isEmpty || currentNbOfPosts === totalNbOfPosts; // reached end if empty, or if we loaded all the posts
  const joinMessage = needToJoinMsg ? 'general.needToJoinMsg_private' : 'general.needToJoinMsg';
  const theFeedId = feedId !== 'all' ? feedId : userData?.feed_id; // get real feedId depending if feed comes from "/feed" page, or any other object
  const [allowPosting, setAllowPosting] = useState(allowPostingProp);

  useEffect(() => {
    setAllowPosting(dataFeed?.[0]?.allow_posting_to_all || allowPostingProp);
  }, [dataFeed?.[0]?.allow_posting_to_all]);

  return (
    <div className="feed">
      {needToJoinMsg !== 'undefined' && !isAdmin && !allowPosting && <Alert type="info" message={t(joinMessage)} />}
      {(allowPosting || isAdmin) && userData && (
        <PostCreate feedId={theFeedId} refresh={mutate} userImg={userData.logo_url_sm} userId={userData.id} />
      )}
      {!isLoadingInitialData ? (
        feedId !== 'all' ? (
          // if feed comes from an object page, the backend render posts in an "posts" array inside the "dataFeed" one, so map through both to get all posts
          dataFeed?.map((feed) => {
            return feed.posts.map((post) => (
              <PostDisplay post={post} key={post.id} feedId={theFeedId} user={userData} isAdmin={isAdmin} />
            ));
          })
        ) : (
          // else map through posts directly (thanks to generalFeedPosts)
          generalFeedPosts.length !== 0 &&
          generalFeedPosts.map((post) => (
            <PostDisplay post={post} key={post.id} feedId={theFeedId} user={userData} isAdmin={isAdmin} />
          ))
        )
      ) : (
        // while posts are loading, show fake loader post card
        <PostFakeLoader />
      )}
      {dataFeed?.length !== 0 && !isReachingEnd && (
        <Button
          btnType="button"
          tw="flex m-auto justify-center items-center"
          onClick={() => setSize(size + 1)}
          disabled={isLoadingMore || isReachingEnd}
        >
          {isLoadingMore && <SpinLoader />}
          {isLoadingMore ? t('general.loading') : !isReachingEnd ? t('general.load') : t('general.noMoreResults')}
        </Button>
      )}
    </div>
  );
};
export default Feed;
