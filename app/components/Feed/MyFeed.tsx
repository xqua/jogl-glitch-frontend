import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import PostDisplay from 'components/Feed/Posts/PostDisplay';
import PostCreate from './Posts/PostCreate';
import Button from '../primitives/Button';
import Alert from '../Tools/Alert';
import SpinLoader from '../Tools/SpinLoader';
import PostFakeLoader from './Posts/PostFakeLoader';
import useInfiniteLoading from 'hooks/useInfiniteLoading';
import { User } from 'types';
import useGet from '~/hooks/useGet';

interface Props {
  user: User;
}

const MyFeed: FC<Props> = ({ user }) => {
  const itemsPerQuery = 10; // number of users per query calls (load more btn click)
  const { t } = useTranslation('common');

  // if user follow object, show their feed, else show them the general JOGL feed
  const isUserFollowingObjects = user.stats.following_count !== 0;
  const apiRoute = isUserFollowingObjects ? '/api/feeds' : '/api/feeds/all';
  // const { data: recommendedPosts } = useGet('/api/posts/recommended');
  const { data: dataFeed, error, mutate, size, setSize } = useInfiniteLoading(
    (index) => `${apiRoute}?items=${itemsPerQuery}&page=${index + 1}&order=desc`
  );

  const posts = dataFeed && [].concat(...dataFeed);
  const isLoadingInitialData = !dataFeed && !error;
  const isLoadingMore = isLoadingInitialData || (size > 0 && dataFeed && typeof dataFeed[size - 1] === 'undefined');
  const isEmpty = posts?.length === 0;
  const isReachingEnd = isEmpty || (dataFeed && dataFeed[dataFeed.length - 1]?.length < itemsPerQuery);

  const displayPosts = () => {
    if (isLoadingInitialData) return <PostFakeLoader />;
    else
      return posts?.length !== 0 ? (
        <>
          {posts?.map((post, index) => (
            <PostDisplay post={post} key={index} feedId={user?.feed_id} user={user} />
          ))}
          {/* {recommendedPosts &&
            recommendedPosts?.[size - 1]?.targetable_node &&
            !isLoadingMore &&
            recommendedPosts?.[size - 1]?.targetable_node?.post.creator.id !== user.id && (
              <>
                <p tw="mb-0 text-gray-500 italic font-size[14px]">recommended</p>
                <PostDisplay
                  post={recommendedPosts?.[size - 1].targetable_node?.post}
                  feedId={user?.feed_id}
                  user={user}
                />
              </>
            )} */}
        </>
      ) : (
        // while posts are loading, show fake loader post card
        <PostFakeLoader />
      );
  };
  return (
    <div className="feed" tw="m-auto lg:m-0">
      {/* <h3 tw="hidden lg:block">{t('feed.title')}</h3> */}
      {user && <PostCreate feedId={user?.feed_id} refresh={mutate} userImg={user?.logo_url_sm} userId={user.id} />}
      {!isUserFollowingObjects && <Alert type="info" message={t('feed.generalFeedMsg')} />}
      {displayPosts()}
      {posts && posts.length !== 0 && !isReachingEnd && (
        <Button
          btnType="button"
          tw="flex m-auto justify-center items-center"
          onClick={() => setSize(size + 1)}
          disabled={isLoadingMore || isReachingEnd}
        >
          {isLoadingMore && <SpinLoader />}
          {isLoadingMore ? t('general.loading') : !isReachingEnd ? t('general.load') : t('general.noMoreResults')}
        </Button>
      )}
    </div>
  );
};

export default React.memo(MyFeed);
