// ! This component is too complicated, there should a fixed number of post types
import { Edit } from '@emotion-icons/boxicons-solid/Edit';
import { Link as LinkIcon, Comments, Flag } from '@emotion-icons/fa-solid';
import Link from 'next/link';
import React, { FC, useEffect, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Card from 'components/Cards/Card';
import CommentDisplay from 'components/Feed/Comments/CommentDisplay';
import PostUpdate from 'components/Feed/Posts/PostUpdate';
import Grid from 'components/Grid';
import UserCard from 'components/User/UserCard';
import BtnClap from 'components/Tools/BtnClap';
import DocumentsList from 'components/Tools/Documents/DocumentsList';
import Loading from 'components/Tools/Loading';
import ShareBtns from 'components/Tools/ShareBtns/ShareBtns';
import { useApi } from 'contexts/apiContext';
import { useModal } from 'contexts/modalContext';
import useGet from 'hooks/useGet';
import { Post, User } from 'types';
import { TextWithPlural } from 'utils/managePlurals';
import { displayObjectDate, linkify, reportContent, copyLink, returnFirstLink } from 'utils/utils';
import CommentCreate from '../Comments/CommentCreate';
import PostDelete from './PostDelete';
import Image from 'components/primitives/Image';
import { Menu, MenuItem, MenuButton, MenuList } from '@reach/menu-button';
import Alert from 'components/Tools/Alert';

// const ReactTinyLink = dynamic(() => import('react-tiny-link').then((mod) => mod.ReactTinyLink), { ssr: false });

interface Props {
  post: Post;
  user: User;
  feedId?: number | string;
  refresh?: () => void;
  isSingle?: boolean;
  isAdmin?: boolean;
  cardNoComments?: boolean;
  width?: string;
}

const PostDisplay: FC<Props> = ({ post: postProp, user, feedId, isAdmin, cardNoComments = false, width, isSingle }) => {
  const modal = useModal();
  const [showComments, setShowComments] = useState(!cardNoComments);
  const [isEditing, setIsEditing] = useState(false);
  const [viewMore, setViewMore] = useState(false);
  const [loadingPosts, setLoadingPosts] = useState(false);
  const [metadata, setMetadata] = useState();
  const [loading, setLoading] = useState(false);
  const [postDeleted, setPostDeleted] = useState(false);
  const api = useApi();
  const { t } = useTranslation('common');
  const { data: post, revalidate: revalidatePost } = useGet<Post>(`/api/posts/${postProp.id}`, {
    initialData: postProp,
  });

  if (post !== undefined) {
    const changeDisplayComments = () => {
      // toggle display of comments unless we don't want the comments to show at all (in post card)
      if (!cardNoComments) {
        setShowComments((prevState) => !prevState);
      } else {
        // else, open single post page in a new page
        const win = window.open(`/post/${post.id}`, '_blank');
        win.focus();
      }
    };

    const customFetcher = async (url: string) => {
      // const response = await fetch(`https://rlp-proxy.herokuapp.com/v2?url=${url}`);
      // const response = await fetch(`https://api.allorigins.win/raw?url=${url}`);
      // const response = await fetch(`https://cors-get-proxy.sirjosh.workers.dev/?url=${url}`);
      // const response = await fetch(`https://api.microlink.io?url=${url}`);
      // https://fireship.io/snippets/link-preview-react/
      // https://github.com/Rob--W/cors-anywhere/issues/301#issuecomment-962623118
      const response = await fetch(`https://link-previews47.herokuapp.com/v2?url=${url}`);
      const json = await response.json();
      setLoading(false);
      setMetadata(json.metadata);
    };

    const isEdit = () => {
      setIsEditing((prevState) => !prevState);
    };

    const showClapModal = () => {
      api.get(`/api/posts/${post.id}/clappers`).then((res) => {
        modal.showModal({
          children: <ClappersModal clappedUsers={res.data.clappers} />,
          title: t('post.clappers'),
          maxWidth: '30rem',
        });
      });
    };

    const callViewMore = () => {
      if (!cardNoComments) {
        setViewMore((prevState) => !prevState);
      } else {
        // else, open single post page in a new page
        var win = window.open(`/post/${post.id}`, '_blank');
        win.focus();
      }
    };

    const contentWithLinks = linkify(post?.content);
    const contentFirstLink = returnFirstLink(post?.content);

    useEffect(() => {
      setLoadingPosts(false);
      customFetcher(contentFirstLink);
    }, []);

    const onPostDelete = () => {
      setPostDeleted(true);
      // refresh();
    };
    if (loadingPosts) {
      return <Loading height="18rem" active={loadingPosts} />;
    }
    if (postDeleted) {
      return <Alert type="success" message={t('feed.object.delete_conf')} />;
    }

    // const user = post !== undefined || !user ? userProp : user;
    const postId = post.id;
    const ownerId = post.creator.id;
    const objImg = post.creator.logo_url ? post.creator.logo_url : '/images/default/default-user.png';
    const postFromName =
      post.from.object_type === 'need' // if post is a need
        ? t('post.need') + post.from.object_name
        : post.from.object_name;

    const commentsCount = post.comments.length;
    const clapsCount = post.claps_count;
    const userImgStyle = { backgroundImage: `url(${objImg})` };

    const maxChar = 280; // maximum character in a post to be displayed by default;
    const isLongText = !viewMore && post.content.length > maxChar;
    const postImages = post.documents?.filter(
      (document) => document.content_type === 'image/jpeg' || document.content_type === 'image/png'
    );
    return (
      // @TODO best would be to apply props in Card only if isSingle is true
      // <Card maxWidth="700px" width={width} margin="auto">
      <Card width={width}>
        <div className={`post post-${postId} ${cardNoComments && 'postCard'}`}>
          <div className="topContent">
            <div className="topBar">
              <div className="left">
                <div className="userImgContainer">
                  <Link href={`/user/${ownerId}`}>
                    <a>
                      <div className="userImg" style={userImgStyle} />
                    </a>
                  </Link>
                </div>
                <div className="topInfo">
                  <Link href={`/user/${ownerId}`}>
                    <a>{`${post.creator.first_name} ${post.creator.last_name}`}</a>
                  </Link>

                  <div className="date">{displayObjectDate(post.created_at)}</div>
                  <div className="from">
                    {t('post.from')}
                    <Link href={`/${post.from.object_type}/${post.from.object_id}`}>
                      <a>{postFromName}</a>
                    </Link>
                  </div>
                </div>
              </div>
              {/* Manage dropdown menu */}
              <Menu>
                <MenuButton tw="font-size[.8rem] height[fit-content] text-gray-700 px-1 hover:bg-gray-300 rounded-sm">
                  •••
                </MenuButton>
                <MenuList className="post-manage-dropdown">
                  {user && user.id === ownerId && !isEditing && (
                    <MenuItem onSelect={() => setIsEditing((prevState) => !prevState)}>
                      <Edit size={25} title="Edit post" />
                      {t('feed.object.update')}
                    </MenuItem>
                  )}
                  {
                    user &&
                    user.id === ownerId && ( // if it's user's post
                        <MenuItem>
                          <PostDelete postId={postId} type="post" origin="self" refresh={onPostDelete} />
                        </MenuItem>
                      ) // set origin to self (it's your post)
                  }
                  {
                    user &&
                    user.id !== ownerId &&
                    isAdmin && ( // if user is admin and it's NOT his post
                        <MenuItem>
                          <PostDelete
                            postId={postId}
                            type="post"
                            origin="other"
                            feedId={feedId}
                            refresh={onPostDelete}
                          />
                        </MenuItem>
                      ) // set origin to other (not your post)
                  }
                  {user && user.id !== ownerId && (
                    <MenuItem onSelect={() => reportContent('post', postId, undefined, api, t)}>
                      {/* on click, launch report function (hide if user is owner */}
                      <Flag size={20} title="Report post" />
                      {t('feed.object.report')}
                    </MenuItem>
                  )}
                  <MenuItem onSelect={() => copyLink(postId, 'post', undefined, t)}>
                    {/* on click, launch copyLink function */}
                    <LinkIcon size={20} title="Copy post's link" />
                    {t('feed.object.copyLink')}
                  </MenuItem>
                </MenuList>
              </Menu>
            </div>
            {!isEditing ? ( // if post is not being edited, show post content
              <div className={`postTextContainer ${isLongText && 'hideText'}`}>
                {/* add hideText class to hide text if it's too long */}
                <div className="text extra" dangerouslySetInnerHTML={{ __html: contentWithLinks }} />
                {isLongText && ( // show "view more" link if text is too long
                  <button type="button" className="viewMore" onClick={callViewMore}>
                    ...
                    {t('general.showmore')}
                  </button>
                )}
              </div>
            ) : (
              // else show post edition component
              <PostUpdate
                postId={postId}
                content={post.content}
                closeOrCancelEdit={isEdit}
                refresh={revalidatePost}
                userImg={user.logo_url_sm}
                userId={user.id}
              />
            )}
            {postImages?.length > 0 /* If post has image, display it */ && (
              <div className="postImage">
                <Image src={postImages?.[0].url} alt="preview" />
              </div>
            )}
            {/* If post has a link and no image, display link metaData*/}
            {/* + don't show link preview if prop is cardNoComment */}
            {postImages?.length === 0 && contentFirstLink && !cardNoComments && (
              <div className="postLinkMeta">
                {loading ? (
                  <Loading />
                ) : (
                  metadata && (
                    <a href={contentFirstLink} target="_blank">
                      {metadata?.image !== 'localhost:3000/img-placeholder.jpg' && (
                        <img className="postLinkMeta--image" src={metadata?.image} />
                      )}
                      <div className="postLinkMeta--infos">
                        <h5>{metadata?.title}</h5>
                        {/* <p>{metadata?.description}</p> */}
                        {/* <div tw="inline-flex">{metadata?.siteName} • {metadata?.hostname}</div> */}
                        <p>{metadata?.hostname}</p>
                      </div>
                    </a>
                  )
                )}
              </div>
            )}
            <DocumentsList
              documents={post.documents}
              postId={post.id}
              cardType="feed"
              isEditing={isEditing}
              refresh={revalidatePost}
            />
          </div>
          <div className="actionBar">
            {!(commentsCount === 0 && clapsCount === 0) && ( // show post stats unless post has 0 clap and comment
              <div className="postStats">
                {/* on clap icon click, show modal */}
                <span>
                  {/* show claps count only if there are */}
                  {clapsCount > 0 && (
                    <button tw="flex flex-col" onClick={showClapModal}>
                      {clapsCount + ' '}
                      <TextWithPlural type="clap" count={clapsCount} />
                    </button>
                  )}
                </span>
                <span>
                  {/* show comments count only if there are */}
                  {commentsCount > 0 && (
                    <button tw="flex flex-col" onClick={changeDisplayComments}>
                      {commentsCount + ' '}
                      <TextWithPlural type="comment" count={commentsCount} />
                    </button>
                  )}
                </span>
              </div>
            )}
            <div tw="flex mt-2.5 pt-2 border-0 border-t border-[#d3d3d3]">
              <BtnClap itemType="posts" itemId={post.id} clapState={post.has_clapped} refresh={revalidatePost} />
              <button className="btn-postcard" onClick={changeDisplayComments} type="button">
                <Comments size={22} title="Show/hide comments" />
                {t('post.commentAction')}
              </button>
              <ShareBtns type="post" specialObjId={post.id} />
            </div>
            {showComments && (
              <CommentDisplay
                comments={post.comments}
                postId={postId}
                refresh={revalidatePost}
                user={user}
                isAdmin={isAdmin}
                isSingle={isSingle}
              />
            )}
            {!cardNoComments && <CommentCreate postId={postId} refresh={revalidatePost} user={user} />}
            {/* {cardNoComments && (
              <Button>
                <A href={`/post/${post.id}`}>
                  View comments
                </A>
              </Button>
            )} */}
          </div>
        </div>
      </Card>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};
const ClappersModal = ({ clappedUsers }) => (
  <>
    {/* Show modal content only if there are clappers */}
    {clappedUsers.length !== 0 ? (
      <Grid tw="gap-4">
        {clappedUsers?.map((user, i) => (
          <UserCard
            key={i}
            id={user.id}
            firstName={user.first_name}
            lastName={user.last_name}
            nickName={user.nickname}
            shortBio={user.short_bio}
            logoUrl={user.logo_url}
            // projectsCount={user.projects_count}
            hasFollowed={user.has_followed}
            isCompact
          />
        ))}
      </Grid>
    ) : (
      <Loading />
    )}
  </>
);

export default PostDisplay;
