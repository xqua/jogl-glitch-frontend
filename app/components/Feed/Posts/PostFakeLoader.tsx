import Card from 'components/Cards/Card';

const PostFakeLoader = () => (
  <Card width="100%">
    <div tw="py-8 px-2 w-full mx-auto">
      <div tw="animate-pulse flex space-x-4">
        <div tw="rounded-full bg-blue-200 h-12 w-12"></div>
        <div tw="flex-1 space-y-7 py-1">
          <div tw="h-4 bg-blue-200 rounded w-3/4"></div>
          <div tw="space-y-3">
            <div tw="h-4 bg-blue-200 rounded"></div>
            <div tw="h-4 bg-blue-200 rounded w-5/6"></div>
          </div>
        </div>
      </div>
    </div>
  </Card>
);

export default PostFakeLoader;
