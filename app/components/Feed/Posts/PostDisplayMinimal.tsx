import { Comments } from '@emotion-icons/fa-solid/Comments';
import Link from 'next/link';
import React, { FC, useEffect, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import BtnClap from 'components/Tools/BtnClap';
import Loading from 'components/Tools/Loading';
import ShareBtns from 'components/Tools/ShareBtns/ShareBtns';
import useGet from 'hooks/useGet';
import { Post } from 'types';
import { displayObjectDate, linkify } from 'utils/utils';

interface Props {
  post: Post;
}

const PostDisplayMinimal: FC<Props> = ({ post: postProp }) => {
  const { data: post, revalidate: revalidatePost } = useGet<Post>(`/api/posts/${postProp.id}`, {
    initialData: postProp,
  });
  const [loading, setLoading] = useState(true);
  const { t } = useTranslation('common');
  const openSinglePostPage = (e) => {
    // open single post page in a new page
    if ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which) {
      // if function is launched via keypress, execute only if it's the 'enter' key)
      const win = window.open(`/post/${post.id}`, '_blank');
      win.focus();
    }
  };

  useEffect(() => {
    setLoading(false);
  }, []);

  if (loading) {
    return <Loading active={loading} />;
  }
  if (post !== undefined) {
    const ownerId = post.creator.id;
    const objImg = post.creator.logo_url ? post.creator.logo_url : '/images/default/default-user.png';
    const postFromName =
      post.from.object_type === 'need' // if post is a need
        ? t('post.need') + post.from.object_name
        : post.from.object_name;

    const userImgStyle = { backgroundImage: `url(${objImg})` };
    const maxChar = 200; // maximum character in a post to be displayed by default;
    const isLongText = post.content.length > maxChar;
    const contentWithLinks = linkify(post.content);
    return (
      <div tw="flex flex-col py-2 border-b border-[#D3D3D3]">
        <div className="post postCard" tw="flex flex-col px-3">
          <div className="topContent">
            {/* <div className="topBar"> */}
            <div tw="flex flex-col justify-between mb-2.5" className="topBar">
              <div className="left">
                <div className="userImgContainer">
                  <Link href={`/user/${ownerId}`}>
                    <a>
                      <div className="userImg" style={userImgStyle} />
                    </a>
                  </Link>
                </div>
                <div className="topInfo">
                  <Link href={`/user/${ownerId}`}>
                    <a>{`${post.creator.first_name} ${post.creator.last_name}`}</a>
                  </Link>

                  <div className="date">{displayObjectDate(post.created_at)}</div>
                  <div className="from">
                    {t('post.from')}
                    <Link href={`/${post.from.object_type}/${post.from.object_id}`}>
                      <a>{postFromName}</a>
                    </Link>
                  </div>
                </div>
              </div>
            </div>

            <div className={`postTextContainer ${isLongText && 'hideText'}`}>
              {/* add hideText class to hide text if it's too long */}
              <div className="text extra" dangerouslySetInnerHTML={{ __html: contentWithLinks }} />
              {isLongText && ( // show "view more" link if text is too long
                <button
                  type="button"
                  className="viewMore"
                  onClick={openSinglePostPage}
                  onKeyUp={openSinglePostPage}
                  tabIndex={0}
                >
                  ...
                  {t('general.showmore')}
                </button>
              )}
            </div>
          </div>
          <div className="actionBar">
            <div tw="flex">
              <BtnClap itemType="posts" itemId={post.id} clapState={post.has_clapped} refresh={revalidatePost} />
              <button className="btn-postcard" onClick={openSinglePostPage} type="button">
                <Comments size={22} title="Show/hide comments" />
                {t('post.commentAction')}
              </button>
              <ShareBtns type="post" specialObjId={post.id} />
            </div>
          </div>
        </div>
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};

export default PostDisplayMinimal;
