import { FC, useState, useEffect } from 'react';
import useTranslation from 'next-translate/useTranslation';
import FormPost from './FormPost';
import { findMentions, noHTMLMentions, transformMentions } from 'components/Feed/Mentions';
import { useApi } from 'contexts/apiContext';
import { logEventToGA } from 'utils/analytics';

interface Props {
  content: string;
  userImg: string;
  userId: number;
  postId: number;
  closeOrCancelEdit: () => void;
  refresh: () => void;
}

const PostUpdate: FC<Props> = ({ content: contentProp = '', userImg, userId, postId, closeOrCancelEdit, refresh }) => {
  const [content, setContent] = useState(noHTMLMentions(contentProp));
  const [documents, setDocuments] = useState([]);
  const [uploading, setUploading] = useState(false);
  const [error, setError] = useState();
  const api = useApi();
  const { t } = useTranslation('common');

  const handleChange = (newContent) => {
    setContent(newContent);
  };

  const handleChangeDoc = (newDocuments) => {
    setDocuments(newDocuments);
  };
  useEffect(() => {
    error && console.warn(error);
  }, [error]);

  const handleSubmit = () => {
    const foundMentions = findMentions(content);
    const contentNoMentions = transformMentions(content);
    const updateJson = {
      post: {
        content: contentNoMentions,
      },
    };
    if (foundMentions) {
      updateJson.post.mentions = foundMentions;
    }
    if (documents) {
      updateJson.post.documents = documents;
    }
    setUploading(true);
    api
      .patch(`/api/posts/${postId}`, updateJson)
      .then(() => {
        // record event to Google Analytics
        logEventToGA('post update', 'Post', `[${userId},${postId}]`, { userId, postId });
        if (documents.length > 0) {
          const itemId = postId;
          const itemType = 'posts';
          const type = 'documents';
          if (itemId) {
            const bodyFormData = new FormData();
            Array.from(documents).forEach((file) => {
              bodyFormData.append(`${type}[]`, file);
            });

            const config = {
              headers: { 'Content-Type': 'multipart/form-data' },
            };

            api
              .post(`/api/${itemType}/${itemId}/${type}`, bodyFormData, config)
              .then((res) => {
                if (res.status === 200) {
                  setUploading(false);
                  refresh();
                } else {
                  setUploading(false);
                  setError(t('err-'));
                }
              })
              .catch((err) => {
                setUploading(false);
                setError(`${err.response.data.status} : ${err.response.data.error}`);
              });
          }
        }
        closeOrCancelEdit(); // close update/edit box after update
        refresh();
      })
      .catch((err) => {
        console.error(`Couldn't PATCH post with id=${postId}`, err);
      });
  };
  return (
    <FormPost
      action="update"
      content={content}
      documents={documents}
      handleChange={handleChange}
      handleChangeDoc={handleChangeDoc}
      handleSubmit={handleSubmit}
      uploading={uploading}
      cancelEdit={closeOrCancelEdit}
      userImg={userImg}
    />
  );
};

export default PostUpdate;
