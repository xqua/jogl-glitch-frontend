import { useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from 'contexts/apiContext';
import Button from '../primitives/Button';
import A from '../primitives/A';
import SpinLoader from '../Tools/SpinLoader';

const MemberCardAffiliate = ({
  member,
  itemId,
  itemType,
  callBack = () => {
    console.warn('Missing callback');
  },
  role,
}) => {
  const api = useApi();
  const [accepting, setAccepting] = useState(false);
  const [rejecting, setRejecting] = useState(false);
  const { t } = useTranslation('common');
  const [isButtonDisabled, setIsButtonDisabled] = useState(false);
  const route = `/api/users/${member.id}/affiliations/spaces/${itemId}`;

  const acceptMember = () => {
    setAccepting(true);
    api
      .patch(route, {
        affiliations: {
          status: 'accepted',
        },
      })
      .then(() => {
        setAccepting(false);
        callBack();
      })
      .catch((err) => {
        console.error(`Couldn't POST ${itemType} with itemId=${itemId}`, err);
        setAccepting(false);
        callBack();
      });
  };

  const removeOrRejectMember = () => {
    setRejecting(true);
    api
      .delete(route)
      .then(() => {
        setRejecting(false);
        callBack();
      })
      .catch(() => {
        setRejecting(false);
        callBack();
      });
  };

  if (member) {
    let imgTodisplay = '/images/default/default-user.png';
    if (member.logo_url_sm) {
      imgTodisplay = member.logo_url_sm;
    }

    const bgLogo = {
      backgroundImage: `url(${imgTodisplay})`,
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      border: '1px solid #ced4da',
      borderRadius: '50%',
      height: '50px',
      width: '50px',
    };

    return (
      <div>
        <div tw="flex flex-col justify-start sm:(flex-row justify-between)" key={member.id}>
          <div tw="flex items-center space-x-3 pb-3 sm:pb-0">
            <div style={{ width: '50px' }}>
              <div style={bgLogo} />
            </div>
            <A href={`/user/${member.id}`}>
              {member.first_name} {member.last_name}
            </A>
          </div>
          <div tw="flex">
            {role === 'pending' ? (
              // if member role is pending, show buttons to accept or reject the request
              <div tw="flex flex-col space-y-2 xs:(flex-row space-x-2 space-y-0)">
                <Button
                  tw="text-green-500 border-green-500 bg-white hover:(text-white bg-green-500)"
                  disabled={accepting}
                  onClick={acceptMember}
                >
                  {accepting && <SpinLoader />}
                  {t('general.accept')}
                </Button>
                <Button
                  tw="text-danger border-danger bg-white hover:(text-white bg-danger)"
                  disabled={rejecting}
                  onClick={removeOrRejectMember}
                >
                  {rejecting && <SpinLoader />}
                  {t('general.reject')}
                </Button>
              </div>
            ) : (
              // else show member role dropdown, and more action button
              <Button
                btnType="danger"
                disabled={isButtonDisabled}
                onClick={() => {
                  setIsButtonDisabled(true);
                  removeOrRejectMember();
                }}
              >
                {isButtonDisabled && <SpinLoader />}
                {t('general.remove_affiliation')}
              </Button>
            )}
          </div>
        </div>
        <hr />
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};

export default MemberCardAffiliate;
