import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { Project, User } from 'types';
import Link from 'next/link';
import { ExternalLinkAlt } from '@emotion-icons/fa-solid';
import { displayObjectDate, renderTeam, showColoredStatus, proposalStatus } from '~/utils/utils';
import A from '../primitives/A';

interface Props {
  score: number;
  amount: number;
  organizers: User[];
  submitted_at: boolean;
  project: Project;
  peerReviewDates: any;
  isValidated: boolean;
  id: number;
  peerReview: any;
}

const ProposalInfo: FC<Props> = ({
  score = '-',
  amount = '-',
  project,
  organizers,
  submitted_at = false,
  peerReviewDates,
  isValidated,
  id,
  peerReview,
}) => {
  const { t } = useTranslation('common');

  return (
    <div tw="flex-shrink-0 w-full p-4 md:w-80">
      <div tw="flex flex-wrap mt-4 mb-2 justify-around md:justify-center gap-x-4 gap-y-2">
        {submitted_at && (
          <div tw="text-center">
            <p tw="mb-0">{t('proposal.explanations.submissionDate')}</p>
            <div tw="font-bold md:text-xl">{displayObjectDate(submitted_at, 'L', true)}</div>
          </div>
        )}
        {score && (
          <div tw="text-center">
            <p tw="mb-0">{t('proposal.score.title')}</p>
            <div tw="font-bold md:text-xl">{`${score}/5`}</div>
          </div>
        )}
        {amount && (
          <div tw="text-center">
            <p tw="mb-0">{t('proposal.funding.amountRequested')}</p>
            <div tw="font-bold md:text-xl ">{amount || '-'}€</div>
          </div>
        )}
      </div>
      <hr tw="mb-4" />
      <div tw="grid gap-4 grid-cols-1">
        <div tw="flex items-center md:justify-between">
          <div>{t('proposal.card.status')}:&nbsp;</div>
          <span>{showColoredStatus(proposalStatus(submitted_at, peerReviewDates, score, isValidated), t)}</span>
        </div>
        <div tw="flex items-center md:justify-between">
          <div>{t('proposal.card.fromProject')}:&nbsp;</div>
          <Link href={`/project/${project.id}?t=proposals`} passHref>
            <a target="_blank" tw="font-bold">
              {project.title}&nbsp;
              <ExternalLinkAlt size={15} style={{ position: 'relative', top: '-2px' }} title="Go to project" />
            </a>
          </Link>
        </div>
        <div tw="flex items-center md:justify-between">
          <div>{t('proposal.card.forPeerReview')}:&nbsp;</div>
          <span tw="px-1 font-bold text-right hover:underline">
            <A href={`/peer-review/${peerReview?.object?.short_title}`}>{peerReview?.object?.title}</A>
          </span>
        </div>
        <div tw="flex items-center md:justify-between">
          <div>{t('peerReview.info.organizedBy')}:&nbsp;</div>
          <span tw="px-1 font-bold text-right hover:underline">
            <A href={`/${peerReview?.resource?.type}/${peerReview?.parent?.short_title}`}>
              {peerReview?.parent?.title}
            </A>
          </span>
        </div>
        <div tw="flex items-center justify-between">
          <div>{t('proposal.card.team')}:</div>
          {renderTeam(organizers?.slice(0, 6), 'proposal', id, organizers?.length, 'internal')}
        </div>
      </div>
    </div>
  );
};

export default ProposalInfo;
