import React, { FC, ReactNode, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { Proposal } from 'types';
import A from '../primitives/A';
import Button from '../primitives/Button';
import { displayObjectDate, hasPeerReviewDatePassed, showColoredStatus, proposalStatus } from '~/utils/utils';
import ProposalScoreModal from './ProposalScoreModal';
import { useModal } from '~/contexts/modalContext';
import PeerReviewEmailModalSingle from '../PeerReview/PeerReviewEmailModalSingle';
import Alert from 'components/Tools/Alert';
import { useApi } from 'contexts/apiContext';
import SpinLoader from '../Tools/SpinLoader';

interface Props {
  proposal: Proposal;
  score_threshold: number;
  peerReviewDates: any;
  peerReviewId: number;
  callBack: () => void;
  hasGrant: boolean;
}

const ProposalAdminCard: FC<Props> = ({
  proposal,
  score_threshold,
  peerReviewDates,
  peerReviewId,
  hasGrant,
  callBack,
}) => {
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();
  const [sending, setSending] = useState<'remove' | 'accepted' | undefined>(undefined);
  const [error, setError] = useState<string | ReactNode>(undefined);
  const api = useApi();

  const route = `/api/peer_reviews/${peerReviewId}/proposals/${proposal.id}`;

  const onSuccess = () => {
    setSending(undefined);
    callBack();
  };

  const onError = (err) => {
    console.error(err);
    setSending(undefined);
    setError(t('err-'));
  };

  const removeProposal = () => {
    setSending('remove');
    api
      .delete(route)
      .then(() => onSuccess())
      .catch((err) => onError(err));
  };

  const revalidateAndCloseModal = () => {
    callBack();
    closeModal();
  };

  const sendMessageToProposalAdmins = (id) => {
    showModal({
      children: <PeerReviewEmailModalSingle peerReviewId={peerReviewId} itemId={id} type="admins" />,
      title: t('user.contactModal.title_simple'),
    });
  };

  if (proposal) {
    return (
      <tr tw="border border-gray-500 md:border-none block md:table-row" key={proposal.id}>
        {/* Title */}
        <td tw="p-2 md:border md:border-gray-500 text-left block md:table-cell">
          <span tw="inline-block w-1/3 md:hidden font-bold">{t('entity.info.title')}</span>{' '}
          {proposal.submitted_at ? <A href={`/proposal/${proposal.id}`}>{proposal.title}</A> : proposal.title}
        </td>
        {/* Status */}
        <td tw="p-2 md:border md:border-gray-500 text-left block md:table-cell">
          <span tw="inline-block w-1/3 md:hidden font-bold">{t('attach.status')}</span>
          {showColoredStatus(
            proposalStatus(proposal.submitted_at, peerReviewDates, proposal.score, proposal.is_validated),
            t
          )}
        </td>
        {/* Submitted date */}
        <td tw="p-2 md:border md:border-gray-500 text-left block md:table-cell">
          <span tw="inline-block w-1/3 md:hidden font-bold">{t('proposal.card.submittedDate')}</span>
          {proposal.submitted_at ? displayObjectDate(proposal.submitted_at, 'L', true) : '-'}
        </td>
        {/* Funding */}
        {hasGrant && (
          <td tw="p-2 md:border md:border-gray-500 text-left block md:table-cell">
            <span tw="inline-block w-1/3 md:hidden font-bold">{t('proposal.card.fundingRequested')}</span>
            {proposal.funding}€
          </td>
        )}
        {/* Score */}
        <td tw="p-2 md:border md:border-gray-500 text-left block md:table-cell">
          <span tw="inline-block w-1/3 md:hidden font-bold">{t('proposal.score.title')}</span>
          {proposal?.score || '-'}/5
        </td>
        {/* Actions */}
        <td tw="p-2 md:border md:border-gray-500 text-left block md:table-cell">
          <div tw="grid gap-2">
            {/* Show button to give score only when proposal has been submitted, and we are after the deadline, and the peer review is not closed */}
            {proposal?.submitted_at &&
              (peerReviewDates.stop
                ? !hasPeerReviewDatePassed(peerReviewDates.stop)
                : hasPeerReviewDatePassed(peerReviewDates.deadline)) && (
                <Button
                  onClick={() => {
                    showModal({
                      children: (
                        <ProposalScoreModal
                          score_threshold={score_threshold}
                          id={proposal?.id}
                          score={proposal?.score}
                          callBack={revalidateAndCloseModal}
                        />
                      ),
                      title: !proposal?.score ? t('proposal.score.giveScore') : t('proposal.score.changeScore'),
                    });
                  }}
                >
                  {!proposal?.score ? t('proposal.score.giveScore') : t('proposal.score.changeScore')}
                </Button>
              )}
            <Button onClick={() => sendMessageToProposalAdmins(proposal.id)}>
              {t('user.contactModal.title_simple')}
            </Button>
            {!proposal?.submitted_at && (
              <Button btnType="danger" disabled={sending === 'remove'} onClick={removeProposal}>
                {sending === 'remove' && <SpinLoader />}
                {t('general.remove')}
              </Button>
            )}
          </div>
          {error && <Alert type="danger" message={error} />}
        </td>
      </tr>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};
export default ProposalAdminCard;
