import { FC, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import SpinLoader from '../Tools/SpinLoader';
import Alert from '../Tools/Alert';
import Button from '../primitives/Button';
import FormWysiwygComponent from '../Tools/Forms/FormWysiwygComponent';
import { useApi } from '~/contexts/apiContext';

interface Props {
  answer: any;
  questionId: number;
  proposalId: number;
}

const ProposalAnswerForm: FC<Props> = ({ answer, questionId, proposalId }) => {
  const { t } = useTranslation('common');
  const api = useApi();
  const [sending, setSending] = useState(false);
  const [newAnswer, setNewAnswer] = useState(answer);
  const [requestSent, setRequestSent] = useState(false);
  const [createdAnswerId, setCreatedAnswerId] = useState();
  const [showSubmitBtn, setShowSubmitBtn] = useState(false);

  const handleChange = (key, content) => {
    setNewAnswer((prev) => ({ ...prev, [key]: content }));
    setShowSubmitBtn(content !== answer?.content && content !== '<p><br></p>');
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    // if answer is not undefined, that means we are updating an answer
    if (answer !== undefined || createdAnswerId) {
      setSending(true);
      api
        .patch(`/api/answers/${answer?.id || createdAnswerId}`, { content: newAnswer.content })
        .then((_res) => {
          setSending(false);
          setRequestSent(true);
          setShowSubmitBtn(false);
          setTimeout(() => {
            setRequestSent(false);
          }, 2500);
        })
        .catch((err) => {
          console.error(err);
          setSending(false);
        });
    } else {
      // else, we are creating an answer
      setSending(true);
      api
        .post(`/api/answers`, {
          content: newAnswer.content,
          proposal_id: proposalId,
          document_id: questionId,
        })
        .then((_res) => {
          setSending(false);
          setRequestSent(true);
          setCreatedAnswerId(_res.data.id);
          setShowSubmitBtn(false);
          setTimeout(() => {
            setRequestSent(false);
          }, 2500);
        })
        .catch((err) => {
          console.error(err);
          setSending(false);
        });
    }
  };

  return (
    <form className="proposalForm">
      <FormWysiwygComponent
        id="content"
        title=""
        placeholder={t('proposal.form.writeAnswerplaceholder')}
        content={newAnswer?.content}
        onChange={handleChange}
        show
      />
      <Button disabled={sending || !showSubmitBtn} onClick={handleSubmit}>
        {sending && <SpinLoader />}
        {t('entity.form.btnSave')}
      </Button>
      {requestSent && <Alert type="success" message={t('general.editSuccessMsg')} />}
    </form>
  );
};

export default ProposalAnswerForm;
