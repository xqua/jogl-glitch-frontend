import React, { FC, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { Edit } from '@emotion-icons/boxicons-solid/Edit';
import Button from 'components/primitives/Button';
import ShareBtns from 'components/Tools/ShareBtns/ShareBtns';
import H1 from 'components/primitives/H2';
import { Project } from 'types';
import Link from 'next/link';
import ProposalInfo from './ProposalInfo';
import { useApi } from '~/contexts/apiContext';
import BtnJoin from '../Tools/BtnJoin';
import useMembers from '~/hooks/useMembers';
import ReactTooltip from 'react-tooltip';
import Swal from 'sweetalert2';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import { hasPeerReviewDatePassed } from '~/utils/utils';

interface Props {
  project: Project;
  submitted_at: Date;
  title: string;
  id: number;
  score: string;
  summary: string;
  funding: string;
  peerReviewDates: any;
  isAdmin: boolean;
  isMember: boolean;
  isPeerReviewAdmin: boolean;
  isValidated: boolean;
  hasAnsweredAllQuestions: boolean;
  peerReview: any;
}

const ProposalHeader: FC<Props> = ({
  project,
  submitted_at,
  score,
  isAdmin,
  isMember,
  funding,
  title,
  id,
  summary,
  peerReviewDates,
  isPeerReviewAdmin,
  isValidated,
  hasAnsweredAllQuestions,
  peerReview,
}) => {
  const { t } = useTranslation('common');
  const api = useApi();
  const { members } = useMembers('proposals', id, 25); // take 25 first members
  const [showSubmitBtn, setShowSubmitBtn] = useState(true);

  return (
    <header tw="relative w-full">
      <div tw="flex flex-wrap divide-x divide-gray-200 md:flex-nowrap">
        <div tw="order-2 w-full md:(order-1 w-auto) border-t border-solid border-gray-200">
          <ProposalInfo
            organizers={members}
            submitted_at={submitted_at}
            score={score}
            amount={funding}
            project={project}
            peerReviewDates={peerReviewDates}
            isValidated={isValidated}
            id={id}
            peerReview={peerReview}
          />
        </div>
        <div tw="px-4 py-4 md:px-8 md:mt-4 order-1 md:order-2">
          <div tw="inline-flex items-center">
            <div>
              <H1 tw="pr-3 pb-2 font-size[1.8rem] md:font-size[2.3rem]">{title}</H1>
            </div>
          </div>
          <div tw="mt-4 text-gray-600">
            <InfoHtmlComponent content={summary} />
          </div>

          <div tw="inline-flex space-x-4">
            {isAdmin && !submitted_at && (
              <Link href={`/proposal/${id}/edit`}>
                <Button tw="w-40">
                  <Edit size={23} title="Edit proposal" />
                  {t('entity.form.btnAdmin')}
                </Button>
              </Link>
            )}
            {!isAdmin && !submitted_at && (
              <BtnJoin
                joinState={isMember}
                itemType="proposals"
                itemId={id}
                // count={members_count}
                // showMembersModal={showMembersModal}
              />
            )}
            {/* {!submitted_at && <BtnFollow followState={true} itemType="proposals" itemId={id} />} */}
            {/* Show submit button if you're admin, if proposal have not been submitted, and we are before deadline date */}
            {!submitted_at && isAdmin && !hasPeerReviewDatePassed(peerReviewDates.deadline) && showSubmitBtn && (
              <div>
                <div
                  {...(!hasAnsweredAllQuestions && {
                    'data-tip': t('proposal.explanations.cantSubmitExplanation'),
                    'data-for': 'cantSubmitExplanation',
                  })}
                >
                  <Button
                    onClick={() => {
                      Swal.fire({
                        title: t('proposal.explanations.submissionAreYouSure'),
                        text: t('proposal.explanations.submissionExplanation'),
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: t('peerReview.form.btnSubmitProposal'),
                      }).then(({ isConfirmed }) => {
                        if (isConfirmed) {
                          api
                            .patch(`/api/proposals/${id}`, { proposal: { submitted_at: new Date(Date.now()) } })
                            .then(() => {
                              Swal.fire(
                                t('proposal.explanations.submissionSent'),
                                t('proposal.explanations.submissionSentMessage'),
                                'success'
                              ).then(({ isConfirmed }) => {
                                setShowSubmitBtn(false);
                                location.reload(); // force page refresh
                              });
                            });
                        }
                      });
                    }}
                    disabled={!hasAnsweredAllQuestions}
                  >
                    {t('peerReview.form.btnSubmitProposal')}
                  </Button>
                </div>
                <ReactTooltip
                  id="cantSubmitExplanation"
                  effect="solid"
                  role="tooltip"
                  type="dark"
                  className="forceTooltipBg"
                />
              </div>
            )}
            {submitted_at && <ShareBtns type="proposal" specialObjId={id} />}
          </div>
        </div>
      </div>
      {/* {(project.interests?.length > 0 || project.skills?.length > 0) && (
        <div>
          <hr tw="mb-4" />
          <div tw="hidden md:block">
            <PeerReviewSkillSdg interests={project.interests} skills={project.skills} />
          </div>
        </div>
      )} */}
    </header>
  );
};

export default ProposalHeader;
