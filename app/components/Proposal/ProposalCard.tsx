import Link from 'next/link';
import React, { FC } from 'react';
import ObjectCard from 'components/Cards/ObjectCard';
import H2 from 'components/primitives/H2';
import Title from 'components/primitives/Title';
import { Project, Proposal, Space, Program, PeerReview } from 'types';
import useGet from '~/hooks/useGet';
import useTranslation from 'next-translate/useTranslation';
import { displayObjectDate, showColoredStatus, proposalStatus } from '~/utils/utils';
import Loading from '../Tools/Loading';

interface Props {
  id: Pick<Proposal, 'id'>;
  peerReviewId: Pick<Proposal, 'peer_review_id'>;
  projectId: Pick<Proposal, 'project_id'>;
  cardFormat?: string;
  chip?: string;
}
const ProposalCard: FC<Props> = ({ id, peerReviewId, projectId, cardFormat, chip }) => {
  const { t } = useTranslation('common');
  const proposalUrl = `/proposal/${id}`;
  const { data: proposal } = useGet<Proposal>(`/api/proposals/${id}`);
  const { data: proposal_project } = useGet<Project>(`/api/projects/${projectId}`);
  const { data: peer_review } = useGet<PeerReview>(`/api/peer_reviews/${peerReviewId}`);
  const peerReviewResource = { type: peer_review?.resource_type.toLowerCase(), id: peer_review?.resource_id };
  const { data: peer_review_parent } = useGet<Space | Program>(
    `/api/${peerReviewResource.type}s/${peerReviewResource.id}`
  );

  return (
    <ObjectCard imgUrl={proposal_project?.banner_url} href={proposalUrl} chip={chip} cardFormat={cardFormat}>
      {proposal && proposal_project && peer_review ? (
        <>
          {cardFormat === 'showObjType' && (
            <div tw="bg-[#9fd39f] w-[calc(100% + 32px)] pl-3 flex flex-wrap -mx-4 mt-[-.8rem] mb-[.8rem]">
              <span tw="px-1 space-x-1 font-bold text-sm">{t('general.proposal_1')}</span>
            </div>
          )}
          <div tw="inline-flex items-center md:h-14">
            <Link href={proposalUrl} passHref>
              <Title>
                <H2 tw="word-break[break-word] items-center line-clamp-2">
                  {proposal?.title || t('proposal.card.defaultTitle')}
                </H2>
              </Title>
            </Link>
          </div>
          <Hr tw="mt-3 pt-3" />
          <div tw="inline-flex flex-wrap">
            {t('proposal.card.fromProject')}:&nbsp;
            <Link href={`/project/${proposal_project?.id}?t=proposals`} passHref>
              <a tw="text-black font-medium underline hover:(text-black font-bold)">{proposal_project?.title}</a>
            </Link>
          </div>
          <div tw="inline-flex flex-wrap">
            <span tw="font-bold">
              {showColoredStatus(
                proposalStatus(
                  proposal?.submitted_at,
                  { start: peer_review?.start, deadline: peer_review?.deadline },
                  proposal?.score,
                  proposal?.is_validated
                ),
                t
              )}
            </span>
          </div>
          <Hr tw="mt-2 pt-4" />
          <div tw="inline-flex flex-wrap">
            {t('proposal.card.forPeerReview')}:&nbsp;
            <span tw="font-bold">
              <Link href={`/peer-review/${peer_review?.short_title}`} passHref>
                <a tw="text-black font-medium underline hover:(text-black font-bold)">{peer_review?.title}</a>
              </Link>
            </span>
          </div>
          <div tw="inline-flex flex-wrap mt-2">
            {t('proposal.card.organizedBy')}:&nbsp;
            <Link href={`/${peerReviewResource.type}/${peer_review_parent?.short_title}`} passHref>
              <a tw="text-black font-medium underline hover:(text-black font-bold)">{peer_review_parent?.title}</a>
            </Link>
          </div>
          {/* Stats */}
          <Hr tw="mt-3 pt-2" />
          <div tw="items-center justify-around space-x-2 flex flex-wrap">
            {proposal?.score && <CardData value={`${proposal?.score}/5`} title={'Score'} />}
            {/* {proposal?.funding && <CardData value={`${proposal?.funding}€`} title={t('proposal.card.fundingRequested')} />} */}
            {proposal?.funding && <CardData value={`${proposal?.funding}€`} title={t('proposal.card.micro_grant')} />}
            {proposal?.submitted_at && (
              <CardData
                value={displayObjectDate(proposal?.submitted_at, 'L', true)}
                title={t('proposal.card.submittedDate')}
              />
            )}
          </div>
        </>
      ) : (
        <Loading />
      )}
    </ObjectCard>
  );
};

const CardData = ({ value, title }) => (
  <div tw="flex flex-col justify-center items-center">
    <div tw="font-bold font-size[17px] -mb-1">{value}</div>
    <div tw="text-gray-500 font-medium text-sm">{title}</div>
  </div>
);
const Hr = (props) => <div tw="-mx-4 border-0 border-t border-solid border-color[rgba(0, 0, 0, 0.07)]" {...props} />;

export default ProposalCard;
