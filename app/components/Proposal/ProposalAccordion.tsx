import React, { useState, useRef, useEffect } from 'react';
import { ChevronRight } from '@emotion-icons/bootstrap';

const ProposalAccordion = ({ allactive, title, children }) => {
  const [active, setActive] = useState(false);
  const contentRef = useRef(null);

  useEffect(() => {
    contentRef.current.style.maxHeight = active ? `50000px` : '0px';
  }, [contentRef, active]);

  useEffect(() => {
    setActive(allactive);
  }, [allactive]);

  const toogleActive = () => {
    setActive(!active);
  };

  return (
    <div className="accordion-section">
      <button className="accordion-title" tw="py-2 text-left" onClick={toogleActive}>
        <h4 tw="font-bold mb-0 text-[1.2rem]">{title}</h4>
        <span className={active ? 'accordion-icon rotate' : 'accordion-icon'}>
          <ChevronRight size={25} title="collapse/un-collapse" />
        </span>
      </button>

      <div ref={contentRef} className="accordion-content">
        {/* tw="md:max-w-[1000px]" */}
        {active && children}
      </div>
    </div>
  );
};

export default ProposalAccordion;
