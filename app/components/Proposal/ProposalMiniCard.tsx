import Link from 'next/link';
import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import styled from 'utils/styled';
import Card from 'components/Cards/Card';
import { PeerReview } from '~/types';
import useGet from '~/hooks/useGet';
import { ExternalLinkAlt } from '@emotion-icons/fa-solid';
import A from '../primitives/A';
import { proposalStatus } from '~/utils/utils';

interface Props {
  title: string;
  submitted_at?: boolean;
  id?: number;
  peerReviewId: number;
}

const ProposalMiniCard: FC<Props> = ({ title, submitted_at = 'pending', id, peerReviewId }) => {
  const { t } = useTranslation('common');
  const { data: peer_review } = useGet<PeerReview>(`/api/peer_reviews/${peerReviewId}`);
  return (
    <CustomCard>
      <div tw="flex flex-col h-full w-[14rem] justify-between">
        <Link href={`/proposal/${id}`}>
          <a>
            <div tw="flex flex-col justify-center items-center space-y-6">
              <div tw="flex flex-col text-2xl font-black letter-spacing[-1pX]">
                <h3 tw="line-clamp-2 hover:underline">{title}</h3>
              </div>
            </div>
          </a>
        </Link>
        <div tw="inline-flex flex-wrap">
          <span tw="font-medium">{t('peerReview.title')}:&nbsp;</span>
          <A href={`/peer-review/${peer_review?.short_title}`}>
            {peer_review?.title}&nbsp;
            <ExternalLinkAlt size={15} style={{ position: 'relative', top: '-2px' }} title="Go to peer review" />
          </A>
        </div>
        <div
          tw="flex flex-col py-1 px-2 rounded-sm text-white mt-3 w-[fit-content]"
          style={{ backgroundColor: submitted_at ? '#008b2b' : '#dea500' }}
        >
          {/* {t(
            proposalStatus(submitted_at, { start: peer_review?.start, deadline: peer_review?.deadline }, 'manual score')
          )} */}
        </div>
      </div>
    </CustomCard>
  );
};

const CustomCard = styled(Card)`
  border: 1px solid lightgray;
  transition: box-shadow 0.3s ease-in-out;
  a {
    color: #38424f;
    text-decoration: none;
  }
`;

export default ProposalMiniCard;
