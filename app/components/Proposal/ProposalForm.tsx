import { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import FormDefaultComponent from 'components/Tools/Forms/FormDefaultComponent';
// import FormValidator from 'components/Tools/Forms/FormValidator';
// import proposalFormRules from './proposalFormRules.json';
import { Proposal } from 'types';
import { changesSavedConfAlert } from 'utils/utils';
import { useRouter } from 'next/router';
import SpinLoader from '../Tools/SpinLoader';
import Button from '../primitives/Button';
// import FormWysiwygComponent from '../Tools/Forms/FormWysiwygComponent';
import useGet from '~/hooks/useGet';
import FormTextAreaComponent from '../Tools/Forms/FormTextAreaComponent';
// import FormWysiwygComponent from '../Tools/Forms/FormWysiwygComponent';

interface Props {
  mode: 'edit' | 'create';
  proposal: Proposal;
  sending: boolean;
  hasUpdated: boolean;
  handleChange: (key, content) => void;
  handleSubmit: () => void;
}

const ProposalForm: FC<Props> = ({ proposal, sending = false, hasUpdated = false, handleChange, handleSubmit }) => {
  const { t } = useTranslation('common');
  const urlBack = `/proposal/${proposal.id}`;
  const router = useRouter();
  const { data: peer_review } = useGet(`/api/peer_reviews/${proposal.peer_review_id}`);

  // show conf message when proposal has been successfully saved/updated
  hasUpdated &&
    changesSavedConfAlert(t)
      .fire()
      // go back to proposal page if user clicked on conf button
      .then(({ isConfirmed }) => isConfirmed && router.push(urlBack));

  return (
    <form className="proposalForm">
      {/* Show warning msg if proposal's status is not submitted */}
      {/* {!proposal.submitted_at && (
        <Alert type="warning" message="proposal is in review and needs to be marked as submitted to be sent!" />
      )} */}
      <FormDefaultComponent
        content={proposal.title}
        id="title"
        mandatory
        onChange={handleChange}
        placeholder={t('proposal.form.title_placeholder')}
        title={t('entity.info.title')}
      />
      <FormTextAreaComponent
        content={proposal.summary}
        id="summary"
        maxChar={500}
        onChange={handleChange}
        rows={3}
        placeholder={t('proposal.form.description_placeholder')}
        title={t('entity.info.short_description')}
        mandatory
      />
      {peer_review?.maximum_disbursement !== null && (
        <FormDefaultComponent
          type="number"
          content={proposal.funding}
          id="funding"
          mandatory
          onChange={handleChange}
          placeholder={t('proposal.form.funding_placeholder')}
          title={`${t('proposal.funding.amountRequested')} (max = ${peer_review?.maximum_disbursement}€)`}
          minDate={0}
          maxDate={peer_review?.maximum_disbursement}
        />
      )}
      <div tw="space-x-2 mt-10 mb-3 text-center">
        <Link href={urlBack} passHref>
          <Button btnType="secondary" disabled={sending}>
            {t('entity.form.back')}
          </Button>
        </Link>
        <Button disabled={sending} onClick={handleSubmit}>
          {sending && <SpinLoader />}
          {t('entity.form.btnUpdate')}
        </Button>
      </div>
    </form>
  );
};

export default ProposalForm;
