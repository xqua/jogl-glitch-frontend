import React, { FC, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Button from '../primitives/Button';
import SpinLoader from '../Tools/SpinLoader';
import { useApi } from '~/contexts/apiContext';
import Alert from '../Tools/Alert';

interface Props {
  id: number;
  score: number;
  score_threshold: number;
  callBack: any;
}

const ProposalScoreModal: FC<Props> = ({ id, score: scoreProp, score_threshold = 5, callBack }) => {
  const { t } = useTranslation('common');
  const api = useApi();
  const [score, setScore] = useState(scoreProp);
  const [sending, setSending] = useState(false);
  const [hasUpdated, setHasUpdated] = useState(false);
  const scoreThreshold = 5;

  const handleChange = (e) => {
    let newScore = e.target.value === '' ? '' : parseFloat(e.target.value);
    newScore = newScore > scoreThreshold ? '' : newScore;
    // newScore = newScore > 5 ? '' : newScore;

    setScore(newScore);
  };

  const handleSubmit = () => {
    setSending(true);
    api
      .patch(`/api/proposals/${id}`, { proposal: { score } })
      .then(() => {
        setSending(false);
        setHasUpdated(true);
        callBack();
      })
      .catch(() => {
        setSending(false);
      });
  };

  return (
    <>
      <div className="content">
        <p>{t('proposal.score.message', { score_threshold: scoreThreshold })}</p>
        <div tw="flex items-center w-full">
          <span tw="font-bold mr-2">{t('proposal.score.title')}</span>
          <input
            type="number"
            value={score}
            onChange={handleChange}
            className="form-control"
            tw="w-1/4 px-3 py-[0.4rem] rounded-md border-gray-300 focus:(ring-primary border-primary)"
            min={0}
            max={scoreThreshold}
          />
        </div>
      </div>
      <Button disabled={sending} onClick={handleSubmit} tw="mt-5 mb-2">
        {sending && <SpinLoader />}
        {t('entity.form.btnUpdate')}
      </Button>
      {hasUpdated && <Alert type="success" message="Score was given!" />}
    </>
  );
};

export default ProposalScoreModal;
