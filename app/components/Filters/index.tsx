import useTranslation from 'next-translate/useTranslation';
import React, { FC } from 'react';
import FilterCheckbox from '../FilterCheckbox';
import Loading from '../Tools/Loading';

interface Props {
  resetButtonLabel: string;
  content: any; // not satisfactory type but really depending on JOGL object where filter is applied
  onChange: (e: any) => void;
  isError: boolean;
  errorMessage: string;
  selectedId: number;
  customWording: string;
}

const Filters: FC<Props> = ({
  resetButtonLabel,
  content,
  onChange,
  isError = false,
  errorMessage,
  selectedId,
  customWording = '',
}) => {
  const { t } = useTranslation('common');
  if (isError) {
    return <div>{errorMessage}</div>;
  }
  if (!content) {
    return <Loading />;
  }
  return (
    <div tw="flex space-x-4 w-full overflow-x-scroll" className="overflow-x">
      <FilterCheckbox
        label={customWording ? `All ${customWording}` : t(resetButtonLabel)}
        onChange={onChange}
        checked={selectedId === undefined}
        value={undefined}
      />
      {content.map((item, index) => (
        <FilterCheckbox
          iconSrc={item.img}
          key={index}
          onChange={onChange}
          checked={item.id === selectedId}
          value={item.id}
          label={item.title}
        />
      ))}
    </div>
  );
};

export default Filters;
