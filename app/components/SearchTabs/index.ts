import { ChallengesTab } from './ChallengesTab';
// import { GroupsTab } from './GroupsTab';
import { MembersTab } from './MembersTab';
import { NeedsTab } from './NeedsTab';
import { ProjectsTab } from './ProjectsTab';
import { SpacesTab } from './SpacesTab';
import { ProgramsTab } from './ProgramsTab';
import { PeerReviewsTab } from './PeerReviewsTab';
import { ProposalsTab } from './ProposalsTab';
import { TabIndex } from './types';

export const getSearchTab = (index: TabIndex): React.FC => {
  switch (index) {
    case 'challenges':
      return ChallengesTab;

    // case 'groups':
    //   return GroupsTab;

    case 'needs':
      return NeedsTab;

    case 'members':
    default:
      return MembersTab;

    case 'projects':
      return ProjectsTab;

    case 'spaces':
      return SpacesTab;

    case 'programs':
      return ProgramsTab;

    case 'peer-reviews':
      return PeerReviewsTab;

    case 'proposals':
      return ProposalsTab;
  }
};

export * from './ChallengesTab';
// export * from './GroupsTab';
export * from './MembersTab';
export * from './NeedsTab';
export * from './ProjectsTab';
export * from './SpacesTab';
export * from './ProgramsTab';
export * from './PeerReviewsTab';
export * from './types';
export * from './utils';
