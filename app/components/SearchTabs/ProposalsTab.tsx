import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Search from 'components/Search/Search';
import { useSearchStateContext } from 'contexts/searchStateContext';
import { TabPanel } from '@reach/tabs';

export const ProposalsTab: FC = () => {
  const { t } = useTranslation('common');
  const { index, searchState, setSearchState } = useSearchStateContext();

  return (
    <TabPanel id={index}>
      <Search
        searchState={searchState}
        index="Proposal"
        onSearchStateChange={setSearchState}
        refinements={[{ attribute: 'is_validated', searchable: false, showMore: false, type: 'toggle' }]}
        sortByItems={[{ label: t('general.filter.newly_updated'), index: 'Proposal' }]}
      />
    </TabPanel>
  );
};
