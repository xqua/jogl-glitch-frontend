import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Search from 'components/Search/Search';
import { useSearchStateContext } from 'contexts/searchStateContext';
import { TabPanel } from '@reach/tabs';

export const ChallengesTab: FC = () => {
  const { t } = useTranslation('common');
  const { index, searchState, setSearchState } = useSearchStateContext();

  return (
    <TabPanel id={index}>
      <Search
        searchState={searchState}
        index="Challenge"
        onSearchStateChange={setSearchState}
        refinements={[
          // { attribute: 'status', searchable: false, showMore: false },
          { attribute: 'skills' },
          // { attribute: 'program.title', searchable: false, showMore: false },
          // { attribute: 'space.title', searchable: false, showMore: false },
          { attribute: 'interests', searchable: false, limit: 17 },
        ]}
        sortByItems={[
          { label: t('general.filter.newly_updated'), index: 'Challenge_updated_at' },
          { label: t('general.filter.object.date2'), index: 'Challenge_id_des' },
          { label: t('general.filter.pop'), index: 'Challenge' },
          { label: t('general.star'), index: 'Challenge_saves_desc' },
          { label: t('general.member', { count: 2 }), index: 'Challenge_members' },
          { label: t('user.profile.tab.projects'), index: 'Challenge_projects' },
          { label: t('general.need', { count: 2 }), index: 'Challenge_needs' },
          { label: t('general.filter.object.date1'), index: 'Challenge_id_asc' },
        ]}
      />
    </TabPanel>
  );
};
