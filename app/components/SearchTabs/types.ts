export type TabIndex =
  | 'members'
  | 'needs'
  | 'projects'
  | 'groups'
  | 'challenges'
  | 'spaces'
  | 'programs'
  | 'peer-reviews'
  | 'proposals';
