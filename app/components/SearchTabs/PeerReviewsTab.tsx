import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Search from 'components/Search/Search';
import { useSearchStateContext } from 'contexts/searchStateContext';
import { TabPanel } from '@reach/tabs';

export const PeerReviewsTab: FC = () => {
  const { t } = useTranslation('common');
  const { index, searchState, setSearchState } = useSearchStateContext();

  return (
    <TabPanel id={index}>
      <Search
        searchState={searchState}
        index="PeerReview"
        onSearchStateChange={setSearchState}
        // refinements={[{ attribute: 'status', searchable: false, showMore: false }]}
        sortByItems={[{ label: t('general.filter.newly_updated'), index: 'PeerReview' }]}
      />
    </TabPanel>
  );
};
