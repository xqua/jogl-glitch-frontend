import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Search from 'components/Search/Search';
import { useSearchStateContext } from 'contexts/searchStateContext';
import { TabPanel } from '@reach/tabs';

export const SpacesTab: FC = () => {
  const { t } = useTranslation('common');
  const { index, searchState, setSearchState } = useSearchStateContext();

  return (
    <TabPanel id={index}>
      <Search
        searchState={searchState}
        index="Space"
        onSearchStateChange={setSearchState}
        refinements={[
          { attribute: 'space_type', searchable: false },
          { attribute: 'status', searchable: false, showMore: false },
        ]}
        sortByItems={[{ label: t('general.filter.pop'), index: 'Space' }]}
      />
    </TabPanel>
  );
};
