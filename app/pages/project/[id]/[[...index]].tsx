import { NextPage } from 'next';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Feed from 'components/Feed/Feed';
import Layout from 'components/Layout';
import ProjectAbout from 'components/Project/ProjectAbout';
import ProjectHeader from 'components/Project/ProjectHeader';
import BtnStar from 'components/Tools/BtnStar';
import { Project } from 'types';
import { getApiFromCtx } from 'utils/getApi';
import Image from 'components/primitives/Image';
import tw from 'twin.macro';
import 'intro.js/introjs.css';
import ProjectInfo from 'components/Project/ProjectInfo';
import { Tab, TabPanel, NavContainer, Nav, OverflowGradient } from '~/utils/style';
import ProposalCard from '~/components/Proposal/ProposalCard';
import Grid from '~/components/Grid';
import DocumentsManager from '~/components/Tools/Documents/DocumentsManager';
import useNeeds from '~/hooks/useNeeds';
import NeedCard from '~/components/Need/NeedCard';
import NeedCreate from '~/components/Need/NeedCreate';
import NoResults from '~/components/Tools/NoResults';
import useUserData from '~/hooks/useUserData';
import Loading from '~/components/Tools/Loading';
import ProjectSkillSdg from '~/components/Project/ProjectSkillSdg';
import ProjectMembers from '~/components/Project/ProjectMembers';
import useGet from '~/hooks/useGet';
import { displayObjectDate, formatNumberToShowCommas } from '~/utils/utils';
import Trans from 'next-translate/Trans';

const DEFAULT_TAB = 'about';

const ProjectDetails: NextPage<{ project: Project }> = ({ project }) => {
  const router = useRouter();
  const { t } = useTranslation('common');
  const { userData } = useUserData();
  const { dataNeeds, needsRevalidate } = useNeeds('projects', project?.id);
  const [selectedTab, setSelectedTab] = useState(DEFAULT_TAB);
  const headerRef = useRef();
  const needTabRef = useRef();
  const navRef = useRef();
  const grant_info = project.grant_info?.split(', ');
  // get the first project's proposal that is validated
  const validated_proposal = project.proposals?.filter((p) => p.is_validated)?.[0];
  const { data: peer_review } = useGet(validated_proposal && `/api/peer_reviews/${validated_proposal?.peer_review_id}`);

  // Test data for displaying vertical timeline
  // const testTimelineData = [
  //   {
  //     id: 'prototyping',
  //     title: 'Prototyping',
  //     color: 'green',
  //     date_start: '2020-10-25',
  //     date_end: '2020-12-10',
  //     moments: [
  //       {
  //         id: '1',
  //         title: 'First community meeting',
  //         description: '...',
  //         date: '2020-10-25',
  //       },
  //       {
  //         id: '3',
  //         title: 'Start design',
  //         description: '...',
  //         date: '2020-11-14',
  //       },
  //     ],
  //   },
  //   {
  //     id: 'testing',
  //     title: 'Testing',
  //     color: 'blue',
  //     date_start: '2020-12-10',
  //     date_end: '2021-02-05',
  //     moments: [
  //       {
  //         id: '4',
  //         title: 'Prototype testing',
  //         description: '...',
  //         date: '2021-01-08',
  //       },
  //     ],
  //   },
  // ];

  const showNeedsTab = (project.needs_count !== 0 && (!project.is_admin || !userData)) || project.is_admin;
  const showDocumentsTab = (project?.documents.length !== 0 && (!project?.is_admin || !userData)) || project.is_admin;

  const tabs = [
    { value: 'info', translationId: 'general.tab.info' },
    { value: 'feed', translationId: 'user.profile.tab.feed' },
    { value: 'about', translationId: 'general.tab.about' },
    { value: 'members', translationId: 'entity.card.members' },
    // hide needs tab if user is not admin and there are no needs
    ...(showNeedsTab ? [{ value: 'needs', translationId: 'entity.tab.needs' }] : []),
    // hide documents tab if user is not admin and there are no docs
    ...(showDocumentsTab ? [{ value: 'documents', translationId: 'entity.tab.documents' }] : []),
    ...(project.proposals.filter((proposal) => proposal.submitted_at).length > 0
      ? [{ value: 'proposals', translationId: 'peerReview.tabs.proposals' }]
      : []),
    // NOTE: "..." shows information with conditions, here with tab, without having to show all prop
  ];

  // useEffect(() => {
  //   (project.is_member || router.query.t === 'feed') && setIsMember(true);
  // }, [project.is_member]);

  useEffect(() => {
    // on first load, check if url has a tab param, and if it does, select this tab
    router.query.tab && setSelectedTab(router.query.tab as string);
  }, []);

  // function when changing tab (or when router change in general)
  useEffect(() => {
    const tabValues = tabs.map(({ value }) => value);
    if (tabValues.includes(router.query.tab as string)) {
      if (router.query.tab === 'about') {
        router.push(`/project/${router.query.id}`, undefined, {
          shallow: true,
        });
      }
      const navContainerTopPosition = navRef.current.getBoundingClientRect().top + window.scrollY; // get y position of nav container
      const yAdjustment = window.innerWidth < 768 ? 40 : 150; // change yAdustement depending on if it's mobile/tablet or desktop
      window.scrollTo(0, navContainerTopPosition - yAdjustment); // force scroll to top of the nav/tab (remove a little to be really on top)
      setSelectedTab(router.query.tab as string);
    }
    if (!router.query.tab) setSelectedTab(DEFAULT_TAB);
  }, [router]);

  const displayInfoBox = () => (
    <div tw="bg-[#E7F0F3] border-l-4 border-blue-200 p-4 mb-6">
      <div tw="flex items-center mb-2">
        <div tw="flex-shrink-0">
          <svg
            tw="h-5 w-5 text-[#193c47]"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 20 20"
            fill="currentColor"
            aria-hidden="true"
          >
            <path
              fill-rule="evenodd"
              d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
              clip-rule="evenodd"
            />
          </svg>
        </div>
        <span tw="font-bold text-[#193c47]">{t('space.form.home_info')}</span>
      </div>
      <ul tw="space-y-2 mb-0">
        <li>
          <span tw="underline">{t('entity.info.short_name')}</span>: #{project.short_title}
        </li>

        {project?.created_at && (
          <li>
            <span tw="underline">{t('general.created_at')}</span>
            {displayObjectDate(project?.created_at, 'LL', true)}
          </li>
        )}
        {((project?.updated_at &&
          project?.created_at &&
          project?.created_at.substr(0, 10) !== project?.updated_at.substr(0, 10)) ||
          (project?.updated_at && !project?.created_at)) && (
          <li>
            <span tw="underline">{t('general.updated_at')}</span>
            {displayObjectDate(project?.updated_at, 'LL', true)}
          </li>
        )}
        {project.is_looking_for_collaborators && (
          <li>
            <span tw="underline">{t('project.form.looking_for_collab_title')}</span>:&nbsp;✅
          </li>
        )}
        {grant_info && (
          <li>
            <span tw="underline">{t('project.info.grantInfoTitle')}</span>
            <Trans
              i18nKey="common:project.info.grantInfoDescription"
              components={[
                <span />,
                <strong />,
                // transform name to link if last param is link
                grant_info[3] ? <a href={grant_info[3]} target="_blank" /> : <strong />,
                <strong />,
              ]}
              values={{
                grantAmount: formatNumberToShowCommas(grant_info[1]) + '€',
                grantNameRound: grant_info[0],
                grantDate: displayObjectDate(new Date(grant_info[2]), 'L', true),
              }}
            />
          </li>
        )}
        {validated_proposal && peer_review && (
          <li>
            <span tw="underline">{t('project.info.grantInfoTitle')}</span>
            <Trans
              i18nKey="common:project.info.grantInfoDescription"
              components={[
                <span />,
                <strong />,
                <a href={`/peer-review/${peer_review?.short_title}`} target="_blank" />,
                <strong />,
              ]}
              values={{
                grantAmount: validated_proposal?.funding
                  ? formatNumberToShowCommas(validated_proposal?.funding) + '€'
                  : 'positive review',
                grantNameRound: peer_review?.title,
                grantDate: displayObjectDate(new Date(peer_review?.stop), 'L', true),
              }}
            />
          </li>
        )}
      </ul>
    </div>
  );

  return (
    <Layout
      title={project?.title && `${project.title} | JOGL`}
      desc={project?.short_description}
      img={project?.banner_url || '/images/default/default-project.jpg'}
      className="small-top-margin"
      noIndex={project?.status === 'draft'}
    >
      <div tw="sm:shadow-custom2">
        <div tw="relative">
          <Image
            quality="100"
            priority
            src={project?.banner_url || '/images/default/default-project.jpg'}
            alt={`${project?.title} banner`}
            tw="w-full! max-h-[400px]! object-cover"
          />
          <div tw="absolute right-0 top-0 m-4">
            <BtnStar
              itemType="projects"
              itemId={project.id}
              hasStarred={project.has_saved}
              count={project.saves_count}
            />
          </div>
        </div>
        <div tw="bg-[#A9CCF7] w-full pl-4 py-1 flex flex-wrap" ref={headerRef}>
          <span tw="px-1 space-x-1 font-bold md:text-xl">{t('project.title')}</span>
        </div>

        <ProjectHeader project={project} forceClickNeedTab={() => needTabRef?.current?.click()} />

        {/* ---- Page grid starts here ---- */}
        <div
          tw="mb-6 grid grid-cols-1 md:(bg-white grid-template-columns[16rem calc(100% - 16rem)]) bg-white border-t border-solid border-gray-200"
          ref={navRef}
        >
          {/* Header and nav (left col on desktop, top col on mobile */}
          <div tw="flex flex-col bg-[#F7F7F9] sticky top-[60px] z-[8] md:(items-center pt-5)">
            <NavContainer>
              <OverflowGradient tw="md:hidden" gradientPosition="left" />
              <OverflowGradient tw="md:hidden" gradientPosition="right" />
              <Nav as="nav" tw="flex gap-3 pt-2.5 w-full md:(flex-col gap-0 pt-0)">
                {tabs.map((item, index) => (
                  <Link
                    shallow
                    scroll={false}
                    key={index}
                    href={`/project/${router.query.id}${item.value === DEFAULT_TAB ? '' : `?tab=${item.value}`}`}
                  >
                    <Tab
                      selected={item.value === selectedTab}
                      tw="px-1 py-3 md:px-3"
                      as="button"
                      id={`${item.value}-tab`}
                      css={[item.value === 'info' ? tw`md:hidden` : tw`block`]}
                    >
                      {t(item.translationId)}
                    </Tab>
                  </Link>
                ))}
              </Nav>
            </NavContainer>
          </div>

          {/* Main content, that change content depending on the selected tab (middle content on desktop) */}
          <div tw="flex flex-col px-0 pb-10 relative md:px-4 md:(pl-4) xl:(pr-4)">
            {selectedTab === 'info' && (
              <TabPanel id="info" tw="md:hidden">
                <div>
                  <ProjectInfo project={project} />
                  <hr tw="mb-4" />
                  {displayInfoBox()}
                </div>
                {(project.interests?.length > 0 || project.skills?.length > 0) && (
                  <div>
                    {/* <hr tw="mb-4" /> */}
                    <ProjectSkillSdg interests={project.interests} skills={project.skills} />
                  </div>
                )}
              </TabPanel>
            )}

            {selectedTab === 'about' && (
              <TabPanel id="about">
                <div tw="w-full xl:w-[86ch]">
                  <ProjectAbout project={project} />
                  <div tw="hidden md:block mt-4">{displayInfoBox()}</div>
                  {(project.interests?.length > 0 || project.skills?.length > 0) && (
                    <div tw="hidden md:block">
                      <ProjectSkillSdg interests={project.interests} skills={project.skills} />
                    </div>
                  )}
                </div>
              </TabPanel>
            )}

            {selectedTab === 'feed' && (
              <TabPanel id="feed" tw="mx-auto w-full px-0">
                {/* Show feed, and pass DisplayCreate to admins */}
                {project.feed_id && (
                  <Feed
                    feedId={project.feed_id}
                    allowPosting={project?.is_member}
                    isAdmin={project.is_admin || project.is_owner}
                    needToJoinMsg={project.is_private}
                  />
                )}
              </TabPanel>
            )}

            {/* Members */}
            {selectedTab === 'members' && (
              <TabPanel id="members" tw="relative ">
                <ProjectMembers projectId={project.id} />
              </TabPanel>
            )}

            {selectedTab === 'needs' && (
              <TabPanel id="needs">
                {!!project.is_admin && <NeedCreate projectId={project.id} refresh={needsRevalidate} />}
                <Grid>
                  {!dataNeeds ? (
                    <Loading />
                  ) : dataNeeds.length === 0 ? (
                    <NoResults type="need" />
                  ) : (
                    [...dataNeeds]
                      .reverse()
                      .map((need, i) => (
                        <NeedCard
                          key={i}
                          title={need.title}
                          skills={need.skills}
                          resources={need.ressources}
                          hasSaved={need.has_saved}
                          id={need.id}
                          postsCount={need.posts_count}
                          membersCount={need.members_count}
                          publishedDate={need.created_at}
                          dueDate={need.end_date}
                          status={need.status}
                        />
                      ))
                  )}
                </Grid>
              </TabPanel>
            )}

            {selectedTab === 'documents' && (
              <TabPanel id="documents">
                <div>
                  <DocumentsManager
                    isAdmin={project.is_admin}
                    documents={project.documents}
                    itemId={project.id}
                    itemType="projects"
                  />
                </div>
              </TabPanel>
            )}

            {selectedTab === 'proposals' && (
              <TabPanel id="proposals">
                <Grid>
                  {project.proposals
                    .filter(({ submitted_at }) => (!project.is_admin ? submitted_at : true)) // don't show proposals that have not yet been submitted (except for admin)
                    .map((proposal, index) => (
                      <ProposalCard
                        key={index}
                        id={proposal.id}
                        projectId={proposal.project_id}
                        peerReviewId={proposal.peer_review_id}
                      />
                    ))}
                </Grid>
              </TabPanel>
            )}
          </div>
        </div>
      </div>
    </Layout>
  );
};

// const TimelinePhaseBox = styled.div(({ color }) => [
//   tw`relative p-4 text-xl font-bold text-center bg-white border-solid rounded`,
//   `border-color: ${color};`,
// ]);

export const getServerSideProps = async (ctx) => {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/api/projects/${ctx.query.id}`).catch((err) => console.error(err));
  if (res) return { props: { project: res.data } };
  return { redirect: { destination: '/search/projects', permanent: false } };
};

export default ProjectDetails;
