import { useEffect, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import Layout from 'components/Layout';
import Loading from 'components/Tools/Loading';
import UserCard from 'components/User/UserCard';
import { useApi } from 'contexts/apiContext';
import useUserData from 'hooks/useUserData';
import { useModal } from 'contexts/modalContext';
import { showCompleteProfileModal } from 'utils/onboarding/index';
import styled from 'utils/styled';
import UserForm from '~/components/User/UserForm';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  box-shadow: ${(p) => p.theme.shadows.default};
  border-radius: ${(p) => p.theme.radii.xl};
  border: 1px solid ${(p) => p.theme.colors.greys['200']};
  background: white;
`;

const CompleteProfile = () => {
  const [suggestedUser, setSuggestedUser] = useState(undefined);
  const [user, setUser] = useState(undefined);
  const [hasSetUserData, setHasSetUserData] = useState(false);
  const { t } = useTranslation('common');
  const modal = useModal();
  const sawCompleteProfileModal =
    typeof window !== 'undefined' && JSON.parse(localStorage.getItem('sawCompleteProfileModal'));
  const api = useApi();
  const router = useRouter();
  const { userData } = useUserData();
  // if env is dev, suggestuserId is 1, else if it's prod, make it 191 which is the JOGL user on app.jogl.io
  const suggestedUserId = process.env.NODE_ENV !== 'production' ? 1 : 191;
  useEffect(() => {
    api
      .get(`/api/users/${suggestedUserId}`) // get jogl user
      .then((res) => setSuggestedUser(res.data))
      .catch((err) => console.error(`Couldn't GET user id=${suggestedUserId}`, err));
  }, []);

  // get userData once only
  useEffect(() => {
    !hasSetUserData && userData && setUser(userData);
    userData && setHasSetUserData(true);
  }, [userData]);

  useEffect(() => {
    // [Onboarding][1][New user] -  Launch complete-profile onboarding modal, only on 1st signin
    if (router.query?.launch_onboarding === 'true') {
      localStorage.setItem('isOnboarding', 'true');
      // so modal is shown only once, not each time we reload the complete-profile, has to be localStorage instead of state.
      if (sawCompleteProfileModal !== true) {
        setTimeout(() => {
          showCompleteProfileModal(modal, t);
        }, 1500);
      }
    }
  }, [sawCompleteProfileModal, router.query]);

  return (
    <Layout title={`${t('completeProfile.title')} | JOGL`} noHeaderFooter>
      <Container tw="m-auto w-[96%] md:w-[90%] lg:(mt-24 w-[70%])">
        <div tw="text-center pt-6 pb-5 background[#2987cd] rounded-t-2xl text-white lg:(pb-8 pt-10)">
          <h2 id="signModalLabel">{t('user.profile.edit.complete')}</h2>
        </div>
        <div tw="p-3 lg:p-6">
          <Loading active={!user} height="200px">
            <UserForm user={user} inCompleteProfile />
          </Loading>
          {!user && <p tw="text-center italic">{t('signUp.still_loading')}</p>}
        </div>
      </Container>
      {/* "You might want to follow" - section */}
      <div tw="flex flex-col text-center mx-auto my-8 width[fit-content]">
        {suggestedUser && (
          <>
            <p>{t('completeProfile.suggestFollow')}</p>
            <UserCard
              id={suggestedUser.id}
              firstName={suggestedUser.first_name}
              lastName={suggestedUser.last_name}
              nickName={suggestedUser.nickname}
              shortBio={suggestedUser.short_bio}
              logoUrl={suggestedUser.logo_url}
              hasFollowed={suggestedUser.has_followed}
              isCompact
            />
          </>
        )}
      </div>
    </Layout>
  );
};
export default CompleteProfile;
