import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useContext, useEffect, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { UserContext } from 'contexts/UserProvider';
import MyFeed from 'components/Feed/MyFeed';
// import Grid from 'components/Grid';
import Image from 'components/primitives/Image';
import Layout from 'components/Layout';
// import NeedCard from 'components/Need/NeedCard';
import A from 'components/primitives/A';
import Button from 'components/primitives/Button';
// import ProjectList from 'components/Project/ProjectList';
// import ProjectRecommended from 'components/Project/ProjectRecommended';
import Loading from 'components/Tools/Loading';
import useGet from 'hooks/useGet';
import Link from 'next/link';
import tw, { css, styled } from 'twin.macro';
// import { NavTab, TabListStyle } from 'components/Tabs/TabsStyles';
import 'intro.js/introjs.css';
import useUserData from 'hooks/useUserData';
import { useModal } from 'contexts/modalContext';
import { startHomeIntro, showOnboardingNewFeature } from 'utils/onboarding/index';
import FeaturedObjectsList from 'components/Tools/FeaturedObjectsList';
import FeaturedObjectsCarousel from 'components/Tools/FeaturedObjectsCarousel';
import ObjectCard from '~/components/Cards/ObjectCard';
import Title from '~/components/primitives/Title';
import H2 from '~/components/primitives/H2';
import { FolderShared } from '@emotion-icons/material';
import { ChevronDown, ChevronUp } from '@emotion-icons/bootstrap';
import UserObjects from '~/components/User/UserObjects';
import { Star } from '@emotion-icons/boxicons-solid';

const CustomLoader = () => {
  return (
    <div
      style={{
        display: 'flex',
        height: '100vh',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        backgroundColor: '#f0f0f0',
      }}
    >
      <img src="/images/logo-vertical.png" alt="JOGL icon" width="150px" />
      <Loading />
    </div>
  );
};

interface PageProps {}
const Home: NextPage<PageProps> = () => {
  const { userData, isConnected } = useContext(UserContext);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(false);
  }, [isConnected]);

  if (!loading) {
    if (isConnected) {
      return <HomeConnected user={userData} />;
    }
    return <HomeNotConnected />;
  } else return <CustomLoader />;
};

const HomeConnected = ({ user }) => {
  const router = useRouter();
  const { t } = useTranslation('common');
  const [userInfo, setUserInfo] = useState(undefined);
  const { showModal } = useModal();
  const [isFavListCollapsed, setIsFavListCollapsed] = useState(true);
  // const { data: projects } = useGet('/api/projects?items=8&order=desc');
  // const { data: needs } = useGet('/api/needs?items=4&order=desc');
  const { data: savedObjects } = useGet(`/api/users/saved_objects`);
  const savedObjectsNb = savedObjects && Object.values(savedObjects).flat().length;

  const isOnboarding = typeof window !== 'undefined' && JSON.parse(localStorage.getItem('isOnboarding'));
  const isAnIndividual = typeof window !== 'undefined' && JSON.parse(localStorage.getItem('isIndividual'));
  // const isAnOrganization = typeof window !== "undefined" && JSON.parse(localStorage.getItem('isOrganization'));
  const isDesktop = typeof window !== 'undefined' && JSON.parse(localStorage.getItem('isDesktop'));
  const sawOnboardingFeature =
    typeof window !== 'undefined' && JSON.parse(localStorage.getItem('sawOnboardingFeature'));
  const { userData } = useUserData();
  const modal = useModal();
  // const hasRecommendedObjects = recommendedNeeds?.length !== 0 && recommendedProjects?.length !== 0;
  const [activeTab, setActiveTab] = useState('feed');

  // const filteredRecommendedUsers =
  //   recommendedUsers &&
  //   [...recommendedUsers]
  //     .filter((user) => user.targetable_node !== null)
  //     // don't show archived users
  //     .filter((user) => user.targetable_node?.user.active_status !== 'archived')
  //     .slice(0, 4);
  // // take only if of recommended users and add them in an array
  // const filteredRecommendedUsersIds =
  //   filteredRecommendedUsers && Array.from(filteredRecommendedUsers, (u) => u.targetable_node?.user.id);

  // const filteredRecommendedProjects =
  //   recommendedProjects &&
  //   recommendedProjects
  //     .filter((proj) => proj.targetable_node !== null)
  //     .filter((proj) => proj.targetable_node?.project?.status !== 'draft')
  //     .slice(0, 4); // don't show draft projects, and get first 4 projects

  // const filteredRecommendedProjectsIds =
  //   filteredRecommendedProjects && Array.from(filteredRecommendedProjects, (p) => p.targetable_node?.project.id);

  // const filteredRecommendedNeeds =
  //   recommendedNeeds && [...recommendedNeeds].filter((need) => need.targetable_node !== null).slice(0, 4);
  // const filteredRecommendedNeedsIds =
  //   filteredRecommendedNeeds && Array.from(filteredRecommendedNeeds, (n) => n.targetable_node?.need.id);

  // [Onboarding][6] - Retaking onboarding flow on last page, home
  useEffect(() => {
    if (isOnboarding && isAnIndividual) {
      setTimeout(() => {
        startHomeIntro(modal, t, isDesktop);
      }, 2000);
    }
  }, [isOnboarding, isAnIndividual]);

  // [Onboarding][1][Former user but new feature] - Trigger announcing modal
  useEffect(() => {
    setUserInfo(userData);
  }, [userData]);

  useEffect(() => {
    if (sawOnboardingFeature !== true && userInfo) {
      showOnboardingNewFeature(modal, t, router, userInfo);
    }
  }, [sawOnboardingFeature, userInfo]);

  return (
    <>
      {user.id ? (
        <Layout>
          <div tw="mt-24 flex flex-col mx-auto md:px-3 xl:px-0">
            <nav tw="px-2 mb-2 inline-flex border-solid border-0 border-b border-gray-200 md:hidden">
              <div
                className="feedtab"
                tw="px-2 pb-1 text-lg cursor-pointer hocus:(border-solid border-0 border-b-2 border-gray-500)"
                css={[activeTab === 'feed' && tw`border-0 border-b-2 border-gray-500 border-solid`]}
                onClick={() => setActiveTab('feed')}
              >
                {t('feed.title')}
              </div>
              <div
                className="newstab"
                tw="px-2 pb-1 text-lg cursor-pointer hocus:(border-solid border-0 border-b-2 border-gray-500)"
                css={[activeTab === 'news' && tw`border-0 border-b-2 border-gray-500 border-solid`]}
                onClick={() => setActiveTab('news')}
              >
                {t('home.featured')}
              </div>
            </nav>

            <div tw="mt-5 lg:(w-full mt-0)">
              <div tw="flex mx-0">
                {/* Left column */}
                <div tw="min-w-[270px] px-2 relative hidden lg:(block pl-0)">
                  <div tw="sticky top-[4.8rem]">
                    <ObjectCard tw="justify-between" width="100%">
                      <div>
                        <div tw="-mx-4 -mt-4 height[96px]">
                          <img
                            src="/images/userCardShapeMember.svg"
                            tw="w-full h-[fit-content] object-cover"
                            loading="lazy"
                          />
                        </div>
                        <A href={`/user/${user.id}/${user.nickname}`} noStyle>
                          <div
                            css={[
                              tw`w-24 h-24 mx-auto mt-[-5.5rem]`,
                              css`
                                img {
                                  ${tw`(object-cover rounded-full w-24 h-24 border-white bg-white border-4 border-solid)!`};
                                }
                              `,
                            ]}
                          >
                            <Image src={user.logo_url} priority />
                          </div>
                        </A>
                        <Link href={`/user/${user.id}/${user.nickname}`} passHref>
                          <Title>
                            <H2 tw="word-break[break-word] text-[1.3rem] text-center my-3">
                              {user.first_name} {user.last_name}
                            </H2>
                          </Title>
                        </Link>
                        <div tw="flex items-center justify-between flex-wrap mb-2">
                          <div>{t('user.profile.tab.projects')}</div>
                          {user.stats.projects_count}
                        </div>
                        <div tw="flex items-center justify-between flex-wrap">
                          <div>{t('general.spaces')}</div>
                          {user.stats.spaces_count}
                        </div>
                        <hr />
                        <div
                          tw="cursor-pointer hover:opacity-90"
                          onClick={() => {
                            showModal({
                              children: <UserObjects userId={user.id} userStats={user.stats} isUser />,
                              title: t('user.objects.title'),
                              maxWidth: '70rem',
                            });
                          }}
                        >
                          <FolderShared size={20} title="My portfolio" />
                          {t('user.objects.title')}
                        </div>
                      </div>
                    </ObjectCard>
                    <div tw="my-5"></div>
                    {/*  */}
                    <div tw="pt-4 flex flex-col overflow-hidden bg-white shadow-custom rounded-xl w-full text-sm">
                      <p tw="mb-1 px-4 underline">{t('user.profile.starred_items')}</p>
                      {savedObjectsNb > 0 ? (
                        <div tw="space-y-2" css={isFavListCollapsed && tw`h-[10.5rem] overflow-hidden`}>
                          {savedObjects?.challenges.map((challenge) => (
                            <ObjectLink link={`/challenge/${challenge.short_title}`} title={challenge.title} />
                          ))}
                          {savedObjects?.projects.map((project) => (
                            <ObjectLink link={`/project/${project.id}`} title={project.title} />
                          ))}
                          {savedObjects?.needs.map((need) => (
                            <ObjectLink link={`/need/${need.id}`} title={need.title} />
                          ))}
                          {savedObjects?.programs.map((program) => (
                            <ObjectLink link={`/program/${program.short_title}`} title={program.title} />
                          ))}
                          {savedObjects?.spaces.map((space) => (
                            <ObjectLink link={`/space/${space.short_title}`} title={space.title} />
                          ))}
                          {savedObjects?.peer_reviews.map((peerReview) => (
                            <ObjectLink link={`/peer-review/${peerReview.short_title}`} title={peerReview.title} />
                          ))}
                          {savedObjects?.proposals.map((proposal) => (
                            <ObjectLink link={`/proposal/${proposal.id}`} title={proposal.title} />
                          ))}
                        </div>
                      ) : (
                        <div tw="px-4 my-4 text-gray-600">{t('list.noResult.star')}</div>
                      )}
                      {savedObjectsNb > 6 && (
                        <div
                          tw="cursor-pointer px-4 pt-2"
                          onClick={() => setIsFavListCollapsed((prevState) => !prevState)}
                        >
                          {isFavListCollapsed ? (
                            <p>
                              {t('general.showmore')}&nbsp;
                              <ChevronDown size={15} />
                            </p>
                          ) : (
                            <p>
                              {t('general.showless')}&nbsp;
                              <ChevronUp size={15} />
                            </p>
                          )}
                        </div>
                      )}
                    </div>
                  </div>
                </div>

                {/* Middle column, feed */}
                <div
                  tw="px-0 w-full relative hidden md:(w-2/3) lg:(block w-full)"
                  css={[activeTab === 'feed' && tw`block`]}
                >
                  <MyFeed user={user} />
                </div>

                {/* Right column */}
                <div
                  tw="px-3 w-full relative hidden md:(pr-0 pl-2 block w-1/3) lg:w-[310px] xl:(px-0)"
                  css={[activeTab === 'news' && tw`block`]}
                >
                  {/* <div className="justify-content-center"> */}
                  <div tw="sticky top-[4.8rem]">
                    <FeaturedObjectsList />
                    {/* Latest elements */}
                    {/* <div tw="mt-8">
                      <Tabs>
                        <TabPanels>
                          <TabPanel>
                            <section>
                              // Latest needs
                              <h4 style={{ margin: '24px 0 0' }}>{t('needs.latest')}</h4>
                              {needs ? (
                                <Grid tw="pt-3">
                                  {[...needs].reverse().map((need) => (
                                    <NeedCard
                                      key={need.id}
                                      title={need.title}
                                      project={need.project}
                                      hasSaved={need.has_saved}
                                      id={need.id}
                                      cardFormat="compact"
                                      width="250px"
                                    />
                                  ))}
                                </Grid>
                              ) : (
                                <Loading />
                              )}
                              <div tw="flex flex-col pt-1 pb-4">
                                <A href="/search/needs" passHref>
                                  {t('home.viewMoreRecent')}
                                </A>
                              </div>
                              // Latest projects
                              <div tw="sticky top-[4.8rem]">
                                <h4 style={{ margin: '10px 0 0' }}>{t('projects.latest')}</h4>
                                {projects && (
                                  <ProjectList
                                    listProjects={projects.filter(({ status }) => status !== 'draft').slice(0, 4)} // don't show draft projects, and get first 4 last projects out of the 6
                                    cardFormat="compact"
                                  />
                                )}
                                <div tw="flex flex-col pt-1 pb-4">
                                  <A href="/search/projects" passHref>
                                    {t('home.viewMoreRecent')}
                                  </A>
                                </div>
                              </div>
                            </section>
                          </TabPanel>
                        </TabPanels>
                      </Tabs>
                    </div> */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Layout>
      ) : (
        // Splash loader
        <CustomLoader />
      )}
    </>
  );
};

const HomeNotConnected = () => {
  const { t } = useTranslation('common');

  return (
    <Layout className="no-margin">
      <Background>
        <div tw="max-width[1280px] mx-auto pb-20 px-6 xl:px-3">
          <div tw="flex flex-col items-center pt-12 text-gray-800 mb-8 md:pt-16 lg:(flex-row justify-between) xl:pt-20">
            <div tw="max-width[60ch] flex flex-col flex-1 items-center lg:(items-start justify-between flex-auto)">
              <h1 tw="text-center font-bold text-[2rem] leading-tight lg:(text-left text-[2.6rem])">
                {t('home.intro.title')}
              </h1>
              <p tw="text-lg text-justify mt-8 mb-12 md:text-xl lg:(text-left text-2xl)">{t('home.intro.subTitle')}</p>
              <Link href="/signup">
                <Button tw="px-12 py-3 text-lg lg:(text-2xl px-14 py-3.5)">{t('home.intro.join')}</Button>
              </Link>
            </div>
            <div tw="flex flex-none w-full mx-auto justify-center mt-8 lg:(mt-0 w-2/4)">
              <Image tw="w-full sm:w-80! md:w-96! lg:w-full!" src="/images/home/Heading.svg" alt="Heading Homepage" />
            </div>
          </div>
          <div tw="flex flex-col mt-24 md:mt-40" className="content">
            <h2 tw="uppercase font-medium text-2xl lg:text-3xl">{t('home.howItWorks')}</h2>
            <div tw="flex flex-col justify-center mt-4 gap-y-8 gap-x-0 my-5 mx-auto lg:(flex-row gap-y-0 gap-x-20)">
              <div className="singleCard" tw="w-3/4 mx-auto sm:w-2/4 lg:w-2/5">
                <Image
                  tw="(flex mx-auto w-3/5 mb-3 min-height[auto] min-width[auto])!"
                  src="/images/home/Community.png"
                  alt="Community"
                />
                <div className="cardContent" tw="-mt-20 pt-12 px-6 pb-8 shadow-md rounded-xl bg-white">
                  <h3 tw="mt-8 uppercase text-primary text-center text-2xl font-medium leading-9">
                    {t('home.community.title')}
                  </h3>
                  <p tw="text-base leading-5 h-16">{t('home.community.valueProposal')}</p>
                </div>
              </div>
              <div className="singleCard" tw="w-3/4 mx-auto sm:w-2/4 lg:w-2/5">
                <Image
                  tw="(flex mx-auto w-3/5 mb-3 min-height[auto] min-width[auto])!"
                  src="/images/home/Contribute.svg"
                  alt="Contribute"
                />
                <div className="cardContent" tw="-mt-20 pt-12 px-6 pb-8 shadow-md rounded-xl bg-white">
                  <h3 tw="mt-8 uppercase text-primary text-center text-2xl font-medium leading-9">
                    {t('home.contribute.title')}
                  </h3>
                  <p tw="text-base leading-5 h-16">{t('home.contribute.valueProposal')}</p>
                </div>
              </div>
              <div className="singleCard" tw="w-3/4 mx-auto sm:w-2/4 lg:w-2/5">
                <Image
                  tw="(flex mx-auto w-3/5 mb-3 min-height[auto] min-width[auto])!"
                  src="/images/home/Create.svg"
                  alt="Contribute"
                />
                <div className="cardContent" tw="-mt-20 pt-12 px-6 pb-8 shadow-md rounded-xl bg-white">
                  <h3 tw="mt-8 uppercase text-primary text-center text-2xl font-medium leading-9">
                    {t('home.create.title')}
                  </h3>
                  <p tw="text-base leading-5 h-16">{t('home.create.valueProposal')}</p>
                </div>
              </div>
            </div>
          </div>
          <div tw="mt-24">
            <FeaturedObjectsCarousel />
          </div>
        </div>
      </Background>
    </Layout>
  );
};

const ObjectLink = ({ link, title }) => (
  <A noStyle href={link}>
    <p tw="mb-0 py-1 text-[#00000099] hover:bg-gray-200 text-sm font-medium px-4" className="truncate">
      <Star size={14} title="Favorite icon" />
      {title}
    </p>
  </A>
);

const Background = styled.div(() => [
  css`
    background-color: #fdfdfd;
    color: #5c5d5d;
    background-image: url("data:image/svg+xml,%3Csvg width='100' height='100' viewBox='0 0 100 100' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M11 18c3.866 0 7-3.134 7-7s-3.134-7-7-7-7 3.134-7 7 3.134 7 7 7zm48 25c3.866 0 7-3.134 7-7s-3.134-7-7-7-7 3.134-7 7 3.134 7 7 7zm-43-7c1.657 0 3-1.343 3-3s-1.343-3-3-3-3 1.343-3 3 1.343 3 3 3zm63 31c1.657 0 3-1.343 3-3s-1.343-3-3-3-3 1.343-3 3 1.343 3 3 3zM34 90c1.657 0 3-1.343 3-3s-1.343-3-3-3-3 1.343-3 3 1.343 3 3 3zm56-76c1.657 0 3-1.343 3-3s-1.343-3-3-3-3 1.343-3 3 1.343 3 3 3zM12 86c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm28-65c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm23-11c2.76 0 5-2.24 5-5s-2.24-5-5-5-5 2.24-5 5 2.24 5 5 5zm-6 60c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm29 22c2.76 0 5-2.24 5-5s-2.24-5-5-5-5 2.24-5 5 2.24 5 5 5zM32 63c2.76 0 5-2.24 5-5s-2.24-5-5-5-5 2.24-5 5 2.24 5 5 5zm57-13c2.76 0 5-2.24 5-5s-2.24-5-5-5-5 2.24-5 5 2.24 5 5 5zm-9-21c1.105 0 2-.895 2-2s-.895-2-2-2-2 .895-2 2 .895 2 2 2zM60 91c1.105 0 2-.895 2-2s-.895-2-2-2-2 .895-2 2 .895 2 2 2zM35 41c1.105 0 2-.895 2-2s-.895-2-2-2-2 .895-2 2 .895 2 2 2zM12 60c1.105 0 2-.895 2-2s-.895-2-2-2-2 .895-2 2 .895 2 2 2z' fill='%23cacaca' fill-opacity='0.05' fill-rule='evenodd'/%3E%3C/svg%3E");
  `,
]);

export default Home;

export async function getStaticProps(context) {
  return { props: {} };
}
