import useTranslation from 'next-translate/useTranslation';
import Feed from 'components/Feed/Feed';
import Layout from 'components/Layout';
// import useUserData from 'hooks/useUserData';

const GeneralFeed = () => {
  // const { userData } = useUserData();
  const { t } = useTranslation('common');
  return (
    <Layout>
      <div tw="flex flex-col items-center">
        <h3 style={{ marginBottom: '20px' }}>{t('feed.all')}</h3>
        {/* NOTE: set allowPosting to false, cause new post don't appear when we post in this page/feed */}
        {/* <Feed feedId="all" allowPosting={!!userData} isAdmin={false} /> */}
        <Feed feedId="all" allowPosting={false} isAdmin={false} />
      </div>
    </Layout>
  );
};
export default GeneralFeed;
