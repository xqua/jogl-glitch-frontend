import Router from 'next/router';
import useTranslation from 'next-translate/useTranslation';
import { NextPage } from 'next';
import { useContext, useState } from 'react';
import CommunityForm from 'components/Community/CommunityForm';
import Layout from 'components/Layout';
import { UserContext } from 'contexts/UserProvider';
import { useApi } from 'contexts/apiContext';
import { logEventToGA } from 'utils/analytics';

const CreatePage: NextPage = () => {
  const userContext = useContext(UserContext);
  const api = useApi();
  const [newCommunity, setNewCommunity] = useState({
    creator_id: Number(userContext.credentials.userId),
    interests: [],
    short_description: '',
    short_title: '',
    skills: [],
    resources: [],
    title: '',
    is_private: false,
  });
  const [sending, setSending] = useState(false);
  const { t } = useTranslation('common');

  const handleChange = (key, content) => {
    setNewCommunity((prevState) => ({ ...prevState, [key]: content }));
  };

  const handleSubmit = () => {
    setSending(true);
    api
      .post('/api/communities/', { community: newCommunity })
      .then((res) => {
        const userId = res.config.headers.userId;
        // record event to Google Analytics
        logEventToGA('create group', 'Group', `[${userId},${res.data.id}]`, { userId, itemId: res.data.id });
        // follow the object after having created it
        api.put(`/api/communities/${res.data.id}/follow`);
        // go to created group edition page
        Router.push(`/community/${res.data.id}/edit`);
      })
      .catch((err) => {
        console.error("Couldn't post new community", err);
        setSending(false);
        // if error response data is that shortTitle is already taken, show message in an alert warning
        if (err.response.data.data === 'ShortTitle is already taken') {
          alert(t('err-4006'));
        }
      });
  };
  return (
    <Layout title={`${t('community.create.title')} | JOGL`}>
      <div className="communityCreate" tw="px-4 xl:px-0">
        <h1>{t('community.create.title')}</h1>
        <CommunityForm
          community={newCommunity}
          handleChange={handleChange}
          handleSubmit={handleSubmit}
          mode="create"
          sending={sending}
        />
      </div>
    </Layout>
  );
};
export default CreatePage;
