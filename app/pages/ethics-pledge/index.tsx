import { NextPage } from 'next';
import useTranslation from 'next-translate/useTranslation';
import Layout from 'components/Layout';

const EthicsPledge: NextPage = () => {
  const { t } = useTranslation('common');
  const title = `${t('ethics.title')} | JOGL`;
  return (
    <Layout title={title}>
      <div className="legal" tw="px-4 xl:px-0">
        <h1>{t('ethics.title')}</h1>
        <h2>{t('ethics.aware.title')}</h2>
        <h4>{t('ethics.aware.1')}</h4>
        <p>{t('ethics.aware.1_text')}</p>
        <h4>{t('ethics.aware.2')}</h4>
        <p>{t('ethics.aware.2_text')}</p>
        <h4>{t('ethics.aware.3')}</h4>
        <p>{t('ethics.aware.3_text')}</p>
        <h4>{t('ethics.aware.4')}</h4>
        <p>{t('ethics.aware.4_text')}</p>
        <h4>{t('ethics.aware.5')}</h4>
        <p>{t('ethics.aware.5_text')}</p>
        <h2>{t('ethics.pledge.title')}</h2>
        <p>{t('ethics.pledge.engage')}</p>
        <h5>{t('ethics.pledge.1')}</h5>
        <p>{t('ethics.pledge.1_text')}</p>
        <h5>{t('ethics.pledge.2')}</h5>
        <p>{t('ethics.pledge.2_text')}</p>
        <h5>{t('ethics.pledge.3')}</h5>
        <p>{t('ethics.pledge.3_text')}</p>
        <h5>{t('ethics.pledge.4')}</h5>
        <p>{t('ethics.pledge.4_text')}</p>
        <h5>{t('ethics.pledge.5')}</h5>
        <p>{t('ethics.pledge.5_text')}</p>
        <h5>{t('ethics.pledge.6')}</h5>
        <p>{t('ethics.pledge.6_text')}</p>
        <h5>{t('ethics.pledge.7')}</h5>
        <p>{t('ethics.pledge.7_text')}</p>
        <h5>{t('ethics.pledge.8')}</h5>
        <p>{t('ethics.pledge.8_text')}</p>
        <h5>{t('ethics.pledge.9')}</h5>
        <p>{t('ethics.pledge.9_text')}</p>
      </div>
    </Layout>
  );
};

export default EthicsPledge;
