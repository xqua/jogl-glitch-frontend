import { useRouter } from 'next/router';
import useTranslation from 'next-translate/useTranslation';
import { NextPage } from 'next';
import { useContext, useEffect, useState } from 'react';
import { UserContext } from 'contexts/UserProvider';
import Layout from 'components/Layout';
import PeerReviewForm from 'components/PeerReview/PeerReviewForm';
import Loading from 'components/Tools/Loading';
import { useApi } from 'contexts/apiContext';
// import useGet from 'hooks/useGet';
// import useUserData from 'hooks/useUserData';

const CreatePeerReview: NextPage = () => {
  const userContext = useContext(UserContext);
  const api = useApi();
  const router = useRouter();
  const [loading, setLoading] = useState(true);
  const [sending, setSending] = useState(false);
  const { t } = useTranslation('common');
  const [newPeerReview, setNewPeerReview] = useState({
    title: '',
    short_title: '',
    description: '',
    summary: '',
    creator_id: Number(userContext.credentials.userId),
    interests: [],
    skills: [],
    banner_url: '',
    options: { restrict_to_parent_members: false },
  });
  // const { userData } = useUserData();
  // Limit PeerReviews access to a set of specific users while the feature is built
  // Check for :peer_reviews_creator role on backend
  // const { data: canCreate, error: cannotCreate } = useGet('/api/peer_reviews/can_create', { user: userData });

  const canCreate = true;
  const cannotCreate = false;

  // If can't create a peer review, display forbidden page
  useEffect(() => {
    if (canCreate) {
      setLoading(false);
    }
    if (cannotCreate) {
      setLoading(false);
      router.push('/peer-review/forbidden');
    }
  }, [canCreate, cannotCreate]);

  const handleChange = (key, content) => {
    setNewPeerReview((prevPeerReview) => ({ ...prevPeerReview, [key]: content }));
  };

  const handleSubmit = () => {
    setSending(true);
    // add resource_id and resource_type to object to create
    const peerReview = {
      ...newPeerReview,
      ...{ resource_type: router.query.parentType, resource_id: router.query.parentId },
    };
    api
      .post('/api/peer_reviews/', { peerReview })
      .then((res) => router.push(`/peer-review/${res.data.short_title}/edit`))
      .catch(() => setSending(false));
  };

  if (canCreate) {
    return (
      <Layout title={`${t('peerReview.create.title')} | JOGL`} noIndex>
        <Loading active={loading}>
          <div className="programCreate" tw="px-4 xl:px-0">
            <h1>{t('peerReview.create.title')}</h1>
            <PeerReviewForm
              mode="create"
              peerReview={newPeerReview}
              handleChange={handleChange}
              handleSubmit={handleSubmit}
              sending={sending}
            />
          </div>
        </Loading>
      </Layout>
    );
  }

  // eslint-disable-next-line @rushstack/no-null
  return null;
};
export default CreatePeerReview;
