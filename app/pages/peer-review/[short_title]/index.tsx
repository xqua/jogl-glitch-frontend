import { NextPage } from 'next';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Layout from 'components/Layout';
import H2 from 'components/primitives/H2';
import P from 'components/primitives/P';
import PeerReviewInfo from 'components/PeerReview/PeerReviewInfo';
import PeerReviewSkillSdg from 'components/PeerReview/PeerReviewSkillSdg';
import PeerReviewAbout from 'components/PeerReview/PeerReviewAbout';
import { ContactForm } from 'components/Tools/ContactForm';
// import ShareBtns from 'components/Tools/ShareBtns/ShareBtns';
import { useModal } from 'contexts/modalContext';
import { PeerReview } from 'types';
import { getApiFromCtx } from 'utils/getApi';
// import { useScrollHandler } from 'utils/utils';
import ReactGA from 'react-ga';
import ReactTooltip from 'react-tooltip';
import Image from 'components/primitives/Image';
import tw from 'twin.macro';
import PeerReviewHeader from '~/components/PeerReview/PeerReviewHeader';
import PeerReviewProposals from '~/components/PeerReview/PeerReviewProposals';
// import A from '~/components/primitives/A';
import useMembers from '~/hooks/useMembers';
import InfoHtmlComponent from '~/components/Tools/Info/InfoHtmlComponent';
import { Tab, TabPanel, NavContainer, Nav, OverflowGradient, ContactButton, StickyHeading } from '~/utils/style';

const DEFAULT_TAB = 'about';

const PeerReviewDetails: NextPage<{ peerReview: PeerReview }> = ({ peerReview }) => {
  const router = useRouter();
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();
  const [selectedTab, setSelectedTab] = useState(DEFAULT_TAB);
  // const [offSetTop, setOffSetTop] = useState();
  // const { data: faqList } = useGet<{ documents: Faq[] }>(`/api/peer_reviews/${peerReview.id}/faq`);
  // const { data: dataBoards } = useGet<Board>(`/api/peer_reviews/${peerReview.id}/boards`);
  const { members } = useMembers('peer_reviews', peerReview?.id, 25); // take 25 first members
  const headerRef = useRef();
  const navRef = useRef();
  // const isSticky = useScrollHandler(offSetTop);

  const proposalsCount = peerReview.proposals.filter(({ submitted_at }) => submitted_at).length;

  const tabs = [
    { value: 'info', translationId: 'general.tab.info' },
    { value: 'about', translationId: 'general.tab.about' },
    { value: 'rules', translationId: 'peerReview.tabs.rules' },
    ...(proposalsCount !== 0 ? [{ value: 'proposals', translationId: 'peerReview.tabs.proposals' }] : []),
    // { value: 'faq', translationId: 'peerReview.tabs.faqs' },
    { value: 'admins', translationId: 'member.role.admins' },
  ];

  // useEffect(() => {
  //   setOffSetTop(headerRef?.current?.offsetTop);
  // }, [headerRef]);

  useEffect(() => {
    // on first load, check if url has a tab param, and if it does, select this tab
    router.query.tab && setSelectedTab(router.query.tab as string);
  }, []);

  // function when changing tab (or when router change in general)
  useEffect(() => {
    const tabValues = tabs.map(({ value }) => value);
    if (tabValues.includes(router.query.tab as string)) {
      if (router.query.tab === 'about') {
        router.push(`/peer-reviews/${router.query.short_title}`, undefined, {
          shallow: true,
        });
      }
      const navContainerTopPosition = navRef.current.getBoundingClientRect().top + window.scrollY; // get y position of nav container
      const yAdjustment = window.innerWidth < 768 ? 40 : 150; // change yAdustement depending on if it's mobile/tablet or desktop
      window.scrollTo(0, navContainerTopPosition - yAdjustment); // force scroll to top of the nav/tab (remove a little to be really on top)
      setSelectedTab(router.query.tab as string);
    }
    if (!router.query.tab) setSelectedTab(DEFAULT_TAB);
  }, [router]);

  const sendMessageToAdmin = (id, first_name, last_name) => {
    // capture the opening of the modal as a special modal page view to google analytics
    ReactGA.modalview('/send-message');
    showModal({
      children: <ContactForm itemId={id} closeModal={closeModal} />,
      title: t('user.contactModal.title', {
        userFullName: `${first_name} ${last_name}`,
      }),
    });
  };

  return (
    <Layout
      className="small-top-margin"
      desc={peerReview.summary}
      img={peerReview.banner_url || '/images/default/default-peer-review.jpg'}
      noIndex={peerReview.visibility === 'hidden'}
      title={peerReview.title && `${peerReview.title} | JOGL`}
    >
      <div tw="sm:shadow-custom2">
        <div tw="relative">
          <Image
            alt={`${peerReview.title} banner`}
            priority
            quality="100"
            src={peerReview.banner_url || '/images/default/default-peer-review.jpg'}
            tw="w-full! max-h-[400px]! object-cover"
          />
          {/* <div tw="absolute right-0 top-0 m-4">
          <BtnStar
            itemType="peer_reviews"
            itemId={peerReview.id}
            hasStarred={peerReview.has_saved}
            count={peerReview.saves_count}
          />
        </div> */}
        </div>
        <div tw="bg-[#FFA1C3] w-full pl-4 py-1 flex flex-wrap md:text-xl" ref={headerRef}>
          {/* <span>[{t('general.activity_1')}]</span> */}
          <span tw="font-bold px-1">{t('entity.card.peer-reviews')}</span>
        </div>
        {peerReview.visibility === 'hidden' && (
          <div tw="bg-[#E6932B] w-full pl-4 py-1 flex">{t('peerReview.visibilityMessage')}</div>
        )}

        <PeerReviewHeader peerReview={peerReview} />

        {/* <StickyHeading isSticky={isSticky} className="stickyHeading">
          <div tw="flex items-center px-4 xl:px-0">
            <H2 tw="font-size[1.8rem] sm:font-size[2.18rem] line-height[26px]">{peerReview.title}</H2>
            <div tw="flex pl-5" className="actions">
              <ShareBtns type="peerReview" specialObjId={peerReview.id} />
            </div>
          </div>
        </StickyHeading> */}

        {/* ---- Page grid starts here ---- */}
        <div
          tw="mb-6 grid grid-cols-1 md:(bg-white grid-template-columns[16rem calc(100% - 16rem)]) bg-white border-t border-solid border-gray-200"
          ref={navRef}
        >
          {/* Nav (left col on desktop, top col on mobile */}
          <div tw="flex flex-col bg-[#F7F7F9] sticky top-[60px] z-[8] md:(items-center pt-5)">
            <NavContainer>
              <OverflowGradient tw="md:hidden" gradientPosition="left" />
              <OverflowGradient tw="md:hidden" gradientPosition="right" />
              <Nav as="nav" tw="flex gap-3 pt-2.5 w-full md:(flex-col gap-0 pt-0)">
                {tabs.map((item, index) => (
                  <Link
                    shallow
                    scroll={false}
                    key={index}
                    href={`/peer-review/${router.query.short_title}${
                      item.value === DEFAULT_TAB ? '' : `?tab=${item.value}`
                    }`}
                  >
                    <Tab
                      selected={item.value === selectedTab}
                      tw="px-1 py-3 md:px-3"
                      as="button"
                      id={`${item.value}-tab`}
                      css={[item.value === 'info' ? tw`md:hidden` : tw`block`]}
                    >
                      {t(item.translationId)}
                    </Tab>
                  </Link>
                ))}
              </Nav>
            </NavContainer>
          </div>

          {/* Main content, that change content depending on the selected tab (middle content on desktop) */}
          <div tw="flex flex-col px-0 pb-6 relative md:px-4 border-l-0 border-r-0 border-color[#f4f4f4] md:(border-l-2 pl-4) xl:(border-r-2 pr-4)">
            {selectedTab === 'info' && (
              <TabPanel id="info" tw=" md:hidden">
                <div>
                  <PeerReviewInfo peerReview={peerReview} />
                </div>
                {(peerReview.interests?.length > 0 || peerReview.skills?.length > 0) && (
                  <div>
                    <hr tw="mb-4" />
                    <PeerReviewSkillSdg interests={peerReview.interests} skills={peerReview.skills} />
                  </div>
                )}
              </TabPanel>
            )}

            {selectedTab === 'about' && (
              <TabPanel id="about">
                <div tw="max-w-3xl">
                  <PeerReviewAbout
                    description={peerReview.description}
                    maximum_disbursement={peerReview.maximum_disbursement}
                  />
                  {(peerReview.interests?.length > 0 || peerReview.skills?.length > 0) && (
                    <div tw="hidden md:block mt-6">
                      <PeerReviewSkillSdg interests={peerReview.interests} skills={peerReview.skills} />
                    </div>
                  )}
                </div>
              </TabPanel>
            )}

            {selectedTab === 'rules' && (
              <TabPanel id="rules">
                <H2>{t('peerReview.rules.title')}</H2>
                <InfoHtmlComponent content={peerReview.rules} />
              </TabPanel>
            )}

            {selectedTab === 'proposals' && (
              <TabPanel id="proposals">
                <PeerReviewProposals proposals={peerReview.proposals} peerReviewId={peerReview.id} />
              </TabPanel>
            )}

            {selectedTab === 'admins' && (
              <TabPanel id="contact" tw="px-3 pt-3 sm:px-4 md:px-3 xl:px-0">
                <H2>{t('footer.contactUs')}</H2>
                <div tw="flex flex-col pt-5 space-y-4">
                  {members
                    ?.filter(({ admin, owner }) => owner || admin) // only show leaders and admins
                    .map(({ id, first_name, last_name, logo_url_sm }, index) => (
                      <div tw="flex space-x-4 items-center" key={index}>
                        <Link href={`/user/${id}`}>
                          <a>
                            <div tw="flex space-x-4 items-center">
                              <img
                                tw="object-cover rounded-full w-12 h-12"
                                src={logo_url_sm}
                                alt={`${first_name} ${last_name}`}
                              />
                              <P tw="mb-0 font-bold font-size[1.2rem]">{first_name + ' ' + last_name}</P>
                            </div>
                          </a>
                        </Link>
                        <div tw="flex flex-col">
                          <ContactButton
                            size={20}
                            title="Contact user"
                            onClick={() => sendMessageToAdmin(id, first_name, last_name)}
                            onKeyUp={(e) =>
                              // execute only if it's the 'enter' key
                              (e.which === 13 || e.keyCode === 13) && sendMessageToAdmin(id, first_name, last_name)
                            }
                            tabIndex={0}
                            data-tip={t('user.contactModal.title', { userFullName: first_name + ' ' + last_name })}
                            data-for="contactAdmin"
                            // show/hide tooltip on element focus/blur
                            onFocus={(e) => ReactTooltip.show(e.target)}
                            onBlur={(e) => ReactTooltip.hide(e.target)}
                          />
                          <ReactTooltip id="contactAdmin" effect="solid" place="bottom" />
                        </div>
                      </div>
                    ))}
                </div>
              </TabPanel>
            )}
          </div>
        </div>
      </div>
    </Layout>
  );
};

const getPeerReview = async (api, peerReviewId) => {
  const res = await api.get(`/api/peer_reviews/${peerReviewId}`).catch((err) => console.error(err));
  if (res?.data) {
    return res.data;
  }
  return undefined;
};

export async function getServerSideProps({ query, ...ctx }) {
  const api = getApiFromCtx(ctx);
  // eslint-disable-next-line no-restricted-globals
  const res = await api.get(`/api/peer_reviews/getid/${query.short_title}`).catch((err) => console.error(err));
  if (res && res.data) {
    const peerReview = await getPeerReview(api, res.data.id).catch((err) => console.error(err));
    if ((peerReview && peerReview.visibility !== 'hidden') || (peerReview && peerReview.is_admin)) {
      return { props: { peerReview } };
    } else return { redirect: { destination: '/search/peer-reviews', permanent: false } };
  }
  return { redirect: { destination: '/search/peer-reviews', permanent: false } };
}

export default PeerReviewDetails;
