import { ArrowLeft } from '@emotion-icons/fa-solid/ArrowLeft';
import { TabPanel, TabPanels, Tabs } from '@reach/tabs';
import { NextPage } from 'next';
import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import Layout from 'components/Layout';
import PeerReviewForm from 'components/PeerReview/PeerReviewForm';
import PeerReviewEmailModalAll from '~/components/PeerReview/PeerReviewEmailModalAll';
import PeerReviewRulesForm from 'components/PeerReview/PeerReviewRulesForm';
import MembersList from 'components/Members/MembersList';
import ManageFaq from 'components/Tools/ManageFaq';
import { NavTab, TabListStyle } from 'components/Tabs/TabsStyles';
import { useApi } from 'contexts/apiContext';
import { PeerReview, Proposal } from 'types';
import { getApiFromCtx } from 'utils/getApi';
import A from '~/components/primitives/A';
import ProposalAdminCard from '~/components/Proposal/ProposalAdminCard';
import Alert from '~/components/Tools/Alert';
import Button from '~/components/primitives/Button';
import { useModal } from '~/contexts/modalContext';
import P from '~/components/primitives/P';
import { logEventToGA } from '~/utils/analytics';
import useUser from '~/hooks/useUser';
import { hasPeerReviewDatePassed, hasPeerReviewStarted } from 'utils/utils';
import useInfiniteLoading from '~/hooks/useInfiniteLoading';
import SpinLoader from '~/components/Tools/SpinLoader';
import MemberCard from '~/components/Members/MemberCard';
import useGet from '~/hooks/useGet';
import Loading from '~/components/Tools/Loading';
import NoResults from '~/components/Tools/NoResults';
import ReactTooltip from 'react-tooltip';
import Swal from 'sweetalert2';

const PeerReviewEdit: NextPage<{ peerReview: PeerReview }> = ({ peerReview: peerReviewProp }) => {
  const [peerReview, setPeerReview] = useState(peerReviewProp);
  const [updatedPeerReview, setUpdatedPeerReview] = useState(undefined);
  const [isDownloadingInfo, setIsDownloadingInfo] = useState(false);
  const [sending, setSending] = useState(false);
  const [hasUpdated, setHasUpdated] = useState(false);
  const api = useApi();
  const router = useRouter();
  const { t } = useTranslation('common');
  const { user } = useUser();
  const { showModal, closeModal } = useModal();
  const membersPerQuery = 50;

  const { data: dataMembers, error, size, setSize } = useInfiniteLoading(
    (index) => `/api/peer_reviews/${peerReview.id}/people?items=${membersPerQuery}&sort=roles&page=${index + 1}`
  );
  const { data: proposals, revalidate: proposalsRevalidate } = useGet<{
    proposals: Proposal[];
  }>(`/api/peer_reviews/${peerReview.id}/proposals`);

  const { data: peerReviewQuestions } = useGet(`/api/peer_reviews/${peerReview.id}/faq`);

  const members = dataMembers ? [].concat(...dataMembers?.map((d) => d.people)) : [];
  const isLoadingInitialData = !dataMembers && !error;
  const isLoadingMore =
    isLoadingInitialData || (size > 0 && dataMembers && typeof dataMembers[size - 1] === 'undefined');
  const isEmpty = dataMembers?.[0]?.people.length === 0;
  const isReachingEnd =
    isEmpty || (dataMembers && dataMembers[dataMembers.length - 1]?.people.length < membersPerQuery);

  const handleChange = (key, content) => {
    setPeerReview((prevPeerReview) => ({ ...prevPeerReview, [key]: content })); // update fields as user changes them
    // TODO: have only one place where we manage need content
    setUpdatedPeerReview((prevUpdatedPeerReview) => ({ ...prevUpdatedPeerReview, [key]: content })); // set an object containing only the fields/inputs that are updated by user
  };

  const [showCloseBtn, setShowCloseBtn] = useState(
    peerReview.stop === null &&
      hasPeerReviewStarted(peerReview.proposals.length) &&
      hasPeerReviewDatePassed(peerReview.deadline)
  );
  const submittedScoredProposalsNb = peerReview.proposals.filter((p) => p.submitted_at && p.score).length;
  const submittedProposalsNb = peerReview.proposals.filter((p) => p.submitted_at).length;
  const allProposalsGotScore = submittedScoredProposalsNb === submittedProposalsNb;

  const handleSubmit = async () => {
    setSending(true);
    const res = await api
      .patch(`/api/peer_reviews/${peerReview.id}`, { peerReview: updatedPeerReview })
      .catch((err) => {
        console.error(`Couldn't patch peer review with id=${peerReview.id}`, err);
        setSending(false);
      });

    if (res) {
      // on success
      setSending(false);
      setUpdatedPeerReview(undefined); // reset updated peer review component
      setHasUpdated(true); // show update confirmation message
      proposalsRevalidate(); // revalidate proposals to update status for ex
      setTimeout(() => {
        setHasUpdated(false);
      }, 3000); // hide confirmation message after 3 seconds
    }
  };

  const deletePeerReview = () => {
    api.delete(`api/peer_reviews/${peerReview.id}`).then(() => {
      // record event to Google Analytics
      logEventToGA('delete peerReview', 'peerReview', `[${user.id},${peerReview.id}]`, {
        userId: user.id,
        itemId: peerReview.id,
      });
      closeModal(); // close modal
      router.push('/search/peer-reviews');
    });
  };

  const downloadUsersInfo = (type) => {
    setIsDownloadingInfo(true);
    api
      .post(`api/peer_reviews/${peerReview.id}/get-${type}-info.csv`)
      .then((res) => {
        const data = `text/csv;charset=utf-8,${encodeURIComponent(res.data)}`;
        const link: HTMLElement = document.querySelector('#dlHiddenLink') as HTMLElement;
        link.href = `data:${data}`;
        link.download = `${type}-info.csv`;
        link.innerHTML = 'download CSV';
        link.click();
        setIsDownloadingInfo(false);
      })
      .catch((err) => {
        console.log(`this is a doc csv download error: ${err}`);
        setIsDownloadingInfo(false);
      });
  };

  const tabs = [
    { value: 'general', translationId: 'peerReview.tabs.general' },
    { value: 'about', translationId: 'general.tab.about' },
    { value: 'rules', translationId: 'peerReview.tabs.rules' },
    { value: 'templates', translationId: 'peerReview.tabs.template' },
    { value: 'admins', translationId: 'member.role.admins' },
    { value: 'proposals', translationId: 'peerReview.tabs.proposals' },
    { value: 'reviewers', translationId: 'peerReview.tabs.reviewers' },
    ...(peerReview.visibility !== 'open' ? [{ value: 'advanced', translationId: 'feed.object.delete' }] : []),
  ];

  const handleTabsChange = (index) => {
    const element = document.querySelector('[data-reach-tabs]');
    const headerHeight = 80;
    const y = element.getBoundingClientRect().top + window.pageYOffset - headerHeight;
    window.scrollTo({ top: y, behavior: 'smooth' });
    router.push(`/peer-review/${peerReview.short_title}/edit?t=${tabs[index].value}`, undefined, { shallow: true });
  };

  return (
    <Layout title={`${peerReview.title} | JOGL`}>
      <div className="peerReviewEdit" tw="mx-auto px-4">
        <h1>{t('peerReview.edit.title')}</h1>
        <A href={`/peer-review/${peerReview.short_title}`}>
          <ArrowLeft size={15} title="Go back" />
          {t('peerReview.edit.back')}
        </A>

        <Tabs
          defaultIndex={router.query.t ? tabs.findIndex((t) => t.value === router.query.t) : null}
          onChange={handleTabsChange}
        >
          <TabListStyle>
            {tabs.map((item, key) => (
              <NavTab key={key}>{t(item.translationId)}</NavTab>
            ))}
          </TabListStyle>

          <TabPanels tw="justify-center">
            {/* General info */}
            <TabPanel>
              <p tw="italic">{t('peerReview.explanations.info')}</p>
              <PeerReviewForm
                mode="edit"
                peerReview={peerReview}
                updatedPeerReview={updatedPeerReview}
                handleChange={handleChange}
                handleSubmit={handleSubmit}
                sending={sending}
                hasUpdated={hasUpdated}
              />
            </TabPanel>
            {/* About section */}
            <TabPanel>
              <PeerReviewForm
                mode="edit_about"
                peerReview={peerReview}
                updatedPeerReview={updatedPeerReview}
                handleChange={handleChange}
                handleSubmit={handleSubmit}
                sending={sending}
                hasUpdated={hasUpdated}
              />
            </TabPanel>
            {/* Rules */}
            <TabPanel>
              <p tw="italic">{t('peerReview.explanations.rules')}</p>
              <PeerReviewRulesForm
                peerReview={peerReview}
                handleChange={handleChange}
                handleSubmit={handleSubmit}
                sending={sending}
                hasUpdated={hasUpdated}
              />
            </TabPanel>
            {/* Questions */}
            <TabPanel>
              <p tw="italic">{t('peerReview.explanations.questions')}</p>
              {!hasPeerReviewStarted(peerReview.proposals.length) ? (
                <ManageFaq itemType="peer_reviews" itemId={peerReview.id} isQuestion />
              ) : (
                <>
                  <Alert type="info" message={t('peerReview.form.cant_edit_questions_alert')} />
                  {peerReviewQuestions?.documents?.map((question, i) => (
                    <div key={i} tw="mt-8 opacity-40">
                      <h4>{question.title}</h4>
                      <h6>{question.content}</h6>
                      <hr />
                    </div>
                  ))}
                </>
              )}
            </TabPanel>
            {/* Admins */}
            <TabPanel>
              <p tw="italic">{t('peerReview.explanations.admins')}</p>
              <MembersList itemType="peer_reviews" itemId={peerReview.id} specialTab="admins" />
            </TabPanel>
            {/* Proposals */}
            <TabPanel>
              <p tw="italic mb-8">{t('peerReview.explanations.proposals')}</p>
              {showCloseBtn && (
                <div tw="mb-4">
                  <div
                    {...(!allProposalsGotScore && {
                      'data-tip': t('peerReview.form.close_btn_condition'),
                      'data-for': 'cantCloseExplanation',
                    })}
                  >
                    <Button
                      disabled={!allProposalsGotScore}
                      onClick={() => {
                        Swal.fire({
                          title: t('peerReview.form.close_btn_warnTitle'),
                          text: t('peerReview.form.close_btn_warnDesc'),
                          icon: 'warning',
                          showCancelButton: true,
                          confirmButtonColor: '#3085d6',
                          cancelButtonColor: '#d33',
                          confirmButtonText: t('general.yes'),
                        }).then(({ isConfirmed }) => {
                          if (isConfirmed) {
                            api
                              .patch(`/api/peer_reviews/${peerReview.id}`, {
                                peer_review: { stop: new Date(Date.now()) },
                              })
                              .then(() => {
                                Swal.fire({
                                  icon: 'success',
                                  showConfirmButton: false,
                                  text: t('peerReview.form.is_closed'),
                                  // timer: 2500,
                                });
                                setShowCloseBtn(false);
                              });
                          }
                        });
                      }}
                    >
                      {t('peerReview.form.close_btn')}
                    </Button>
                  </div>
                  <ReactTooltip
                    id="cantCloseExplanation"
                    effect="solid"
                    role="tooltip"
                    type="dark"
                    className="forceTooltipBg"
                  />
                  <p tw="italic text-gray-600">{t('peerReview.explanations.proposals_close')}</p>
                </div>
              )}
              {!proposals ? (
                <Loading />
              ) : proposals?.proposals.length === 0 ? (
                <NoResults type="proposal" />
              ) : (
                <table tw="min-w-full border-collapse block md:table mb-8">
                  <thead tw="block md:table-header-group">
                    <tr tw="border border-gray-500 md:border-none block md:table-row absolute -top-full md:top-auto -left-full md:left-auto  md:relative ">
                      <th tw="bg-gray-200 p-2 font-bold md:border md:border-gray-500 text-left block md:table-cell">
                        {t('entity.info.title')}
                      </th>
                      <th tw="bg-gray-200 p-2 font-bold md:border md:border-gray-500 text-left block md:table-cell">
                        {t('attach.status')}
                      </th>
                      <th tw="bg-gray-200 p-2 font-bold md:border md:border-gray-500 text-left block md:table-cell">
                        {t('proposal.card.submittedDate')}
                      </th>
                      {peerReview.maximum_disbursement !== null && (
                        <th tw="bg-gray-200 p-2 font-bold md:border md:border-gray-500 text-left block md:table-cell">
                          {t('peerReview.funding.fundingRequested')}
                        </th>
                      )}
                      <th tw="bg-gray-200 p-2 font-bold md:border md:border-gray-500 text-left block md:table-cell">
                        {t('proposal.score.title')}
                      </th>
                      <th tw="bg-gray-200 p-2 font-bold md:border md:border-gray-500 text-left block md:table-cell">
                        {t('peerReview.form.actions')}
                      </th>
                    </tr>
                  </thead>
                  <tbody tw="block md:table-row-group">
                    {proposals?.proposals.map((proposal, i) => (
                      <ProposalAdminCard
                        proposal={proposal}
                        key={i}
                        score_threshold={peerReview.score_threshold}
                        peerReviewId={peerReview.id}
                        peerReviewDates={{
                          start: peerReview.start,
                          deadline: peerReview.deadline,
                          stop: peerReview.stop,
                        }}
                        hasGrant={peerReview.maximum_disbursement !== null}
                        callBack={proposalsRevalidate}
                      />
                    ))}
                  </tbody>
                </table>
              )}
              {proposals?.proposals.length > 0 && (
                <div tw="inline-flex space-x-4">
                  <Button
                    onClick={() => {
                      showModal({
                        children: <PeerReviewEmailModalAll peerReviewId={peerReview.id} recipient="admins" />,
                        title: t('peerReview.form.sendEmailToProposalAdmins'),
                        maxWidth: '50rem',
                      });
                    }}
                    btnType="secondary"
                  >
                    {t('peerReview.form.sendEmailToProposalAdmins')}
                  </Button>
                  <Button btnType="primary" onClick={() => downloadUsersInfo('admins')} disabled={isDownloadingInfo}>
                    {t('peerReview.form.downloadDataAdmins')}
                    {isDownloadingInfo && <SpinLoader />}
                  </Button>
                  <a href="#" id="dlHiddenLink" tw="visibility[hidden]" />
                </div>
              )}
              <PeerReviewForm
                mode="edit_proposal"
                peerReview={peerReview}
                updatedPeerReview={updatedPeerReview}
                handleChange={handleChange}
                handleSubmit={handleSubmit}
                sending={sending}
                hasUpdated={hasUpdated}
              />
            </TabPanel>
            {/* Reviewers */}
            <TabPanel>
              <p tw="italic">{t('peerReview.explanations.reviewers')}</p>
              {members && (
                <>
                  <div tw="py-4">
                    {members.map((member, i) => (
                      <MemberCard
                        itemId={peerReview.id}
                        itemType="peer_reviews"
                        member={member}
                        key={i}
                        role={
                          member.relation === 'member'
                            ? t('peerReview.form.independent_reviewer_role')
                            : t('peerReview.form.participant_role')
                        }
                        isSending={sending}
                        specialTab="reviewers"
                      />
                    ))}
                  </div>
                  {members?.length >= membersPerQuery && (
                    <div tw="flex flex-col self-center mb-8">
                      <Button onClick={() => setSize(size + 1)} disabled={isLoadingMore || isReachingEnd}>
                        {isLoadingMore && <SpinLoader />}
                        {isLoadingMore
                          ? t('general.loading')
                          : !isReachingEnd
                          ? t('general.load')
                          : t('general.noMoreResults')}
                      </Button>
                    </div>
                  )}
                </>
              )}
              {members?.length !== 0 && (
                <div tw="inline-flex space-x-4">
                  <Button
                    onClick={() => {
                      showModal({
                        children: <PeerReviewEmailModalAll peerReviewId={peerReview.id} recipient="reviewers" />,
                        title: t('peerReview.form.sendEmailToAllReviewers'),
                        maxWidth: '50rem',
                      });
                    }}
                    btnType="secondary"
                  >
                    {t('peerReview.form.sendEmailToAllReviewers')}
                  </Button>
                  <Button btnType="primary" onClick={() => downloadUsersInfo('reviewers')} disabled={isDownloadingInfo}>
                    {t('peerReview.form.downloadDataReviewers')}
                    {isDownloadingInfo && <SpinLoader />}
                    <a href="#" id="dlHiddenLink" tw="visibility[hidden]" />
                  </Button>
                </div>
              )}
            </TabPanel>
            {/* Advanced */}
            <TabPanel>
              {/* <h4>{t('project.advancedParam')}</h4> */}
              {peerReview.visibility !== 'open' && (
                <div className="deleteBtns">
                  <p>{t('peerReview.explanations.delete')}</p>
                  <Button
                    onClick={() => {
                      showModal({
                        children: (
                          <>
                            <P tw="text-base">{t('general.delete.text')}</P>
                            <div tw="inline-flex space-x-3">
                              <Button btnType="danger" onClick={deletePeerReview}>
                                {t('general.yes')}
                              </Button>
                              <Button onClick={closeModal}>{t('general.no')}</Button>
                            </div>
                          </>
                        ),
                        title: t('general.delete.title'),
                        maxWidth: '30rem',
                      });
                    }}
                    btnType="danger"
                  >
                    {t('general.delete.title')}
                  </Button>
                </div>
              )}
            </TabPanel>
          </TabPanels>
        </Tabs>
      </div>
    </Layout>
  );
};

export async function getServerSideProps({ query, ...ctx }) {
  const api = getApiFromCtx(ctx);
  const getIdRes = await api
    .get(`/api/peer_reviews/getid/${query.short_title}`)
    .catch((err) => console.error(`Couldn't fetch program with short_title=${query.short_title}`, err));

  if (getIdRes?.data?.id) {
    const peerReviewRes = await api
      .get(`/api/peer_reviews/${getIdRes.data.id}`)
      .catch((err) => console.error(`Couldn't fetch program with id=${getIdRes.data.id}`, err));
    // Check if it got the program and if the user is admin
    if (peerReviewRes?.data?.is_admin) return { props: { peerReview: peerReviewRes.data } };
    return { redirect: { destination: `/peer-review/${peerReviewRes?.data?.short_title}`, permanent: false } };
  }
  return { redirect: { destination: '/', permanent: false } };
}

export default PeerReviewEdit;
