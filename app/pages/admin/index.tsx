import { useState } from 'react';
// import useTranslation from 'next-translate/useTranslation';
import { NextPage } from 'next';
import Layout from 'components/Layout';
import TitleInfo from 'components/Tools/TitleInfo';
import { getApiFromCtx } from 'utils/getApi';
import SkillsForModerator from 'components/Moderator/SkillsForModerator';

const AdminDashboard: NextPage = () => {
  // const { t } = useTranslation('common'); // TODO - at the end when wording is validated actually create lang files
  const [showSkills, setShowSkills] = useState(false);

  return (
    <Layout title="Admin Dashboard | JOGL">
      <section tw="-mt-6 background[linear-gradient(to right, #DEF6FC, #BD94FC)] flex py-4 px-6 max-height[350px] sm:(py-10 px-12)">
        <div tw="flex w-1/2 items-center">
          <h1 tw="text-lg color[#2b5077] sm:text-2xl md:text-3xl lg:text-5xl">
            Welcome to the JOGL Moderator Dashboard!
          </h1>
        </div>
        <div tw="font-size[4px] w-1/2 flex flex-wrap justify-end">
          <img tw="h-full w-full" src="/images/celebration.svg" />
        </div>
      </section>
      <div tw="flex flex-col gap-10 mt-10 mx-4 sm:mx-8 md:mx-20">
        <a href="/search/members" target="_blank" tw="hover:cursor-help">
          <TitleInfo
            title="Delete unappropriate users"
            tooltipMessage='Click to open the Explore page on a different tab. As a JOGL Moderator, you can delete spam users either on their member card, or on their specific profile page, hitting the "Delete"
            button and then confirming your choice.'
          />
        </a>
        <div>
          <a onClick={() => setShowSkills(!showSkills)}>
            <TitleInfo
              title="Interact with skills"
              tooltipMessage="Click and see below. As a JOGL Moderator, you have access to a JOGLers' filter based on a specific set of skills. That done, you can either send that group of selected members an email, or download a CSV format document listing them."
            />
          </a>
          {showSkills && <SkillsForModerator />}
        </div>
      </div>
    </Layout>
  );
};

export default AdminDashboard;

// in case the user trying to access /admin url is no moderator, triggers 404 not found page
export const getServerSideProps = async (ctx) => {
  const api = getApiFromCtx(ctx);
  const res = await api
    .get('/api/admin/moderator')
    .catch((err) => console.error('Could not fetch moderator status of the current user'));
  if (!res?.data?.moderator) return { notFound: true };
  return { props: {} };
};
