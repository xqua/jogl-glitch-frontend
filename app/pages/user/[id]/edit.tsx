/* eslint-disable camelcase */
import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import nextCookie from 'next-cookies';
import Layout from 'components/Layout';
import { getApiFromCtx } from 'utils/getApi';
import Button from 'components/primitives/Button';
import { TabPanel, TabPanels, Tabs } from '@reach/tabs';
import { NavTab, TabListStyle } from 'components/Tabs/TabsStyles';
import UserDelete from 'components/User/Settings/UserDelete';
import UserDownloadInfos from 'components/User/Settings/UserDownloadInfos';
import UserNotificationsSettings from 'components/User/Settings/UserNotificationsSettings';
import FormChangePwd from 'components/Tools/Forms/FormChangePwd';
import useUserData from 'hooks/useUserData';
import { Disclosure, DisclosureButton, DisclosurePanel } from '@reach/disclosure';
import UserForm from '~/components/User/UserForm';

const UserProfileEdit = ({ user }) => {
  const router = useRouter();
  const { t } = useTranslation('common');
  const { userData } = useUserData();

  const tabs = [
    { value: 'information', translationId: 'entity.tab.basic_info' },
    { value: 'notification-settings', translationId: 'settings.notifications.name' },
    { value: 'admin', translationId: 'member.role.admin' },
  ];

  const handleTabsChange = (index) => {
    const element = document.querySelector('[data-reach-tabs]');
    const headerHeight = 80;
    const y = element.getBoundingClientRect().top + window.pageYOffset - headerHeight;
    window.scrollTo({ top: y, behavior: 'smooth' });
    router.push(`/user/${user.id}/edit?t=${tabs[index].value}`, undefined, { shallow: true });
  };

  return (
    <Layout title={`${user?.first_name} ${user?.last_name} | JOGL`} desc={user?.bio} img={user?.logo_url}>
      {user ? (
        <div className="userProfileEdit">
          <div tw="px-4 xl:px-0">
            <h1>{t('user.profile.edit.title')}</h1>
            <Tabs
              defaultIndex={router.query.t ? tabs.findIndex((t) => t.value === router.query.t) : null}
              onChange={handleTabsChange}
            >
              <TabListStyle>
                {tabs.map((item, key) => (
                  <NavTab key={key}>{t(item.translationId)}</NavTab>
                ))}
              </TabListStyle>
              <TabPanels tw="justify-center">
                {/* User form */}
                <TabPanel>
                  <UserForm user={user} />
                </TabPanel>
                {/* Notification settings */}
                <TabPanel>
                  <div className="userSettings">
                    <UserNotificationsSettings />
                  </div>
                </TabPanel>
                {/* Advanced */}
                <TabPanel>
                  <div className="userSettings">
                    <Disclosure>
                      <Button tw="mb-2">
                        <DisclosureButton tw="text-white!">{t('auth.changePwd.title')}</DisclosureButton>
                      </Button>
                      <DisclosurePanel className="slide-down">
                        <FormChangePwd />
                        <hr />
                      </DisclosurePanel>
                    </Disclosure>
                    <UserDownloadInfos userId={userData?.id} />
                    <UserDelete userId={userData?.id} />
                  </div>
                </TabPanel>
              </TabPanels>
            </Tabs>
          </div>
        </div>
      ) : (
        <div className="errorMessage" tw="text-center">
          {t('user.profile.edit.unable_load')}
        </div>
      )}
    </Layout>
  );
};

export async function getServerSideProps({ query, ...ctx }) {
  const api = getApiFromCtx(ctx);
  const { userId } = nextCookie(ctx);
  const res = await api
    .get(`/api/users/${query.id}`)
    .catch((err) => console.error(`Could not fetch user with id=${query.id}`, err));
  // Check if it got the user and if user is the connected user, else redirect to user page
  if (query.id === userId) return { props: { user: res?.data } };
  return { redirect: { destination: `/user/${query.id}`, permanent: false } };
}

export default UserProfileEdit;
