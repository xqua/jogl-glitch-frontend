import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Feed from 'components/Feed/Feed';
import Layout from 'components/Layout';
import UserHeader from 'components/User/UserHeader';
import useGet from 'hooks/useGet';
import { getApiFromCtx } from 'utils/getApi';
import tw from 'twin.macro';
import 'intro.js/introjs.css';
import UserInfo from 'components/User/UserInfo';
import { useModal } from '~/contexts/modalContext';
import { Tab, TabPanel, NavContainer, Nav, OverflowGradient } from '~/utils/style';
import useUserData from '~/hooks/useUserData';
import useBreakpoint from '~/hooks/useBreakpoint';
import InfoDefaultComponent from '~/components/Tools/Info/InfoDefaultComponent';
import TitleInfo from '~/components/Tools/TitleInfo';
import Chips from '~/components/Chip/Chips';
import InfoInterestsComponent from '~/components/Tools/Info/InfoInterestsComponent';
import { TextWithPlural } from '~/utils/managePlurals';
import { displayObjectDate, linkify } from '~/utils/utils';
import { showOnboardingStartModal, startUserIntro } from '~/utils/onboarding';
import UserObjects from '~/components/User/UserObjects';

const DEFAULT_TAB = 'about';

export default function UserProfile({ user }) {
  const router = useRouter();
  const { t } = useTranslation('common');
  const [selectedTab, setSelectedTab] = useState(DEFAULT_TAB);
  const headerRef = useRef();
  const navRef = useRef();
  const { userData } = useUserData();
  const modal = useModal();
  const [loadCollection, setLoadCollection] = useState(false);
  const [sawIntroModal, setSawIntroModal] = useState(false);
  const isOnboarding = typeof window !== 'undefined' && JSON.parse(localStorage.getItem('isOnboarding'));
  const isAnIndividual = typeof window !== 'undefined' && JSON.parse(localStorage.getItem('isIndividual'));
  const isAnOrganization = typeof window !== 'undefined' && JSON.parse(localStorage.getItem('isOrganization'));
  const isDesktop = typeof window !== 'undefined' && JSON.parse(localStorage.getItem('isDesktop'));
  const breakpoint = useBreakpoint();

  const bioWithLinks = user.bio ? linkify(user.bio) : '';
  const hasObjInPortfolio =
    user.stats.projects_count !== 0 ||
    // user.stats.communities_count !== 0 ||
    user.stats.challenges_count !== 0 ||
    user.stats.programs_count !== 0 ||
    user.stats.spaces_count !== 0 ||
    user.id === userData?.id; // even if user has no obj in Portfolio, show the tab if it's him

  // All explained in pages-project-index file
  const tabs = [
    { value: 'info', translationId: 'general.tab.info' },
    { value: 'feed', translationId: 'user.profile.tab.feed' },
    { value: 'about', translationId: 'general.tab.about' },
    ...(hasObjInPortfolio ? [{ value: 'collection', translationId: 'user.profile.tab.collections' }] : []),
  ];

  useEffect(() => {
    // load content of "collection" tab only when user clicks on its tab
    router.query.tab === 'collection' && setLoadCollection(true);
  }, [router.query.tab]);

  useEffect(() => {
    // [Onboarding][2] -  Lauching first onboarding modal, choosing individual/organization and triggering flow
    if (isOnboarding && !sawIntroModal && breakpoint !== 'mobile' && breakpoint !== 'tablet') {
      const isNewUser = router.query?.new_user === 'true';
      showOnboardingStartModal(modal, setSawIntroModal, setSelectedTab, t, isNewUser);
      localStorage.setItem('sawOnboardingFeature', 'true'); // so new feature announcement modal does not come back on homepage at the end of this onboarding
    }
  }, [isOnboarding, sawIntroModal, breakpoint]);

  useEffect(() => {
    // Set the breakpoint steadily for the whole tour
    if (breakpoint !== 'mobile' && breakpoint !== 'tablet') {
      localStorage.setItem('isDesktop', 'true');
    } else {
      localStorage.setItem('isDesktop', 'false');
    }
  }, [breakpoint]);

  useEffect(() => {
    // We have to check that this is the user profile as well as if we are on an onboarding flow to launch it
    if (user?.id === userData?.id && isOnboarding && (isAnIndividual || isAnOrganization)) {
      // time reduced compared to other pages because there is already the modal to start with
      setTimeout(() => {
        startUserIntro(isAnIndividual, isAnOrganization, t, router, isDesktop);
      }, 1000);
    }
  }, [userData, isOnboarding, isAnIndividual, isAnOrganization, breakpoint]);

  const openFollowingModal = (e) => {
    user?.stats.following_count && // open modal only if user is following objects
    ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which) && // if function is launched via keypress, execute only if it's the 'enter' key
      modal.showModal({
        children: <FollowingModal userId={user?.id} isUser={userData?.id === user?.id} userStats={user?.stats} />,
        title: t('general.following_other'),
        maxWidth: '70rem',
      });
  };

  useEffect(() => {
    // on first load, check if url has a tab param, and if it does, select this tab
    router.query.tab && setSelectedTab(router.query.tab as string);
  }, []);

  // function when changing tab (or when router change in general)
  useEffect(() => {
    const tabValues = tabs.map(({ value }) => value);
    if (tabValues.includes(router.query.tab as string)) {
      if (router.query.tab === 'about') {
        router.push(`/user/${router.query.id}`, undefined, {
          shallow: true,
        });
      }
      const navContainerTopPosition = navRef.current.getBoundingClientRect().top + window.scrollY; // get y position of nav container
      const yAdjustment = window.innerWidth < 768 ? 40 : 150; // change yAdustement depending on if it's mobile/tablet or desktop
      window.scrollTo(0, navContainerTopPosition - yAdjustment); // force scroll to top of the nav/tab (remove a little to be really on top)
      setSelectedTab(router.query.tab as string);
    }
    if (!router.query.tab) setSelectedTab(DEFAULT_TAB);
  }, [router]);

  const displayInfoBox = () => (
    <div tw="bg-[#E7F0F3] border-l-4 border-blue-200 p-4 mb-6">
      <div tw="flex items-center mb-2">
        <div tw="flex-shrink-0">
          <svg
            tw="h-5 w-5 text-[#193c47]"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 20 20"
            fill="currentColor"
            aria-hidden="true"
          >
            <path
              fill-rule="evenodd"
              d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
              clip-rule="evenodd"
            />
          </svg>
        </div>
        <span tw="font-bold text-[#193c47]">{t('space.form.home_info')}</span>
      </div>
      <ul tw="space-y-2 mb-0">
        <li>
          <span tw="underline">Joined on</span>: {displayObjectDate(user?.confirmed_at, 'LL')}
        </li>
        {(user.city || user.country) && (
          <li>
            <span tw="underline">{t('user.profile.address')}</span>:{' '}
            <span id="full_address">
              {user.city && user.country ? `${user.city}, ` : user.city && user.city}
              {user.country}
            </span>
          </li>
        )}
        {user.category && user.category !== 'default' && (
          <li>
            <span tw="underline">{t('user.profile.category')}</span>: {t(`user.profile.edit.select.${user.category}`)}
          </li>
        )}
        <li>
          <span tw="underline">
            <TextWithPlural type="following" count={user?.stats.following_count || 0} />
          </span>
          :{' '}
          <span
            tw="cursor-pointer hover:opacity-80"
            tabIndex={0}
            onClick={openFollowingModal}
            onKeyUp={openFollowingModal}
          >
            {user?.stats.following_count} objects (see)
          </span>
        </li>
      </ul>
    </div>
  );

  return (
    <Layout
      title={`${user.first_name} ${user.last_name} | JOGL`}
      desc={user.short_bio || user.bio}
      img={user.logo_url}
      className="small-top-margin"
      noIndex={user.confirmed_at === undefined} // don't index user if they didn't confirm their account
    >
      <div tw="sm:shadow-custom2">
        <div tw="bg-[#E9BD72] w-full pl-4 py-1 flex flex-wrap" ref={headerRef}>
          <span tw="px-1 md:text-xl font-bold">{t(`member.invite.user.label`)}</span>
        </div>

        <UserHeader user={user} />

        {/* ---- Page grid starts here ---- */}
        <div
          tw="mb-6 grid grid-cols-1 md:(bg-white grid-template-columns[16rem calc(100% - 16rem)]) bg-white border-t border-solid border-gray-200"
          ref={navRef}
        >
          {/* Header and nav (left col on desktop, top col on mobile */}
          <div tw="flex flex-col bg-[#F7F7F9] sticky top-[60px] z-[8] md:(items-center pt-5)">
            <NavContainer>
              <OverflowGradient tw="md:hidden" gradientPosition="left" />
              <OverflowGradient tw="md:hidden" gradientPosition="right" />
              <Nav as="nav" tw="flex gap-3 pt-2.5 w-full md:(flex-col gap-0 pt-0)">
                {tabs.map((item, index) => (
                  <Link
                    shallow
                    scroll={false}
                    key={index}
                    href={`/user/${router.query.id}${item.value === DEFAULT_TAB ? '' : `?tab=${item.value}`}`}
                  >
                    <Tab
                      selected={item.value === selectedTab}
                      tw="px-1 py-3 md:px-3"
                      as="button"
                      id={`${item.value}-tab`}
                      css={[item.value === 'info' ? tw`md:hidden` : tw`block`]}
                    >
                      {t(item.translationId)}
                    </Tab>
                  </Link>
                ))}
              </Nav>
            </NavContainer>
          </div>

          {/* Main content, that change content depending on the selected tab (middle content on desktop) */}
          <div tw="flex flex-col px-0 pb-10 relative md:px-4 md:(pl-4) xl:(pr-4)">
            {selectedTab === 'info' && (
              <TabPanel id="info" tw="md:hidden">
                <UserInfo user={user} />
                {user.skills.length !== 0 && (
                  <div className="infoSkills" tw="flex flex-col mb-4">
                    <TitleInfo title={t('user.profile.skills')} />
                    <Chips
                      data={user.skills.map((skill) => ({
                        title: skill,
                        href: `/search/members/?refinementList[skills][0]=${skill}`,
                      }))}
                      type="skills"
                    />
                  </div>
                )}
                {user.ressources.length !== 0 && (
                  <div className="infoResources" tw="flex flex-col mb-4">
                    <TitleInfo title={t('user.profile.resources')} />
                    <Chips
                      data={user.ressources.map((resource) => ({
                        title: resource,
                        href: `/search/members/?refinementList[ressources][0]=${resource}`,
                      }))}
                      type="resources"
                    />
                  </div>
                )}
                <InfoInterestsComponent title={t('user.profile.interests')} content={user.interests} />
                <div tw="md:hidden mt-6">{displayInfoBox()}</div>
              </TabPanel>
            )}

            {selectedTab === 'about' && (
              <TabPanel id="about">
                <div tw="flex flex-col w-full lg:w-[69%]">
                  <InfoDefaultComponent content={bioWithLinks} containsHtml />
                  <div tw="hidden md:block">
                    {displayInfoBox()}
                    <InfoInterestsComponent title={t('user.profile.interests')} content={user.interests} />
                  </div>
                </div>
              </TabPanel>
            )}

            {selectedTab === 'feed' && (
              <TabPanel id="feed" tw="mx-auto w-full px-0">
                {/* Show feed, and pass DisplayCreate to admins */}
                {user.feed_id && (
                  <Feed
                    feedId={user.feed_id}
                    // show post creation box only to the user
                    allowPosting={userData && user.id === userData.id}
                    isAdmin={user.is_admin}
                  />
                )}
              </TabPanel>
            )}

            {selectedTab === 'collection' && (
              <TabPanel id="collection">
                {loadCollection && (
                  <UserObjects userId={user.id} userStats={user.stats} isUser={userData?.id === user.id} />
                )}
              </TabPanel>
            )}
          </div>
        </div>
      </div>
    </Layout>
  );
}

const FollowingModal = ({ userId, isUser }) => {
  const { data: followings } = useGet(`/api/users/${userId}/following`);
  return <UserObjects userId={userId} isUser={isUser} list={followings} />;
};

export async function getServerSideProps(ctx) {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/api/users/${ctx.query.id}`).catch((err) => console.error(err));
  if (res) return { props: { user: res.data } };
  return { redirect: { destination: '/search/members', permanent: false } };
}
