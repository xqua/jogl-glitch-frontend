import Link from 'next/link';
import { useRouter } from 'next/router';
import useTranslation from 'next-translate/useTranslation';
import { NextPage } from 'next';
import { ReactNode, useEffect, useState } from 'react';
import Alert from 'components/Tools/Alert';
import Layout from 'components/Layout';
import { useApi } from 'contexts/apiContext';
import useUser from 'hooks/useUser';
import Image from 'components/primitives/Image';
import { logEventToGA } from 'utils/analytics';
import SpinLoader from 'components/Tools/SpinLoader';
import SignInUpCarousel from 'components/SignInUpCarousel';
import Button from '~/components/primitives/Button';

const Signin: NextPage = () => {
  const router = useRouter();
  const userContext = useUser();
  const api = useApi();
  const { t } = useTranslation('common');
  const [loading, setLoading] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const [errorMessage, setErrorMessage] = useState<string | ReactNode>('');
  const redirectUrl = (router.query.redirectUrl as string) ? (router.query.redirectUrl as string) : '/';

  const handleChange = (event) => {
    switch (event.target.name) {
      case 'email':
        setEmail(event.target.value);
        break;
      case 'password':
        setPassword(event.target.value);
        break;
      default:
        break;
    }
  };
  const resendVerifEmail = () => {
    const param = { user: { email: email.toLowerCase() } };
    api.post('/api/users/resend_confirmation', param).then(() => {
      // show conf msg in an alert
      alert(t('signIn.resendConf_msgSent'));
    });
  };

  const handleSubmit = (event) => {
    const firstSignIn = router.query?.confirmed === 'true';
    event.preventDefault();
    setLoading(true);
    userContext
      .signIn(email.toLowerCase(), password)
      .then((user) => {
        // send event to google analytics
        logEventToGA('login', 'User', `[${user.id}]`, { userId: user?.id, method: 'Website' });
        // if user doesn't have SDG or Skills, country or short bio, redirect to complete-profile page
        if (
          user?.skills?.length === 0 ||
          user?.interests?.length === 0 ||
          user?.short_bio === null ||
          user?.country === null
        ) {
          router.push({
            pathname: '/complete-profile',
            query: { launch_onboarding: firstSignIn },
          });
        } else {
          // else redirect to the page he was before signin, or the default
          router.push(redirectUrl);
        }
      })
      .catch((errors) => {
        if (errors) {
          // different msgs depending on the case
          const errorMsg = errors?.response?.data?.errors
            ? errors?.response?.data?.errors['email or password'][0]
            : errors?.response?.data?.error;
          console.warn(errorMsg);
          setError(errorMsg || '');
          setLoading(false);
        }
      });
  };

  useEffect(() => {
    if (error.includes('is invalid')) {
      // if error is about invalid login, show this message (translated)
      setErrorMessage(t('err-4010'));
    } else if (error.includes('You have to confirm your email address before continuing')) {
      // if error is that account has not been validated, show this message (translated) + button to resend mail
      setErrorMessage(
        <>
          <p>{t('signIn.resendConf')}</p>
          <Button onClick={resendVerifEmail}>{t('signIn.resendConf_btn')}</Button>
        </>
      );
      ``;
    } else {
      // else simply display error
      setErrorMessage(error);
    }
  }, [error, resendVerifEmail]);

  return (
    <Layout
      className="no-margin"
      title={`${t('header.signIn')} | JOGL`}
      // if there is a should_signin_again query param in sign in page, that means user is signed in with old credential
      // and we want them to signin again (without showing header/footer on signin page), so they can't see they are already "signed in"
      noHeaderFooter={router.query.should_signin_again === 'true'}
    >
      <div tw="pb-10 lg:pb-0">
        <div className="auth-form" tw="items-center flex flex-col lg:flex-row">
          {/* Left section carousel on desktop (and top section jogl logo on mobile) */}
          <div
            className="leftPannel"
            tw="h-[150px] background[linear-gradient(90deg, #0084ff 0%, #661cab 100%)] px-4 justify-center items-center flex w-full lg:(w-1/2 background[transparent] bg-primary height[calc(100vh - 65px)])"
          >
            <div tw="lg:hidden">
              <Link href="/">
                <a>
                  <Image
                    src="/images/logo_single.svg"
                    className="logo"
                    alt="JOGL icon"
                    tw="(w-[80px] lg:max-w-[400px])!"
                  />
                </a>
              </Link>
            </div>
            <SignInUpCarousel />
          </div>

          {/* Right section (or bottom on mobile) */}
          <div className="rightPannel" tw="w-full lg:w-1/2">
            {router.query?.confirmed === 'true' && (
              <Alert
                type="success"
                message={
                  <>
                    <h4 className="alert-heading">{t('newJogler.title')}</h4>
                    <p tw="mb-0">{t('newJogler.message')}</p>
                  </>
                }
              />
            )}

            <div className="form-content">
              <div className="form-header">
                <h2 id="signModalLabel">{t('signIn.title')}</h2>
                <p>{t('signIn.description')}</p>
              </div>
              <form>
                <div tw="mb-4">
                  <label className="form-check-label" htmlFor="email">
                    {t('auth.email.title')}
                  </label>
                  <input
                    type="email"
                    name="email"
                    id="email"
                    className="form-control styledInputAuth"
                    placeholder={t('auth.email.placeholder')}
                    onChange={handleChange}
                  />
                </div>
                <div tw="mb-4">
                  <div className="rowPwd" tw="inline-flex justify-between w-full">
                    <label className="form-check-label" htmlFor="password">
                      {t('signIn.pwd')}
                    </label>
                    <div className="forgotPwd" tw="w-7/12 text-right">
                      <Link href="/auth/forgot-password">
                        <a tabIndex="-1">{t('signIn.forgotPwd')}</a>
                      </Link>
                    </div>
                  </div>
                  <input
                    type="password"
                    name="password"
                    id="password"
                    className="form-control styledInputAuth"
                    placeholder={t('signIn.pwd_placeholder')}
                    onChange={handleChange}
                  />
                </div>
                {error !== '' && <Alert type="danger" message={errorMessage} />}

                <div className="goToSignUp">
                  <span>{t('signIn.newJoin')}</span>
                  <span className="goToSignUp--signup">
                    <Link href="/signup">
                      <a>{t('header.signUp')}</a>
                    </Link>
                  </span>
                </div>
                <Button type="submit" disabled={loading} onClick={handleSubmit} tw="flex">
                  {loading && <SpinLoader />}
                  {t('signIn.btnSignIn')}
                </Button>
                {/* <div className="form-check remember">
                    <input type="checkbox" className="form-check-input" id="rememberMe" />
                    <label className="form-check-label" htmlFor="rememberMe">
                      {t("signIn.remember")}
                    </label>
                  </div> */}
              </form>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default Signin;
