import { NextPage } from 'next';
import React, { useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Layout from 'components/Layout';
import { Proposal, PeerReview, Program, Project, Space } from 'types';
import { getApiFromCtx } from 'utils/getApi';
import Image from 'components/primitives/Image';
import ProposalHeader from '~/components/Proposal/ProposalHeader';
import useGet from '~/hooks/useGet';
import InfoHtmlComponent from '~/components/Tools/Info/InfoHtmlComponent';
import ProposalAccordion from '~/components/Proposal/ProposalAccordion';
import Button from '~/components/primitives/Button';

const ProposalShow: NextPage<{ proposal: Proposal }> = ({ proposal }) => {
  const { t } = useTranslation('common');
  const { data: proposal_project } = useGet<Project>(`/api/projects/${proposal.project_id}`);
  const { data: peer_review } = useGet<PeerReview>(`/api/peer_reviews/${proposal.peer_review_id}`);
  const peerReviewResource = { type: peer_review?.resource_type?.toLowerCase(), id: peer_review?.resource_id };
  const peer_review_parent_route = `${peerReviewResource.type?.toLowerCase() + 's'}/${peerReviewResource.id}`;
  const { data: peer_review_parent } = useGet<Space | Program>(`/api/${peer_review_parent_route}`);
  const { data: answers } = useGet(`/api/proposals/${proposal.id}/answers`);
  const title = proposal.title || `Proposal for project "${proposal_project?.title}"`;
  const [active, setActive] = useState(false);

  const toogleActive = () => setActive(!active);

  return (
    <Layout className="small-top-margin" noIndex={!proposal.submitted_at} title={title && `${title} | JOGL`}>
      <div tw="sm:shadow-custom2">
        <img
          alt={`${title} banner`}
          // priority
          // quality="100"
          src={proposal_project?.banner_url || '/images/default/default-peer-review.jpg'}
          tw="w-full! max-h-[400px]! object-cover"
        />
        <div tw="bg-[#9fd39f] w-full pl-4 py-1 font-bold md:text-xl">{t('general.proposal_1')}</div>
        {/* {!proposal.submitted_at && (
        <div tw="bg-[#E6932B] w-full pl-4 py-1 flex">
          This application is undergoing submission. It will be uneditable and permanent on JOGL once all team members
          have approved.
        </div>
      )} */}
        {proposal_project && peer_review && answers && (
          <ProposalHeader
            project={proposal_project}
            submitted_at={proposal.submitted_at}
            title={title}
            score={proposal.score}
            funding={proposal.funding}
            id={proposal.id}
            summary={proposal.summary}
            isAdmin={proposal.is_admin}
            isMember={proposal.is_member}
            isPeerReviewAdmin={proposal.is_peer_review_admin}
            peerReviewDates={{ start: peer_review?.start, deadline: peer_review?.deadline }}
            isValidated={proposal.is_validated}
            hasAnsweredAllQuestions={answers?.answers?.length === peer_review?.template_question_count}
            peerReview={{ resource: peerReviewResource, object: peer_review, parent: peer_review_parent }}
          />
        )}
        {/* ---- CONTENT ---- */}
        <div tw="bg-white p-4 border-t border-solid border-gray-200 max-w-[100ch]">
          <Button tw="mt-5 mb-3" onClick={toogleActive}>
            {active ? 'Collapse all' : 'Expand all'}
          </Button>
          {answers?.answers
            ?.sort((a, b) => a.document.position - b.document.position)
            .map((answer, i) => (
              <ProposalAccordion key={i} title={answer.document.title} allactive={active}>
                <span className="accordion-text">
                  <InfoHtmlComponent content={answer.content} />
                </span>
              </ProposalAccordion>
            ))}
        </div>
      </div>
    </Layout>
  );
};

export async function getServerSideProps(ctx) {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/api/proposals/${ctx.query.id}`).catch((err) => console.error(err));
  // don't show page to non members if proposal has not been submitted
  if ((res?.data && res?.data?.submitted_at !== null) || (res?.data && res?.data?.is_member))
    return { props: { proposal: res.data } };
  return { redirect: { destination: '/', permanent: false } };
}

export default ProposalShow;
