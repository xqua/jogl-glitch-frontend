import { ArrowLeft } from '@emotion-icons/fa-solid/ArrowLeft';
import { TabPanel, TabPanels, Tabs } from '@reach/tabs';
import { NextPage } from 'next';
import useTranslation from 'next-translate/useTranslation';
import A from '~/components/primitives/A';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import Layout from 'components/Layout';
import MembersList from 'components/Members/MembersList';
import Button from 'components/primitives/Button';
import P from 'components/primitives/P';
import ProposalForm from 'components/Proposal/ProposalForm';
import ProposalAnswerForm from 'components/Proposal/ProposalAnswerForm';
import Alert from 'components/Tools/Alert';
import { useApi } from 'contexts/apiContext';
import { useModal } from 'contexts/modalContext';
import useUser from 'hooks/useUser';
import { Project, Proposal } from 'types';
import { getApiFromCtx } from 'utils/getApi';
import { NavTab, TabListStyle } from 'components/Tabs/TabsStyles';
import { logEventToGA } from 'utils/analytics';
import useGet from '~/hooks/useGet';
import Link from 'next/link';

interface Props {
  proposal: Proposal;
}

const ProposalEdit: NextPage<Props> = ({ proposal: proposalProp }) => {
  const [proposal, setProposal] = useState(proposalProp);
  const router = useRouter();
  const [sending, setSending] = useState(false);
  const [updatedProposal, setUpdatedProposal] = useState(undefined);
  const [hasUpdated, setHasUpdated] = useState(false);
  const [errors, setErrors] = useState(undefined);
  const urlBack = `/proposal/${router.query.id}`;
  const { showModal, closeModal } = useModal();
  const api = useApi();
  const { user } = useUser();
  const { t } = useTranslation('common');
  const { data: peer_review_questions } = useGet(`/api/peer_reviews/${proposal.peer_review_id}/faq`);
  const { data: proposal_project } = useGet<Project>(`/api/projects/${proposal.project_id}`);
  const { data: answers } = useGet(`/api/proposals/${proposal.id}/answers`);

  const handleChange = (key, content) => {
    setProposal((prevProposal) => ({ ...prevProposal, [key]: content })); // update fields as user changes them
    // TODO: have only one place where we manage need content
    setUpdatedProposal((prevUpdatedProposal) => ({ ...prevUpdatedProposal, [key]: content })); // set an object containing only the fields/inputs that are updated by user
  };

  const handleSubmit = () => {
    setSending(true);
    api
      .patch(`/api/proposals/${proposal.id}`, { proposal: updatedProposal })
      .then(() => {
        setSending(false);
        setUpdatedProposal(undefined); // reset updated program component
        setHasUpdated(true); // show update confirmation message
        setTimeout(() => {
          setHasUpdated(false);
        }, 3000); // hide confirmation message after 3 seconds
      })
      .catch(() => {
        setSending(false);
      });
  };

  const deleteProposal = () => {
    api
      .delete(`api/proposals/${proposal.id}`)
      .then(() => {
        // record event to Google Analytics
        logEventToGA('delete proposal', 'Proposal', `[${user.id},${proposal.id}]`, {
          userId: user.id,
          itemId: proposal.id,
        });
        closeModal(); // close modal
        router.push('/');
      })
      .catch((error) => {
        setErrors(error.toString());
      });
  };

  const errorMessage = errors?.includes('err-') ? t(errors) : errors;

  // Tabs elements: tabs list & handleTabsChange
  // Explained on pages-proposal-index file
  const tabs = [
    { value: 'basic_info', translationId: 'entity.tab.basic_info' },
    { value: 'answers', translationId: 'proposal.answers' },
    { value: 'members', translationId: 'proposal.card.team' },
    { value: 'advanced', translationId: 'entity.tab.advanced' },
  ];

  const handleTabsChange = (index) => {
    const element = document.querySelector('[data-reach-tabs]');
    const headerHeight = 80;
    const y = element.getBoundingClientRect().top + window.pageYOffset - headerHeight;
    window.scrollTo({ top: y, behavior: 'smooth' });
    router.push(`/proposal/${proposal.id}/edit?t=${tabs[index].value}`, undefined, { shallow: true });
  };

  return (
    <Layout title={`Edit ${proposal?.title || 'proposal'} | JOGL`}>
      <div className="projectEdit" tw="mx-auto px-4">
        <div>
          <h1 tw="mb-0">{t('proposal.edit.title')}</h1>
          {/* proposal's project link */}
          <div tw="inline-flex flex-wrap mb-3">
            {t('proposal.card.fromProject')}:&nbsp;
            <Link href={`/project/${proposal_project?.id}?t=proposals`} passHref>
              <a tw="text-black font-medium underline hover:(text-black font-bold)">{proposal_project?.title}</a>
            </Link>
          </div>
        </div>
        {/* go back link */}
        <A href={urlBack}>
          <ArrowLeft size={15} title="Go back" />
          {t('proposal.edit.back')}
        </A>
        {/* TODO switch to custom nav 'same as program' */}
        <Tabs
          defaultIndex={router.query.t ? tabs.findIndex((t) => t.value === router.query.t) : null}
          onChange={handleTabsChange}
        >
          <TabListStyle>
            {tabs.map((item, key) => (
              <NavTab key={key}>{t(item.translationId)}</NavTab>
            ))}
          </TabListStyle>
          <TabPanels tw="justify-center">
            {/* Basic info tab */}
            <TabPanel>
              <p tw="italic">{t('proposal.explanations.info')}</p>
              <ProposalForm
                handleChange={handleChange}
                handleSubmit={handleSubmit}
                mode="edit"
                proposal={proposal}
                sending={sending}
                hasUpdated={hasUpdated}
              />
            </TabPanel>
            {/* Answers tab */}
            <TabPanel>
              <p tw="italic">{t('proposal.explanations.answers')}</p>
              <Alert type="info" message={t('proposal.form.save_answer_alert')} />
              {answers &&
                peer_review_questions?.documents
                  ?.sort((a, b) => a.position - b.position)
                  .map((question, i) => (
                    <div key={i} tw="mt-8">
                      <h4>{question.title}</h4>
                      {question.content !== '‎' && <p tw="mb-0">{question.content}</p>}
                      <ProposalAnswerForm
                        answer={answers?.answers?.find((answer) => answer.document.id === question.id)}
                        questionId={question.id}
                        proposalId={proposal.id}
                      />
                      <hr />
                    </div>
                  ))}
            </TabPanel>
            {/* Members tab */}
            <TabPanel>
              <p tw="italic">{t('proposal.explanations.members')}</p>
              <MembersList itemType="proposals" itemId={proposal?.id} isOwner={proposal?.is_owner} />
            </TabPanel>
            {/* Advanced tab */}
            <TabPanel>
              <h4>{t('project.advancedParam')}</h4>
              {!proposal?.submitted_at && (
                <div className="deleteBtns">
                  {proposal && <p>{t('general.delete.explain')}</p>}
                  <Button
                    onClick={() => {
                      showModal({
                        children: (
                          <>
                            {errors && <Alert type="danger" message={errorMessage} />}
                            <P tw="text-base">{t('general.delete.text')}</P>
                            <div tw="inline-flex space-x-3">
                              <Button btnType="danger" onClick={deleteProposal}>
                                {t('general.yes')}
                              </Button>
                              <Button onClick={closeModal}>{t('general.no')}</Button>
                            </div>
                          </>
                        ),
                        title: t('general.delete.title'),
                        maxWidth: '30rem',
                      });
                    }}
                    btnType="danger"
                  >
                    {t('general.delete.title')}
                  </Button>
                </div>
              )}
            </TabPanel>
          </TabPanels>
        </Tabs>
      </div>
    </Layout>
  );
};

export async function getServerSideProps({ query, ...ctx }) {
  const api = getApiFromCtx(ctx);
  const res = await api
    .get(`/api/proposals/${query.id}`)
    .catch((err) => console.error(`Couldn't fetch proposal with id=${query.id}`, err));
  // return data/page if user is admin & submitted_at is not set
  // (meaning proposal has not been sent for review), else redirect to proposal show page
  if (!res?.data?.submitted_at && res?.data?.is_admin) return { props: { proposal: res.data } };
  return { redirect: { destination: `/proposal/${res?.data?.id}`, permanent: false } };
}

export default ProposalEdit;
