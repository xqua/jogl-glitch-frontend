import Link from 'next/link';
import useTranslation from 'next-translate/useTranslation';
import { NextPage } from 'next';
import { ReactNode, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Alert from 'components/Tools/Alert';
import Layout from 'components/Layout';
import { useApi } from 'contexts/apiContext';
import Image from 'components/primitives/Image';
import SpinLoader from 'components/Tools/SpinLoader';
import Button from '~/components/primitives/Button';
import { addressFront } from '~/utils/utils';

const ForgotPwd: NextPage = () => {
  const [email, setEmail] = useState('');
  const [error, setError] = useState<string | ReactNode>('');
  const [fireRedirect, setFireRedirect] = useState(false);
  const [sending, setSending] = useState(false);
  const [success, setSuccess] = useState(false);
  const router = useRouter();
  const { t } = useTranslation('common');

  const api = useApi();

  const handleChange = (e) => {
    setError('');
    switch (e.target.name) {
      case 'email':
        setEmail(e.target.value);
        break;
      default:
        setError('Unknown input');
        break;
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    if (email === '') {
      setError(t('err-4003'));
    } else {
      const redirectUrl = `${addressFront}/auth/new-password`;
      setError('');
      setSending(true);

      api
        .post('/api/auth/password/unknown', { email: email.toLowerCase(), redirect_url: redirectUrl })
        .then(() => {
          setSending(false);
          setSuccess(true);
          setTimeout(() => {
            setFireRedirect(true);
          }, 10000);
        })
        .catch((err) => {
          setSending(false);
          setError(err.response.data.error);
        });
    }
  };
  useEffect(() => {
    if (fireRedirect) {
      router.push('/signin');
    }
  }, [fireRedirect]);

  return (
    <Layout className="no-margin" title={`${t('forgotPwd.title')} | JOGL`} noIndex>
      <div className="auth-form" tw="pb-10 items-center flex flex-col lg:(flex-row pb-0)">
        {/* Left section carousel on desktop (and top section jogl logo on mobile) */}
        <div
          className="leftPannel"
          tw="h-[150px] background[linear-gradient(90deg, #0084ff 0%, #661cab 100%)] px-4 justify-center items-center flex w-full lg:(w-5/12 background[transparent] bg-primary height[calc(100vh - 65px)])"
        >
          <Link href="/">
            <a>
              <Image
                src="/images/logo_single.svg"
                className="logo"
                alt="JOGL icon"
                tw="(max-width[120px] lg:max-width[400px])!"
              />
            </a>
          </Link>
        </div>

        {/* Right section (or bottom on mobile) */}
        <div className="rightPannel" tw="flex justify-center lg:w-7/12!">
          <div className="form-content">
            <div className="form-header">
              <h2 id="signModalLabel">{t(success ? 'forgotPwd.confTitle' : 'forgotPwd.title')}</h2>
              <p>{t(success ? 'forgotPwd.confMsg' : 'forgotPwd.description')}</p>
            </div>
            {!success && (
              <form onSubmit={handleSubmit}>
                <div tw="mb-4">
                  <label className="form-check-label" htmlFor="password">
                    {t('auth.email.title')}
                  </label>
                  <input
                    type="email"
                    name="email"
                    id="email"
                    className="form-control styledInputAuth"
                    placeholder={t('auth.email.placeholder')}
                    onChange={handleChange}
                  />
                </div>
                {error !== '' && <Alert type="danger" message={error} />}
                {success && <Alert type="success" message={t('info-4000')} />}

                <Button type="submit" disabled={!!sending} tw="flex">
                  {sending && <SpinLoader />}
                  {t('forgotPwd.btnSendMail')}
                </Button>
              </form>
            )}
            {success && (
              <div className="form-message">
                <img src="/images/envelope.svg" alt="Message sent envelope" />
              </div>
            )}
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default ForgotPwd;
