import Link from 'next/link';
import { NextPage } from 'next';
import useTranslation from 'next-translate/useTranslation';
import Layout from 'components/Layout';
import Image from 'components/primitives/Image';
import FormChangePwd from '~/components/Tools/Forms/FormChangePwd';

const ChangePassword: NextPage = () => {
  const { t } = useTranslation('common');
  return (
    <Layout className="no-margin" title={`${t('auth.changePwd.title')} | JOGL`} noIndex>
      <div className="auth-form" tw="pb-10 items-center flex flex-col lg:(flex-row pb-0)">
        {/* Left section carousel on desktop (and top section jogl logo on mobile) */}
        <div
          className="leftPannel"
          tw="h-[150px] background[linear-gradient(90deg, #0084ff 0%, #661cab 100%)] px-4 justify-center items-center flex w-full lg:(w-5/12 background[transparent] bg-primary height[calc(100vh - 65px)])"
        >
          <Link href="/">
            <a>
              <Image
                src="/images/logo_single.svg"
                className="logo"
                alt="JOGL icon"
                tw="(max-width[120px] lg:max-width[400px])!"
              />
            </a>
          </Link>
        </div>
        {/* Right section (or bottom on mobile) */}
        <div className="rightPannel" tw="flex justify-center lg:w-7/12!">
          <div className="form-content">
            <div className="form-header">
              <h2 id="signModalLabel">{t('auth.newPwd.title')}</h2>
              <p>{t('auth.newPwd.description')}</p>
            </div>
            <FormChangePwd />
          </div>
        </div>
      </div>
    </Layout>
  );
};
export default ChangePassword;
