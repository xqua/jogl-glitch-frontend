import { NextPage } from 'next';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import React from 'react';
import Layout from 'components/Layout';
import Button from '~/components/primitives/Button';

const ProgramForbiddenPage: NextPage = () => {
  const { t } = useTranslation('common');

  return (
    <Layout>
      <div className="ProgramPage" tw="text-center px-2">
        <br />
        <br />
        {t('program.info.forbidden')}
        <br />
        <a href="mailto:hello@jogl.io">hello@jogl.io</a>
        <br />
        <br />
        <br />
        <Link href="/search/programs" passHref>
          <Button>{t('program.info.forbiddenBtn')}</Button>
        </Link>
      </div>
    </Layout>
  );
};
export default ProgramForbiddenPage;
