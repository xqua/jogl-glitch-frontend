import { NextPage } from 'next';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Feed from 'components/Feed/Feed';
import Layout from 'components/Layout';
import A from 'components/primitives/A';
import Button from 'components/primitives/Button';
import H2 from 'components/primitives/H2';
import P from 'components/primitives/P';
import ProgramAbout from 'components/Program/ProgramAbout';
import ShowFaq from 'components/Tools/ShowFaq';
import ProgramHeader from 'components/Program/ProgramHeader';
import ProgramMembers from 'components/Program/ProgramMembers';
import ProgramNeeds from 'components/Program/ProgramNeeds';
import ProgramProjects from 'components/Program/ProgramProjects';
import ProgramResources from 'components/Program/ProgramResources';
import BtnStar from 'components/Tools/BtnStar';
// import ShareBtns from 'components/Tools/ShareBtns/ShareBtns';
import useGet from 'hooks/useGet';
import useUserData from 'hooks/useUserData';
import { Challenge, Faq, PeerReview, Program } from 'types';
import { getApiFromCtx } from 'utils/getApi';
// import { useScrollHandler } from 'utils/utils';
import Image from 'components/primitives/Image';
import tw from 'twin.macro';
import 'intro.js/introjs.css';
import { startProgramIntro } from 'utils/onboarding/index';
import ProgramActivities from '~/components/Program/ProgramActivities';
import ProgramInfo from 'components/Program/ProgramInfo';
import useMembers from '~/hooks/useMembers';
import { Envelope } from '@emotion-icons/fa-solid';
import ReactGA from 'react-ga';
import { useModal } from '~/contexts/modalContext';
import { ContactForm } from '~/components/Tools/ContactForm';
import ReactTooltip from 'react-tooltip';
import { Tab, TabPanel, NavContainer, Nav, OverflowGradient, ContactButton } from '~/utils/style';
import Search2 from '~/components/Search/Search2';

const DEFAULT_TAB = 'about';

const ProgramDetails: NextPage<{ program: Program }> = ({ program }) => {
  const router = useRouter();
  const { locale } = router;
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();
  const [selectedTab, setSelectedTab] = useState(DEFAULT_TAB);
  // const [offSetTop, setOffSetTop] = useState();
  const customChalName = program.custom_challenge_name;
  const isOnboarding = typeof window !== 'undefined' && JSON.parse(localStorage.getItem('isOnboarding'));
  const isAnIndividual = typeof window !== 'undefined' && JSON.parse(localStorage.getItem('isIndividual'));
  const isDesktop = typeof window !== 'undefined' && JSON.parse(localStorage.getItem('isDesktop'));
  const { data: dataChallenges } = useGet<{ challenges: Challenge }>(`/api/programs/${program?.id}/challenges`);
  const { data: dataPeerReviews } = useGet<PeerReview[]>(`/api/programs/${program.id}/peer_reviews`);
  const { data: faqList } = useGet<{ documents: Faq[] }>(`/api/programs/${program.id}/faq`);
  const { members } = useMembers('programs', program?.id, 25); // take 25 first members
  const { userData } = useUserData();
  const headerRef = useRef();
  const navRef = useRef();
  // const isSticky = useScrollHandler(offSetTop);

  const tabs = [
    { value: 'info', translationId: 'general.tab.info' },
    { value: 'about', translationId: 'general.tab.about' },
    { value: 'news', translationId: 'user.profile.tab.feed' },
    ...(dataChallenges?.challenges.length !== 0 || dataPeerReviews?.length !== 0
      ? [{ value: 'activities', translationId: 'general.activities' }]
      : []),
    ...(program.projects_count !== 0 ? [{ value: 'projects', translationId: 'entity.tab.projects' }] : []),
    ...(program.needs_count !== 0 ? [{ value: 'needs', translationId: 'entity.tab.needs' }] : []),
    { value: 'members', translationId: 'entity.card.members' },
    { value: 'resources', translationId: 'program.tab.resources' },
    ...(faqList?.documents.length !== 0 ? [{ value: 'faq', translationId: 'entity.info.faq' }] : []),
    { value: 'contact', translationId: 'footer.contactUs' },
  ];

  const sendMessageToAdmin = (id, first_name, last_name) => {
    // capture the opening of the modal as a special modal page view to google analytics
    ReactGA.modalview('/send-message');
    showModal({
      children: <ContactForm itemId={id} closeModal={closeModal} />,
      title: t('user.contactModal.title', {
        userFullName: `${first_name} ${last_name}`,
      }),
    });
  };

  // useEffect(() => {
  //   setOffSetTop(headerRef?.current?.offsetTop);
  // }, [headerRef]);

  useEffect(() => {
    // on first load, check if url has a tab param, and if it does, select this tab
    router.query.tab && setSelectedTab(router.query.tab as string);
  }, []);

  // function when changing tab (or when router change in general)
  useEffect(() => {
    const tabValues = tabs.map(({ value }) => value);
    if (tabValues.includes(router.query.tab as string)) {
      if (router.query.tab === 'about') {
        router.push(`/program/${router.query.short_title}`, undefined, {
          shallow: true,
        });
      }
      const navContainerTopPosition = navRef.current.getBoundingClientRect().top + window.scrollY; // get y position of nav container
      const yAdjustment = window.innerWidth < 768 ? 40 : 150; // change yAdustement depending on if it's mobile/tablet or desktop
      window.scrollTo(0, navContainerTopPosition - yAdjustment); // force scroll to top of the nav/tab (remove a little to be really on top)
      setSelectedTab(router.query.tab as string);
    }
    if (!router.query.tab) setSelectedTab(DEFAULT_TAB);
  }, [router]);

  // [Onboarding][5] - Retaking onboarding flow on program
  useEffect(() => {
    if (isOnboarding && isAnIndividual) {
      setTimeout(() => {
        startProgramIntro(t, router, isDesktop);
      }, 2000);
    }
  }, [isOnboarding, isAnIndividual]);

  return (
    <Layout
      title={program?.title && `${program.title} | JOGL`}
      desc={program?.short_description}
      img={program?.banner_url || '/images/default/default-program.jpg'}
      className="small-top-margin"
      noIndex={program?.status === 'draft'}
    >
      <div tw="sm:shadow-custom2">
        <div tw="relative">
          <Image
            quality="100"
            priority
            src={program?.banner_url || '/images/default/default-program.jpg'}
            alt={`${program?.title} banner`}
            tw="w-full! max-h-[400px]! object-cover"
          />
          <div tw="absolute right-0 top-0 m-4">
            <BtnStar
              itemType="programs"
              itemId={program.id}
              hasStarred={program.has_saved}
              count={program.saves_count}
            />
          </div>
        </div>
        <div tw="bg-[#CEDE7C] w-full pl-4 py-1 flex flex-wrap" ref={headerRef}>
          <span tw="font-bold px-1 text-xl">{t('program.title')}</span>
        </div>

        <ProgramHeader program={program} lang={locale} />
        {/* <StickyHeading isSticky={isSticky} className="stickyHeading">
          <div tw="flex flex-row items-center px-4 xl:px-0">
            <H2 tw="font-size[1.8rem] sm:font-size[2.18rem] line-height[26px]">
              {(locale === 'fr' && program?.title_fr) || program?.title}
            </H2>
            <div tw="flex pl-5" className="actions">
              <ShareBtns type="program" specialObjId={program.id} />
            </div>
          </div>
        </StickyHeading> */}

        {/* ---- Page grid starts here ---- */}
        <div
          tw="mb-6 grid grid-cols-1 md:(bg-white grid-template-columns[16rem calc(100% - 16rem)]) bg-white border-t border-solid border-gray-200"
          ref={navRef}
        >
          {/* Header and nav (left col on desktop, top col on mobile */}
          <div tw="flex flex-col bg-[#F7F7F9] sticky top-[60px] z-[8] md:(items-center pt-5)">
            <NavContainer>
              <OverflowGradient tw="md:hidden" gradientPosition="left" />
              <OverflowGradient tw="md:hidden" gradientPosition="right" />
              <Nav as="nav" tw="flex gap-3 pt-2.5 w-full md:(flex-col gap-0 pt-0)">
                {tabs.map((item, index) => (
                  <Link
                    shallow
                    scroll={false}
                    key={index}
                    href={`/program/${router.query.short_title}${
                      item.value === DEFAULT_TAB ? '' : `?tab=${item.value}`
                    }`}
                  >
                    <Tab
                      selected={item.value === selectedTab}
                      tw="px-1 py-3 md:px-3"
                      as="button"
                      id={`${item.value}-tab`}
                      css={[item.value === 'info' ? tw`md:hidden` : tw`block`]}
                    >
                      {t(item.translationId)}
                    </Tab>
                  </Link>
                ))}
              </Nav>
            </NavContainer>
          </div>

          {/* Main content, that change content depending on the selected tab (middle content on desktop) */}
          <div tw="flex flex-col px-0 pb-10 relative md:px-4 md:(pl-4) xl:(pr-4)">
            {selectedTab === 'info' && (
              <TabPanel id="info" tw="md:hidden">
                <ProgramInfo program={program} />
              </TabPanel>
            )}

            {selectedTab === 'about' && (
              <TabPanel id="about">
                <ProgramAbout program={program} locale={locale} />
              </TabPanel>
            )}

            {selectedTab === 'news' && (
              <TabPanel id="news" tw="mx-auto w-full px-0">
                {/* Show feed, and pass DisplayCreate to admins */}
                {program.feed_id && (
                  <Feed
                    feedId={program.feed_id}
                    allowPosting={program.is_member}
                    isAdmin={program.is_admin || program.is_owner}
                  />
                )}
              </TabPanel>
            )}

            {selectedTab === 'activities' && (
              <TabPanel id="activities">
                <ProgramActivities
                  programId={program.id}
                  programShortTitle={program.short_title}
                  isAdmin={program.is_admin}
                  // customChallengeName={program.custom_challenge_name}
                />
              </TabPanel>
            )}

            {selectedTab === 'projects' && (
              <TabPanel id="projects">
                {router.query.showAlgoProj === 'true' && (
                  <div className="searchPageAll AlgoliaResultsPages">
                    <Search2
                      index="Project"
                      programTitle={program.title}
                      refinements={[
                        { attribute: 'reviews_count', searchable: false, showMore: false, type: 'toggle' },
                        { attribute: 'status', searchable: false, showMore: false },
                        { attribute: 'maturity', searchable: false, showMore: false },
                        { attribute: 'interests', searchable: false, limit: 17 },
                        { attribute: 'programs.title', searchable: false },
                        { attribute: 'challenges.title' },
                      ]}
                      sortByItems={[
                        { label: t('general.filter.newly_updated'), index: 'Project_updated_at' },
                        { label: t('general.filter.object.date2'), index: 'Project_id_des' },
                        { label: t('general.filter.pop'), index: 'Project' },
                        { label: t('general.star'), index: 'Project_saves_desc' },
                        { label: t('general.need', { count: 2 }), index: 'Project_needs' },
                        { label: t('general.member', { count: 2 }), index: 'Project_members' },
                        { label: t('general.filter.object.date1'), index: 'Project_id_asc' },
                        { label: t('general.filter.object.alpha1'), index: 'Project_title_asc' },
                        { label: t('general.filter.object.alpha2'), index: 'Project_title_desc' },
                      ]}
                    />
                  </div>
                )}
                {/* <SearchStateContextProgramProjectProvider>
								<Tabs>
									<TabPanels>
										<div
											className="searchPageAll AlgoliaResultsPages"
											css={[
												css`
													.container-wrapper {
														display: none;
													}
												`,
											]}
										>
											<ProjectsTab />
										</div>
									</TabPanels>
								</Tabs>
							</SearchStateContextProgramProjectProvider> */}
                <ProgramProjects programId={program.id} customChalName={customChalName} />
              </TabPanel>
            )}

            {selectedTab === 'needs' && (
              <TabPanel id="needs">
                <ProgramNeeds programId={program.id} customChalName={customChalName} />
              </TabPanel>
            )}

            {selectedTab === 'members' && (
              <TabPanel id="members" tw="relative ">
                <ProgramMembers programId={program.id} customChalName={customChalName} />
              </TabPanel>
            )}

            {selectedTab === 'resources' && (
              <TabPanel id="resources">
                <ProgramResources programId={program?.id} />
              </TabPanel>
            )}

            {selectedTab === 'faq' && (
              <TabPanel id="faq">
                <ShowFaq faqList={faqList} />
              </TabPanel>
            )}

            {selectedTab === 'contact' && (
              <TabPanel id="contact">
                <H2 tw="mb-4">{t('footer.contactUs')}</H2>
                <a href={`mailto:${program?.contact_email || 'hello@jogl.io'}`}>
                  <Button tw="flex justify-center items-center space-x-2">
                    <Envelope size={18} title="Contact" />
                    <span>Contact program</span>
                  </Button>
                </a>
                <div tw="flex flex-col mt-6 space-y-4">
                  {members
                    ?.filter(({ admin, owner }) => owner || admin) // only show leaders and admins
                    .map(({ id, first_name, last_name, logo_url_sm }, index) => (
                      <div tw="flex space-x-4 items-center" key={index}>
                        <Link href={`/user/${id}`}>
                          <a>
                            <div tw="flex space-x-4 items-center">
                              <img
                                tw="object-cover rounded-full w-12 h-12"
                                src={logo_url_sm}
                                alt={`${first_name} ${last_name}`}
                              />
                              <P tw="mb-0 font-bold font-size[1.2rem]">{first_name + ' ' + last_name}</P>
                            </div>
                          </a>
                        </Link>
                        <div tw="flex flex-col">
                          <ContactButton
                            size={20}
                            title="Contact user"
                            onClick={() => sendMessageToAdmin(id, first_name, last_name)}
                            onKeyUp={(e) =>
                              // execute only if it's the 'enter' key
                              (e.which === 13 || e.keyCode === 13) && sendMessageToAdmin(id, first_name, last_name)
                            }
                            tabIndex={0}
                            data-tip={t('user.contactModal.title', { userFullName: first_name + ' ' + last_name })}
                            data-for="contactAdmin"
                            // show/hide tooltip on element focus/blur
                            onFocus={(e) => ReactTooltip.show(e.target)}
                            onBlur={(e) => ReactTooltip.hide(e.target)}
                          />
                          <ReactTooltip id="contactAdmin" effect="solid" place="bottom" />
                        </div>
                      </div>
                    ))}
                </div>
              </TabPanel>
            )}

            <div tw="pt-11 pb-7 px-4 block">
              {!userData && ( // if user is not connected
                <div tw="flex items-start pt-4 pb-6 flex-wrap xl:items-center">
                  <A href="/signup">
                    <Button>{t('program.rightCompo.createAccount.btn')}</Button>
                  </A>
                  <P tw="pt-2 pl-1 mb-0">{t('program.rightCompo.createAccount.text')}</P>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

const getProgram = async (api, programId) => {
  const res = await api.get(`/api/programs/${programId}`).catch((err) => console.error(err));
  if (res?.data) {
    return res.data;
  }
  return undefined;
};

export async function getServerSideProps({ query, ...ctx }) {
  const api = getApiFromCtx(ctx);
  // Case short_title is a string for pretty URL
  // eslint-disable-next-line no-restricted-globals
  if (isNaN(Number(query.short_title as string))) {
    const res = await api.get(`/api/programs/getid/${query.short_title}`).catch((err) => console.error(err));
    if (res) {
      const program = await getProgram(api, res.data.id).catch((err) => console.error(err));
      return { props: { program } };
    }
    return { redirect: { destination: '/', permanent: false } };
  }
  // Case short_title is actually an id
  const program = await getProgram(api, query.short_title).catch((err) => console.error(err));
  if (program) {
    return { props: { program } };
  } else {
    return { redirect: { destination: '/', permanent: false } };
  }
}

export default ProgramDetails;
