import { NextPage } from 'next';
import React from 'react';
import ProjectCard from 'components/Project/ProjectCard';
import ProgramCard from 'components/Program/ProgramCard';
import ChallengeCard from 'components/Challenge/ChallengeCard';
import NeedCard from 'components/Need/NeedCard';
// import CommunityCard from 'components/Community/CommunityCard';
import SpaceCard from 'components/Space/SpaceCard';
import PostDisplay from 'components/Feed/Posts/PostDisplay';
import { getApiFromCtx } from 'utils/getApi';
import PeerReviewCard from '~/components/PeerReview/PeerReviewCard';
import ProposalCard from '~/components/Proposal/ProposalCard';
import UserCard from '~/components/User/UserCard';

const EmbeddedPage: NextPage = ({ object, objectType }) => {
  const renderObject = () => {
    switch (objectType) {
      case 'projects':
        return (
          <ProjectCard
            id={object.id}
            key={object.id}
            title={object.title}
            shortTitle={object.short_title}
            short_description={object.short_description}
            members_count={object.members_count}
            postsCount={object.posts_count}
            needs_count={object.needs_count}
            skills={object.skills}
            reviewsCount={object.reviews_count}
            status={object.status}
            banner_url={object.banner_url || '/images/default/default-project.jpg'}
          />
        );
      case 'needs':
        return (
          <NeedCard
            title={object.title}
            project={object.project}
            skills={object.skills}
            resources={object.ressources}
            id={object.id}
            key={object.id}
            postsCount={object.posts_count}
            publishedDate={object.created_at}
            membersCount={object.members_count}
            dueDate={object.end_date}
            status={object.status}
          />
        );
      // case 'communities':
      //   return (
      //     <CommunityCard
      //       id={object.id}
      //       key={object.id}
      //       title={object.title}
      //       shortTitle={object.short_title}
      //       short_description={object.short_description}
      //       members_count={object.members_count}
      //       skills={object.skills}
      //       banner_url={object.banner_url || '/images/default/default-group.jpg'}
      //     />
      //   );
      case 'challenges':
        return (
          <ChallengeCard
            id={object.id}
            key={object.id}
            short_title={object.short_title}
            title={object.title}
            title_fr={object.title_fr}
            short_description={object.short_description}
            short_description_fr={object.short_description_fr}
            membersCount={object.members_count}
            needsCount={object.needs_count}
            status={object.status}
            program={object.program}
            customType={object.custom_type}
            space={object.space}
            projectsCount={object.projects_count}
            createdAt={object.created_at}
            banner_url={object.banner_url || '/images/default/default-challenge.jpg'}
          />
        );
      case 'spaces':
        return (
          <SpaceCard
            id={object.id}
            key={object.id}
            short_title={object.short_title}
            title={object.title}
            title_fr={object.title_fr}
            short_description={object.short_description}
            short_description_fr={object.short_description_fr}
            membersCount={object.members_count}
            status={object.status}
            // activitiesCount={object.activities_count + object.challenges_count + object.programs_count}
            projectsCount={object.projects_count}
            spaceType={object.space_type}
            banner_url={object.banner_url || '/images/default/default-space.jpg'}
          />
        );
      case 'programs':
        return (
          <ProgramCard
            id={object.id}
            key={object.id}
            short_title={object.short_title}
            title={object.title}
            title_fr={object.title_fr}
            short_description={object.short_description}
            short_description_fr={object.short_description_fr}
            membersCount={object.members_count}
            challengesCount={object.challenges_count}
            projectsCount={object.projects_count}
            banner_url={object.banner_url || '/images/default/default-program.jpg'}
          />
        );
      case 'users':
        return (
          <UserCard
            id={object.id}
            key={object.id}
            firstName={object.first_name}
            lastName={object.last_name}
            nickName={object.nickname}
            shortBio={object.short_bio}
            logoUrl={object.logo_url}
            affiliation={object.affiliation}
            projectsCount={object.stats?.projects_count}
            followersCount={object.stats?.followers_count}
            spacesCount={object.stats?.spaces_count}
            skills={object.skills}
            resources={object.ressources}
          />
        );
      case 'peer_reviews':
        return <PeerReviewCard key={object.id} peerReview={object} />;
      case 'proposals':
        return <ProposalCard id={object.id} projectId={object.project_id} peerReviewId={object.peer_review_id} />;
      case 'posts':
        return <PostDisplay post={object} user={object.owner} isSingle cardNoComments />;
      default:
        return null;
    }
  };

  return (
    <>
      <div tw="border-gray-400 border-solid rounded-xl border">{renderObject()}</div>
      {/* to hide cookie banner on this page only */}
      <style jsx global>{`
        #klaro {
          display: none;
        }
      `}</style>
    </>
  );
};

export async function getServerSideProps(ctx) {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/api/${ctx.query.objectType}/${ctx.query.id}`).catch((err) => console.error(err));
  if (res) return { props: { object: res.data, objectType: ctx.query.objectType } };
  return { redirect: { destination: '/404', permanent: false } };
}

export default EmbeddedPage;
