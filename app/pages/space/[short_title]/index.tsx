import { NextPage } from 'next';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Feed from 'components/Feed/Feed';
import Layout from 'components/Layout';
import H2 from 'components/primitives/H2';
import P from 'components/primitives/P';
import SpaceAbout from 'components/Space/SpaceAbout';
import ShowFaq from 'components/Tools/ShowFaq';
import SpaceHeader from 'components/Space/SpaceHeader';
import SpaceMembers from 'components/Space/SpaceMembers';
import SpaceNeeds from 'components/Space/SpaceNeeds';
import SpaceProjects from 'components/Space/SpaceProjects';
import SpaceResources from 'components/Space/SpaceResources';
import BtnStar from 'components/Tools/BtnStar';
// import ShareBtns from 'components/Tools/ShareBtns/ShareBtns';
import useGet from 'hooks/useGet';
import { Faq, Space } from 'types';
import { getApiFromCtx } from 'utils/getApi';
// import { useScrollHandler } from 'utils/utils';
import Image from 'components/primitives/Image';
import tw from 'twin.macro';
import 'intro.js/introjs.css';
import SpaceActivities from '~/components/Space/SpaceActivities';
import SpaceInfo from 'components/Space/SpaceInfo';
import useMembers from '~/hooks/useMembers';
import ReactGA from 'react-ga';
import { useModal } from '~/contexts/modalContext';
import { ContactForm } from '~/components/Tools/ContactForm';
import ReactTooltip from 'react-tooltip';
import { Tab, TabPanel, NavContainer, Nav, OverflowGradient, ContactButton } from '~/utils/style';
import Button from '~/components/primitives/Button';
import { Envelope } from '@emotion-icons/fa-solid';

const DEFAULT_TAB = 'about';

const SpaceDetails: NextPage<{ space: Space }> = ({ space }) => {
  const router = useRouter();
  const { locale } = router;
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();
  // const [offSetTop, setOffSetTop] = useState();
  const { members } = useMembers('spaces', space?.id, 25); // take 25 first members
  const [selectedTab, setSelectedTab] = useState(DEFAULT_TAB);
  const { data: faqList } = useGet<{ documents: Faq[] }>(`/api/spaces/${space.id}/faq`);
  const { data: resourcesList } = useGet(`/api/spaces/${space.id}/resources`);
  const headerRef = useRef();
  const navRef = useRef();
  // const isSticky = useScrollHandler(offSetTop);
  const hasActivities = space.challenges_count !== 0 || space.activities_count !== 0;

  const tabs = [
    // add certain tabs to array only if admin had selected those tabs to be displayed
    { value: 'info', translationId: 'general.tab.info' },
    { value: 'about', translationId: 'general.tab.about' },
    // ...(space.selected_tabs.challenges && (space.challenges_count !== 0 || space.activities_count !== 0)
    ...(space.selected_tabs.challenges && (hasActivities || space.is_admin || space.id === 133)
      ? [{ value: 'activities', translationId: space.id !== 133 ? 'general.activities' : 'general.tracks' }] // force name of activities tab to be "Tracks" for space 133
      : []),
    ...(space.projects_count !== 0 ? [{ value: 'projects', translationId: 'entity.tab.projects' }] : []),
    { value: 'members', translationId: 'entity.card.members' },
    { value: 'feed', translationId: 'user.profile.tab.feed' },
    ...(space.needs_count !== 0 ? [{ value: 'needs', translationId: 'entity.tab.needs' }] : []),
    // show resources tab only if admin wanted it to show, and if space has at least a resource OR a document uploaded
    ...(space.selected_tabs.resources && (space.documents.length !== 0 || resourcesList?.length !== 0)
      ? [{ value: 'resources', translationId: 'program.tab.resources' }]
      : []),
    ...(space.selected_tabs.faqs ? [{ value: 'faq', translationId: 'entity.info.faq' }] : []),
    { value: 'contact', translationId: 'user.btn.contact' },
  ];

  const sendMessageToAdmin = (id, first_name, last_name) => {
    // capture the opening of the modal as a special modal page view to google analytics
    ReactGA.modalview('/send-message');
    showModal({
      children: <ContactForm itemId={id} closeModal={closeModal} />,
      title: t('user.contactModal.title', {
        userFullName: `${first_name} ${last_name}`,
      }),
    });
  };

  // useEffect(() => {
  //   setOffSetTop(headerRef?.current?.offsetTop);
  // }, [headerRef]);

  useEffect(() => {
    // on first load, check if url has a tab param, and if it does, select this tab
    router.query.tab && setSelectedTab(router.query.tab as string);
  }, []);

  // function when changing tab (or when router change in general)
  useEffect(() => {
    const tabValues = tabs.map(({ value }) => value);
    if (tabValues.includes(router.query.tab as string)) {
      if (router.query.tab === 'about') {
        router.push(`/space/${router.query.short_title}`, undefined, {
          shallow: true,
        });
      }
      const navContainerTopPosition = navRef.current.getBoundingClientRect().top + window.scrollY; // get y position of nav container
      const yAdjustment = window.innerWidth < 768 ? 40 : 150; // change yAdustement depending on if it's mobile/tablet or desktop
      window.scrollTo(0, navContainerTopPosition - yAdjustment); // force scroll to top of the nav/tab (remove a little to be really on top)
      setSelectedTab(router.query.tab as string);
    }
    if (!router.query.tab) setSelectedTab(DEFAULT_TAB);
  }, [router]);

  return (
    <Layout
      title={space?.title && `${space.title} | JOGL`}
      desc={space?.short_description}
      img={space?.banner_url || '/images/default/default-space.jpg'}
      className="small-top-margin"
      noIndex={space?.status === 'draft'}
    >
      <div tw="sm:shadow-custom2">
        <div tw="relative">
          <Image
            quality="100"
            priority
            src={space?.banner_url || '/images/default/default-space.jpg'}
            alt={`${space?.title} banner`}
            tw="w-full! max-h-[400px]! object-cover"
          />
          <div tw="absolute right-0 top-0 m-4">
            <BtnStar itemType="spaces" itemId={space.id} hasStarred={space.has_saved} count={space.saves_count} />
          </div>
        </div>
        <div tw="bg-[#F5E56C] w-full pl-4 py-1 flex flex-wrap" ref={headerRef}>
          <span tw="px-1 space-x-1 md:text-xl">
            <span tw="font-bold">{t('space.title')}</span>
            <span>({t(`space.type.${space.space_type}`)})</span>
          </span>
        </div>

        <SpaceHeader space={space} />
        {/* <StickyHeading isSticky={isSticky} className="stickyHeading">
          <div tw="flex flex-row items-center px-4 xl:px-0">
            <H2 tw="font-size[1.8rem] sm:font-size[2.18rem] line-height[26px]">
              {(locale === 'fr' && space?.title_fr) || space?.title}
            </H2>
            <div tw="flex pl-5" className="actions">
              <ShareBtns type="space" specialObjId={space.id} />
            </div>
          </div>
        </StickyHeading> */}

        {/* ---- Page grid starts here ---- */}
        <div
          tw="mb-6 grid grid-cols-1 md:(bg-white grid-template-columns[16rem calc(100% - 16rem)]) bg-white border-t border-solid border-gray-200"
          ref={navRef}
        >
          {/* Header and nav (left col on desktop, top col on mobile */}
          <div tw="flex flex-col bg-[#F7F7F9] sticky top-[60px] z-[8] md:(items-center pt-5)">
            <NavContainer>
              <OverflowGradient tw="md:hidden" gradientPosition="left" />
              <OverflowGradient tw="md:hidden" gradientPosition="right" />
              <Nav as="nav" tw="flex gap-3 pt-2.5 w-full md:(flex-col gap-0 pt-0)">
                {tabs.map((item, index) => (
                  <Link
                    shallow
                    scroll={false}
                    key={index}
                    href={`/space/${router.query.short_title}${item.value === DEFAULT_TAB ? '' : `?tab=${item.value}`}`}
                  >
                    <Tab
                      selected={item.value === selectedTab}
                      tw="px-1 py-3 md:px-3"
                      as="button"
                      id={`${item.value}-tab`}
                      css={[item.value === 'info' ? tw`md:hidden` : tw`block`]}
                    >
                      {t(item.translationId)}
                    </Tab>
                  </Link>
                ))}
              </Nav>
            </NavContainer>
          </div>

          {/* Main content, that change content depending on the selected tab (middle content on desktop) */}
          <div tw="flex flex-col px-0 pb-10 relative md:px-4 md:(pl-4) xl:(pr-4)">
            {selectedTab === 'info' && (
              <TabPanel id="info" tw="md:hidden">
                <SpaceInfo space={space} />
              </TabPanel>
            )}

            {selectedTab === 'about' && (
              <TabPanel id="about">
                <SpaceAbout description={space.description} spaceId={space.id} enablers={space.enablers} />
              </TabPanel>
            )}

            {selectedTab === 'feed' && (
              <TabPanel id="feed" tw="mx-auto w-full px-0">
                {/* Show feed, and pass DisplayCreate to admins */}
                {space.feed_id && (
                  <Feed
                    feedId={space.feed_id}
                    allowPosting={space?.is_member}
                    isAdmin={space.is_admin || space.is_owner}
                  />
                )}
              </TabPanel>
            )}

            {selectedTab === 'activities' && (
              <TabPanel id="activities">
                <SpaceActivities spaceId={space.id} spaceShortTitle={space.short_title} isAdmin={space.is_admin} />
              </TabPanel>
            )}

            {selectedTab === 'projects' && (
              <TabPanel id="projects">
                <SpaceProjects spaceId={space.id} isMember={space.is_member} hasFollowed={space.has_followed} />
              </TabPanel>
            )}

            {selectedTab === 'needs' && (
              <TabPanel id="needs">
                <SpaceNeeds spaceId={space.id} />
              </TabPanel>
            )}

            {selectedTab === 'members' && (
              <TabPanel id="members" tw="relative ">
                <SpaceMembers spaceId={space.id} isMember={space.is_member} isAdmin={space.is_admin} />
              </TabPanel>
            )}

            {selectedTab === 'resources' && (
              <TabPanel id="resources">
                <SpaceResources
                  spaceId={space?.id}
                  isAdmin={space?.is_admin}
                  documents={space?.documents}
                  resources={resourcesList}
                />
              </TabPanel>
            )}

            {selectedTab === 'faq' && (
              <TabPanel id="faq">
                <ShowFaq faqList={faqList} />
              </TabPanel>
            )}

            {selectedTab === 'contact' && (
              <TabPanel id="contact">
                <H2 tw="mb-4">{t('footer.contactUs')}</H2>
                {space?.contact_email && (
                  <a href={`mailto:${space?.contact_email || 'hello@jogl.io'}`}>
                    <Button tw="flex justify-center items-center space-x-2">
                      <Envelope size={18} title="Contact" />
                      <span>Contact space</span>
                    </Button>
                  </a>
                )}
                <div tw="flex flex-col mt-6 space-y-4">
                  {members
                    ?.filter(({ admin, owner }) => owner || admin) // only show leaders and admins
                    .map(({ id, first_name, last_name, logo_url_sm }, index) => (
                      <div tw="flex space-x-4 items-center" key={index}>
                        <Link href={`/user/${id}`}>
                          <a>
                            <div tw="flex space-x-4 items-center">
                              <img
                                tw="object-cover rounded-full w-12 h-12"
                                src={logo_url_sm}
                                alt={`${first_name} ${last_name}`}
                              />
                              <P tw="mb-0 font-bold font-size[1.2rem]">{first_name + ' ' + last_name}</P>
                            </div>
                          </a>
                        </Link>
                        <div tw="flex flex-col">
                          <ContactButton
                            size={20}
                            title="Contact user"
                            onClick={() => sendMessageToAdmin(id, first_name, last_name)}
                            onKeyUp={(e) =>
                              // execute only if it's the 'enter' key
                              (e.which === 13 || e.keyCode === 13) && sendMessageToAdmin(id, first_name, last_name)
                            }
                            tabIndex={0}
                            data-tip={t('user.contactModal.title', { userFullName: first_name + ' ' + last_name })}
                            data-for="contactAdmin"
                            // show/hide tooltip on element focus/blur
                            onFocus={(e) => ReactTooltip.show(e.target)}
                            onBlur={(e) => ReactTooltip.hide(e.target)}
                          />
                          <ReactTooltip id="contactAdmin" effect="solid" place="bottom" />
                        </div>
                      </div>
                    ))}
                </div>
              </TabPanel>
            )}
          </div>
        </div>
      </div>
    </Layout>
  );
};

const getSpace = async (api, spaceId) => {
  const res = await api.get(`/api/spaces/${spaceId}`).catch((err) => console.error(err));
  if (res?.data) {
    return res.data;
  }
  return undefined;
};

export async function getServerSideProps({ query, ...ctx }) {
  const api = getApiFromCtx(ctx);
  // Case short_title is a string for pretty URL
  // eslint-disable-next-line no-restricted-globals
  if (isNaN(Number(query.short_title as string))) {
    const res = await api.get(`/api/spaces/getid/${query.short_title}`).catch((err) => console.error(err));
    if (res) {
      const space = await getSpace(api, res.data.id).catch((err) => console.error(err));
      return { props: { space } };
    }
    return { redirect: { destination: '/', permanent: false } };
  }
  // Case short_title is actually an id
  const space = await getSpace(api, query.short_title).catch((err) => console.error(err));
  if (space) {
    return { props: { space } };
  } else {
    return { redirect: { destination: '/', permanent: false } };
  }
}

export default SpaceDetails;
