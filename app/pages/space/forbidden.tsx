import { NextPage } from 'next';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import React from 'react';
import Layout from 'components/Layout';
import Button from '~/components/primitives/Button';

const SpaceForbiddenPage: NextPage = () => {
  const { t } = useTranslation('common');

  return (
    <Layout noIndex>
      <div className="SpacePage" tw="text-center px-2">
        <br />
        <br />
        {t('space.info.forbidden')}
        <br />
        <a href="mailto:hello@jogl.io">hello@jogl.io</a>
        <br />
        <br />
        <br />
        <Link href="/search/spaces" passHref>
          <Button>{t('space.info.forbiddenBtn')}</Button>
        </Link>
      </div>
    </Layout>
  );
};
export default SpaceForbiddenPage;
