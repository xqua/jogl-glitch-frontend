import { NextPage } from 'next';
import { useEffect } from 'react';
import { useRouter } from 'next/router';
import Layout from 'components/Layout';
import useGet from 'hooks/useGet';
import useUserData from 'hooks/useUserData';
import ChallengeCreate from 'components/Challenge/ChallengeCreate';
import useTranslation from 'next-translate/useTranslation';

const ChallengeCreatePage: NextPage = () => {
  const router = useRouter();
  const { userData } = useUserData();
  const { t } = useTranslation('common');

  // canCreate has to stay here and in all elements that will render ChallengeCreate component
  // Check for :challenges_creator role on backend
  const { data: canCreate, error: cannotCreate } = useGet('/api/challenges/can_create', { user: userData });
  useEffect(() => {
    if (cannotCreate) {
      router.push('/challenge/forbidden');
    }
  }, [canCreate, cannotCreate]);

  if (canCreate) {
    return (
      <Layout title="Create a new challenge | JOGL">
        <div className="challengeCreate" tw="px-4 xl:px-0">
          <h1>{t('challenge.create.title', { challenge_wording: t('challenge.title') })}</h1>
          <ChallengeCreate />
        </div>
      </Layout>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};

export default ChallengeCreatePage;
