import { NextPage } from 'next';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import React from 'react';
import Layout from 'components/Layout';
import Button from '~/components/primitives/Button';
// import "assets/css/ChallengePage.scss";

const ChallengeForbiddenPage: NextPage = () => {
  const { t } = useTranslation('common');
  return (
    <Layout noIndex>
      <div className="ChallengePage" tw="text-center px-2">
        <br />
        <br />
        {t('challenge.info.forbidden')}
        <br />
        <a href="mailto:hello@jogl.io">hello@jogl.io</a>
        <br />
        <br />
        <br />
        <Link href="/search/challenges" passHref>
          <Button>{t('challenge.info.forbiddenBtn')}</Button>
        </Link>
      </div>
    </Layout>
  );
};
export default ChallengeForbiddenPage;
