/* eslint-disable camelcase */
import { NextPage } from 'next';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Feed from 'components/Feed/Feed';
import Layout from 'components/Layout';
import H2 from 'components/primitives/H2';
import ChallengeMembers from 'components/Challenge/ChallengeMembers';
import ChallengeNeeds from 'components/Challenge/ChallengeNeeds';
import ChallengeProjects from 'components/Challenge/ChallengeProjects';
import InfoHtmlComponent from 'components/Tools/Info/InfoHtmlComponent';
import useGet from 'hooks/useGet';
import { getApiFromCtx } from 'utils/getApi';
// import { useScrollHandler } from 'utils/utils';
// import InfoInterestsComponent from 'components/Tools/Info/InfoInterestsComponent'
import ChallengeHeader from 'components/Challenge/ChallengeHeader';
import InviteMember from 'components/Tools/InviteMember';
import ShowFaq from 'components/Tools/ShowFaq';
// import Chips from 'components/Chip/Chips';
import Image from 'components/primitives/Image';
import DocumentsList from 'components/Tools/Documents/DocumentsList';
import { Challenge } from 'types';
import tw from 'twin.macro';
import BtnStar from 'components/Tools/BtnStar';
// import InfoSkillSdg from 'components/Tools/Info/InfoSkillSdg';
import ChallengeInfo from 'components/Challenge/ChallengeInfo';
// import ShareBtns from 'components/Tools/ShareBtns/ShareBtns';
import { Tab, TabPanel, NavContainer, Nav, OverflowGradient } from '~/utils/style';
import ChallengeSkillSdg from '~/components/Challenge/ChallengeSkillSdg';

const DEFAULT_TAB = 'about';

const ChallengeDetails: NextPage<{ challenge: Challenge }> = ({ challenge }) => {
  const router = useRouter();
  const { t } = useTranslation('common');
  const [selectedTab, setSelectedTab] = useState(DEFAULT_TAB);

  // const [offSetTop, setOffSetTop] = useState();

  const [tabs, setTabs] = useState([
    { value: 'info', translationId: 'general.tab.info' },
    { value: 'about', translationId: 'general.tab.about' },
    { value: 'feed', translationId: 'user.profile.tab.feed' },
    { value: 'projects', translationId: 'entity.tab.projects' },
    { value: 'needs', translationId: 'entity.tab.needs' },
    { value: 'members', translationId: 'entity.card.members' },
  ]);
  const { data: dataExternalLink } = useGet<{ url: string; icon_url: string }[]>(
    `/api/challenges/${challenge?.id}/links`
  );
  const { data: faqList } = useGet<{ documents: Faq[] }>(`/api/challenges/${challenge.id}/faq`);
  const headerRef = useRef();
  const navRef = useRef();
  // const isSticky = useScrollHandler(offSetTop);
  const { locale } = router;

  useEffect(() => {
    // on first load, check if url has a tab param, and if it does, select this tab
    router.query.tab && setSelectedTab(router.query.tab as string);
  }, []);

  // useEffect(() => {
  //   setOffSetTop(headerRef?.current?.offsetTop);
  // }, [headerRef]);

  // add faq tab only if has faq
  useEffect(() => {
    tabs[6]?.value !== 'faq' &&
      faqList &&
      faqList.documents.length !== 0 &&
      setTabs([...tabs, { value: 'faq', translationId: 'entity.info.faq' }]); // don't add another time if tab already exists
  }, [faqList]);

  useEffect(() => {
    const tabValues = tabs.map(({ value }) => value);
    if (tabValues.includes(router.query.tab as string)) {
      if (router.query.tab === 'about') {
        router.push(`/challenge/${router.query.short_title}`, undefined, {
          shallow: true,
        });
      }
      setSelectedTab(router.query.tab as string);
      const navContainerTopPosition = navRef.current.getBoundingClientRect().top + window.scrollY; // get y position of nav container
      const yAdjustment = window.innerWidth < 768 ? 120 : 145; // change yAdjustment depending on if it's mobile/tablet or desktop
      window.scrollTo(0, navContainerTopPosition - yAdjustment); // force scroll to top of the nav/tab (remove a little to be really on top)
    }
    if (!router.query.tab) setSelectedTab(DEFAULT_TAB);
  }, [router]);

  const bannerUrl = challenge?.banner_url || '/images/default/default-challenge.jpg';

  return (
    <Layout
      title={challenge?.title && `${challenge.title} | JOGL`}
      desc={challenge?.short_description}
      img={bannerUrl}
      className="small-top-margin"
      noIndex={challenge?.status === 'draft'}
    >
      <div tw="sm:shadow-custom2">
        <div tw="relative">
          <Image
            quality="100"
            priority
            src={bannerUrl}
            alt={`${challenge?.title} banner`}
            tw="w-full! max-h-[400px]! object-cover"
          />
          <div tw="absolute right-0 top-0 m-4">
            <BtnStar
              itemType="challenges"
              itemId={challenge.id}
              hasStarred={challenge.has_saved}
              count={challenge.saves_count}
            />
          </div>
        </div>
        <div tw="bg-[#A1A3FF] w-full pl-4 py-1 flex flex-wrap md:text-xl" ref={headerRef}>
          {/* <span>[{t('general.activity_1')}]</span> */}
          <span tw="font-bold px-1">
            {/* {challenge.custom_type === 'Challenge' ? t('challenge.title') : challenge.custom_type} */}
            {/* {challenge.custom_type === 'Challenge' ? t('general.challenge_1') : challenge.custom_type} */}
            {t('challenge.title')}
          </span>
          {challenge.custom_type !== 'Group' && <span>({challenge.custom_type})</span>}
        </div>

        <ChallengeHeader challenge={challenge} lang={locale} />
        {/* <StickyHeading isSticky={isSticky} className="stickyHeading">
          <div tw="flex flex-row items-center px-4 xl:px-0">
            <H2 tw="font-size[1.8rem] sm:font-size[2.18rem] line-height[26px]">
              {(locale === 'fr' && challenge?.title_fr) || challenge?.title}
            </H2>
            <div tw="flex pl-5" className="actions">
              <ShareBtns type="challenge" specialObjId={challenge.id} />
            </div>
          </div>
        </StickyHeading> */}

        {/* ---- Page grid starts here ---- */}
        <div
          tw="mb-6 grid grid-cols-1 md:(bg-white grid-template-columns[16rem calc(100% - 16rem)]) bg-white border-t border-solid border-gray-200"
          ref={navRef}
        >
          {/* Header and nav (left col on desktop, top col on mobile */}
          <div tw="flex flex-col bg-[#F7F7F9] sticky top-[60px] z-[8] md:(items-center pt-5)">
            <NavContainer>
              <OverflowGradient tw="md:hidden" gradientPosition="left" />
              <OverflowGradient tw="md:hidden" gradientPosition="right" />
              <Nav as="nav" tw="flex gap-3 pt-2.5 w-full md:(flex-col gap-0 pt-0)">
                {tabs.map((item, index) => (
                  <Link
                    shallow
                    scroll={false}
                    key={index}
                    href={`/challenge/${router.query.short_title}${
                      item.value === DEFAULT_TAB ? '' : `?tab=${item.value}`
                    }`}
                  >
                    <Tab
                      selected={item.value === selectedTab}
                      tw="px-1 py-3 md:px-3"
                      as="button"
                      id={`${item.value}-tab`}
                      css={[item.value === 'info' ? tw`md:hidden` : tw`block`]}
                    >
                      {t(item.translationId)}
                    </Tab>
                  </Link>
                ))}
              </Nav>
            </NavContainer>
          </div>

          {/* Main content, that change content depending on the selected tab (middle content on desktop) */}
          <div tw="flex flex-col px-0 pb-10 relative md:px-4 md:(pl-4) xl:(pr-4)">
            {selectedTab === 'info' && (
              <TabPanel id="info" tw="md:hidden">
                <ChallengeInfo challenge={challenge} />
                {(challenge.interests?.length > 0 || challenge.skills?.length > 0) && (
                  <div>
                    <hr tw="mb-4" />
                    <ChallengeSkillSdg interests={challenge.interests} skills={challenge.skills} />
                  </div>
                )}
              </TabPanel>
            )}

            {selectedTab === 'about' && (
              <TabPanel id="about">
                <div tw="max-w-3xl">
                  <InfoHtmlComponent
                    content={(locale === 'fr' && challenge?.description_fr) || challenge?.description}
                  />
                  <div tw="flex flex-wrap pt-8">
                    <DocumentsList
                      documents={challenge?.documents}
                      itemId={challenge?.id}
                      isAdmin={challenge?.is_admin}
                      itemType="challenges"
                      cardType="cards"
                    />
                  </div>
                  <div tw="hidden md:block mb-3">
                    <ChallengeSkillSdg interests={challenge.interests} skills={challenge.skills} />
                  </div>
                </div>
              </TabPanel>
            )}

            {selectedTab === 'feed' && (
              <TabPanel id="feed" tw="mx-auto w-full px-0">
                {/* Show feed, and pass allowPosting to admins */}
                {challenge.feed_id && (
                  <Feed
                    feedId={challenge.feed_id}
                    allowPosting={challenge.is_member}
                    isAdmin={challenge.is_admin || challenge.is_owner}
                  />
                )}
              </TabPanel>
            )}

            {selectedTab === 'projects' && (
              <TabPanel id="projects">
                <ChallengeProjects challengeId={challenge.id} />
              </TabPanel>
            )}

            {selectedTab === 'needs' && (
              <TabPanel id="needs">
                <ChallengeNeeds challengeId={challenge.id} />
              </TabPanel>
            )}

            {selectedTab === 'members' && (
              <TabPanel id="members" tw="relative">
                <ChallengeMembers challengeId={challenge.id} />
              </TabPanel>
            )}

            {selectedTab === 'faq' && (
              <TabPanel id="faq">
                <ShowFaq faqList={faqList} />
              </TabPanel>
            )}

            <div tw="pt-11 pb-7 px-4 block xl:hidden">
              <CtaAndLinks
                dataExternalLink={dataExternalLink}
                challengeId={challenge.id}
                isAdmin={challenge.is_admin}
                t={t}
              />
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

const CtaAndLinks = ({ dataExternalLink, t, challengeId, isAdmin }) => {
  return (
    <>
      {/* Invite someone component */}
      {isAdmin && (
        <div tw="flex flex-col pt-5">
          <H2>{t('member.invite.general')}</H2>
          <InviteMember itemType="challenges" itemId={challengeId} />
        </div>
      )}
      {dataExternalLink && dataExternalLink?.length !== 0 && (
        <div tw="flex flex-col pt-8">
          <H2 tw="pb-4">{t('general.externalLink.findUs')}</H2>
          <div tw="flex flex-wrap gap-3">
            {[...dataExternalLink].map((link, i) => (
              <a tw="items-center self-center" key={i} href={link.url} target="_blank">
                <img tw="w-10 hover:opacity-80" src={link.icon_url} />
              </a>
            ))}
          </div>
        </div>
      )}
    </>
  );
};

const getChallenge = async (api, challengeId) => {
  const res = await api.get(`/api/challenges/${challengeId}`).catch((err) => console.error(err));
  if (res?.data) {
    return res.data;
  }
  return undefined;
};

export async function getServerSideProps({ query, ...ctx }) {
  const api = getApiFromCtx(ctx);
  // Case short_title is a string for pretty URL
  // eslint-disable-next-line no-restricted-globals
  if (isNaN(Number(query.short_title as string))) {
    const res = await api.get(`/api/challenges/getid/${query.short_title}`).catch((err) => console.error(err));
    if (res) {
      const challenge = await getChallenge(api, res.data.id).catch((err) => console.error(err));
      return { props: { challenge } };
    }
    return {
      redirect: { destination: '/search/challenges', permanent: false },
    };
  }
  // Case short_title is actually an id
  const challenge = await getChallenge(api, query.short_title).catch((err) => console.error(err));
  if (challenge) {
    return { props: { challenge } };
  } else {
    return {
      redirect: { destination: '/search/challenges', permanent: false },
    };
  }
}

export default ChallengeDetails;
