import { NextPage } from 'next';
import useTranslation from 'next-translate/useTranslation';
import Layout from 'components/Layout';
import { SearchStateContextProvider, useSearchStateContext } from 'contexts/searchStateContext';
// import { useFeatureFlags } from 'hooks/useFeatureFlags';
// import useUserData from 'hooks/useUserData';
import { getSearchTab, TabIndex } from 'components/SearchTabs';
import { useCallback } from 'react';
import { Tabs, TabList, TabPanels } from '@reach/tabs';

interface TabLinkProps {
  indexName: string;
  active: boolean;
}

const TabLink: React.FC<TabLinkProps> = ({ indexName, children, active }) => {
  const { setIndex } = useSearchStateContext();
  const handleClick = useCallback(() => setIndex(indexName as TabIndex), [indexName, setIndex]);
  return (
    <div
      className={`indexType ${active ? 'active' : ''}`}
      data-toggle="tab"
      onClick={handleClick}
      onKeyDown={handleClick}
      role="link"
      tabIndex={0}
    >
      {children}
    </div>
  );
};
// TODO: replace TabLink by Tab from reachui (replacing searchStateContext logic & including style)

const SearchPage: React.FC = () => {
  const tabs = [
    { value: 'spaces', name: 'Spaces', translationId: 'general.spaces' },
    { value: 'challenges', name: 'Challenge', translationId: 'general.challenges' },
    { value: 'projects', name: 'Project', translationId: 'user.profile.tab.projects' },
    { value: 'needs', name: 'Need', translationId: 'entity.tab.needs' },
    { value: 'members', name: 'User', translationId: 'entity.card.members' },
    { value: 'peer-reviews', name: 'PeerReview', translationId: 'general.peerReviews' },
    { value: 'programs', name: 'Program', translationId: 'general.programs' },
    // { value: 'proposals', name: 'Proposal', translationId: 'general.proposal_other' },
  ];
  const { t } = useTranslation('common');
  const { index } = useSearchStateContext();
  const TabPanelBySearch = getSearchTab(index);

  return (
    <Layout title={`${t('header.search')} ${t(tabs[tabs.findIndex((x) => x.value === index)].translationId)} | JOGL`}>
      <div className="searchPageAll AlgoliaResultsPages" tw="px-4 xl:px-0">
        <Tabs>
          <TabList tw="bg-white">
            {tabs.map(({ value, translationId }, key) => (
              <TabLink key={key} indexName={value} active={value === index}>
                {t(translationId)}
              </TabLink>
            ))}
          </TabList>
          <TabPanels>
            <TabPanelBySearch />
          </TabPanels>
        </Tabs>
      </div>
    </Layout>
  );
};

const SearchPageTabs: NextPage = () => (
  <SearchStateContextProvider>
    <SearchPage />
  </SearchStateContextProvider>
);

export default SearchPageTabs;
