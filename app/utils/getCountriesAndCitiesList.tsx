import { useEffect } from 'react';
import axios from 'axios';

export function getCountriesAndCitiesList(
  countriesCitiesData,
  setCountriesCitiesData,
  countryNotInList,
  setCountryNotInList,
  setCountriesList,
  setCitiesList,
  selectedCountry
) {
  const apiUrl = 'https://gitlab.com/api/v4/projects/9964040/snippets/2084062/raw';

  async function getCountriesCities() {
    await axios.get(apiUrl).then((response) => setCountriesCitiesData(response.data));
  }

  useEffect(() => {
    getCountriesCities();
  }, []);

  useEffect(() => {
    if (countriesCitiesData) {
      // check if current user country is in the official array of countries (had to do this for when users could type their country with a text input, and they could type it differently..)
      setCountryNotInList(!Object.keys(countriesCitiesData).includes(selectedCountry));

      setCountriesList(
        Object.keys(countriesCitiesData) // an array of all the countries from the list
          .sort((a, b) => a.localeCompare(b))
          .map((content) => {
            return { value: content, label: content };
          })
      );

      if (selectedCountry && !countryNotInList) {
        setCitiesList(
          countriesCitiesData[selectedCountry] // to access the country array containing all its cities
            ?.sort((a, b) => a.localeCompare(b))
            .map((content) => {
              return { value: content, label: content };
            })
        );
      }
    }
  }, [countriesCitiesData, selectedCountry, countryNotInList]);
}

export default getCountriesAndCitiesList;
