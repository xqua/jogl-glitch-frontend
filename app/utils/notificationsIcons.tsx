import { At } from '@emotion-icons/boxicons-regular';
import { PersonPlusFill } from '@emotion-icons/bootstrap';
import { CommentDots, Comments } from '@emotion-icons/fa-solid';
import { AdminPanelSettings, Event } from '@emotion-icons/material';
import { Groups } from '@emotion-icons/material-outlined';
import { SignLanguage } from '@emotion-icons/fa-solid';
import { EmotionIconBase } from '@emotion-icons/emotion-icon';
import styled from 'utils/styled';

export const NotifIconWrapper = styled.div`
  ${EmotionIconBase} {
    color: ${(p) => p.theme.colors.primary};
    width: 25px;
    height: 25px;
  }
`;

export const notifIconTypes = {
  administration: <AdminPanelSettings title="Admin notification" />,
  clap: <SignLanguage title="Clap notification" />,
  comment: <Comments title="Comment notification" />,
  follow: <PersonPlusFill title="Follow notification" />,
  feed: <CommentDots title="Feed notification" />,
  program: <Event title="Program notification" />,
  mention: <At title="Mention notification" />,
  membership: <Groups title="Membership notification" />,
  space: <Event title="Space notification" />,
};
