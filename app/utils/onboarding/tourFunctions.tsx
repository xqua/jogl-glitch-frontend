export const goToNextIntroPage = (router, nextUrl) => {
  router.push(nextUrl);
};

export const triggerOnExit = (router) => {
  cleanOnboarding();
  router.push('/');
};

export const completeTour = () => {
  localStorage.setItem('multipage', 'false');
  cleanOnboarding();
};

export const cleanOnboarding = () => {
  localStorage.setItem('isOnboarding', 'false');
  localStorage.setItem('isIndividual', 'false');
  localStorage.setItem('isOrganization', 'false');
  // localStorage.setItem('firstOnboardingView', 'false');
};

// export const startMobileIntro = () => console.log('mobile/tablet onboarding flow, still to develop!');
